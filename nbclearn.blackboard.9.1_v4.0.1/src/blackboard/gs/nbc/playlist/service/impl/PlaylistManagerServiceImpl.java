package blackboard.gs.nbc.playlist.service.impl;

import static blackboard.gs.nbc.util.TemplateRenderUtils.renderTemplate;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import blackboard.data.user.User;
import blackboard.gs.nbc.Constants;
import blackboard.gs.nbc.context.NBCAppContext;
import blackboard.gs.nbc.playlist.data.playlistitem.PlaylistItem;
import blackboard.gs.nbc.playlist.data.playlistitem.UserPlaylist;
import blackboard.gs.nbc.playlist.data.video.Video;
import blackboard.gs.nbc.playlist.persist.playlistitem.PlaylistItemDbLoader;
import blackboard.gs.nbc.playlist.persist.playlistitem.PlaylistItemDbPersister;
import blackboard.gs.nbc.playlist.persist.video.VideoDbLoader;
import blackboard.gs.nbc.playlist.persist.video.VideoDbPersister;
import blackboard.gs.nbc.playlist.persist.video.impl.VideoDbPersisterImpl;
import blackboard.gs.nbc.playlist.service.PlaylistManagerService;
import blackboard.gs.nbc.util.ContextService;
import blackboard.gs.nbc.util.persist.Id;
import blackboard.gs.nbc.util.persist.PersistUtil;
import blackboard.gs.nbc.util.persist.PersistUtil.Work;
import blackboard.persist.PersistenceException;

public class PlaylistManagerServiceImpl implements PlaylistManagerService {
	private static final Logger log = Logger.getLogger(PlaylistManagerServiceImpl.class.getName());

	private final NBCAppContext nbcAppContext;

	static String snasUrl;

	static String cuecardXmlUrl;

	public static final String NON_USER_ATTR_EXTENSION = "extension", NON_USER_ATTR_COMMUNITY_NAME = "communityName",
			NON_USER_ATTR_OLD_COMMUNITY_NAME = "oldCommunityName",
			NON_USER_ATTR_HOW_DID_HEAR_SPECIFY = "HowDidHearSpecify",

			/* Days Left to Expiration - for trial users */
			NON_USER_ATTR_DAYS_TO_EXPIRE = "daysToExpire",

			/* new email */
			NON_USER_ATTR_NEW_EMAIL = "newEmail",

			USER_ATTR_UUID = "uniqueIdentifier", USER_ATTR_STATUS = "status",

			USER_ATTR_FIRSTNAME = "firstName", USER_ATTR_LASTNAME = "lastName", USER_ATTR_GENDER = "gender",
			USER_ATTR_PHONE = "phone", USER_ATTR_PHONE1 = "phone1",

			USER_ATTR_EMAIL = "email", USER_ATTR_COMMUNICATION_EMAIL = "communicationEmail",
			USER_ATTR_COMMUNICATION = "communication", USER_ATTR_NEWS_LETTER_OPTIN = "newsletterOptin",
			USER_ATTR_HOW_DID_HEAR = "HowDidHear", USER_ATTR_COMMENTS = "Comments",
			USER_ATTR_REGISTRATION_DATE = "registrationDate", USER_ATTR_INSTITUTION_NAME = "InstitutionName",
			// Create user in higherbb.icue.nbclearn.com
			USER_ATTR_PRODUCT_TYPE = "productType",
			// from archivesbb onthe fly, lmsinfo, csradmin
			USER_ATTR_LMS_TYPE = "LMSType",
			// crs admin
			USER_ATTR_USER_TYPE = "userType", USER_ATTR_EXPIRATION_DATE = "expiration", USER_ATTR_ROLE = "myrole",
			/* editor_role is applicable to only editor site */
			USER_ATTR_EDITOR_ROLE = "editor_role", USER_ATTR_AGE_13_OR_OVER = "age13orover",
			USER_ATTR_BB_REFERRAL = "blackboarduser", USER_ATTR_WORKPHONE = "workPhone",
			USER_ATTR_HOME_PHONE = "homePhone", USER_ATTR_PROFILE_PRIVACY = "myProfilePrivacyLevel",
			USER_ATTR_NEEDS_PWD_CHANGE = "needspasswordchange", USER_ATTR_MODIFIED_BY = "LastEditedBy",
			USER_ATTR_MODIFIED_DATE = "DateEdited", USER_ATTR_ADDRESS1 = "addressLine1",
			USER_ATTR_ADDRESS2 = "addressLine2", USER_ATTR_ADDRESS3 = "addressLine3", USER_ATTR_CITY = "city",
			USER_ATTR_STATE_CODE = "stateCode", USER_ATTR_STATE_NAME = "stateName",
			USER_ATTR_COUNTRY_CODE = "countryCode", USER_ATTR_ZIP = "zipCode", USER_ATTR_AVATAR_URI = "avatarURI",
			USER_ATTR_TOKEN = "token", USER_ATTR_LOGIN_TIMESTAMPS = "loggedInTimestamps",
			USER_ATTR_LOGIN_ATTEMPTS = "LoginAttempts", USER_ATTR_NEEDS_SHIBBOLETH_AUTH = "needSihbAuth",
			USER_ATTR_USER_NAME = "userName", USER_ATTR_SITE = "site", USER_ATTR_PASSWORD = "password",
			USER_ATTR_EXTERN_LOGINID = "sso_external_loginid", USER_ATTR_EXTERN_USERID = "sso_external_userid",
			USER_ATTR_EXTERN_DISTRICT_SCHOOLNAME = "district_schoolname",
			// New Attributes grade and subject added
			USER_ATTR_GRADE = "grade", USER_ATTR_SUBJECT = "subject";

	/**
	 * @param nbcAppContext
	 */
	public PlaylistManagerServiceImpl(NBCAppContext nbcAppContext) {
		this.nbcAppContext = nbcAppContext;
	}

	public List<PlaylistItem> retrieveVideos(blackboard.persist.Id userId) throws Exception {
		try {
			PlaylistItemDbLoader loader = this.nbcAppContext.getPlaylistItemDbLoader();
			List<PlaylistItem> videoList = loader.loadByUserId(userId);
			/*if (log.isDebugEnabled()) {
				log.debug("Videos have been successfully retrieved");
			}*/
			return videoList;
		} catch (Exception ex) {
			throw new Exception("Retrieving videos for user failed", ex);
		}

	}

	public PlaylistItem saveVideo(final User user, final Video video) throws Exception {
		Work<PlaylistItem> work = new Work<PlaylistItem>() {
			/**
			 * {@inheritDoc}
			 */
			@Override
			public PlaylistItem execute(NBCAppContext appContext) throws Exception {
				// log.info("EXECUTE METHOD");
				log.info("LOAD VIDEO");
				VideoDbLoader videoLoader = appContext.getVideoDbLoader();
				VideoDbPersister videoPersister = appContext.getVideoDbPersister();
				Video v = videoLoader.loadByVideoId(video.getVideoId());
				if (v != null) {
					video.setId(v.getId());
					videoPersister.persist(video);
				} else {
					videoPersister.persist(video);
				}
				v = videoLoader.loadByVideoId(video.getVideoId());

				PlaylistItemDbLoader playlistLoader = appContext.getPlaylistItemDbLoader();
				PlaylistItemDbPersister playlistPersister = appContext.getPlaylistItemDbPersister();

				PlaylistItem item = new PlaylistItem();
				item.setUser(user);
				item.setVideo(v);
				// log.info("for user id: " + user.getId().getExternalString());
				// log.info("Video with video key:"+v.getId().getKey() + " is
				// getting saved");
				/*f (log.isDebugEnabled()) {
					log.debug("for user id: " + user.getId().getExternalString());
					log.debug("Video with video key:" + v.getId().getKey() + " is getting saved");
				}*/
				PlaylistItem pItem = playlistLoader.loadByUserIdAndVideoId(user.getId(), v.getId());

				if (pItem != null) {
					item.setId(pItem.getId());
					playlistPersister.persist(item);
				} else {
					playlistPersister.persist(item);
					pItem = item;
				}

				/*if (log.isDebugEnabled()) {
					log.debug("Video " + video.getVideoId() + " successfully saved in the playlist for user "
							+ user.getBatchUid());
				}*/

				return pItem;
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public boolean releaseConnection() {
				return false; // do NOT release
			}

		};

		PlaylistItem item = PersistUtil.doWork(work, nbcAppContext);
		return item;

		/*
		 * VideoDbLoader videoLoader = this.nbcAppContext.getVideoDbLoader();
		 * VideoDbPersister videoPersister =
		 * this.nbcAppContext.getVideoDbPersister();
		 * 
		 * try { Video v = videoLoader.loadByVideoId(video.getVideoId(),
		 * connection); if (v != null) { video.setId(v.getId());
		 * videoPersister.persist(video, connection); } else {
		 * videoPersister.persist(video, connection); }
		 * 
		 * v = videoLoader.loadByVideoId(video.getVideoId(), connection);
		 * 
		 * PlaylistItemDbLoader playlistLoader =
		 * this.nbcAppContext.getPlaylistItemDbLoader(); PlaylistItemDbPersister
		 * playlistPersister = this.nbcAppContext.getPlaylistItemDbPersister();
		 * 
		 * PlaylistItem item = new PlaylistItem(); item.setUser(user);
		 * item.setVideo(v); if (log.isDebugEnabled()) { log.debug(
		 * "for user id: " + user.getId().getExternalString()); log.debug(
		 * "Video with video key:"+v.getId().getKey() + " is getting saved"); }
		 * PlaylistItem pItem =
		 * playlistLoader.loadByUserIdAndVideoId(user.getId(), v.getId(),
		 * connection); if (pItem != null) { item.setId(pItem.getId());
		 * playlistPersister.persist(item, connection); } else {
		 * playlistPersister.persist(item, connection); pItem = item; }
		 * 
		 * if (log.isDebugEnabled()) { log.debug("Video " + video.getVideoId() +
		 * " successfully saved in the playlist for user " +
		 * user.getBatchUid()); }
		 * 
		 * return pItem; } catch (Exception ex) { throw new Exception(
		 * "Saving video in the playlist failed", ex); }
		 */ }

	public void removeVideos(final List<PlaylistItem> list) throws Exception {
		Work<Object> work = new Work<Object>() {
			/**
			 * {@inheritDoc}
			 */
			@Override
			public Object execute(NBCAppContext appContext) throws Exception {
				try {
					PlaylistItemDbPersister playlistPersister = appContext.getPlaylistItemDbPersister();
					PlaylistItemDbLoader playlistLoader = appContext.getPlaylistItemDbLoader();
					VideoDbPersister videoPersister = appContext.getVideoDbPersister();
					HashMap<Id, Video> videoMap = new HashMap<Id, Video>();
					for (PlaylistItem item : list) {
						playlistPersister.deleteById(item.getId());
						videoMap.put(item.getVideo().getId(), null);
					}
					if (!videoMap.isEmpty()) {
						Set<Id> st = videoMap.keySet();
						Iterator<Id> itr = st.iterator();
						while (itr.hasNext()) {
							Id id = (Id) itr.next();
							int videoNumber = playlistLoader.loadByVideoId(id);
							if (videoNumber == 0) {
								videoPersister.deleteById(id);
							}
						}
					}
					if (log.isDebugEnabled()) {
						log.debug("Videos successfully removed");
					}
				} catch (Exception e) {
					log.info("REMOVEEEEEEEEEEEEE" + e.getMessage());
				}
				return null;
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public boolean releaseConnection() {
				return false; // do NOT release
			}

		};

		PersistUtil.doWork(work, nbcAppContext);

		/*
		 * Connection connection = null; try { connection =
		 * PersistUtil.getConnection(DatabaseKey.BLACKBOARD);
		 * PersistUtil.setAutoCommit(connection); PlaylistItemDbPersister
		 * playlistPersister = this.nbcAppContext.getPlaylistItemDbPersister();
		 * PlaylistItemDbLoader playlistLoader =
		 * this.nbcAppContext.getPlaylistItemDbLoader(); VideoDbPersister
		 * videoPersister = this.nbcAppContext.getVideoDbPersister();
		 * HashMap<Id, Video> videoMap = new HashMap<Id, Video>(); for
		 * (PlaylistItem item : list) {
		 * playlistPersister.deleteById(item.getId(), connection);
		 * videoMap.put(item.getVideo().getId(), null); } if
		 * (!videoMap.isEmpty()) { Set<Id> st = videoMap.keySet(); Iterator<Id>
		 * itr = st.iterator(); while (itr.hasNext()) { Id id = (Id) itr.next();
		 * int videoNumber = playlistLoader.loadByVideoId(id, connection); if
		 * (videoNumber == 0) { videoPersister.deleteById(id, connection); } } }
		 * connection.commit(); if (log.isDebugEnabled()) { log.debug(
		 * "Videos successfully removed"); } } catch (Exception ex) { try { if
		 * (null != connection) { connection.rollback(); } } catch (SQLException
		 * sqex) { log.fatal("Rollback failed for removeVideos"); } throw new
		 * Exception("Removing videos for user failed", ex); } finally {
		 * PersistUtil.closeConnection(connection, DatabaseKey.BLACKBOARD); }
		 */
	}

	public PlaylistItem getPlaylistItem(Id id) throws Exception {
		try {
			PlaylistItemDbLoader playlistLoader = this.nbcAppContext.getPlaylistItemDbLoader();
			PlaylistItem item = playlistLoader.loadById(id);
			/*if (log.isDebugEnabled()) {
				log.debug("Playlist item successfully retrieved");
			}*/
			return item;
		} catch (Exception ex) {
			throw new Exception("Playlist item load failed", ex);
		}

		/*
		 * Connection connection = null; PlaylistItem item = null; try {
		 * connection = PersistUtil.getConnection(DatabaseKey.BLACKBOARD);
		 * PlaylistItemDbLoader playlistLoader =
		 * this.nbcAppContext.getPlaylistItemDbLoader(); item =
		 * playlistLoader.loadById(id, connection); if (log.isDebugEnabled()) {
		 * log.debug("Playlist item successfully retrieved"); } return item; }
		 * catch (Exception ex) { throw new Exception(
		 * "Playlist item load failed", ex); } finally {
		 * PersistUtil.closeConnection(connection, DatabaseKey.BLACKBOARD); }
		 */
	}

	@Override
	public boolean exportUsersPlaylist() {
		/*log.debug("exportUsersPlaylist------>");*/
		try {
			PlaylistItemDbLoader loader = this.nbcAppContext.getPlaylistItemDbLoader();
			snasUrl = this.nbcAppContext.getEnvironmentConfiguration().getSnasUrl();
			if (!isExportedAlready()) {
				List<UserPlaylist> userPlayListItems = loader.retriveUserAndPlaylistInfo();
				if (!userPlayListItems.isEmpty()) {
					List<UserPlaylist> formattedPlaylist = consolidateUserPlaylistItems(userPlayListItems);
					/*log.debug("ALL USERS PLAYLIST FETCHED AND CONSOLIDATED------>");*/
					if (!formattedPlaylist.isEmpty()) {
						for (UserPlaylist userPlaylist : formattedPlaylist) {
							if (userPlaylist.getEmailId() == null) {
								userPlaylist.setEmailId(generateEmailId(userPlaylist.getUserId()));
							}
							Map<String, String> map = new HashMap<String, String>();
							map.put("firstName", userPlaylist.getFirstName());
							map.put("lastName", userPlaylist.getLastName());
							map.put("userName", userPlaylist.getUserId());
							map.put("email", userPlaylist.getEmailId());
							String personUUID = null;
							if (!isEmailExists(userPlaylist.getEmailId())) {
								/*log.debug("CREATING NEW USER IN SNAS------>" + userPlaylist.getEmailId());*/
								personUUID = savePerson(map, "admin@nbclearn.com");
							} else {
								/*log.debug("USER ALREADY EXIST IN SNAS------>" + userPlaylist.getEmailId());*/
								personUUID = getPersonUUID(userPlaylist.getEmailId());
							}
							/*log.debug("PERSONUUID---->" + personUUID);*/
							if (personUUID != null) {
								String[] vcmArray = userPlaylist.getVcmIdList().split(",");
								addPlaylist(personUUID, vcmArray);
								updatePlaylistExportStatus(userPlaylist.getUserId());
								/*log.debug("EXPORT STATUS UPDATED FOR USER---->" + userPlaylist.getUserId());*/
							}

						}
					} else {
						/*if (log.isDebugEnabled()) {
							log.debug("PLAYLIST NOT AVAILABLE FOR EXPORTING");
						}*/
					}
				} else {
					/*if (log.isDebugEnabled()) {
						log.debug("PLAYLIST NOT AVAILABLE FOR EXPORTING");
					}*/
				}
			} else {
				/*if (log.isDebugEnabled()) {
					log.debug("PLAYLIST ALREADY EXPORTED");
				}*/
			}
		} catch (Exception ex) {
			ex.getStackTrace();
			return false;
		}
		return true;

	}

	private boolean isExportedAlready() {
		/*log.debug("isExportedAlready------>");*/
		boolean isExported = false;
		try {
			PlaylistItemDbLoader loader = this.nbcAppContext.getPlaylistItemDbLoader();
			List<Integer> list = loader.checkAlreadyImported();
			if (!list.isEmpty()) {
				isExported = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isExported;
	}

	private void updatePlaylistExportStatus(String userId) {

		try {
			PlaylistItemDbLoader loader = this.nbcAppContext.getPlaylistItemDbLoader();
			loader.updatePlaylistExportStatus(userId);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String getPersonUUID(String emailId) {

		String personUUID = null;
		try {

			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new NameValuePair("siteName", "higheredbb.icue.nbcuni.com"));
			params.add(new NameValuePair("emailAddress", emailId));

			String personXml = sendPost(getSnasApiUrl("listPersonByEmail"), params);
			personUUID = getDataFromResponse(personXml, "personUniqueIdentifier");
			/*if (personUUID == null)
				log.debug("ERROR IN GETTING PERSON UUID- FETCHING PERSON UUID");
*/
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return personUUID;
	}

	private String addPlaylist(String personUUID, String[] vcmArray) {
		String status = null;
		try {
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new NameValuePair("personUUID", personUUID));
			params.add(new NameValuePair("siteName", "higheredbb.icue.nbcuni.com"));
			params.add(new NameValuePair("siteDomainName", "higheredbb.icue.nbcuni.com"));
			params.add(new NameValuePair("contentGroupName", "Blackboard Playlist"));

			for (String vcmId : vcmArray) {
				params.add(new NameValuePair("contentSSUniqueKey", vcmId));
				params.add(new NameValuePair("contentType", "BLOG_POST"));
				params.add(new NameValuePair("contentGroupTypeCode", "BLOG"));
			}

			String personXml = sendPost(getSnasApiUrl("addContentToAlbum"), params);

			status = getDataFromResponse(personXml, "success");
			if (status != null && status.equalsIgnoreCase("ok")) {
				/*log.debug("PLAYLIST SUCCESSFULLY EXPORTED FOR " + personUUID);*/
			} else {
				/*log.debug("PLAYLIST EXPORTED FAILED FOR " + personUUID);*/
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return status;
	}

	public String generateEmailId(String userId) {
		String newEmailId = null;
		try {
			Random random = new Random();
			newEmailId = userId + random.nextInt(100) + new Date().getTime() + "@nbcuni.com";
			PlaylistItemDbLoader loader = this.nbcAppContext.getPlaylistItemDbLoader();
			loader.updateUserMailId(userId, newEmailId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return newEmailId;
	}

	private String savePerson(Map<String, String> map, String adminEmail) throws Exception {
		XMLOutputFactory output = XMLOutputFactory.newInstance();
		StringWriter sw = new StringWriter();
		XMLStreamWriter xsw = null;
		String snasSiteName = "higheredbb.icue.nbcuni.com";
		boolean xmlBuildingError = false;

		String uuid = "";
		try {
			xsw = output.createXMLStreamWriter(new BufferedWriter(sw));
			xsw.writeStartDocument();
			String today = new SimpleDateFormat("MM/dd/yyyy").format(new Date());
			/* start person */
			xsw.writeStartElement("person");
			writeCoreAttribute(xsw, "status", "ACTIVE", null, null);

			writeCoreAttribute(xsw, USER_ATTR_FIRSTNAME, map.get(USER_ATTR_FIRSTNAME), "private", null);
			writeCoreAttribute(xsw, USER_ATTR_LASTNAME, map.get(USER_ATTR_LASTNAME), "private", null);
			writeCoreAttribute(xsw, USER_ATTR_PHONE, map.get(USER_ATTR_PHONE), "private", "mobile");
			writeCoreAttribute(xsw, USER_ATTR_EMAIL, map.get(USER_ATTR_EMAIL), "private", "personal");
			writeCoreAttribute(xsw, USER_ATTR_EMAIL, map.get(USER_ATTR_EMAIL), "private", "business");
			writeProfileAttribute(xsw, USER_ATTR_REGISTRATION_DATE, today, null, null);
			writeProfileAttribute(xsw, USER_ATTR_PROFILE_PRIVACY, "Friends", "public", null);
			writeProfileAttribute(xsw, USER_ATTR_MODIFIED_BY, adminEmail, null, null);
			writeProfileAttribute(xsw, USER_ATTR_MODIFIED_DATE, today, null, null);

			/* start address */
			xsw.writeStartElement("address");
			xsw.writeAttribute("type", "work");
			xsw.writeAttribute("privacyLevel", "public");
			xsw.writeEndElement();
			/* end address */

			String userName = getUniqueUserName(snasSiteName, map.get(USER_ATTR_USER_NAME));

			writeCoreAttribute(xsw, USER_ATTR_USER_NAME, userName, null, null);
			writeCoreAttribute(xsw, USER_ATTR_SITE, snasSiteName, null, null);

			if (isEmpty(map.get(USER_ATTR_PASSWORD))) {
				writeCoreAttribute(xsw, USER_ATTR_PASSWORD, "pa55w0rd", null, null);
			} else {
				writeCoreAttribute(xsw, USER_ATTR_PASSWORD, map.get(USER_ATTR_PASSWORD), null, null);
			}
			xsw.writeEndElement();
			/* end person */
			xsw.writeEndDocument();
		} catch (Exception ex) {
			ex.printStackTrace();
			log.error("Error in building personXML to create user", ex);
			xmlBuildingError = true;
		} finally {
			try {
				xsw.flush();
				xsw.close();
			} catch (Exception ignored) {
			}
		}
		if (!xmlBuildingError) {
			/*log.debug(
					"ready to call snas to create user:" + map.get(USER_ATTR_EMAIL) + " in adminEmail: " + adminEmail);*/

			List<NameValuePair> params = new ArrayList<NameValuePair>();

			params.add(new NameValuePair("siteName", snasSiteName));
			params.add(new NameValuePair("siteDomainName", snasSiteName));
			params.add(new NameValuePair("personXml", sw.toString()));

			String savePersonResponseXml = sendPost(getSnasApiUrl("savePerson"), params);

			uuid = getDataFromResponse(savePersonResponseXml, "uniqueIdentifier");

			/*if (uuid == null)
				log.debug("ERROR WHILE GETTING THE PERSON UUID-SAVE PERSON");*/

		}

		return uuid;

	}

	private void writeCoreAttribute(XMLStreamWriter xsw, String name, String value, String privacy, String type)
			throws XMLStreamException {

		if (!isEmpty(value)) {

			xsw.writeStartElement(name);

			if (!isEmpty(privacy)) {
				xsw.writeAttribute("privacyLevel", privacy);
			}

			if (!isEmpty(type)) {
				xsw.writeAttribute("type", type);
			}
			xsw.writeCharacters(value);
			xsw.writeEndElement();
		}
	}

	public static boolean isEmpty(String string) {
		return string == null || string.trim().isEmpty();
	}

	public static boolean hasLength(String string) {
		return !isEmpty(string);
	}

	private void writeProfileAttribute(XMLStreamWriter xsw, String name, String value, String privacy, String type)
			throws XMLStreamException {
		if (name == "grade" || name == "subject") {

			xsw.writeStartElement("profileAttribute");

			if (!isEmpty(privacy)) {
				xsw.writeAttribute("privacyLevel", privacy);
			} else {
				xsw.writeAttribute("privacyLevel", "private");
			}
			xsw.writeAttribute("attributeName", name);
			if (!isEmpty(type)) {
				xsw.writeAttribute("type", type);
			}
			xsw.writeCharacters(value);
			xsw.writeEndElement();

		} else {
			if (!isEmpty(value)) {
				xsw.writeStartElement("profileAttribute");

				if (!isEmpty(privacy)) {
					xsw.writeAttribute("privacyLevel", privacy);
				} else {
					xsw.writeAttribute("privacyLevel", "private");
				}

				xsw.writeAttribute("attributeName", name);
				if (!isEmpty(type)) {
					xsw.writeAttribute("type", type);
				}
				xsw.writeCharacters(value);
				xsw.writeEndElement();
			}
		}
	}

	public String getUniqueUserName(String siteName, String userNameFromMap) {

		String userName = getRandomLetters(3);

		if (hasLength(userNameFromMap) && isUserNameAvailable(userNameFromMap, siteName)) {
			userName = userNameFromMap;
		} else {
			String randomToken = "";
			String tempuserName = "";
			/* try only ten times */
			for (int i = 0; i < 10; i++) {
				randomToken = getRandomToken(10);
				tempuserName = userName + randomToken;

				if (isUserNameAvailable(tempuserName, siteName)) {
					userName = tempuserName;
					break;
				}
			}
		}

		return userName;
	}

	public static String getRandomToken(int tokenLength) {
		StringBuilder token = new StringBuilder();
		Random r = new Random();
		int randInt = 0;
		for (int i = 1; i <= tokenLength; i++) {
			randInt = r.nextInt(35);
			token = token.append(Character.forDigit(randInt, 36));
		}
		return token.toString();
	}

	public static String getRandomLetters(int tokenLength) {
		StringBuilder token = new StringBuilder();
		Random r = new Random();
		int randInt = 0, min = 10, max = 35;

		for (int i = 1; i <= tokenLength; i++) {
			randInt = r.nextInt(max - min + 1) + min;
			token = token.append(Character.forDigit(randInt, 36));
		}
		return token.toString();
	}

	public boolean isUserNameAvailable(String userName, String siteName) {

		boolean retVal = false;

		if (hasLength(userName)) {
			try {

				List<NameValuePair> params = new ArrayList<NameValuePair>();

				params.add(new NameValuePair("siteName", siteName));
				params.add(new NameValuePair("siteDomainName", siteName));
				params.add(new NameValuePair("userName", userName));

				String responseString = sendPost(getSnasApiUrl("isUsernameAvailable"), params);

				retVal = responseString.trim().equalsIgnoreCase("true");

			} catch (Exception ex) {
				ex.printStackTrace();
				log.error("Errror in calling isUsernameAvailable API", ex);
			}
		}
		return retVal;

	}

	public static String getSnasApiUrl(String apiName) {

		StringBuilder url = new StringBuilder("");
		url.append(snasUrl).append(apiName);
		return url.toString();
	}

	public boolean isEmailExists(String email) throws Exception {

		/*log.debug("isEmailExists------------>" + email);
*/
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new NameValuePair("email", email));
		String responseString = sendPost(getSnasApiUrl("checkEmailExists"), params);
		String isExist = getDataFromResponse(responseString, "status");
		if (isExist != null && isExist.equalsIgnoreCase("true"))
			return true;
		else
			return false;

	}

	private List<UserPlaylist> consolidateUserPlaylistItems(List<UserPlaylist> userPlayListItems) {
		List<UserPlaylist> list = new ArrayList<UserPlaylist>();
		try {
			String emailId = null;
			String vcmIdList = null;
			String firstName = null;
			String lastName = null;
			String userId = null;
			UserPlaylist userPlaylistItem = null;
			for (UserPlaylist userPlaylist : userPlayListItems) {
				if (userId == null) {
					emailId = userPlaylist.getEmailId();
					vcmIdList = userPlaylist.getVcmId();
					firstName = userPlaylist.getFirstName();
					lastName = userPlaylist.getLastName();
					userId = userPlaylist.getUserId();
				} else if (userId.equals(userPlaylist.getUserId())) {
					vcmIdList += "," + userPlaylist.getVcmId();
				} else if (!userId.equals(userPlaylist.getUserId())) {
					userPlaylistItem = new UserPlaylist();
					userPlaylistItem.setEmailId(emailId);
					userPlaylistItem.setVcmIdList(vcmIdList);
					userPlaylistItem.setFirstName(firstName);
					userPlaylistItem.setLastName(lastName);
					userPlaylistItem.setUserId(userId);
					list.add(userPlaylistItem);
					emailId = userPlaylist.getEmailId();
					vcmIdList = userPlaylist.getVcmId();
					firstName = userPlaylist.getFirstName();
					lastName = userPlaylist.getLastName();
					userId = userPlaylist.getUserId();
				}
			}
			userPlaylistItem = new UserPlaylist();
			userPlaylistItem.setEmailId(emailId);
			userPlaylistItem.setVcmIdList(vcmIdList);
			userPlaylistItem.setFirstName(firstName);
			userPlaylistItem.setLastName(lastName);
			userPlaylistItem.setUserId(userId);
			list.add(userPlaylistItem);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	private String sendPost(String url, List<NameValuePair> nvps) throws Exception {

		try {

			// log.debug("sendPost----url-------->" + url);
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			// add reuqest header
			con.setRequestMethod("POST");

			StringBuffer urlParameters = new StringBuffer();
			// log.debug("nvps------------>" + nvps.size());
			if (!nvps.isEmpty()) {
				for (NameValuePair nameValuePair : nvps) {
					urlParameters.append(nameValuePair.getName() + "=" + nameValuePair.getValue() + "&");
				}
			}
			if (urlParameters != null && urlParameters.length() > 0) {
				urlParameters = urlParameters.deleteCharAt(urlParameters.toString().length() - 1);
			}
			// log.debug("sendPost----final -------urlParameters-------->" +
			// urlParameters);

			// Send post request
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(urlParameters.toString());
			wr.flush();
			wr.close();

			int responseCode = con.getResponseCode();

			// log.debug("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
			// log.debug("RESPONSE------>" + response.toString());

			return response.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}

	// HTTP GET request
	private void sendGet(String url) throws Exception {

		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod("GET");

		int responseCode = con.getResponseCode();

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

	}

	private String getDataFromResponse(String responseXml, String tagName) {

		try {
			Document doc = convertStringToDocument(responseXml);
			doc.getDocumentElement().normalize();

			/*if (log.isDebugEnabled()) {
				log.debug("Root element :" + doc.getDocumentElement().getNodeName());
				log.debug("Tag Name :" + tagName + "------------------->"
						+ doc.getElementsByTagName(tagName).item(0).getTextContent());
			}*/

			return doc.getElementsByTagName(tagName).item(0).getTextContent();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private static Document convertStringToDocument(String xmlStr) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		try {
			builder = factory.newDocumentBuilder();
			Document doc = builder.parse(new InputSource(new StringReader(xmlStr)));
			return doc;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void updateOldPlayerContent(String token, String cuecardXmlUrl, String baseUrl, String peacockImage,
			String playIcon, String nbcVi, String courseId) throws Exception {

		/*if (log.isDebugEnabled()) {
			log.debug("UPDATING OLD CONTENT STARTS---->");
		}*/
		PlaylistItemDbLoader loader = nbcAppContext.getPlaylistItemDbLoader();
		List<HashMap<String, Object>> oldContentList = null;

		/*if (log.isDebugEnabled()) {
			log.debug("GETTING EMBED/ITEM/TOOL STARTS---->");
		}*/
		oldContentList = loader.getOldPlayerContent(courseId);
		replaceOldContent(token, loader, oldContentList, "COURSE_CONTENTS", cuecardXmlUrl, baseUrl, nbcVi, peacockImage,
				playIcon);
		/*if (log.isDebugEnabled()) {
			log.debug("GETTING EMBED/ITEM/TOOL ENDS---->");
		}
		if (log.isDebugEnabled()) {
			log.debug("GETTING FORUM STARTS---->");
		}*/
		oldContentList = loader.getOldPlayerForumContent(courseId);
		if(oldContentList==null || oldContentList.size()==0){
			oldContentList = loader.getLegacyPlayerForumContent(courseId);
		}
		replaceOldContent(token, loader, oldContentList, "FORUM_MAIN", cuecardXmlUrl, baseUrl, nbcVi, peacockImage,
				playIcon);
		/*if (log.isDebugEnabled()) {
			log.debug("GETTING FORUM ENDS---->");
		}

		if (log.isDebugEnabled()) {
			log.debug("GETTING THREAD STARTS---->");
		}*/
		oldContentList = loader.getOldPlayerThreadContent(courseId);
		if(oldContentList==null || oldContentList.size()==0){
			oldContentList = loader.getLegacyPlayerThreadContent(courseId);
		}
		replaceOldContent(token, loader, oldContentList, "MSG_MAIN", cuecardXmlUrl, baseUrl, nbcVi, peacockImage,
				playIcon);
		/*if (log.isDebugEnabled()) {
			log.debug("GETTING THREAD ENDS---->");
		}

		if (log.isDebugEnabled()) {
			log.debug("GETTING ANNOUNCEMENT STARTS---->");
		}*/
		oldContentList = loader.getOldPlayerAnnouncementContent(courseId);
		replaceOldContent(token, loader, oldContentList, "ANNOUNCEMENTS", cuecardXmlUrl, baseUrl, nbcVi, peacockImage,
				playIcon);
		/*if (log.isDebugEnabled()) {
			log.debug("GETTING ANNOUNCEMENT ENDS---->");
		}

		if (log.isDebugEnabled()) {
			log.debug("GETTING TASKS STARTS---->");
		}*/
		oldContentList = loader.getOldPlayerTaskContent(courseId);
		replaceOldContent(token, loader, oldContentList, "TASKS", cuecardXmlUrl, baseUrl, nbcVi, peacockImage,
				playIcon);
		/*if (log.isDebugEnabled()) {
			log.debug("GETTING TASKS ENDS---->");
		}

		if (log.isDebugEnabled()) {
			log.debug("UPDATING OLD CONTENT ENDS");
		}*/

	}

	/**
	 * @param token
	 * @param loader
	 * @param oldContentList
	 * @param tableName
	 * @param playIcon
	 * @param filePath
	 * @param peacockImage
	 * @param nbcVi
	 * @param baseUrl
	 * @param cuecardXmlUrl
	 */
	private void replaceOldContent(String token, PlaylistItemDbLoader loader,
			List<HashMap<String, Object>> oldContentList, String tableName, String cuecardXmlUrl, String baseUrl,
			String nbcVi, String peacockImage, String playIcon) throws Exception {

		if (oldContentList != null && oldContentList.size() > 0) {
			/*if (log.isDebugEnabled()) {
				log.debug("OLD CONTENT COUNT---->" + oldContentList.size());
			}*/
			int totalCount = oldContentList.size();
			int successCounter = 0;
			for (HashMap<String, Object> content : oldContentList) {
				try {
					if (content.get("mainData") != null) {
						successCounter++;
						String exisMetadata = (String) content.get("mainData");
						String iframeSrc = null;
						String cuecardId = null;
						String finalTemplate = null;
						String xmlResponse = null;
						Video video = null;
						String[] contentArray = null;
						int videoCount = 0;
						String newFinalTemplate = "";

						// Check for multiple videos and other contents
						if (!exisMetadata.contains("<table style=\"width: 100%;\">")) {
							finalTemplate = constructTemplate(loader, cuecardXmlUrl, baseUrl, nbcVi, peacockImage,
									playIcon, exisMetadata, finalTemplate, video);
							newFinalTemplate += finalTemplate;
							String exisSubMetadata = exisMetadata.substring(exisMetadata.lastIndexOf("</table>") + 8,
									exisMetadata.length());
							if (exisSubMetadata != null && exisSubMetadata.trim().length() > 0) {
								newFinalTemplate += finalTemplate;
							}
						} else {
							contentArray = exisMetadata.split("<table style=\"width: 100%;\">");
							/*log.debug("contentArray-->" + contentArray.length);*/
							if (contentArray != null && contentArray.length > 0) {
								for (String individualItem : contentArray) {
									if (individualItem.contains("archivesbb.nbclearn.com/portal/site/root/widget")) {
										videoCount++;
										/*log.debug("videoCount-->" + videoCount);*/
										String[] innerTableArray = individualItem.split("<table");
										/*log.debug("innerTableArray-->" + innerTableArray.length);*/
										if (innerTableArray != null && innerTableArray.length >= 2) {
											int ctr = 0;
											for (String addedString : innerTableArray) {
												ctr++;
												if (ctr == 1)
													continue;
												if (addedString
														.contains("archivesbb.nbclearn.com/portal/site/root/widget")) {
													/*log.debug("addedString iframe-->" + addedString);*/
													finalTemplate = constructTemplate(loader, cuecardXmlUrl, baseUrl,
															nbcVi, peacockImage, playIcon, addedString, finalTemplate,
															video);
													/*log.debug("addedString iframe final-->" + finalTemplate);*/
													newFinalTemplate += finalTemplate;
													if (addedString.lastIndexOf("</table>") != -1) {
														addedString = addedString.substring(
																addedString.lastIndexOf("</table>") + 8,
																addedString.length());
														/*log.debug("Extra string-->" + addedString);*/
														if (addedString != null && addedString.trim().length() > 0) {
															newFinalTemplate += addedString;
														}
													}
												} else {
													/*log.debug("New Table string-->" + addedString);*/
													newFinalTemplate += addedString;
												}
											}
										}
									} else {
										newFinalTemplate += individualItem;
									}
								}
							}
						}
						/*log.debug("New Final template->" + newFinalTemplate);*/
						// log.debug("finalTemplate---->" + finalTemplate);
						// Update course content table

						if (tableName.equalsIgnoreCase("COURSE_CONTENTS"))
							loader.updateNewContent((Integer) content.get("pk1"), newFinalTemplate);
						else if (tableName.equalsIgnoreCase("FORUM_MAIN"))
							loader.updateNewForumContent((Integer) content.get("pk1"), newFinalTemplate);
						else if (tableName.equalsIgnoreCase("MSG_MAIN"))
							loader.updateNewThreadContent((Integer) content.get("pk1"), newFinalTemplate);
						else if (tableName.equalsIgnoreCase("ANNOUNCEMENTS"))
							loader.updateNewAnnouncementContent((Integer) content.get("pk1"), newFinalTemplate);
						else if (tableName.equalsIgnoreCase("TASKS"))
							loader.updateNewTaskContent((Integer) content.get("pk1"), newFinalTemplate);

					}
				} catch (Exception e) {
					/*log.debug("Exception--->" + e.getMessage());*/
					e.printStackTrace();
				}

				continue;
			}
			if (totalCount == successCounter) {
				/*log.debug("Updated all assets without error");*/
			}
		}
	}

	private String constructTemplate(PlaylistItemDbLoader loader, String cuecardXmlUrl, String baseUrl, String nbcVi,
			String peacockImage, String playIcon, String exisMetadata, String finalTemplate, Video video)
			throws Exception {
		String iframeSrc;
		String cuecardId;
		String xmlResponse;
		iframeSrc = exisMetadata.substring(exisMetadata.indexOf("<iframe"), exisMetadata.indexOf("</iframe"));
		if (iframeSrc.contains("RCRD")) {
			iframeSrc = iframeSrc.substring(iframeSrc.indexOf("src=") + 5, iframeSrc.indexOf("RCRD") + 4);
			cuecardId = iframeSrc.substring(iframeSrc.lastIndexOf("/") + 1, iframeSrc.length());
		} else {
			iframeSrc = iframeSrc.substring(iframeSrc.indexOf("src=") + 5, iframeSrc.indexOf("allowfullscreen"));
			cuecardId = iframeSrc.substring(iframeSrc.lastIndexOf("/") + 1, iframeSrc.indexOf("\""));

			iframeSrc = iframeSrc.substring(0, iframeSrc.lastIndexOf("/") + 1);
			iframeSrc += cuecardId;

			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new NameValuePair("id", cuecardId));
			xmlResponse = sendPost(cuecardXmlUrl, params);
			cuecardId = parseXml(video, xmlResponse);
			iframeSrc = iframeSrc.substring(0, iframeSrc.lastIndexOf("/") + 1);
			iframeSrc += cuecardId;

		}
		video = loader.loadVideoById(cuecardId);

		if (video != null) {
			video.setIframeSource(iframeSrc);

			finalTemplate = renderFinalTemplate(video, baseUrl, peacockImage, playIcon, nbcVi);
		}
		return finalTemplate;
	}

	private String renderFinalTemplate(Video video, String baseUrl, String peacockImage, String playIcon,
			String nbcVi) {
		String finalTemplate = null;

		Map<String, String> replText = new HashMap<String, String>();
		replText.put(Constants.NBC_URL, baseUrl);
		replText.put(Constants.NBC_THUMBNAIL, video.getThumbnailUrl());
		replText.put(Constants.NBC_PEACOCK_IMAGE, peacockImage);
		if (video.getType().equalsIgnoreCase("I")) {
			replText.put(Constants.NBC_CUECARD_TYPE_IMAGE,
					"https://static.nbclearn.com/files/highered/site/download/blackboard/install/photo.gif");
		} else if (video.getType().equalsIgnoreCase("D") || video.getType().equalsIgnoreCase("T")) {
			replText.put(Constants.NBC_CUECARD_TYPE_IMAGE,
					"https://static.nbclearn.com/files/highered/site/download/blackboard/install/text.gif");
		} else {
			replText.put(Constants.NBC_CUECARD_TYPE_IMAGE,
					"https://static.nbclearn.com/files/highered/site/download/blackboard/install/video.gif");
		}
		replText.put(Constants.NBC_CUECARD_TITLE, video.getName());
		replText.put(Constants.NBC_CUECARD_AIRDATE, video.getAirDateInString());
		replText.put(Constants.NBC_CUECARD_DURATION, video.getDuration());
		replText.put(Constants.NBC_VI, nbcVi);
		replText.put(Constants.NBC_CUECARD_IFRAMESOURCE, video.getIframeSource());
		replText.put(Constants.NBC_PLAY_ICON, playIcon);
		replText.put(Constants.NBC_CUECARD_ID, video.getVideoId());

		replText.put(Constants.NBC_THUMBNAIL_ALT, video.getName());
		replText.put(Constants.NBC_PEACOCK_IMAGE_ALT, "NBC Learn");
		replText.put(Constants.NBC_CUECARD_TYPE_IMAGE_ALT, video.getType());
		/*if (log.isDebugEnabled()) {
			log.debug("After addAltAttributeValues");
		}*/

		finalTemplate = renderTemplate("/" + Constants.TEMPLATE_MASHUP_CUECARD_THUMBNAIL, replText,
				"There was an issue while rendering a Mashup Cue Card.");
		return finalTemplate;
	}

	private String parseXml(Video video, String xmlResponse) throws Exception {
		String vcmId = null;
		Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder()
				.parse(new InputSource(new StringReader(xmlResponse)));
		NodeList errNodes = doc.getElementsByTagName("CueCard");
		if (errNodes.getLength() > 0) {
			Element err = (Element) errNodes.item(0);
			/*if (log.isDebugEnabled()) {
				log.debug("Asset_vcmId -" + err.getElementsByTagName("Asset_vcmId").item(0).getTextContent());
			}*/
			vcmId = err.getElementsByTagName("Asset_vcmId").item(0).getTextContent();

		}
		return vcmId;
	}

	public String renderDescription(String iframeContent) {
		Map<String, String> replText = new HashMap<String, String>();
		replText.put(Constants.NBC_CUECARD_CONTENT, iframeContent);
		String rendered = renderTemplate("/" + Constants.TEMPLATE_CONTENT_DESCRIPTION, replText,
				"There was an issue while rendering content description.");

		return rendered;
	}

	@Override
	public void updateIframeSecurityToken(String securityToken, String secTokenUrl) throws PersistenceException {
		boolean updateStatus = false;
		/*if (log.isDebugEnabled()) {
			log.debug("UPDATING SECURITY TOKEN STARTS---->");
		}*/
		String emailId, firstName, lastName, role = null;
		String pathVariable = "";
		try {
			PlaylistItemDbLoader loader = nbcAppContext.getPlaylistItemDbLoader();

			emailId = "bbadmin@nbclearn.com";
			firstName = "bbadmin";
			lastName = "bbadmin";
			role = "Adm";

			pathVariable += securityToken + "&E=" + emailId + "&Fn=" + firstName + "&Ln=" + lastName + "&r=" + role;
			securityToken = sendPost(secTokenUrl + pathVariable, new ArrayList<NameValuePair>());
			/*if (log.isDebugEnabled()) {
				log.debug("securityToken---->" + securityToken);
			}*/
			loader.updateIframeSecurityToken(securityToken);
		} catch (Exception e) {
			log.error("ERROR OCCURS WHILE UPDATING TOKEN---->" + e);
		}

	}

	@Override
	public String getSecurityToken() throws PersistenceException {
		PlaylistItemDbLoader loader = this.nbcAppContext.getPlaylistItemDbLoader();
		return loader.getSecurityToken();
	}
}
