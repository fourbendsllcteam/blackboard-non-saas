package blackboard.gs.nbc.playlist.service;

import java.util.List;
import blackboard.data.user.User;
import blackboard.gs.nbc.context.NBCAppContext;
import blackboard.gs.nbc.playlist.data.playlistitem.PlaylistItem;
import blackboard.gs.nbc.playlist.data.video.Video;
import blackboard.gs.nbc.util.persist.Id;
import blackboard.persist.PersistenceException;

public interface PlaylistManagerService {

	public List<PlaylistItem> retrieveVideos(blackboard.persist.Id userId) throws Exception;

	public PlaylistItem saveVideo(User user, Video video) throws Exception;

	public void removeVideos(List<PlaylistItem> list) throws Exception;

	public PlaylistItem getPlaylistItem(Id id) throws Exception;

	public boolean exportUsersPlaylist() throws Exception;

	public String generateEmailId(String username);

	public void updateOldPlayerContent(String securityToken, String cuecardXmlUrl, String baseUrl, String peacockImage,  String playIcon, String nbcVi, String courseId) throws Exception;

	public void updateIframeSecurityToken(String securityToken, String secTokenUrl) throws Exception;

	public String getSecurityToken() throws PersistenceException;
	
}
