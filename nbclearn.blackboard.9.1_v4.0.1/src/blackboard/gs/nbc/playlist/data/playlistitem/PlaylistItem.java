package blackboard.gs.nbc.playlist.data.playlistitem;

import java.util.Calendar;
import blackboard.data.user.User;
import blackboard.gs.nbc.playlist.data.video.Video;
import blackboard.gs.nbc.util.persist.Id;

public class PlaylistItem {

	private User user;

	private Video video;

	private Calendar dtModified;
	
	private Id id;

	
	
	
	/**
	 * @param dtModified
	 *            the dtModified to set
	 */
	public void setDtModified(Calendar dtModified) {
		this.dtModified = dtModified;
	}

	/**
	 * @return the dtModified
	 */
	public Calendar getDtModified() {
		return dtModified;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Id id) {
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public Id getId() {
		return id;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param video the video to set
	 */
	public void setVideo(Video video) {
		this.video = video;
	}

	/**
	 * @return the video
	 */
	public Video getVideo() {
		return video;
	}


}
