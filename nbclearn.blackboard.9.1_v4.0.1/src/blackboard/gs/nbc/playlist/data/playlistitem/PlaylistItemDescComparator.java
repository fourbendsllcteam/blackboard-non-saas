package blackboard.gs.nbc.playlist.data.playlistitem;

import blackboard.base.BaseComparator;

@SuppressWarnings("serial")
public final class PlaylistItemDescComparator extends BaseComparator<PlaylistItem> {
	public PlaylistItemDescComparator(boolean ascending)
			throws IllegalArgumentException, SecurityException {
		super(ascending);
	}

	@Override
	protected int doCompare(PlaylistItem item1, PlaylistItem item2)
			throws ClassCastException, IllegalArgumentException {
		if ((null==item1)||(null==item2))
		{
			throw new IllegalArgumentException("List is empty");
		}
		
		if ((item1.getVideo().getDescription().equals(""))&&(item2.getVideo().getDescription().equals("")))
		{
			return 0;
		}
		if (item1.getVideo().getDescription().equals(""))
		{
			return 1;
		}
		if (item2.getVideo().getDescription().equals(""))
		{
			return 1;
		}
		int comparsionResult = item1.getVideo().getDescription().compareTo(item2.getVideo().getDescription());
		if (comparsionResult < 0) {
			return -1;

		} else if (comparsionResult > 0) {
			return 1;
		}
		return 0;
	}
}
