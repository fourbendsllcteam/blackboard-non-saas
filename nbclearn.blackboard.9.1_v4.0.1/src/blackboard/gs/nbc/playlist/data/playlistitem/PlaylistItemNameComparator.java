package blackboard.gs.nbc.playlist.data.playlistitem;

import blackboard.base.BaseComparator;

@SuppressWarnings("serial")
public final class PlaylistItemNameComparator extends BaseComparator<PlaylistItem> {
	public PlaylistItemNameComparator(boolean ascending)
			throws IllegalArgumentException, SecurityException {
		super(ascending);
	}

	@Override
	protected int doCompare(PlaylistItem item1, PlaylistItem item2)
			throws ClassCastException, IllegalArgumentException {
		if ((null==item1)||(null==item2))
		{
			throw new IllegalArgumentException("List is empty");
		}
		if ((item1.getVideo().getName().equals(""))&&(item2.getVideo().getName().equals("")))
		{
			return 0;
		}
		if (item1.getVideo().getName().equals(""))
		{
			return 1;
		}
		if (item2.getVideo().getName().equals(""))
		{
			return 1;
		}
		int comparisonResult = item1.getVideo().getName().compareTo(item2.getVideo().getName());
		if (comparisonResult < 0) {
			return -1;

		} else if (comparisonResult > 0) {
			return 1;
		}
		return 0;
	}
}
