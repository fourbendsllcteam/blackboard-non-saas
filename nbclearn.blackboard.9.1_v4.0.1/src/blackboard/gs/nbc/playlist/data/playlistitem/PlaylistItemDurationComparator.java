package blackboard.gs.nbc.playlist.data.playlistitem;

import blackboard.base.BaseComparator;

@SuppressWarnings("serial")
public class PlaylistItemDurationComparator extends
		BaseComparator<PlaylistItem> {

	public PlaylistItemDurationComparator(boolean ascending)
			throws IllegalArgumentException, SecurityException {
		super(ascending);
	}

	@Override
	protected int doCompare(PlaylistItem item1, PlaylistItem item2)
			throws ClassCastException, IllegalArgumentException {
		if ((null==item1)||(null==item2))
		{
			throw new IllegalArgumentException("List is empty");
		}
		if ((item1.getVideo().getDuration().equals(""))&&(item2.getVideo().getDuration().equals("")))
		{
			return 0;
		}
		if (item1.getVideo().getDuration().equals(""))
		{
			return 1;
		}
		if (item2.getVideo().getDuration().equals(""))
		{
			return 1;
		}
		try
		{
			String[] item1_hours = item1.getVideo().getDuration().split(":");
			String[] item2_hours = item2.getVideo().getDuration().split(":");
			int comparisonResult1 = Integer.parseInt(item1_hours[0])- (Integer.parseInt(item2_hours[0]));
			int comparisonResult2 = Integer.parseInt(item1_hours[1])- (Integer.parseInt(item2_hours[1]));
			int comparisonResult3 = Integer.parseInt(item1_hours[2])- (Integer.parseInt(item2_hours[2]));
	
			if (comparisonResult1 > 0) { return  1; }
			if (comparisonResult1 < 0) { return -1; }
	
			if (comparisonResult2 > 0) { return  1; }
			if (comparisonResult2 < 0) { return -1; }
			    
			if (comparisonResult3 > 0) { return  1; } 
			if (comparisonResult3 < 0) { return -1; } 
	
			return 0;
		} catch (NumberFormatException ne)
		{
			return 1;
		}
		
	}

}
