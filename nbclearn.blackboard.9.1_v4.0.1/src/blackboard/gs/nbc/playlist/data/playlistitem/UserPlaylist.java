package blackboard.gs.nbc.playlist.data.playlistitem;

public class UserPlaylist {
	private String emailId;

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getVcmId() {
		return vcmId;
	}

	public void setVcmId(String vcmId) {
		this.vcmId = vcmId;
	}

	private String vcmId;

	private String vcmIdList;

	public String getVcmIdList() {
		return vcmIdList;
	}

	public void setVcmIdList(String vcmIdList) {
		this.vcmIdList = vcmIdList;
	}

	private String firstName;

	private String lastName;

	private String userId;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
