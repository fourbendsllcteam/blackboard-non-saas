package blackboard.gs.nbc.playlist.data.playlistitem;

import blackboard.base.BaseComparator;

@SuppressWarnings("serial")
public final class PlaylistItemAirDateComparator extends BaseComparator<PlaylistItem> {
	public PlaylistItemAirDateComparator(boolean ascending)
			throws IllegalArgumentException, SecurityException {
		super(ascending);
	}

	@Override
	protected int doCompare(PlaylistItem item1, PlaylistItem item2)
			throws ClassCastException, IllegalArgumentException {
		if ((null==item1)||(null==item2))
		{
			throw new IllegalArgumentException("List is empty");
		}
		if ((item1.getVideo().getAirDate()==null)&&(item2.getVideo().getAirDate()==null))
		{
			return 0;
		}
		if (item1.getVideo().getAirDate()==null)
		{
			return 1;
		}
		if (item2.getVideo().getAirDate()==null)
		{
			return 1;
		}
		if (item1.getVideo().getAirDate().before(item2.getVideo().getAirDate())) {
			return -1;
		}
		if (item1.getVideo().getAirDate().after(item2.getVideo().getAirDate())) {
			return 1;
		}
		return 0;
	}
}
