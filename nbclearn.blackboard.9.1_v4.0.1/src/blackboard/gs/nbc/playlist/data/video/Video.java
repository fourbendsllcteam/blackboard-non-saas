package blackboard.gs.nbc.playlist.data.video;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.apache.log4j.Logger;

import blackboard.gs.nbc.context.NBCAppContext;
import blackboard.gs.nbc.util.persist.Id;

/**
 * 
 * @author Ryan Hardy (rhardy@guilfordgroup.com)
 */
public class Video {
	private static final Logger logger = Logger.getLogger(Video.class);
	
	private String videoId;
	private Id id;
	private String name;
	private String description;
	private String duration;
	private Calendar airDate;
	private String airDateInString;
	private String thumbnailUrl;
	private String thumbnailDecodedUrl;
	private String type;
	private String shortType;
	private String contentTypeImage;
	private String cueCardWithJavascriptAndCss;
	private String cueCardWithoutJavascriptAndCss;
	private String cueCardCourseContent;
	private String cueCardMashupContent;
	private String iframeContent;
	private String iframeSource;
	private String assetType;

	private final NBCAppContext appContext;

	/**
	 * @para appContext
	 */
	public Video(NBCAppContext appContext) {
		this.appContext = appContext;
	}

	/**
	 * @param videoId
	 *            the videoId to set
	 */
	public void setVideoId(String videoId) {
		this.videoId = videoId;
	}

	/**
	 * @return the videoId
	 */
	public String getVideoId() {
		return videoId;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Id id) {
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public Id getId() {
		return id;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the name
	 */
	public String getName() {		
		return getReturnString(name);
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return getReturnString(description);
	}

	/**
	 * @param duration the duration to set
	 * @throws ParseException
	 */
	public void setDuration(String duration) {
		this.duration = duration;
		
	}

	/**
	 * @return the duration
	 */
	public String getDuration() {
		return getReturnString(duration);		
	}

	/**
	 * @param airDate the airDate to set
	 */
	public void setAirDate(Calendar airDate) {
		this.airDate = airDate;
	}

	/**
	 * @return the airDate
	 */
	public Calendar getAirDate() {
		return airDate;
	}

	/**
	 * @param airDateInString the airDateInString to set
     */
	public void setAirDateInString(String airDateInString) {
		try {
			if (null != airDateInString) {
				this.airDateInString = airDateInString;
				SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.0");
				Date date = dt.parse(airDateInString);
				Calendar cal = Calendar.getInstance();
				cal.setTime(date);
				airDate = cal;
			} else {
				airDate = null;
			}
		} catch (ParseException pe) {
			airDate = null;
		}

	}

	/**
	 * @return the airDate
	 */
	public String getAirDateInString() {
		if (null != getAirDate()) {
			DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.getDefault());
			df.setTimeZone(TimeZone.getDefault());
			airDateInString = df.format(getAirDate().getTime());
			return airDateInString;
		} else {
			return "";
		}
	}

	/**
	 * @param thumbnailUrl the thumbnailUrl to set
	 * @throws UnsupportedEncodingException 
	 */
	public void setThumbnailUrl(String thumbnailUrl){
		this.thumbnailUrl = thumbnailUrl;
		
	}
	

	/**
	 * @return the thumbnailUrl
	 */
	public String getThumbnailUrl() {
		return getReturnString(thumbnailUrl);
	}

	/**
	 * @param thumbnailDecodedUrl the thumbnailDecodedUrl to set
	 */
	public void setThumbnailDecodedUrl(String thumbnailDecodedUrl) {
		try {
			if (null != thumbnailDecodedUrl) {
				this.thumbnailDecodedUrl = URLDecoder.decode(thumbnailDecodedUrl, "UTF-8");
			} else {
				this.thumbnailDecodedUrl = thumbnailDecodedUrl;
			}
		} catch (UnsupportedEncodingException ue) {
			this.thumbnailDecodedUrl = null;
		}
		thumbnailUrl = this.thumbnailDecodedUrl;
	}

	/**
	 * @return the thumbnailDecodedUrl
	 */
	public String getThumbnailDecodedUrl() {
		return getReturnString(thumbnailDecodedUrl);
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	
	//TODO: document use of VideoType
	/**
	 * 
	 * @param videoType
	 */
	public void setVideoType(VideoType videoType) {
		videoType = (videoType == null) ? VideoType.UNKNOWN : videoType;
		this.setShortType(videoType.getShortType());
	}
	

	/**
	 * @param shortType the shortType to set
	 */
	public void setShortType(String shortType) {
		if (null != shortType) {
			VideoType type = VideoType.getVideoType(shortType);
			this.shortType = type.getShortType();
			this.type = type.getShortType();
		} else {
			this.shortType = null;
			this.type = null;
		}
	}

	/**
	 * @return the shortType
	 */
	public String getShortType() {
		return getReturnString(shortType);
	}
	
	/**
	 * Returns the {@link VideoType} associated with this instance's
	 * {@link #getShortType() shortType} (unless shortType is <code>null</code>,
	 * in which case this returns <code>null</code>).
	 * 
	 * @return
	 */
	public VideoType getVideoType() {
		if (this.shortType == null) {
			return null;
		}
		VideoType type = VideoType.getVideoType(this.shortType);
		return type;
	}
	
	/**
	 * @param contentTypeImage the contentTypeImage to set
	 */
	public void setContentTypeImage(String contentTypeImage) {
		this.contentTypeImage = contentTypeImage;
	}

	/**
	 * @return the contentTypeImage
	 */
	public String getContentTypeImage() {
		String imgName = (this.getVideoType() == null) ? null : this.getVideoType().getContentTypeImage();
		/*logger.debug("imgName--->"+imgName);*/
		if (imgName != null) {
			contentTypeImage = this.appContext.getEnvironmentConfiguration().getFilePath(imgName);			
		} else {
			contentTypeImage = "";
		}
		return contentTypeImage;
	}	

	/**
	 * @param cueCardWithJavascriptAndCss the cueCardWithJavascriptAndCss to set
	 */
	public void setCueCardWithJavascriptAndCss(String cueCardWithJavascriptAndCss) {
		this.cueCardWithJavascriptAndCss = cueCardWithJavascriptAndCss;
	}

	/**
	 * @return the cueCardWithJavascriptAndCss
	 */

	public String getCueCardWithJavascriptAndCss() {
		if ((null != videoId) && (null != thumbnailUrl)) {
			this.cueCardWithJavascriptAndCss = this.appContext.getCueCardConfiguration().renderFloatingCueCardIncludes();
			this.cueCardWithJavascriptAndCss += this.appContext.getCueCardConfiguration().renderPlaylistCueCard(this);
		} else {
			this.cueCardWithJavascriptAndCss = "";
		}
		return cueCardWithJavascriptAndCss;

	}

	/**
	 * @param cueCardWithoutJavascriptAndCss the cueCardWithoutJavascriptAndCss to set
	 */
	public void setCueCardWithoutJavascriptAndCss(String cueCardWithoutJavascriptAndCss) {
		this.cueCardWithoutJavascriptAndCss = cueCardWithoutJavascriptAndCss;
	}

	/**
	 * @return the cueCardWithoutJavascriptAndCss
	 */
	public String getCueCardWithoutJavascriptAndCss() {
		if ((null != videoId) && (null != thumbnailUrl)) {
			try {
				this.cueCardWithoutJavascriptAndCss = this.appContext.getCueCardConfiguration().renderPlaylistCueCard(this);
			} catch (Exception e) {
				logger.error("Could not render cueCardWithoutJavascriptAndCss.", e);
				this.cueCardWithoutJavascriptAndCss = "";
			}
		} else {
			this.cueCardWithoutJavascriptAndCss = "";
		}
		return cueCardWithoutJavascriptAndCss;
	}
	
	/**
	 * @param cueCardCourseContent the cueCardCourseContent to set
	 */
	public void setCueCardCourseContent(String cueCardCourseContent) {
		this.cueCardCourseContent = cueCardCourseContent;
	}

	/**
	 * @return the cueCardCourseContent
	 */
	public String getCueCardCourseContent() {
		if ((null != videoId) && (null != thumbnailUrl)) {
			this.cueCardCourseContent = this.appContext.getCueCardConfiguration().renderCourseContentCueCard(this);
		} else {
			this.cueCardCourseContent = "";
		}
		return cueCardCourseContent;
	}
	
	/**
	 * @param cueCardMashupContent the cueCardMashupContent to set
	 */
	public void setCueCardMashupContent(String cueCardMashupContent) {
		this.cueCardMashupContent = cueCardMashupContent;
	}
	
	/**
	 * @return the cueCardMashupContent
	 */
	public String getCueCardMashupContent() {
		if ((null != videoId) && (null != thumbnailUrl)) {
			this.cueCardMashupContent = this.appContext.getCueCardConfiguration().renderMashupCueCardThumbnail(this);
		} else {
			this.cueCardMashupContent = "";
		}
		return cueCardMashupContent;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return "Video [videoId=" + videoId + ", name=" + name + ", thumbnailUrl=" + thumbnailUrl + "]";
	}
	

	private String getReturnString(String stringType) {
		if (stringType == null) {
			return "";
		} else {
			return stringType;
		}
	}

	public String getIframeContent() {
		return iframeContent;
	}

	public void setIframeContent(String iframeContent) {
		this.iframeContent = iframeContent;
	}

	public String getIframeSource() {
		return iframeSource;
	}

	public void setIframeSource(String iframeSource) {
		this.iframeSource = iframeSource;
	}

	public String getAssetType() {
		return assetType;
	}

	public void setAssetType(String assetType) {
		this.assetType = assetType;
	}

	

}
