package blackboard.gs.nbc.playlist.data.video;

public enum VideoType {
	IMAGE("Image", "I", "images/photo.gif"),
	VIDEO("Video", "V", "images/video.gif"),
	TEXT("Text", "T", "images/text.gif"),
	DOCUMENT("Document", "D", "images/text.gif"),
	/**
	 * Used when type can not be determined.
	 */
	UNKNOWN("Unknown", "U", null)
	;
	
    private final String name;
	private final String shortType;
	private final String contentTypeImage;
	
	private VideoType(String name, String shortType, String contentTypeImage) {
		this.name = name;
		this.shortType = shortType;
		this.contentTypeImage = contentTypeImage;
	}

	/**
	 * Gets the long descriptive name for this instance.
	 */
	public final String getName() {
		return name;
	}

	/**
	 * Gets the short code for this instance.
	 * 
	 * @return the shortType
	 */
	public final String getShortType() {
		return shortType;
	}
	
	/**
	 * Returns the context-relative path to this image the represents this
	 * video type. May be <code>null</code>.
	 * 
	 * @return the contentTypeImage
	 */
	public final String getContentTypeImage() {
		return contentTypeImage;
	}

	/**
	 * {@inheritDoc}
	 */
	public String toString() {
		return this.getName();
	}

	/**
	 * Obtains the {@link VideoType} that whose ({@link #getName() name} or
	 * {@link #getShortType() shortType} equal the <code>str</code> value
	 * provided, without regard to case. Returns {@link #UNKNOWN} if none
	 * found.
	 * 
	 * @param str
	 * @return
	 */
	public static VideoType getVideoType(String str) {
		for (VideoType type : VideoType.values()) {
			if (type.name.equalsIgnoreCase(str)) {
				return type;
			}
			if (type.shortType.equalsIgnoreCase(str)) {
				return type;
			}
		}
		return UNKNOWN;
	}
}
