package blackboard.gs.nbc.playlist.persist.playlistitem;


import blackboard.gs.nbc.playlist.data.playlistitem.PlaylistItem;
import blackboard.gs.nbc.util.persist.Id;
import blackboard.persist.KeyNotFoundException;
import blackboard.persist.PersistenceException;

public interface PlaylistItemDbPersister {
	
	public void persist(PlaylistItem item) throws PersistenceException;

	public void deleteById(Id id) throws PersistenceException, KeyNotFoundException;

}
