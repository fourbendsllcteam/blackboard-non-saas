	package blackboard.gs.nbc.playlist.persist.playlistitem.impl;
	
	import java.sql.CallableStatement;
	import java.sql.Types;
	
	import org.apache.log4j.Logger;
	
	import blackboard.gs.nbc.context.NBCAppContext;
	import blackboard.gs.nbc.playlist.data.playlistitem.PlaylistItem;
	import blackboard.gs.nbc.playlist.persist.playlistitem.PlaylistItemDbPersister;
	import blackboard.gs.nbc.util.persist.Id;
	import blackboard.gs.nbc.util.persist.PersistUtil;
	import blackboard.gs.nbc.util.persist.PersistUtil.Update;
	import blackboard.gs.nbc.util.persist.PersistenceBase;
	import blackboard.gs.nbc.util.persist.PersistenceOperationException;
	import blackboard.persist.KeyNotFoundException;
	import blackboard.persist.PersistenceException;
	import java.sql.Date;
	import java.sql.Timestamp;
	import blackboard.db.BbDatabase;
	import blackboard.gs.nbc.install.SchemaServletContextListener;
	import blackboard.platform.db.JdbcServiceFactory;
	
	
	public class PlaylistItemDbPersisterImpl extends PersistenceBase implements PlaylistItemDbPersister {
		private static final Logger logger = Logger.getLogger(PlaylistItemDbPersisterImpl.class.getName());
	
		private static final String INSERT_PLAYLIST_ITEM = "{call bbgs_playlist_item_cr(?,?,?,?)}";
		private static final String POSTGRES_INSERT_PLAYLIST_ITEM = "{? = call bbgs_playlist_item_cr(?,?,?)}";
		private static final String UPDATE_PLAYLIST_ITEM = "UPDATE bbgs_playlist_item " + "SET " + " dtmodified=? WHERE pk1=?";
		private static final String DELETE_PLAYLIST_ITEM = "DELETE FROM bbgs_playlist_item WHERE pk1=?";
	
		private final NBCAppContext nbcAppContext;
		/**
		 * @param nbcAppContext
		 */
		public PlaylistItemDbPersisterImpl(NBCAppContext nbcAppContext) {
			this.nbcAppContext = nbcAppContext;
		}
		
	
		public void persist(PlaylistItem item) throws PersistenceException {
			if (item.getId() == null) {
				  BbDatabase db = JdbcServiceFactory.getInstance().getDefaultDatabase();
		            if(SchemaServletContextListener.isOracleDB(db))
		            {
		            	insert(item);
		            }
		            else
		            {
		            	insertPostGres(item);
		            }
		            
			} else {
				update(item);
			}
		}
	
	 private void insertPostGres(final PlaylistItem item) throws PersistenceException {
	        Update<PlaylistItem> update = new Update<PlaylistItem> () {
	            /**
	             * {@inheritDoc}
	             */
	            @Override
	            public String getSQL() {
	                return POSTGRES_INSERT_PLAYLIST_ITEM;
	            }
	 
	            /**
	             * {@inheritDoc}
	             */
	            @Override
	            public void set(CallableStatement statement) throws Exception {
	            	
	            	statement.setInt(2, getPk1(item.getUser().getId()));
	                statement.setInt(3, item.getVideo().getId().getKey());
	                //setCurrentCalendar(statement, 4);
	                java.sql.Timestamp ts = new Timestamp(new java.util.Date().getTime());
	              //  Date sqlDate = new java.sql.Date(new java.util.Date().getTime());
	                statement.setTimestamp(4, ts);
	                statement.registerOutParameter(1, Types.INTEGER);               
	            }
	 
	            /**
	             * {@inheritDoc}
	             */
	            @Override
	            public void onSuccess(int rowCount, CallableStatement statement) throws Exception {
	                int pk = statement.getInt(1);
	                Id id = new Id(PlaylistItem.class, pk);
	                item.setId(id);
	                /*if (logger.isDebugEnabled()) {
	                    logger.debug("Playlist item successfully inserted.");
	                }*/
	            }
	            
	        };
	        try {
	            PersistUtil.doUpdate(update, nbcAppContext);
	        } catch (PersistenceOperationException e) {
	            //TODO: change method sig.
	            logger.error("Error while Updating Playlist under Postgress DB");
	            throw new PersistenceException(e);
	        }
	    }
	
		private void insert(final PlaylistItem item) throws PersistenceException {
			Update<PlaylistItem> update = new Update<PlaylistItem> () {
				/**
				 * {@inheritDoc}
				 */
				@Override
				public String getSQL() {
					return INSERT_PLAYLIST_ITEM;
				}
	
				/**
				 * {@inheritDoc}
				 */
				@Override
				public void set(CallableStatement statement) throws Exception {
					statement.setInt(1, getPk1(item.getUser().getId()));
					statement.setInt(2, item.getVideo().getId().getKey());
					setCurrentCalendar(statement, 3);
					statement.registerOutParameter(4, Types.INTEGER);				
				}
	
				/**
				 * {@inheritDoc}
				 */
				@Override
				public void onSuccess(int rowCount, CallableStatement statement) throws Exception {
					int pk = statement.getInt(4);
					Id id = new Id(PlaylistItem.class, pk);
					item.setId(id);
					/*if (logger.isDebugEnabled()) {
						logger.debug("Playlist item successfully inserted.");
					}*/
				}
			};
			try {
				PersistUtil.doUpdate(update, nbcAppContext);
			} catch (PersistenceOperationException e) {
				//TODO: change method sig.
				throw new PersistenceException(e);
			}
		}
	
		private void update(final PlaylistItem item) throws PersistenceException {
			Update<PlaylistItem> update = new Update<PlaylistItem> () {
				/**
				 * {@inheritDoc}
				 */
				@Override
				public String getSQL() {
					return UPDATE_PLAYLIST_ITEM;
				}
	
				/**
				 * {@inheritDoc}
				 */
				@Override
				public void set(CallableStatement statement) throws Exception {
					setCurrentCalendar(statement, 1);
					statement.setInt(2, item.getId().getKey());
				}
	
				/**
				 * {@inheritDoc}
				 */
				@Override
				public void onSuccess(int rowCount, CallableStatement statement) throws Exception {
					/*if (logger.isDebugEnabled()) {
						logger.debug("Playlist item successfully updated.");
					}*/
				}
			};
			try {
				PersistUtil.doUpdate(update, nbcAppContext);
			} catch (PersistenceOperationException e) {
				//TODO: change method sig.
				throw new PersistenceException(e);
			}
		}
	
		public void deleteById(final Id id) throws PersistenceException, KeyNotFoundException {
			Update<PlaylistItem> update = new Update<PlaylistItem> () {
				/**
				 * {@inheritDoc}
				 */
				@Override
				public String getSQL() {
					return DELETE_PLAYLIST_ITEM;
				}
	
				/**
				 * {@inheritDoc}
				 */
				@Override
				public void set(CallableStatement statement) throws Exception {
					statement.setInt(1, id.getKey());
				}
	
				/**
				 * {@inheritDoc}
				 */
				@Override
				public void onSuccess(int rowCount, CallableStatement statement) throws Exception {
					if (rowCount == 0) {
						throw new KeyNotFoundException("Playlist item key cannot be found.");
					}
					/*if (logger.isDebugEnabled()) {
						logger.debug("Playlist item successfully deleted.");
					}*/
				}
			};
			try {
				PersistUtil.doUpdate(update, nbcAppContext);
			} catch (PersistenceOperationException e) {
				//TODO: change method sig.
				throw new PersistenceException(e);
			}
	
		}
	
	}
