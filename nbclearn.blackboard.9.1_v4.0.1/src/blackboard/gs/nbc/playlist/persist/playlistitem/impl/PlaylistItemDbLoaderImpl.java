package blackboard.gs.nbc.playlist.persist.playlistitem.impl;

import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import blackboard.data.content.Content;
import blackboard.data.user.User;
import blackboard.gs.nbc.context.NBCAppContext;
import blackboard.gs.nbc.playlist.data.playlistitem.PlaylistItem;
import blackboard.gs.nbc.playlist.data.playlistitem.UserPlaylist;
import blackboard.gs.nbc.playlist.data.video.Video;
import blackboard.gs.nbc.playlist.persist.playlistitem.PlaylistItemDbLoader;
import blackboard.gs.nbc.playlist.persist.video.VideoDbLoader;
import blackboard.gs.nbc.util.persist.Id;
import blackboard.gs.nbc.util.persist.PersistUtil;
import blackboard.gs.nbc.util.persist.PersistUtil.Query;
import blackboard.gs.nbc.util.persist.PersistUtil.Update;
import blackboard.gs.nbc.util.persist.PersistenceBase;
import blackboard.gs.nbc.util.persist.PersistenceOperationException;
import blackboard.persist.KeyNotFoundException;
import blackboard.persist.PersistenceException;
import blackboard.persist.user.UserDbLoader;
import blackboard.platform.persistence.PersistenceServiceFactory;

public class PlaylistItemDbLoaderImpl extends PersistenceBase implements PlaylistItemDbLoader {
	private static final Logger logger = Logger.getLogger(PlaylistItemDbLoaderImpl.class);

	private static final String SELECT_PLAYLIST_BY_USER = "SELECT pk1, users_pk1, video_pk1, dtmodified from bbgs_playlist_item WHERE users_pk1=? ORDER BY dtmodified DESC";
	private static final String SELECT_PLAYLIST_BY_VIDEO = "SELECT count(pk1) from bbgs_playlist_item WHERE v,ideo_pk1=? ";
	private static final String SELECT_PLAYLIST_BY_USER_AND_BY_VIDEO = "SELECT pk1, users_pk1, video_pk1, dtmodified from bbgs_playlist_item WHERE users_pk1=? and video_pk1=? ";
	private static final String SELECT_PLAYLIST_BY_ID = "SELECT pk1, users_pk1, video_pk1, dtmodified from bbgs_playlist_item WHERE pk1=? ";
	private static final String SELECT_ALL_PLAYLIST = "SELECT users.FIRSTNAME,users.LASTNAME,users.user_id,users.email,bbgs_video.id FROM users INNER JOIN bbgs_playlist_item ON users.pk1=bbgs_playlist_item.users_pk1 INNER JOIN bbgs_video ON bbgs_playlist_item.video_pk1=bbgs_video.pk1 order by users.user_id desc";
	private static final String UPDATE_USER_MAIL = "update users set email=? where user_id=?";
	private static final String UPDATE_EXPORT_STATUS = "update users set settings=? where user_id=?";
	private static final String IS_ALREADY_EXPORTED = "SELECT settings from users where settings like 'Y'";
	private static final String SELECT_OLD_CONTENT = "select cc.pk1, cc.MAIN_DATA from COURSE_CONTENTS cc, COURSE_MAIN cm where cm.course_id=? and cm.pk1=cc.crsmain_pk1 and cc.main_data like '%iframe%' and cc.MAIN_DATA like '%archivesbb.nbclearn.com/portal/site/root/widget%' and cc.MAIN_DATA not like '%nbc-click%'";
	private static final String SELECT_USER_INFO = "SELECT FIRSTNAME,LASTNAME from users where email=?";
	private static final String UPDATE_NEW_CONTENT = "update COURSE_CONTENTS set MAIN_DATA=? where PK1=?";
	private static final String UPDATE_NEW_FORUM_CONTENT = "update FORUM_MAIN set DESCRIPTION=? where PK1=?";
	private static final String UPDATE_NEW_THREAD_CONTENT = "update MSG_MAIN set MSG_TEXT=? where PK1=?";
	private static final String SELECT_OLD_FORUM_CONTENT = "select fm.pk1, fm.description from COURSE_MAIN cm, conference_main cfm, FORUM_MAIN fm where cm.course_id=? and cm.pk1=cfm.crsmain_pk1 and cfm.pk1=fm.confmain_pk1 and fm.description like '%iframe%' and fm.description like '%archivesbb.nbclearn.com/portal/site/root/widget%' and fm.description not like '%nbc-click%'";
	private static final String SELECT_OLD_THREAD_CONTENT = "select mm.pk1, mm.MSG_TEXT from COURSE_MAIN cm, conference_main cfm, FORUM_MAIN fm, msg_main mm where cm.course_id=? and cm.pk1=cfm.crsmain_pk1 and cfm.pk1=fm.confmain_pk1 and fm.pk1=mm.FORUMMAIN_PK1 and mm.MSG_TEXT like '%iframe%' and mm.MSG_TEXT like '%archivesbb.nbclearn.com/portal/site/root/widget%' and mm.MSG_TEXT not like '%nbc-click%'";
	private static final String SELECT_OLD_ANNOUNCE_CONTENT = "select am.pk1, am.ANNOUNCEMENT from ANNOUNCEMENTS am, COURSE_MAIN cm where cm.course_id=? and cm.pk1=am.crsmain_pk1 and am.ANNOUNCEMENT like '%iframe%' and am.ANNOUNCEMENT like '%archivesbb.nbclearn.com/portal/site/root/widget%' and am.ANNOUNCEMENT not like '%nbc-click%'";
	private static final String SELECT_OLD_TASK_CONTENT = "select tt.pk1, tt.DESCRIPTION from TASKS tt, COURSE_MAIN cm where cm.course_id=? and cm.pk1=tt.crsmain_pk1 and tt.DESCRIPTION like '%iframe%' and tt.DESCRIPTION like '%archivesbb.nbclearn.com/portal/site/root/widget%' and tt.DESCRIPTION not like '%nbc-click%'";
	private static final String UPDATE_NEW_ANNOUNCE_CONTENT = "update ANNOUNCEMENTS set ANNOUNCEMENT=? where PK1=?";
	private static final String UPDATE_NEW_TASK_CONTENT = "update TASKS set DESCRIPTION=? where PK1=?";

	protected static final String SELECT_SECURITY_TOKEN = "select description from system_registry where registry_key=?";
	private static final String UPDATE_SECURITY_TOKEN = "update system_registry set description=? where registry_key=?";
	private static final String SELECT_VIDEO_BY_VIDEOID = "SELECT pk1, id, name, description, duration, air_date, url, type from bbgs_video WHERE id=? ";

	protected static final String SELECT_LEGACY_FORUM_CONTENT = "select fm.pk1, fm.description from COURSE_MAIN cm, conference_main cfm, FORUM_MAIN fm where cm.course_id=? and cm.course_id=cfm.name and cfm.pk1=fm.confmain_pk1 and fm.description like '%iframe%' and fm.description like '%archivesbb.nbclearn.com/portal/site/root/widget%' and fm.description not like '%nbc-click%'";

	protected static final String SELECT_LEGACY_THREAD_CONTENT = "select mm.pk1, mm.MSG_TEXT from COURSE_MAIN cm, conference_main cfm, FORUM_MAIN fm, msg_main mm where cm.course_id=? and cm.course_id=cfm.name and cfm.pk1=fm.confmain_pk1 and fm.pk1=mm.FORUMMAIN_PK1 and mm.MSG_TEXT like '%iframe%' and mm.MSG_TEXT like '%archivesbb.nbclearn.com/portal/site/root/widget%' and mm.MSG_TEXT not like '%nbc-click%'";
	
	protected static final String SELECT_BB_VERSION="SELECT  VERSION_MAJOR||'.'||VERSION_MINOR||'.'||VERSION_PATCH FROM PLUGINS where handle=? and vendor_id =? ";
	
	private final NBCAppContext nbcAppContext;

	/**
	 * @param nbcAppContext
	 */
	public PlaylistItemDbLoaderImpl(NBCAppContext nbcAppContext) {
		this.nbcAppContext = nbcAppContext;
	}

	public List<PlaylistItem> loadByUserId(final blackboard.persist.Id userId)
			throws KeyNotFoundException, PersistenceException {
		Query<PlaylistItem> query = new Query<PlaylistItem>() {
			/**
			 * {@inheritDoc}
			 */
			@Override
			public String getSQL() {
				return SELECT_PLAYLIST_BY_USER;
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public void set(PreparedStatement statement) throws Exception {
				statement.setInt(1, getPk1(userId));
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public PlaylistItem extract(ResultSet resultSet) throws Exception {
				PlaylistItem item = unmarshallPlaylistItem(resultSet);
				return item;
			}
		};

		try {
			List<PlaylistItem> list = PersistUtil.get(query, nbcAppContext);
			return list;
		} catch (Exception e) {
			// TODO: change method sig
			throw new PersistenceException(e);
		}
	}

	public int loadByVideoId(final Id videoId) throws KeyNotFoundException, PersistenceException {
		Query<Integer> query = new Query<Integer>() {
			/**
			 * {@inheritDoc}
			 */
			@Override
			public String getSQL() {
				return SELECT_PLAYLIST_BY_VIDEO;
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public void set(PreparedStatement statement) throws Exception {
				statement.setInt(1, videoId.getKey());
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public Integer extract(ResultSet resultSet) throws Exception {
				int numberOfRows = resultSet.getInt(1);
				return numberOfRows;
			}
		};

		try {
			Integer numberOfRows = PersistUtil.getUnique(query, nbcAppContext);
			return (numberOfRows == null) ? 0 : numberOfRows;
		} catch (PersistenceOperationException e) {
			// TODO: change method sig
			throw new PersistenceException(e);
		}

	}

	public PlaylistItem loadByUserIdAndVideoId(final blackboard.persist.Id userId, final Id videoId)
			throws KeyNotFoundException, PersistenceException {
		Query<PlaylistItem> query = new Query<PlaylistItem>() {
			/**
			 * {@inheritDoc}
			 */
			@Override
			public String getSQL() {
				return SELECT_PLAYLIST_BY_USER_AND_BY_VIDEO;
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public void set(PreparedStatement statement) throws Exception {
				statement.setInt(1, getPk1(userId));
				statement.setInt(2, videoId.getKey());
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public PlaylistItem extract(ResultSet resultSet) throws Exception {
				PlaylistItem item = unmarshallPlaylistItem(resultSet);
				return item;
			}
		};

		try {
			PlaylistItem playlistItem = PersistUtil.getUnique(query, nbcAppContext);
			return playlistItem;
		} catch (PersistenceOperationException e) {
			// TODO: change method sig
			throw new PersistenceException(e);
		}
	}

	public PlaylistItem loadById(final Id id) throws KeyNotFoundException, PersistenceException {
		Query<PlaylistItem> query = new Query<PlaylistItem>() {
			/**
			 * {@inheritDoc}
			 */
			@Override
			public String getSQL() {
				return SELECT_PLAYLIST_BY_ID;
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public void set(PreparedStatement statement) throws Exception {
				statement.setInt(1, id.getKey());
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public PlaylistItem extract(ResultSet resultSet) throws Exception {
				PlaylistItem item = unmarshallPlaylistItem(resultSet);
				return item;
			}
		};

		try {
			PlaylistItem playlistItem = PersistUtil.getUnique(query, nbcAppContext);
			return playlistItem;
		} catch (PersistenceOperationException e) {
			// TODO: change method sig
			throw new PersistenceException(e);
		}

	}

	private PlaylistItem unmarshallPlaylistItem(ResultSet results) throws SQLException, PersistenceException {
		Id id = getId(results, PlaylistItem.class, "pk1");
		int users_pk1 = results.getInt("users_pk1");
		int video_pk1 = results.getInt("video_pk1");
		Calendar dtModified = getCalendar(results, "dtmodified");

		PlaylistItem playlistItem = new PlaylistItem();
		playlistItem.setId(id);

		UserDbLoader userLoader = (UserDbLoader) PersistenceServiceFactory.getInstance().getDbPersistenceManager()
				.getLoader(UserDbLoader.TYPE);
		User user = userLoader.loadById(generateId(User.DATA_TYPE, users_pk1));
		playlistItem.setUser(user);
		VideoDbLoader videoLoader = this.nbcAppContext.getVideoDbLoader();
		Video video = videoLoader.loadById(new Id(Video.class, video_pk1));
		playlistItem.setVideo(video);
		playlistItem.setDtModified(dtModified);

		return playlistItem;
	}

	@Override
	public List<UserPlaylist> retriveUserAndPlaylistInfo() throws PersistenceException {
		Query<UserPlaylist> query = new Query<UserPlaylist>() {
			/**
			 * {@inheritDoc}
			 */
			@Override
			public String getSQL() {
				return SELECT_ALL_PLAYLIST;
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public void set(PreparedStatement statement) throws Exception {

			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public UserPlaylist extract(ResultSet resultSet) throws Exception {
				UserPlaylist item = unmarshallUserPlaylistDetails(resultSet);
				return item;
			}
		};
		List<UserPlaylist> list = null;
		try {
			list = PersistUtil.get(query, nbcAppContext);

		} catch (Exception e) {
			// TODO: change method sig
			// throw new PersistenceException(e);
			logger.error(e);
			throw new PersistenceException(e);
		}
		return list;
	}

	private UserPlaylist unmarshallUserPlaylistDetails(ResultSet results) throws SQLException, PersistenceException {
		UserPlaylist userPlaylist = new UserPlaylist();
		userPlaylist.setFirstName(results.getString("FIRSTNAME"));
		userPlaylist.setLastName(results.getString("LASTNAME"));
		userPlaylist.setUserId(results.getString("user_id"));
		userPlaylist.setEmailId(results.getString("email"));
		userPlaylist.setVcmId(results.getString("id"));
		return userPlaylist;
	}

	@Override
	public void updateUserMailId(final String userId, final String newEmailId) throws PersistenceException {
		Update update = new Update() {
			/**
			 * {@inheritDoc}
			 */
			@Override
			public String getSQL() {
				return UPDATE_USER_MAIL;
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public void set(CallableStatement statement) throws Exception {
				statement.setString(1, newEmailId);
				statement.setString(2, userId);
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public void onSuccess(int rowCount, CallableStatement statement) throws Exception {
				/*if (logger.isDebugEnabled()) {
					logger.debug("User Mail Created Successfully.");
				}*/
			}
		};
		try {
			PersistUtil.doUpdate(update, nbcAppContext);
		} catch (PersistenceOperationException e) {
			// TODO: change method sig.
			e.printStackTrace();
			throw new PersistenceException(e);
		}

	}

	@Override
	public void updatePlaylistExportStatus(final String userId) throws PersistenceException {
		Update update = new Update() {
			/**
			 * {@inheritDoc}
			 */
			@Override
			public String getSQL() {
				return UPDATE_EXPORT_STATUS;
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public void set(CallableStatement statement) throws Exception {
				statement.setString(1, "Y");
				statement.setString(2, userId);
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public void onSuccess(int rowCount, CallableStatement statement) throws Exception {
				/*if (logger.isDebugEnabled()) {
					logger.debug("Update of playlist status successful");
				}*/
			}
		};
		try {
			PersistUtil.doUpdate(update, nbcAppContext);
		} catch (PersistenceOperationException e) {
			// TODO: change method sig.
			e.printStackTrace();
			throw new PersistenceException(e);
		}

	}

	@Override
	public List<Integer> checkAlreadyImported() throws PersistenceException {
		Query<Integer> query = new Query<Integer>() {
			/**
			 * {@inheritDoc}
			 */
			@Override
			public String getSQL() {
				return IS_ALREADY_EXPORTED;
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public void set(PreparedStatement statement) throws Exception {

			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public Integer extract(ResultSet resultSet) throws Exception {
				return 1;
			}
		};

		try {
			List<Integer> list = PersistUtil.get(query, nbcAppContext);
			return list;
		} catch (Exception e) {
			// TODO: change method sig
			e.printStackTrace();
			throw new PersistenceException(e);
		}

	}

	@Override
	public List<HashMap<String, Object>> getOldPlayerContent(final String courseId) throws PersistenceException {
		Query<HashMap<String, Object>> query = new Query<HashMap<String, Object>>() {
			/**
			 * {@inheritDoc}
			 */
			@Override
			public String getSQL() {
				return SELECT_OLD_CONTENT;
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public void set(PreparedStatement statement) throws Exception {
				statement.setString(1, courseId);
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public HashMap<String, Object> extract(ResultSet resultSet) throws Exception {
				HashMap<String, Object> item = unmarshallContentDetails(resultSet);
				return item;
			}

		};
		List<HashMap<String, Object>> list = null;
		try {
			list = PersistUtil.get(query, nbcAppContext);

		} catch (Exception e) {
			// TODO: change method sig
			// throw new PersistenceException(e);
			logger.error(e);
			throw new PersistenceException(e);
		}
		return list;
	}

	private HashMap<String, Object> unmarshallContentDetails(ResultSet resultSet) {
		HashMap<String, Object> contentMap = new HashMap<String, Object>();
		try {
			contentMap.put("pk1", resultSet.getInt("PK1"));
			contentMap.put("mainData", resultSet.getString("MAIN_DATA"));
			/*logger.debug("pk1-->" + resultSet.getInt("PK1"));*/
			/*logger.debug("mainData-->" + resultSet.getString("MAIN_DATA"));*/
		} catch (Exception e) {
			e.printStackTrace();
		}
		return contentMap;
	}

	@Override
	public List<HashMap<String, String>> getUserInfo(final String emailId) throws PersistenceException {
		Query<HashMap<String, String>> query = new Query<HashMap<String, String>>() {
			/**
			 * {@inheritDoc}
			 */
			@Override
			public String getSQL() {
				return SELECT_USER_INFO;
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public void set(PreparedStatement statement) throws Exception {
				statement.setString(1, emailId);
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public HashMap<String, String> extract(ResultSet resultSet) throws Exception {
				HashMap<String, String> item = unmarshallUserDetails(resultSet);
				return item;
			}

		};
		List<HashMap<String, String>> list = null;
		try {
			list = PersistUtil.get(query, nbcAppContext);

		} catch (Exception e) {
			// TODO: change method sig
			// throw new PersistenceException(e);
			logger.error(e);
			throw new PersistenceException(e);
		}
		return list;
	}

	private HashMap<String, String> unmarshallUserDetails(ResultSet resultSet) {
		HashMap<String, String> userMap = new HashMap<String, String>();
		try {
			userMap.put("firstName", resultSet.getString("FIRSTNAME"));
			userMap.put("lastName", resultSet.getString("LASTNAME"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return userMap;
	}

	@Override
	public void updateNewContent(final Integer pk1, final String finalTemplate) throws PersistenceException {
		Update update = new Update() {
			/**
			 * {@inheritDoc}
			 */
			@Override
			public String getSQL() {
				return UPDATE_NEW_CONTENT;
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public void set(CallableStatement statement) throws Exception {
				statement.setString(1, finalTemplate);
				statement.setInt(2, pk1);
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public void onSuccess(int rowCount, CallableStatement statement) throws Exception {
				/*if (logger.isDebugEnabled()) {
					logger.debug("NEW CONTENT UPDATED SUCCESSFULLY--->" + pk1);
				}*/
			}
		};
		try {
			PersistUtil.doUpdate(update, nbcAppContext);
		} catch (PersistenceOperationException e) {
			// TODO: change method sig.
			e.printStackTrace();
			throw new PersistenceException(e);
		}

	}

	@Override
	public List<HashMap<String, Object>> getOldPlayerForumContent(final String courseId) throws PersistenceException {
		Query<HashMap<String, Object>> query = new Query<HashMap<String, Object>>() {
			/**
			 * {@inheritDoc}
			 */
			@Override
			public String getSQL() {
				return SELECT_OLD_FORUM_CONTENT;
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public void set(PreparedStatement statement) throws Exception {
				statement.setString(1, courseId);
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public HashMap<String, Object> extract(ResultSet resultSet) throws Exception {
				HashMap<String, Object> item = unmarshallForumDetails(resultSet);
				return item;
			}

		};
		List<HashMap<String, Object>> list = null;
		try {
			list = PersistUtil.get(query, nbcAppContext);

		} catch (Exception e) {
			// TODO: change method sig
			// throw new PersistenceException(e);
			logger.error(e);
			throw new PersistenceException(e);
		}
		return list;
	}

	@Override
	public List<HashMap<String, Object>> getOldPlayerThreadContent(final String courseId) throws PersistenceException {
		Query<HashMap<String, Object>> query = new Query<HashMap<String, Object>>() {
			/**
			 * {@inheritDoc}
			 */
			@Override
			public String getSQL() {
				return SELECT_OLD_THREAD_CONTENT;
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public void set(PreparedStatement statement) throws Exception {
				statement.setString(1, courseId);
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public HashMap<String, Object> extract(ResultSet resultSet) throws Exception {
				HashMap<String, Object> item = unmarshallThreadDetails(resultSet);
				return item;
			}

		};
		List<HashMap<String, Object>> list = null;
		try {
			list = PersistUtil.get(query, nbcAppContext);

		} catch (Exception e) {
			// TODO: change method sig
			// throw new PersistenceException(e);
			logger.error(e);
			throw new PersistenceException(e);
		}
		return list;
	}

	private HashMap<String, Object> unmarshallForumDetails(ResultSet resultSet) {
		HashMap<String, Object> userMap = new HashMap<String, Object>();
		try {
			userMap.put("pk1", resultSet.getInt("PK1"));
			userMap.put("mainData", resultSet.getString("DESCRIPTION"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return userMap;
	}

	private HashMap<String, Object> unmarshallThreadDetails(ResultSet resultSet) {
		HashMap<String, Object> userMap = new HashMap<String, Object>();
		try {
			userMap.put("pk1", resultSet.getInt("PK1"));
			userMap.put("mainData", resultSet.getString("MSG_TEXT"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return userMap;
	}

	@Override
	public void updateNewForumContent(final Integer pk1, final String finalTemplate) throws PersistenceException {
		Update update = new Update() {
			/**
			 * {@inheritDoc}
			 */
			@Override
			public String getSQL() {
				return UPDATE_NEW_FORUM_CONTENT;
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public void set(CallableStatement statement) throws Exception {
				statement.setString(1, finalTemplate);
				statement.setInt(2, pk1);
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public void onSuccess(int rowCount, CallableStatement statement) throws Exception {
				/*if (logger.isDebugEnabled()) {
					logger.debug("NEW CONTENT UPDATED SUCCESSFULLY--->" + pk1);
				}*/
			}
		};
		try {
			PersistUtil.doUpdate(update, nbcAppContext);
		} catch (PersistenceOperationException e) {
			// TODO: change method sig.
			e.printStackTrace();
			throw new PersistenceException(e);
		}

	}

	@Override
	public void updateNewThreadContent(final Integer pk1, final String finalTemplate) throws PersistenceException {
		Update update = new Update() {
			/**
			 * {@inheritDoc}
			 */
			@Override
			public String getSQL() {
				return UPDATE_NEW_THREAD_CONTENT;
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public void set(CallableStatement statement) throws Exception {
				statement.setString(1, finalTemplate);
				statement.setInt(2, pk1);
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public void onSuccess(int rowCount, CallableStatement statement) throws Exception {
				/*if (logger.isDebugEnabled()) {
					logger.debug("NEW CONTENT UPDATED SUCCESSFULLY--->" + pk1);
				}*/
			}
		};
		try {
			PersistUtil.doUpdate(update, nbcAppContext);
		} catch (PersistenceOperationException e) {
			// TODO: change method sig.
			e.printStackTrace();
			throw new PersistenceException(e);
		}

	}

	@Override
	public List<HashMap<String, Object>> getOldPlayerAnnouncementContent(final String courseId) throws PersistenceException {
		Query<HashMap<String, Object>> query = new Query<HashMap<String, Object>>() {
			/**
			 * {@inheritDoc}
			 */
			@Override
			public String getSQL() {
				return SELECT_OLD_ANNOUNCE_CONTENT;
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public void set(PreparedStatement statement) throws Exception {
				statement.setString(1, courseId);
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public HashMap<String, Object> extract(ResultSet resultSet) throws Exception {
				HashMap<String, Object> item = unmarshallAnnounceDetails(resultSet);
				return item;
			}

		};
		List<HashMap<String, Object>> list = null;
		try {
			list = PersistUtil.get(query, nbcAppContext);

		} catch (Exception e) {
			// TODO: change method sig
			// throw new PersistenceException(e);
			logger.error(e);
			throw new PersistenceException(e);
		}
		return list;
	}
	
	private HashMap<String, Object> unmarshallAnnounceDetails(ResultSet resultSet) {
		HashMap<String, Object> userMap = new HashMap<String, Object>();
		try {
			userMap.put("pk1", resultSet.getInt("PK1"));
			userMap.put("mainData", resultSet.getString("ANNOUNCEMENT"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return userMap;
	}

	@Override
	public List<HashMap<String, Object>> getOldPlayerTaskContent(final String courseId) throws PersistenceException {
		Query<HashMap<String, Object>> query = new Query<HashMap<String, Object>>() {
			/**
			 * {@inheritDoc}
			 */
			@Override
			public String getSQL() {
				return SELECT_OLD_TASK_CONTENT;
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public void set(PreparedStatement statement) throws Exception {
				statement.setString(1, courseId);
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public HashMap<String, Object> extract(ResultSet resultSet) throws Exception {
				HashMap<String, Object> item = unmarshallForumDetails(resultSet);
				return item;
			}

		};
		List<HashMap<String, Object>> list = null;
		try {
			list = PersistUtil.get(query, nbcAppContext);

		} catch (Exception e) {
			// TODO: change method sig
			// throw new PersistenceException(e);
			logger.error(e);
			throw new PersistenceException(e);
		}
		return list;
	}

	@Override
	public void updateNewAnnouncementContent(final Integer pk1, final String finalTemplate) throws PersistenceException {
		Update update = new Update() {
			/**
			 * {@inheritDoc}
			 */
			@Override
			public String getSQL() {
				return UPDATE_NEW_ANNOUNCE_CONTENT;
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public void set(CallableStatement statement) throws Exception {
				statement.setString(1, finalTemplate);
				statement.setInt(2, pk1);
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public void onSuccess(int rowCount, CallableStatement statement) throws Exception {
				/*if (logger.isDebugEnabled()) {
					logger.debug("NEW CONTENT UPDATED SUCCESSFULLY--->" + pk1);
				}*/
			}
		};
		try {
			PersistUtil.doUpdate(update, nbcAppContext);
		} catch (PersistenceOperationException e) {
			// TODO: change method sig.
			e.printStackTrace();
			throw new PersistenceException(e);
		}

	}

	@Override
	public void updateNewTaskContent(final Integer pk1, final String finalTemplate) throws PersistenceException {
		Update update = new Update() {
			/**
			 * {@inheritDoc}
			 */
			@Override
			public String getSQL() {
				return UPDATE_NEW_TASK_CONTENT;
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public void set(CallableStatement statement) throws Exception {
				statement.setString(1, finalTemplate);
				statement.setInt(2, pk1);
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public void onSuccess(int rowCount, CallableStatement statement) throws Exception {
				/*if (logger.isDebugEnabled()) {
					logger.debug("NEW CONTENT UPDATED SUCCESSFULLY--->" + pk1);
				}*/
			}
		};
		try {
			PersistUtil.doUpdate(update, nbcAppContext);
		} catch (PersistenceOperationException e) {
			// TODO: change method sig.
			e.printStackTrace();
			throw new PersistenceException(e);
		}

		
	}


	@Override
	public void updateIframeSecurityToken(final String securityToken) throws PersistenceException {
		Update update = new Update() {
			/**
			 * {@inheritDoc}
			 */
			@Override
			public String getSQL() {
				return UPDATE_SECURITY_TOKEN;
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public void set(CallableStatement statement) throws Exception {
				statement.setString(1, securityToken);
				statement.setString(2, "gs-nbc-token");
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public void onSuccess(int rowCount, CallableStatement statement) throws Exception {
				/*if (logger.isDebugEnabled()) {
					logger.debug("Security Token updated successfully");
				}*/
			}
		};
		try {
			PersistUtil.doUpdate(update, nbcAppContext);
		} catch (PersistenceOperationException e) {
			// TODO: change method sig.
			e.printStackTrace();
			throw new PersistenceException(e);
		}
		
	}

	@Override
	public String getSecurityToken() throws PersistenceException {
		String iframeUrl=null;
		Query<String> query = new Query<String>() {
			/**
			 * {@inheritDoc}
			 */
			@Override
			public String getSQL() {
				return SELECT_SECURITY_TOKEN;
			}

			/**
			 * 
			 * {@inheritDoc}
			 */
			@Override
			public void set(PreparedStatement statement) throws Exception {
				statement.setString(1, "gs-nbc-token");
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public String extract(ResultSet resultSet) throws Exception {
				String metaData = resultSet.getString(1);
				return metaData;
			}
		};

		try {
			iframeUrl = PersistUtil.getUnique(query, nbcAppContext);
		} catch (PersistenceOperationException e) {
			// TODO: change method sig
			throw new PersistenceException(e);
		}
		/*logger.debug("Security Token fetched successfully-->"+iframeUrl);*/
		return iframeUrl;
	}

	@Override
	public Video loadVideoById(final String videoId)
			throws Exception {
		Query<Video> query = new Query<Video>() {
			@Override
			public String getSQL() {
				return SELECT_VIDEO_BY_VIDEOID;
			}

			@Override
			public void set(PreparedStatement statement) throws Exception {
				statement.setString(1, videoId);
			}

			@Override
			public Video extract(ResultSet resultSet) throws Exception {
				Video video = unmarshallVideoData(resultSet);
				return video;
			}			
		};
		
		try {
			Video video = PersistUtil.getUnique(query, this.nbcAppContext);
			if (video == null) {
				/*logger.warn("ATTENTION: no Video found for id: '" + videoId + "'");*/
			}
			return video;
		} catch (PersistenceOperationException e) {
			///TODO: change signature on this class to throw PersistenceOperationException
			throw new PersistenceException(e);
		}
	}
	
	private Video unmarshallVideoData(ResultSet results) throws SQLException {
		Id id = getId(results, Video.class, "pk1"); 
		String videoId = results.getString("id");
		String name = results.getString("name");
		String description = results.getString("description");
		// replace apostrophe
		description = description.replaceAll("\\&apos\\;", "'");
		String duration = results.getString("duration");
		Calendar airDate = getCalendar(results, "air_date");
		String thumbnailUrl = results.getString("url");
		String type = results.getString("type");

		Video video = new Video(this.nbcAppContext);
		video.setId(id);
		video.setVideoId(videoId);
		video.setName(name);
		video.setDescription(description);
		video.setDuration(duration);
		video.setAirDate(airDate);
		video.setThumbnailUrl(thumbnailUrl);
		video.setType(type);
		video.setShortType(type);
		return video;
	}

	@Override
	public List<HashMap<String, Object>> getLegacyPlayerForumContent(final String courseId) throws PersistenceException {
		Query<HashMap<String, Object>> query = new Query<HashMap<String, Object>>() {
			/**
			 * {@inheritDoc}
			 */
			@Override
			public String getSQL() {
				return SELECT_LEGACY_FORUM_CONTENT;
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public void set(PreparedStatement statement) throws Exception {
				statement.setString(1, courseId);
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public HashMap<String, Object> extract(ResultSet resultSet) throws Exception {
				HashMap<String, Object> item = unmarshallForumDetails(resultSet);
				return item;
			}

		};
		List<HashMap<String, Object>> list = null;
		try {
			list = PersistUtil.get(query, nbcAppContext);

		} catch (Exception e) {
			// TODO: change method sig
			// throw new PersistenceException(e);
			logger.error(e);
			throw new PersistenceException(e);
		}
		return list;
	}

	@Override
	public List<HashMap<String, Object>> getLegacyPlayerThreadContent(final String courseId) throws PersistenceException {
		Query<HashMap<String, Object>> query = new Query<HashMap<String, Object>>() {
			/**
			 * {@inheritDoc}
			 */
			@Override
			public String getSQL() {
				return SELECT_LEGACY_THREAD_CONTENT;
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public void set(PreparedStatement statement) throws Exception {
				statement.setString(1, courseId);
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public HashMap<String, Object> extract(ResultSet resultSet) throws Exception {
				HashMap<String, Object> item = unmarshallThreadDetails(resultSet);
				return item;
			}

		};
		List<HashMap<String, Object>> list = null;
		try {
			list = PersistUtil.get(query, nbcAppContext);

		} catch (Exception e) {
			// TODO: change method sig
			// throw new PersistenceException(e);
			logger.error(e);
			throw new PersistenceException(e);
		}
		return list;
	}
	
	/*
	 * To get the bb latest version
	 */
	@Override
	public String getNBCBBVersion() throws PersistenceException {
		
		String bbNbcVersion=null;
		Query<String> query = new Query<String>() {
			/**
			 * {@inheritDoc}
			 */
			@Override
			public String getSQL() {
				
				return SELECT_BB_VERSION;
			}

			/**
			 * 
			 * {@inheritDoc}
			 */
			@Override
			public void set(PreparedStatement statement) throws Exception {
				statement.setString(1, "nbc-content-integration");
				statement.setString(2, "bbgs");
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public String extract(ResultSet resultSet) throws Exception {
				String metaData = resultSet.getString(1);
				return metaData;
			}
		};

		try {
			 bbNbcVersion = PersistUtil.getUnique(query, nbcAppContext);
			
		} catch (PersistenceOperationException e) {
			// TODO: change method sig
			throw new PersistenceException(e);
		}
		return bbNbcVersion;

	}

}
