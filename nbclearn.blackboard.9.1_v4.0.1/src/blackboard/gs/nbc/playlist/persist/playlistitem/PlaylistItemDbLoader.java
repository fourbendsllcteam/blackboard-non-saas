package blackboard.gs.nbc.playlist.persist.playlistitem;

import java.util.HashMap;
import java.util.List;

import blackboard.data.content.Content;
import blackboard.data.user.User;
import blackboard.gs.nbc.playlist.data.playlistitem.PlaylistItem;
import blackboard.gs.nbc.playlist.data.playlistitem.UserPlaylist;
import blackboard.gs.nbc.playlist.data.video.Video;
import blackboard.gs.nbc.util.persist.Id;
import blackboard.persist.KeyNotFoundException;
import blackboard.persist.PersistenceException;

public interface PlaylistItemDbLoader {

	public List<PlaylistItem> loadByUserId(blackboard.persist.Id userId) throws KeyNotFoundException, PersistenceException;

	public int loadByVideoId(Id videoId) throws KeyNotFoundException, PersistenceException;

	public PlaylistItem loadByUserIdAndVideoId(blackboard.persist.Id userId, Id videoId) throws KeyNotFoundException, PersistenceException;
	
	public PlaylistItem loadById(Id id) throws KeyNotFoundException, PersistenceException;

	public List<UserPlaylist> retriveUserAndPlaylistInfo() throws PersistenceException;

	public void updateUserMailId(String userId, String newEmailId) throws PersistenceException;

	public void updatePlaylistExportStatus(String userId) throws PersistenceException;

	public List<Integer> checkAlreadyImported() throws PersistenceException;

	public List<HashMap<String, Object>> getOldPlayerContent(String courseId) throws PersistenceException;

	public List<HashMap<String, String>> getUserInfo(String emailId) throws PersistenceException;

	public void updateNewContent(Integer pk1, String finalTemplate) throws PersistenceException;

	public List<HashMap<String, Object>> getOldPlayerForumContent(String courseId)throws PersistenceException;

	public List<HashMap<String, Object>> getOldPlayerThreadContent(String courseId) throws PersistenceException;

	public void updateNewForumContent(Integer pk1, String finalTemplate) throws PersistenceException;

	public void updateNewThreadContent(Integer pk1, String finalTemplate) throws PersistenceException;

	public List<HashMap<String, Object>> getOldPlayerAnnouncementContent(String courseId) throws PersistenceException;

	public List<HashMap<String, Object>> getOldPlayerTaskContent(String courseId) throws PersistenceException;

	public void updateNewAnnouncementContent(Integer pk1, String finalTemplate) throws PersistenceException;

	public void updateNewTaskContent(Integer pk1, String finalTemplate) throws PersistenceException;

	public void updateIframeSecurityToken(String securityToken) throws PersistenceException;

	public String getSecurityToken() throws PersistenceException;
	
	public Video loadVideoById(String videoId) throws Exception;

	public List<HashMap<String, Object>> getLegacyPlayerForumContent(String courseId) throws PersistenceException;

	public List<HashMap<String, Object>> getLegacyPlayerThreadContent(String courseId) throws PersistenceException;

	public String getNBCBBVersion() throws PersistenceException;
	
	
	//public void createExportStatusColumn();

}
