package blackboard.gs.nbc.playlist.persist.video;


import blackboard.gs.nbc.playlist.data.video.Video;
import blackboard.gs.nbc.util.persist.Id;
import blackboard.persist.KeyNotFoundException;
import blackboard.persist.PersistenceException;

public interface VideoDbPersister {

	public void persist(Video video) throws PersistenceException;

	public void deleteById(Id id) throws PersistenceException, KeyNotFoundException;

}