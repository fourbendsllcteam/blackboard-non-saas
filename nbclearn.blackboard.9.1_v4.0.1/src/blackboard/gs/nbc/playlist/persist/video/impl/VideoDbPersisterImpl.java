package blackboard.gs.nbc.playlist.persist.video.impl;

import java.sql.CallableStatement;
import java.sql.Timestamp;
import java.sql.Types;

import org.apache.log4j.Logger;

import blackboard.gs.nbc.context.NBCAppContext;
import blackboard.gs.nbc.playlist.data.video.Video;
import blackboard.gs.nbc.playlist.persist.video.VideoDbPersister;
import blackboard.gs.nbc.util.persist.Id;
import blackboard.gs.nbc.util.persist.PersistUtil;
import blackboard.gs.nbc.util.persist.PersistUtil.Update;
import blackboard.gs.nbc.util.persist.PersistenceBase;
import blackboard.gs.nbc.util.persist.PersistenceOperationException;
import blackboard.persist.KeyNotFoundException;
import blackboard.persist.PersistenceException;

public class VideoDbPersisterImpl extends PersistenceBase implements VideoDbPersister {

	private static final Logger logger = Logger.getLogger(VideoDbPersisterImpl.class);

	private static final String INSERT_VIDEO = "{call bbgs_video_cr(?,?,?,?,?,?,?,?)}";

	private static final String UPDATE_VIDEO = 
			"UPDATE bbgs_video " + "SET "
			+ "id=?, name=?, description=?, duration=?, "
			+ "air_date=?, url=?, type=? WHERE pk1=?";

	private static final String DELETE_VIDEO = "DELETE FROM bbgs_video WHERE pk1=?";

	private final NBCAppContext nbcAppContext;
	
	/**
	 * @param nbcAppContext
	 */
	public VideoDbPersisterImpl(NBCAppContext nbcAppContext) {
		this.nbcAppContext = nbcAppContext;
	}

	public void persist(Video video) throws PersistenceException {
		if (video.getId() == null) {
			insert(video);
		} else {
			update(video);
		}

	}

	private void insert(final Video video) throws PersistenceException {
		Update<Video> update = new Update<Video> () {
			/**
			 * {@inheritDoc}
			 */
			@Override
			public String getSQL() {
				return INSERT_VIDEO;
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public void set(CallableStatement statement) throws Exception {
				statement.setString(1, video.getVideoId());
				statement.setString(2, video.getName());
				statement.setString(3, video.getDescription());
				statement.setString(4, video.getDuration());
				if (video.getAirDate() != null) {
					Timestamp timestamp = new Timestamp(video.getAirDate().getTimeInMillis());
					statement.setTimestamp(5, timestamp);
				}
				else {
					statement.setTimestamp(5, null);
				}
				statement.setString(6, video.getThumbnailUrl());
				statement.setString(7, video.getType());
				statement.registerOutParameter(8, Types.INTEGER);
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public void onSuccess(int rowCount, CallableStatement statement) throws Exception {
				int pk = statement.getInt(8);
				Id id = new Id(Video.class, pk);
				video.setId(id);
			}
		};
		try {
			PersistUtil.doUpdate(update , nbcAppContext);
		} catch (PersistenceOperationException e) {
			//TODO: change method sig.
			throw new PersistenceException(e);
		}
	}

	private void update(final Video video) throws PersistenceException {
		Update<Video> update = new Update<Video> () {
			/**
			 * {@inheritDoc}
			 */
			@Override
			public String getSQL() {
				return UPDATE_VIDEO;
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public void set(CallableStatement statement) throws Exception {
				statement.setString(1, video.getVideoId());
				statement.setString(2, video.getName());
				statement.setString(3, video.getDescription());
				statement.setString(4, video.getDuration());			
				if (null!=video.getAirDate())
				{
					Timestamp timestamp = new Timestamp(video.getAirDate().getTimeInMillis());
					statement.setTimestamp(5, timestamp);
				}
				else
				{
					statement.setTimestamp(5, null);
				}
				statement.setString(6, video.getThumbnailUrl());
				statement.setString(7, video.getType());
				statement.setInt(8, video.getId().getKey());
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public void onSuccess(int rowCount, CallableStatement statement) throws Exception {
				/*if (logger.isDebugEnabled()) {
					logger.debug("Video " + video.getVideoId() + " successfully updated.");
				}*/
			}
			
		};
		try {
			PersistUtil.doUpdate(update , nbcAppContext);
		} catch (PersistenceOperationException e) {
			//TODO: change method sig.
			throw new PersistenceException(e);
		}
	}

	public void deleteById(final Id id) throws PersistenceException, KeyNotFoundException {
		Update<Video> update = new Update<Video> () {
			/**
			 * {@inheritDoc}
			 */
			@Override
			public String getSQL() {
				return DELETE_VIDEO;
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public void set(CallableStatement statement) throws Exception {
				statement.setInt(1, id.getKey());
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public void onSuccess(int rowCount, CallableStatement statement) throws Exception {
				if (rowCount == 0) {
					throw new KeyNotFoundException("Video key cannot be found.");
				}
			}
		};
		try {
			PersistUtil.doUpdate(update, nbcAppContext);
		} catch (PersistenceOperationException e) {
			//TODO: change method sig.
			throw new PersistenceException(e);
		}
	}

}
