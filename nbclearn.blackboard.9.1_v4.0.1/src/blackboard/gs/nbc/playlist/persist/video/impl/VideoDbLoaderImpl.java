package blackboard.gs.nbc.playlist.persist.video.impl;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;

import org.apache.log4j.Logger;

import blackboard.gs.nbc.context.NBCAppContext;
import blackboard.gs.nbc.playlist.data.video.Video;
import blackboard.gs.nbc.playlist.persist.video.VideoDbLoader;
import blackboard.gs.nbc.util.persist.Id;
import blackboard.gs.nbc.util.persist.PersistUtil;
import blackboard.gs.nbc.util.persist.PersistenceBase;
import blackboard.gs.nbc.util.persist.PersistenceOperationException;
import blackboard.gs.nbc.util.persist.PersistUtil.Query;
import blackboard.persist.KeyNotFoundException;
import blackboard.persist.PersistenceException;

public class VideoDbLoaderImpl extends PersistenceBase implements VideoDbLoader {

	private static final Logger logger = Logger.getLogger(VideoDbLoaderImpl.class);
	
	private static final String SELECT_VIDEO_BY_VIDEOID = "SELECT pk1, id, name, description, duration, air_date, url, type from bbgs_video WHERE id=? ";

	private static final String SELECT_VIDEO_BY_ID = "SELECT pk1, id, name, description, duration, air_date, url, type from bbgs_video WHERE pk1=? ";

	private final NBCAppContext nbcAppContext;
	/**
	 * @param nbcAppContext
	 */
	public VideoDbLoaderImpl(NBCAppContext nbcAppContext) {
		this.nbcAppContext = nbcAppContext;
	}

	@Override
	public Video loadByVideoId(final String videoId) throws KeyNotFoundException, PersistenceException {
		Query<Video> query = new Query<Video>() {
			@Override
			public String getSQL() {
				return SELECT_VIDEO_BY_VIDEOID;
			}

			@Override
			public void set(PreparedStatement statement) throws Exception {
				statement.setString(1, videoId);
			}

			@Override
			public Video extract(ResultSet resultSet) throws Exception {
				Video video = unmarshallVideo(resultSet);
				return video;
			}			
		};
		
		try {
			Video video = PersistUtil.getUnique(query, this.nbcAppContext);
			if (video == null) {
				/*logger.warn("ATTENTION: no Video found for id: '" + videoId + "'");*/
			}
			return video;
		} catch (PersistenceOperationException e) {
			///TODO: change signature on this class to throw PersistenceOperationException
			throw new PersistenceException(e);
		}
	}

	@Override
	public Video loadById(final Id id) throws KeyNotFoundException, PersistenceException {
		Query<Video> query = new Query<Video>() {
			@Override
			public String getSQL() {
				return SELECT_VIDEO_BY_ID;
			}

			@Override
			public void set(PreparedStatement statement) throws Exception {
				statement.setInt(1, id.getKey());
			}

			@Override
			public Video extract(ResultSet resultSet) throws Exception {
				Video video = unmarshallVideo(resultSet);
				return video;
			}			
		};
		try {
			Video video = PersistUtil.getUnique(query , this.nbcAppContext);
			if (video == null) {
				/*logger.warn("ATTENTION: no Video found for key: '" + id.getKey() + "'");*/
			}
			return video;
		} catch (PersistenceOperationException e) {
			///TODO: change signature on this class to throw PersistenceOperationException
			throw new PersistenceException(e);
		}
	}

	private Video unmarshallVideo(ResultSet results) throws SQLException {
		Id id = getId(results, Video.class, "pk1"); 
		String videoId = results.getString("id");
		String name = results.getString("name");
		String description = results.getString("description");
		// replace apostrophe
		description = description.replaceAll("\\&apos\\;", "'");
		String duration = results.getString("duration");
		Calendar airDate = getCalendar(results, "air_date");
		String thumbnailUrl = results.getString("url");
		String type = results.getString("type");

		Video video = new Video(this.nbcAppContext);
		video.setId(id);
		video.setVideoId(videoId);
		video.setName(name);
		video.setDescription(description);
		video.setDuration(duration);
		video.setAirDate(airDate);
		video.setThumbnailUrl(thumbnailUrl);
		video.setType(type);
		video.setShortType(type);
		return video;
	}
}
