package blackboard.gs.nbc.playlist.persist.video;


import blackboard.gs.nbc.playlist.data.video.Video;
import blackboard.gs.nbc.util.persist.Id;
import blackboard.persist.KeyNotFoundException;
import blackboard.persist.PersistenceException;

public interface VideoDbLoader {

	public Video loadByVideoId(String videoId) throws KeyNotFoundException, PersistenceException;

	public Video loadById(Id id) throws KeyNotFoundException, PersistenceException;
}
