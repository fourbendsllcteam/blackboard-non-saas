package blackboard.gs.nbc.security.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import blackboard.data.ValidationException;
import blackboard.data.registry.SystemRegistryEntry;
import blackboard.data.user.User;
import blackboard.gs.nbc.Constants;
import blackboard.gs.nbc.context.NBCAppContext;
import blackboard.gs.nbc.security.DesEncrypter;
import blackboard.gs.nbc.security.UserSessionService;
import blackboard.gs.nbc.util.persist.PersistUtil;
import blackboard.gs.nbc.util.persist.PersistUtil.Query;
import blackboard.persist.KeyNotFoundException;
import blackboard.persist.PersistenceException;
import blackboard.persist.registry.SystemRegistryEntryDbLoader;
import blackboard.persist.registry.SystemRegistryEntryDbPersister;
import blackboard.persist.user.UserDbLoader;
import blackboard.platform.persistence.PersistenceServiceFactory;
import blackboard.platform.session.BbSession;
import blackboard.platform.session.BbSessionManagerServiceFactory;

/**
 * 
 */
public class UserSessionServiceImpl implements UserSessionService {
	private static final Logger logger = Logger.getLogger(UserSessionServiceImpl.class);
	
	private static final String SELECT_USER_BY_SESSIONID = "SELECT user_id_pk1 from sessions WHERE md5=?";

	private static final String str = new String("QAa0bcLdUK2eHfJgTP8XhiFj61DOklNm9nBoI5pGqYVrs3CtSuMZvwWx4yE7zR");
	private static final Random r = new Random();
	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
	
	@SuppressWarnings("unused")
	private static final DesEncrypter encryptor = new DesEncrypter();
	
	private NBCAppContext appContext;
	
	/**
	 * @param appContext
	 */
	public UserSessionServiceImpl(NBCAppContext appContext) {
		this.appContext = appContext;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public final User loadUserBySessionId(String md5) throws Exception {
		// first decrypt the id
		final String decrypted = decryptSession(md5);
		if(decrypted == null) {
			throw new Exception("Currently you are not authenticated in Blackboard. You need to login first.");
		}
		
		Query<User> query = new Query<User> () {

			/**
			 * {@inheritDoc}
			 */
			@Override
			public String getSQL() {
				return SELECT_USER_BY_SESSIONID;
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public void set(PreparedStatement statement) throws Exception {
				statement.setString(1, decrypted);
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public User extract(ResultSet resultSet) throws Exception {
				int users_pk1 = resultSet.getInt("user_id_pk1");
				UserDbLoader userLoader = (UserDbLoader) PersistenceServiceFactory.getInstance().getDbPersistenceManager().getLoader(UserDbLoader.TYPE);
				User user = userLoader.loadById(PersistenceServiceFactory.getInstance().getDbPersistenceManager().generateId(User.DATA_TYPE, users_pk1));
				return user;
			}
			
		}; 
		
		User user = PersistUtil.getUnique(query, appContext);
		return user;
		
/*		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet results = null;
		User user = null;
		try {
			connection = PersistUtil.getConnection(DatabaseKey.BLACKBOARD);
			statement = connection.prepareStatement(SELECT_USER_BY_SESSIONID);
			statement.setString(1, md5);
			results = statement.executeQuery();
			while (results.next()) {
				int users_pk1 = results.getInt("user_id_pk1");
				UserDbLoader userLoader = (UserDbLoader) PersistenceServiceFactory
						.getInstance().getDbPersistenceManager().getLoader(
								UserDbLoader.TYPE);
				user = userLoader.loadById(PersistenceServiceFactory
						.getInstance().getDbPersistenceManager().generateId(
								User.DATA_TYPE, users_pk1));
			}

			if (user == null) {
				throw new Exception("Currently you are not authenticated in Blackboard. You need to login first.");
			}
			return user;
		} catch (Exception exception) {
			throw new Exception("Currently you are not authenticated in Blackboard. You need to login first.", exception);

		} finally {
			PersistUtil.closeResultSet(results);
			PersistUtil.closeStatement(statement);
			PersistUtil.closeConnection(connection, DatabaseKey.BLACKBOARD);
		}
*/		
		
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public final User loadUser() throws Exception {
		HttpServletRequest request = (HttpServletRequest) this.appContext.getRequestObject();
		//TODO: create new Exception class for this interface
		if (request == null) {
			throw new Exception("No request object is available to obtain user.");			
		}
		BbSession bbSession = BbSessionManagerServiceFactory.getInstance().getSession(request );
		if (!bbSession.isAuthenticated()) {
			throw new Exception("Currently you are not authenticated in Blackboard. You need to login first.");
		}
		UserDbLoader userLoader = (UserDbLoader) PersistenceServiceFactory.getInstance().getDbPersistenceManager().getLoader(UserDbLoader.TYPE);
		// need to do a 'heavy' load so that Role info can be obtained
		User user = userLoader.loadById(bbSession.getUserId(), null, true);
		
		return user;
	}

	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String encryptSession(String bbSessionIdMd5) {
		
		SystemRegistryEntryDbLoader sysRegistryLoader = null;
    	SystemRegistryEntry sysRegEntry = null;
    	try {
    		sysRegistryLoader = SystemRegistryEntryDbLoader.Default.getInstance();
    		try {
    			sysRegEntry = sysRegistryLoader.loadByKey(Constants.SECRET_KEY);
    		} catch (KeyNotFoundException knfe) {
    			sysRegEntry = new SystemRegistryEntry();
        		sysRegEntry.setKey(Constants.SECRET_KEY);
        		String key = generateRandomString();
        		String curTime = sdf.format(new Date());
        		String secKey = "null";
        		String fullKey = key + "-" + curTime + "-" + secKey;
        		sysRegEntry.setValue(fullKey);
        		try {
					SystemRegistryEntryDbPersister.Default.getInstance().persist(sysRegEntry);
				} catch (ValidationException e) {
					logger.error("Persisting the secret key", e);
					throw new RuntimeException("Internal error obtaining secret key");
				}
        		
//    			logger.warn("No exisiting kay/value found for primary secret.");
    		}
    	} catch (PersistenceException pe) {
    		throw new RuntimeException("Error loading sys reg loader.",pe);
    	}
    	
    	String value = sysRegEntry.getValue();
    	StringTokenizer st = new StringTokenizer(value, "-");
    	String key = null;
    	Date d = null;
    	String key2 = null;
    	for(int i = 0; i < 3; i++)
    	{
    		if(st.hasMoreTokens())
    		{
    			switch(i)
    			{
    			case 0:
    				key = st.nextToken();
    				break;
    			case 1:
    				try {
							d = sdf.parse(st.nextToken());
						} catch (ParseException e) {
							e.printStackTrace();
							
						}
						break;
    			case 2:
    				key2 = st.nextToken();
    				break;
    			}
    		}
    	}
    	Calendar now = Calendar.getInstance();
        now.add(Calendar.DATE, -10);
        if((d == null) && now.getTime().after(d))
        {
        	key2 = key;
        	key = generateRandomString();
        	String curTime = sdf.format(new Date());
    		String fullKey = key + "-" + curTime + "-" + key2;
    		sysRegEntry.setValue(fullKey);
    		try {
				SystemRegistryEntryDbPersister.Default.getInstance().persist(sysRegEntry);
			} catch (Throwable e) {
				logger.error("Persisting the secret key", e);
				throw new RuntimeException("Internal error obtaining secret key");
			} 
        }	
        DesEncrypter des = DesEncrypter.getDesEncrypter(key);
		return des.encrypt(bbSessionIdMd5);
	}
	
	@SuppressWarnings("unused")
	private String decryptSession(String encryptedMd5) {
		SystemRegistryEntryDbLoader sysRegistryLoader = null;
    	SystemRegistryEntry sysRegEntry = null;
    	try {
			sysRegistryLoader = SystemRegistryEntryDbLoader.Default
					.getInstance();
			sysRegEntry = sysRegistryLoader.loadByKey(Constants.SECRET_KEY);
			String value = sysRegEntry.getValue();
	    	StringTokenizer st = new StringTokenizer(value, "-");
	    	String key = null;
			Date d = null;
	    	String key2 = null;
	    	for(int i = 0; i < 3; i++)
	    	{
	    		if(st.hasMoreTokens())
	    		{
	    			switch(i)
	    			{
	    			case 0:
	    				key = st.nextToken();
	    				break;
	    			case 1:
	    				try {
								d = sdf.parse(st.nextToken());
							} catch (ParseException e) {
								e.printStackTrace();
								
							}
							break;
	    			case 2:
	    				key2 = st.nextToken();
	    				break;
	    			}
	    		}
	    	}
	    	DesEncrypter des = DesEncrypter.getDesEncrypter(key);
	    	if(null == des)
	    		return null;
	    	String md5 = des.decrypt(encryptedMd5);
	    	if(null == md5 && !key2.equals("null"))
	    	{
	    		des = DesEncrypter.getDesEncrypter(key2);
	    		if(null == des)
	    			return null;
	    		md5 = des.decrypt(encryptedMd5);
	    	}
	    	return md5;
		} catch (Throwable t) {
			logger.error(" Secret key not found", t);
			return null;
		}
		
	}
	private String generateRandomString() {
		StringBuilder sb = new StringBuilder();
		int te = 0;
		for (int i = 1; i <= 16; i++) {
			te = r.nextInt(62);
			sb.append(str.charAt(te));
		}
		return sb.toString();
	}
}