/**
 * Ryan Hardy
 * Guilford Group
 * 
 * UserSessionService.java
 * Created: Oct 14, 2013
 */
package blackboard.gs.nbc.security;


import blackboard.data.user.User;

/**
 * @author Ryan Hardy (rhardy@guilfordgroup.com)
 */
public interface UserSessionService {
	/**
	 * Attempts to extract the currently authenticated Bb {@link User} from 
	 * the current context. Propagates all exceptions, and throws exception if
	 * there is no currently authenticated user (i.e. this method can
	 * be called for authentication purposes, even if the user object
	 * is not needed).
	 * 
	 * @return
	 * @throws Exception
	 */
	public User loadUser() throws Exception;
	/**
	 * Is called to load a {@link User} using the encrypted session id
	 * provided. Will propagate all exceptions that may arise.
	 * 
	 * @param md5
	 * @return
	 * @throws Exception
	 * 
	 * @see {@link #encryptSession(String)}
	 */
	public User loadUserBySessionId(String md5) throws Exception;

	/**
	 * Is called to encrypt the session id provided.
	 * 
	 * @param bbSessionIdMd5
	 * @return
	 */
	public String encryptSession(String bbSessionIdMd5);

}