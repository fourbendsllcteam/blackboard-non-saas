package blackboard.gs.nbc.security;

import java.util.HashSet;
import java.util.Set;
import blackboard.base.BbEnum;
import blackboard.data.course.CourseMembership.Role;
import blackboard.platform.context.Context;
import blackboard.platform.context.ContextManager;
import blackboard.platform.context.ContextManagerFactory;
import blackboard.platform.security.SecurityUtil;



public final class Authorization {

    private static final Authorization INSTANCE = new Authorization();

    static public Authorization getInstance() {
            return INSTANCE;
    }

    private Authorization() { }
    
    public boolean isCourseRoleGranted(AuthPermission p) throws Exception {
    	boolean ok = false;
        try {
            if (!SecurityUtil.userHasEntitlement(p.toString())) {
                    throw new Exception("You are not authorized to perform this operation.");
            }
        } catch (Exception ex) {
            throw new Exception("You are not authorized to perform this operation.", ex);
        }
        return true;
    }
}
