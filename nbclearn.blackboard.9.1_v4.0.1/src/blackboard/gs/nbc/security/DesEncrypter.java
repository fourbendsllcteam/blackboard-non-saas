package blackboard.gs.nbc.security;

import java.security.InvalidKeyException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

public class DesEncrypter {
	private static final Logger LOG = Logger.getLogger(DesEncrypter.class);
    private Cipher ecipher;
    private Cipher dcipher;
	private String stringKey;
    public static SecretKey key;
    private static ArrayList<DesEncrypter>instances = new ArrayList<DesEncrypter>();
    
    static
    {
    	try {
			key = KeyGenerator.getInstance("DES").generateKey();
		} catch (Throwable e) {
			LOG.error(" Error instantiating encrypto ", e);
		}
    }
    public DesEncrypter()
    {
    	this(key);
    }
    
    public static DesEncrypter getDesEncrypter(String stringKey)
    {
    	if(null == stringKey)
    		return null;
    	DesEncrypter retVal = null;
    	for( DesEncrypter d : instances)
    	{
    		if(stringKey.equals(d.getStringKey()))
    		{
    			retVal = d;
    			break;
    		}
    	}
    	if(null == retVal)
    	{	
    		retVal = new DesEncrypter(stringKey);
    		if(instances.size() <= 1)
    			instances.add(retVal);
    		else
    			instances.add(0, retVal);
    	}
    	return retVal;
    }
    private String getStringKey() {
		return stringKey;
	}

	protected DesEncrypter(SecretKey seckey) {
    	try {
            ecipher = Cipher.getInstance("DES");
            dcipher = Cipher.getInstance("DES");
            ecipher.init(Cipher.ENCRYPT_MODE, seckey);
            dcipher.init(Cipher.DECRYPT_MODE, seckey);

        } catch (Throwable e) {
        	LOG.fatal(" Error instantiating encrypto ", e);
        }
    }

	public DesEncrypter(String stringKey) {
    	try {
    		DESKeySpec key = new DESKeySpec(stringKey.getBytes());		
        	SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
    		SecretKey s = keyFactory.generateSecret(key);
            ecipher = Cipher.getInstance("DES");
            dcipher = Cipher.getInstance("DES");
            ecipher.init(Cipher.ENCRYPT_MODE, s);
            dcipher.init(Cipher.DECRYPT_MODE, s);
            this.stringKey = stringKey; 

        } catch (Throwable e) {
        	LOG.fatal(" Error instantiating encrypto ", e);
        }
    }
    public String encrypt(String str) {
        try {
            // Encode the string into bytes using utf-8
            byte[] utf8 = str.getBytes("UTF8");

            // Encrypt
            byte[] enc = ecipher.doFinal(utf8);

            // Encode bytes to base64 to get a string
            return (new Base64(true)).encodeToString(enc);

            //return new sun.misc.BASE64Encoder().;
        } catch (Throwable e) {
        	LOG.fatal(" Error encrypting the string ", e);
        } 
        return null;
    }

    public String decrypt(String str) {
        try {
            // Decode base64 to get bytes
            byte[] dec = new Base64(true).decode(str);

            // Decrypt
            byte[] utf8 = dcipher.doFinal(dec);

            // Decode using utf-8
            return new String(utf8, "UTF8");
        } catch (Throwable e) {
        	LOG.fatal(" Error decrypting the string ", e);
        }
        return null;
    }
}
