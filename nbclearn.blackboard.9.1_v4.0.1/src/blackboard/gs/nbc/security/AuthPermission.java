package blackboard.gs.nbc.security;


public final class AuthPermission {
	
    public static final AuthPermission EMBED = new AuthPermission("course.content.CREATE");
    private String name;

    private AuthPermission(String name) {
        this.name = name;
    }
	
    @Override
    public String toString() {
        return name;
    }
}
	
