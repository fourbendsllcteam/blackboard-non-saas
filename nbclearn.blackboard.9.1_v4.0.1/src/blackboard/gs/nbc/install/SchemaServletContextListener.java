package blackboard.gs.nbc.install;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import blackboard.base.InitializationException;
import blackboard.data.navigation.Mask;
import blackboard.data.navigation.NavigationApplication;
import blackboard.data.navigation.PaletteItem;
import blackboard.data.registry.SystemRegistryEntry;
import blackboard.data.user.User;
import blackboard.db.BbDatabase;
import blackboard.gs.nbc.Constants;
import blackboard.gs.nbc.context.NBCAppContext;
import blackboard.gs.nbc.context.NBCAppContextFactory;
import blackboard.gs.nbc.playlist.data.playlistitem.PlaylistItem;
import blackboard.gs.nbc.playlist.data.video.Video;
import blackboard.gs.nbc.playlist.data.video.VideoType;
import blackboard.gs.nbc.playlist.service.PlaylistManagerService;
import blackboard.gs.nbc.util.BbVersionService;
import blackboard.persist.KeyNotFoundException;
import blackboard.persist.PersistenceException;
import blackboard.persist.navigation.NavigationApplicationDbLoader;
import blackboard.persist.navigation.NavigationApplicationDbPersister;
import blackboard.persist.navigation.PaletteItemDbLoader;
import blackboard.persist.navigation.PaletteItemDbPersister;
import blackboard.persist.registry.SystemRegistryEntryDbLoader;
import blackboard.persist.registry.SystemRegistryEntryDbPersister;
import blackboard.persist.user.UserDbLoader;
import blackboard.platform.BbServiceException;
import blackboard.platform.BbServiceManager;
import blackboard.platform.context.ContextManager;
import blackboard.platform.db.JdbcServiceFactory;
import blackboard.platform.persistence.PersistenceServiceFactory;
import blackboard.platform.vxi.data.VirtualHost;
import blackboard.platform.vxi.service.VirtualInstallationManager;
import blackboard.platform.vxi.service.VirtualSystemException;
import org.apache.log4j.Logger;

import com.ibatis.common.jdbc.ScriptRunner;

public class SchemaServletContextListener implements ServletContextListener {
	private static final Logger logger = Logger.getLogger(SchemaServletContextListener.class);

	/**
	 * 
	 * {@inheritDoc}
	 */
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		releaseContext();
	}

	/**
	 * 
	 * {@inheritDoc}
	 */
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		logger.info("\n========================================================================================"
				+ "\n    Initializing NBC Learn B2."
				+ "\n========================================================================================");
		ServletContext context = sce.getServletContext();

		setContext();

		PlugInName name = getPlugInName(sce);
		String applicationHandle = name.vendor + Constants.NAV_ITEM_PREFIX;
		String navItemHandle = applicationHandle + Constants.NAV_FIRST_POSTFIX;
		String navItemHandle2 = applicationHandle + Constants.NAV_SECOND_POSTFIX;

		try {

			NavigationApplication app = NavigationApplicationDbLoader.Default.getInstance()
					.loadByApplication(applicationHandle);
			Mask mask = new Mask();
			mask.setValue(Mask.SYSTEM, true);
			app.setAllowGuestMask(mask);
			NavigationApplicationDbPersister.Default.getInstance().persist(app);
			List<PaletteItem> pItem = PaletteItemDbLoader.Default.getInstance().loadByPaletteFamily("tools");
			if (pItem != null) {
				PaletteItemDbPersister.Default.getInstance().persist(pItem.get(0));
			}
		} catch (Throwable e2) {
			logger.error("Error changing the guest access for tool links ", e2);
		}

		int maxPosition = 0;
		boolean foundPalette = false;
		try {
			PaletteItem p = getPaletteItem(navItemHandle);
			if (p != null) {
				foundPalette = true;
			}
		} catch (KeyNotFoundException e) {
		} catch (PersistenceException e) {
			logger.error("Error while trying to load the paletteItem " + e);
		}

		try {
			maxPosition = getLatestPallettePosition();
		} catch (PersistenceException e1) {
			logger.error("error loading the max position on too pallete ", e1);
		}
		if (!foundPalette) {
			/*logger.info("Installing palette item for " + Constants.NBC_ARCHIVE_LABEL);*/
			PaletteItem pi = new PaletteItem();
			pi.setType(PaletteItem.Type.APPLICATION);
			pi.setNavigationItemHandle(navItemHandle2);
			pi.setLabel(Constants.NBC_ARCHIVE_LABEL);
			pi.setPaletteFamily("tools");
			pi.setIsEnabled(true);
			pi.setPosition(++maxPosition);

			try {
				PaletteItemDbPersister.Default.getInstance().persist(pi);
			} catch (Throwable e) {
				logger.error("error adding pallete item", e);
			}

			/*logger.info("Installing palette item for " + Constants.NBC_PLAYLIST_LABEL);*/
			PaletteItem pi2 = new PaletteItem();
			pi2.setType(PaletteItem.Type.APPLICATION);
			pi2.setNavigationItemHandle(navItemHandle);
			pi2.setLabel(Constants.NBC_PLAYLIST_LABEL);
			pi2.setPaletteFamily("tools");
			pi2.setIsEnabled(true);
			pi.setPosition(++maxPosition);
			try {
				PaletteItemDbPersister.Default.getInstance().persist(pi2);
			} catch (Throwable e) {
				logger.error("error adding pallete item", e);
			}
		}

		// =====================================================================

		BbDatabase db = JdbcServiceFactory.getInstance().getDefaultDatabase();

		boolean loadSQL = false;
		String ddlFile = null;
		String dmlFile = null;
		try {
			/*logger.info("Looking for file " + Constants.SCHEMA_VERSION_FILE);*/
			InputStream is = SchemaServletContextListener.class
					.getResourceAsStream("/" + Constants.SCHEMA_VERSION_FILE);
			BufferedReader in = new BufferedReader(new InputStreamReader(is));
			String str;
			String currentVersion = null;
			String overwriteVersion = null;
			/*logger.info("Reading file: " + Constants.SCHEMA_VERSION_FILE);*/
			if ((str = in.readLine()) != null) {
				/*logger.info("Contents of the file: " + str);*/
				StringTokenizer st = new StringTokenizer(str, Constants.SCHEMA_VERSION_FILE_DELIM);
				int count = 0;
				while (st.hasMoreTokens()) {
					switch (count) {
					case 0:
						currentVersion = st.nextToken();
						break;
					case 1:
						overwriteVersion = st.nextToken();
						break;
					case 2:
						ddlFile = st.nextToken();
						break;
					case 3:
						dmlFile = st.nextToken();
						break;
					}
					count++;
				}
			}

			/*logger.debug("Current version: " + currentVersion);*/
			/*logger.debug("Overwrite version: " + overwriteVersion);*/
			/*logger.debug("DDL File: " + ddlFile);*/
			/*logger.debug("DML File: " + dmlFile);*/

			SystemRegistryEntryDbLoader sysRegistryLoader = null;
			SystemRegistryEntry sysRegEntry = null;
			/*logger.info("Loading the existing registry value");*/
			try {
				sysRegistryLoader = SystemRegistryEntryDbLoader.Default.getInstance();
				try {
					sysRegEntry = sysRegistryLoader.loadByKey(Constants.SCHEMA_VER_REGISTRY_KEY);
					/*logger.info("Existing registry value: " + sysRegEntry.getValue());*/
				} catch (KeyNotFoundException knfe) {
					logger.error("Existing value for the NBC Schema version was not found.");
				}
			} catch (PersistenceException pe) {
				throw new RuntimeException("Error loading sys reg loader.", pe);
			}

			if (null == sysRegEntry) {
				/*logger.info("No previous registry value: ");*/
				loadSQL = true;
				sysRegEntry = new SystemRegistryEntry();
				sysRegEntry.setKey(Constants.SCHEMA_VER_REGISTRY_KEY);

			} else {
				/*logger.info("Schema Override allowed");*/
				String existingVersion = sysRegEntry.getValue();
				StringTokenizer st2 = new StringTokenizer(overwriteVersion, Constants.OVERWRITE_VERSION_DELIM);
				while (st2.hasMoreTokens()) {
					if (st2.nextToken().equals(existingVersion)) {
						/*logger.info("Schema Override allowed");*/
						loadSQL = true;
						break;
					}
				}
			}
			// add/ update the system registry
			SystemRegistryEntryDbPersister srpersister = SystemRegistryEntryDbPersister.Default.getInstance();
			sysRegEntry.setValue(currentVersion);
			srpersister.persist(sysRegEntry);
		} catch (Throwable t) {
			logger.error(Constants.SCHEMA_VERSION_FILE + " error processing the file ", t);
			return;
		}
		/*logger.info("LOADSQL---- "+loadSQL);*/
		if (!loadSQL) {
			// do a double-check that the Schema might still need to be loaded
			// due to issues found in SP-13
			/*logger.info("loadSQL----------");*/
			loadSQL = doSchemaLoad();
			/*logger.info("loadSQL AFTER doSchemaLoad(): " + loadSQL);*/
			if (!loadSQL) {
				/*logger.info("SQL scripts shall not be run.");*/
			}
		}

		if (loadSQL) {
			try {
				executeScripts(db, ddlFile, dmlFile);
			} catch (Throwable t) {
				logger.error("An error occurred executing database scripts.", t);
			}
		}
		
		/* ================================================================== */

		try {
			File manifestFile = new File(context.getRealPath("/WEB-INF/bb-manifest.xml"));
			ManifestParser parser = new ManifestParser(manifestFile);
			String bbVersion = parser.getBuildingBlockVersion();

			// sending in null for request will return a 'temp' instance...
			BbVersionService versionSvc = NBCAppContextFactory.createAppContext(null).getBbVersionService();

			logger.info("\n========================================================================================"
					+ "\n    NBC Learn Building Block has been loaded." + "\n        NBC Learn Building Block Version: "
					+ bbVersion + "\n        " + "\n        Blackboard Full version: " + versionSvc.getFullVersion()
					+ "\n        Blackboard Major version: " + versionSvc.getMajorVersion()
					+ "\n        Blackboard Minor version: " + versionSvc.getMinorVersion()
					+ "\n        Blackboard Service Pack: " + versionSvc.getServicePack()
					+ "\n========================================================================================");
		} catch (Exception e) {
			logger.error("Could not get build number info.", e);
			logger.info("\n========================================================================================"
					+ "\n    NBC Learn Building Block has been loaded."
					+ "\n========================================================================================");
		}

	}

	private void executeScripts(BbDatabase db, String ddlFile, String dmlFile) {
		/*logger.info("Executing SQL scripts");*/
		String postfix = null;
		String delim = ";";
		String dbType = null;

		String appVersType = db.getAppVersion().getType();
		/*logger.info("BbDatabase AppVersion type: " + appVersType);*/

		final boolean isMySql = isMySQL(db);
		/*logger.info("BbDatabase.isMySql(): " + isMySql);*/
		final boolean isOracle = db.isOracle();
		/*logger.info("BbDatabase.isOracle(): " + isOracle);*/
		final boolean isSqlServer = db.isSqlServer();
		/*logger.info("BbDatabase.isSqlServer(): " + isSqlServer);*/
		final boolean isPostGres = isPostGresDB(db);
		/*logger.info("BbDatabase.isPostGres(): " + isPostGres);*/

		if (isMySql) {
			postfix = Constants.MYSQL_POSTFIX;
			delim = Constants.MYSQL_DELIM;
			dbType = "mysql";
		}
		if (null == dbType && isOracle) {
			postfix = Constants.ORACLE_POSTFIX;
			delim = Constants.ORACLE_DELIM;
			dbType = "oracle";
		}
		if (null == dbType && isSqlServer) {
			postfix = Constants.SQLSERVER_POSTFIX;
			delim = Constants.SQLSERVER_DELIM;
			dbType = "sqlserver";
		}
		if (null == dbType && isPostGres) {
			postfix = Constants.POSTGRESSERVER_POSTFIX;
			delim = Constants.ORACLE_DELIM;
			dbType = "postgresql";
		}
		try {
			Connection con = db.getConnectionManager().getConnection();
			DatabaseMetaData databaseMetaData = con.getMetaData();
			if (databaseMetaData != null) {
				if (databaseMetaData.getDatabaseProductName() != null
						&& databaseMetaData.getDatabaseProductName().equalsIgnoreCase("oracle")) {
					postfix = Constants.ORACLE_POSTFIX;
					delim = Constants.ORACLE_DELIM;
					dbType = "oracle";
					/*logger.info("Db--------------->oracle");*/
				} else if (databaseMetaData.getDatabaseProductName() != null
						&& databaseMetaData.getDatabaseProductName().equalsIgnoreCase("postgresql")) {
					postfix = Constants.POSTGRESSERVER_POSTFIX;
					delim = Constants.ORACLE_DELIM;
					dbType = "postgresql";
					/*logger.info("Db--------------->postgresql");*/
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		if (null != ddlFile) {
			String script = ddlFile + postfix;
			runScript(db, delim, script);
		}
		if (null != dmlFile) {
			String script = dmlFile + postfix;
			runScript(db, delim, script);
		}
	}

	/**
	 * Performs a check to see if underlying database is MySQL. This was
	 * formerly a call on the {@link BbDatabase} object, but the call was
	 * removed in SP13.
	 * 
	 * @param db
	 * @return
	 */
	private boolean isMySQL(BbDatabase db) {
		final String appVersType = db.getAppVersion().getType().trim();
		final boolean isMySql = appVersType.equalsIgnoreCase("mysql");
		return isMySql;
	}

	public static boolean isPostGresDB(BbDatabase db) {
		final String appVersType = db.getAppVersion().getType().trim();
		final boolean isPostGres = appVersType.equalsIgnoreCase("postgresql");
		return true;
	}

	public static boolean isOracleDB(BbDatabase db) {
		final String appVersType = db.getAppVersion().getType().trim();
		final boolean isPostGres = appVersType.equalsIgnoreCase("oracle");
		return true;
	}

	private void runScript(BbDatabase db, String delim, String script) {
		try {
			/*logger.info("Running script: " + script);*/
			InputStream is = SchemaServletContextListener.class.getResourceAsStream("/" + script);
			Connection con = db.getConnectionManager().getConnection();

			ScriptRunner sr = new ScriptRunner(con, false, true);
			sr.setDelimiter(delim, true);
			sr.runScript(new InputStreamReader(is));
			logger.info("Script Ran normally");
		} catch (Throwable t) {
			logger.error("Error executing " + script, t);
			logger.error(t.getMessage());
		}
	}

	/**
	 * Attempts to perform a save operation to the DB, to see if the schema has
	 * been loaded for the Building Block. Returns true if the operation fails
	 * (indicating that the schema SHOULD be loaded).
	 * 
	 * @return
	 */
	private boolean doSchemaLoad() {
		final NBCAppContext appContext = NBCAppContextFactory.createAppContext(null);
		/*logger.info("INSIDE DOSCEMALOAD----------->");*/
		try {
			UserDbLoader userLoader = (UserDbLoader) PersistenceServiceFactory.getInstance().getDbPersistenceManager()
					.getLoader(UserDbLoader.TYPE);
			User user = userLoader.loadGuestUser();

			Video video = new Video(appContext);
			video.setVideoId("123");
			video.setDuration("2:30");
			video.setAirDateInString(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.0").format(new Date()));
			video.setName("Test Video");
			video.setDescription("Test Video Description");
			video.setThumbnailDecodedUrl("");
			video.setShortType(VideoType.VIDEO.toString());

			PlaylistManagerService playlistManager = appContext.getPlaylistManagerService();
			/*logger.info("BEFORE SAVEVIDEO CALLED----------->");*/
			// logger.info("doSchemaLoad() method saveVideo
			// called----User----------->"+user);
			PlaylistItem item = playlistManager.saveVideo(user, video);
/*//			logger.info("doSchemaLoad() removeVideos called----------->");
*/			playlistManager.removeVideos(Arrays.asList(item));

			return false;
		} catch (Exception ex) {
			if (logger.isTraceEnabled()) {
				logger.error("doSchemaLoad() failed, save error on Video File.", ex);
				ex.printStackTrace();
				ex.getCause();
			} else {
				logger.error("doSchemaLoad() failed, save error on Video: " + ex.getStackTrace());
				ex.printStackTrace();

			}
			return true;
		} finally {
			appContext.getPersistenceSession().close();
		}
	}
	
	private static void setContext() {
		try {
			VirtualInstallationManager vm = (VirtualInstallationManager) BbServiceManager
					.lookupService(VirtualInstallationManager.class);
			ContextManager cm = (ContextManager) BbServiceManager.lookupService(ContextManager.class);

			List<VirtualHost> list = vm.getAllVirtualHosts();
			if (list.isEmpty()) {
				throw new VirtualSystemException("No Virtual Hosts exist.");
			}
			VirtualHost vhost = (VirtualHost) list.get(0);
			cm.setContext(vhost);

		} catch (Throwable oops) {
			logger.error("Failed to set Bb Context", oops);
			throw new RuntimeException("Failed to set Bb Context", oops);
		}
	}

	private void releaseContext() {
		ContextManager cman = null;
		try {
			cman = (ContextManager) BbServiceManager.lookupService(ContextManager.class);
		} catch (InitializationException e) {
			logger.error("error on release context", e);
		} catch (BbServiceException e) {
			logger.error("error on release context", e);
		}
		cman.releaseContext();
	}

	protected PlugInName getPlugInName(ServletContextEvent arg0) {
		PlugInName result = new PlugInName();

		File path = new File(arg0.getServletContext().getRealPath(""));
		File contextDir = path.getParentFile();
		String contextName = contextDir.getName();
		int dash = contextName.indexOf('-');

		File pluginsDir = contextDir.getParentFile();
		File viDir = pluginsDir.getParentFile();

		result.vendor = contextName.substring(0, dash);
		result.handle = contextName.substring(dash + 1);
		result.virtualInstall = viDir.getName();

		return result;
	}

	private int getLatestPallettePosition() throws PersistenceException {
		List<PaletteItem> paletteItems = PaletteItemDbLoader.Default.getInstance().loadByPaletteFamily("tools");
		int maxPosition = 0;
		for (PaletteItem item : paletteItems) {
			if (item.getPosition() > maxPosition) {
				maxPosition = item.getPosition();
			}
		}
		return maxPosition;
	}

	private PaletteItem getPaletteItem(String navItemHandle) throws PersistenceException {
		List<PaletteItem> paletteItems = PaletteItemDbLoader.Default.getInstance().loadByPaletteFamily("tools");
		for (PaletteItem item : paletteItems) {
			if (item.getNavigationItemHandle().equals(navItemHandle)) {
				return item;
			}
		}
		return null;
	}

	static class PlugInName {
		String vendor;
		String handle;
		String virtualInstall;
	}

	/**
	 * Exception thrown if any errors occur in parsing or getting values from
	 * bb-manifest.xml file in {@link ManifestParser}.
	 * 
	 * @author Ryan Hardy (rhardy@guilfordgroup.com)
	 */
	static class ManifestParsingException extends Exception {
		/**
		 * 
		 */
		private static final long serialVersionUID = 5593252773312602784L;

		/**
		 * @param message
		 * @param cause
		 */
		protected ManifestParsingException(String message, Throwable cause) {
			super(message, cause);
		}

		/**
		 * @param message
		 */
		protected ManifestParsingException(String message) {
			super(message);
		}

	}

	/**
	 * DOM parser used to obtain values from bb-manifest file.
	 * 
	 * @author Ryan Hardy (rhardy@guilfordgroup.com)
	 */
	static class ManifestParser {
		private final File manifestFile;
		private final Node pluginNode;

		/**
		 * @param manifestFile
		 * @throws ManifestParsingException
		 */
		public ManifestParser(File manifestFile) throws ManifestParsingException {
			this.manifestFile = manifestFile;
			NodeList pluginList;
			try {
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				Document doc = dBuilder.parse(this.manifestFile);
				doc.getDocumentElement().normalize();
				pluginList = doc.getDocumentElement().getElementsByTagName("plugin");
				if (pluginList.getLength() != 1) {
					throw new ManifestParsingException("Invalid manifest format: no 'plugin' node found.");
				}
			} catch (ParserConfigurationException e) {
				throw new ManifestParsingException("Could not parse bb-manifest file.", e);
			} catch (SAXException e) {
				throw new ManifestParsingException("Could not parse bb-manifest file.", e);
			} catch (IOException e) {
				throw new ManifestParsingException("Could not parse bb-manifest file.", e);
			}
			pluginNode = pluginList.item(0);
		}

		/**
		 * Is called to find a child node with the given name. Returns null if
		 * none found.
		 * 
		 * @param node
		 * @param name
		 * @return
		 */
		private Node findNode(Node node, String name) {
			if (node == null) {
				return null;
			}
			if (node.getNodeName().equalsIgnoreCase(name)) {
				return node;
			}
			NodeList kids = node.getChildNodes();
			for (int i = 0; i < kids.getLength(); i++) {
				Node kid = kids.item(i);
				Node toChk = findNode(kid, name);
				if (toChk != null) {
					return toChk;
				}
			}
			return null;
		}

		/**
		 * Is called to find an attribute value with the given name. Returns
		 * empty string if none found.
		 * 
		 * @param node
		 * @param attName
		 * @return
		 */
		private String getAttrValue(Node node, String attName) {
			if (node == null) {
				return "";
			}
			NamedNodeMap atts = node.getAttributes();
			Node att = atts.getNamedItem(attName);
			if (att == null) {
				return "";
			}
			String val = att.getNodeValue();
			return val;
		}

		/**
		 * Obtains the Building Block version.
		 * 
		 * @return
		 * @throws ManifestParsingException
		 *             if manifest is not appropriate format
		 */
		public String getBuildingBlockVersion() throws ManifestParsingException {
			Node versionNode = findNode(this.pluginNode, "version");
			if (versionNode == null) {
				throw new ManifestParsingException("Invalid manifest format: no 'version' node found.");
			}
			String version = getAttrValue(versionNode, "value");
			return version;
		}

	}
}