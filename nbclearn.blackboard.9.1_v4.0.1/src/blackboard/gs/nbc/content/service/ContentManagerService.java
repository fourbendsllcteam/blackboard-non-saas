package blackboard.gs.nbc.content.service;

import java.sql.Connection;

import blackboard.data.content.Content;


public interface ContentManagerService {
	
	public void createItem(Content content, Connection connection) throws Exception;
	
	public void modifyItem(Content content, Connection connection) throws Exception;

}
