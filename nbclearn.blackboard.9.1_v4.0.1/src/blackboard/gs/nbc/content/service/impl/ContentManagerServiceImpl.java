package blackboard.gs.nbc.content.service.impl;

import java.sql.Connection;

import blackboard.data.ValidationException;
import blackboard.data.content.Content;
import blackboard.gs.nbc.content.service.ContentManagerService;
import blackboard.persist.PersistenceException;
import blackboard.persist.Persister;
import blackboard.persist.content.ContentDbPersister;
import blackboard.platform.persistence.PersistenceServiceFactory;

public class ContentManagerServiceImpl implements ContentManagerService {

	public void createItem(Content content, Connection connection) throws Exception {
		try {
			persistContent(content, connection);
		} catch (Exception ex) {
			throw new Exception("Creating content item failed", ex);
		}
	}

	public void modifyItem(Content content, Connection connection) throws Exception {
		try {
			persistContent(content, connection);
		} catch (Exception ex) {
			throw new Exception("Modifying content item failed", ex);
		}
	}

	private void persistContent(Content content, Connection connection) throws PersistenceException, ValidationException {
		ContentDbPersister persister = (ContentDbPersister) getPersister(ContentDbPersister.TYPE);
		persister.persist(content, connection);
	}

	private Persister getPersister(String type) throws PersistenceException {
		return PersistenceServiceFactory.getInstance().getDbPersistenceManager().getPersister(type);
	}

}
