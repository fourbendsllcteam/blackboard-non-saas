/**
 * Ryan Hardy
 * Guilford Group
 * 
 * CueCardConfiguration.java
 * Created: May 14, 2012
 */
package blackboard.gs.nbc.util;

import blackboard.gs.nbc.playlist.data.video.Video;
import blackboard.util.resolver.Resolver;

/**
 * Collection of utility methods for rendering CueCard templates.
 * 
 * @author Ryan Hardy
 */
public interface CueCardConfiguration {	
	/**
	 * Is called to render the description snippet for the given {@link Video}.
	 * 
	 * @param video
	 * @param embedCode 
	 * @return
	 */
	public String renderDescription(Video video, String embedCode);
	/**
	 * 
	 * @param cueCardId
	 * @return
	 */
	public String renderStaticCueCard(String cueCardId);
	
	/**
	 * 
	 * @return
	 */
	public String renderFloatingCueCardIncludes();
	/**
	 * @param video
	 * @return
	 */
	public String renderPlaylistCueCard(Video video);
	/**
	 * @param video
	 * @return
	 */
	public String renderCourseContentCueCard(Video video);
	
	/**
	 * 
	 * @param video
	 * @return
	 */
	public String renderMashupCueCardThumbnail(Video video);
	
	/**
	 * 
	 * @param video
	 * @return
	 */
	public String renderContentModule(Video video);

	/**
	 * @return
	 */
	public String renderMashupScript();
	/**
	 * Renders the script injected into content documents via the 
	 * Blackboard {@link Resolver} mechanisms.
	 * 
	 * @return
	 */
	public String renderResolverScript();
	
}