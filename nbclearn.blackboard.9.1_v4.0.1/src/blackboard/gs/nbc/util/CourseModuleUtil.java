package blackboard.gs.nbc.util;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import blackboard.data.ValidationException;
import blackboard.data.course.Course;
import blackboard.data.registry.CourseRegistryEntry;
import blackboard.gs.nbc.Constants;
import blackboard.gs.nbc.context.NBCAppContext;
import blackboard.gs.nbc.context.NBCAppContextFactory;
import blackboard.gs.nbc.playlist.data.video.Video;
import blackboard.gs.nbc.web.RequestCache;
import blackboard.persist.Id;
import blackboard.persist.KeyNotFoundException;
import blackboard.persist.PersistenceException;
import blackboard.persist.registry.CourseRegistryEntryDbLoader;
import blackboard.persist.registry.CourseRegistryEntryDbPersister;

public class CourseModuleUtil {
	
	static Logger _log = Logger.getLogger(CourseModuleUtil.class);
	
	public static String loadCurrentCueCardId(Id courseId) {
		
		CourseRegistryEntryDbLoader crseRegistryLoader = null;
		CourseRegistryEntry crseRegEntry = null;
		String currentCueCardId = null;
		
		try {
			crseRegistryLoader = CourseRegistryEntryDbLoader.Default.getInstance();
			try {
				crseRegEntry = crseRegistryLoader.loadByKeyAndCourseId(Constants.COURSE_REGISTRY_KEY, courseId);
				currentCueCardId = crseRegEntry.getValue();
				
			} catch (KeyNotFoundException knfe) {
				_log.error("Could not find current video for course module. Instructor must not have selected one yet.");
			}
		} catch (PersistenceException pe) {
			throw new RuntimeException("Error loading course registry loader.",pe);
		}
		
		return currentCueCardId;
		
	}
	
	public static Video loadCurrentCueCard(Id courseId) 
	{
		CourseRegistryEntryDbLoader crseRegistryLoader = null;
		CourseRegistryEntry crseRegEntry = null;
		String currentCueCardId = null;
		String thumbnailUrl = null;
		String airDate = null;
		String contentType = null;
		String duration = null;
		String name = null; 
		String iframeContent = null; 
		try {
			crseRegistryLoader = CourseRegistryEntryDbLoader.Default.getInstance();
			try {
				crseRegEntry = crseRegistryLoader.loadByKeyAndCourseId(Constants.COURSE_REGISTRY_KEY, courseId);
				currentCueCardId = crseRegEntry.getValue();
				crseRegEntry = crseRegistryLoader.loadByKeyAndCourseId(Constants.COURSE_REGISTRY_IMAGE_KEY, courseId);
				thumbnailUrl = crseRegEntry.getValue();
				crseRegEntry = crseRegistryLoader.loadByKeyAndCourseId(Constants.COURSE_REGISTRY_DATE_KEY, courseId);
				airDate = crseRegEntry.getValue();
				crseRegEntry = crseRegistryLoader.loadByKeyAndCourseId(Constants.COURSE_REGISTRY_TITLE_KEY, courseId);
				name = crseRegEntry.getValue();
				crseRegEntry = crseRegistryLoader.loadByKeyAndCourseId(Constants.COURSE_REGISTRY_TYPE_KEY, courseId);
				contentType = crseRegEntry.getValue();
				crseRegEntry = crseRegistryLoader.loadByKeyAndCourseId(Constants.COURSE_REGISTRY_DURATION_KEY, courseId);
				duration = crseRegEntry.getValue();
				crseRegEntry = crseRegistryLoader.loadByKeyAndCourseId(Constants.COURSE_REGISTRY_IFRAME, courseId);
				iframeContent = crseRegEntry.getValue();
				crseRegEntry = crseRegistryLoader.loadByKeyAndCourseId(Constants.COURSE_REGISTRY_IFRAME_SRC, courseId);
				iframeContent += crseRegEntry.getValue();
				
			} catch (KeyNotFoundException knfe) {
				_log.error("Could not find current video for course module. Instructor must not have selected one yet.");
			}
		} catch (PersistenceException pe) {
			throw new RuntimeException("Error loading course registry loader.",pe);
		}
		HttpServletRequest request = RequestCache.getRequest();
		NBCAppContext appContext = NBCAppContextFactory.createAppContext(request);

		Video video = new Video(appContext);
		video.setAirDateInString(airDate);
		video.setContentTypeImage(contentType);
		video.setDuration(duration);
		video.setName(name);
		video.setThumbnailUrl(thumbnailUrl);
		video.setVideoId(currentCueCardId);
		video.setIframeContent(iframeContent);
		
		return video;
	}
	
	public static void setCurrentCueCardId(String strCourseId, String strCueCardId) {
		
		CourseRegistryEntryDbPersister crseRegistryPersist = null;
		Id courseId = null;
		try {
			courseId = Id.generateId(Course.DATA_TYPE, strCourseId);
		} catch (PersistenceException pe) {
			throw new RuntimeException("Incorrect Course Id parameter provided while saving current NBC course Cue Card.",pe);
		}
		try {
			crseRegistryPersist = CourseRegistryEntryDbPersister.Default.getInstance();
			CourseRegistryEntry crseRegEntry = new CourseRegistryEntry();
			crseRegEntry.setCourseId(courseId);
			crseRegEntry.setKey(Constants.COURSE_REGISTRY_KEY);
			crseRegEntry.setValue(strCueCardId);
			try {
				crseRegEntry.validate();
				crseRegistryPersist.deleteByKeyAndCourseId(Constants.COURSE_REGISTRY_KEY, courseId);
				crseRegistryPersist.persist(crseRegEntry);
			} catch (ValidationException ve) {
				throw new RuntimeException("Error validating sys registry entry.",ve);
			}
		} catch (PersistenceException pe) {
			throw new RuntimeException("Error saving current NBC course Cue Card Id.",pe);
		}
	
	}
	
	public static void setCurrentCurCardId(String strCourseId, Video video, String embedCode)
	{
		CourseRegistryEntryDbPersister crseRegistryPersist = null;
		Id courseId = null;
		try {
			courseId = Id.generateId(Course.DATA_TYPE, strCourseId);
		} catch (PersistenceException pe) {
			throw new RuntimeException("Incorrect Course Id parameter provided while saving current NBC course Cue Card.",pe);
		}
		try {
			crseRegistryPersist = CourseRegistryEntryDbPersister.Default.getInstance();
			CourseRegistryEntry crseRegEntry0 = new CourseRegistryEntry();
			crseRegEntry0.setCourseId(courseId);
			crseRegEntry0.setKey(Constants.COURSE_REGISTRY_KEY);
			crseRegEntry0.setValue(video.getVideoId());
			
			crseRegistryPersist = CourseRegistryEntryDbPersister.Default.getInstance();
			CourseRegistryEntry crseRegEntry = new CourseRegistryEntry();
			crseRegEntry.setCourseId(courseId);
			crseRegEntry.setKey(Constants.COURSE_REGISTRY_IMAGE_KEY);
			crseRegEntry.setValue(video.getThumbnailUrl());
			
			CourseRegistryEntry crseRegEntry1 = new CourseRegistryEntry();
			crseRegEntry1.setCourseId(courseId);
			crseRegEntry1.setKey(Constants.COURSE_REGISTRY_DATE_KEY);
			crseRegEntry1.setValue(video.getAirDateInString());
			
			CourseRegistryEntry crseRegEntry2 = new CourseRegistryEntry();
			crseRegEntry2.setCourseId(courseId);
			crseRegEntry2.setKey(Constants.COURSE_REGISTRY_TITLE_KEY);
			crseRegEntry2.setValue(video.getName());
			
			CourseRegistryEntry crseRegEntry3 = new CourseRegistryEntry();
			crseRegEntry3.setCourseId(courseId);
			crseRegEntry3.setKey(Constants.COURSE_REGISTRY_TYPE_KEY);
			
			crseRegEntry3.setValue(video.getContentTypeImage());
			
			CourseRegistryEntry crseRegEntry4 = new CourseRegistryEntry();
			crseRegEntry4.setCourseId(courseId);
			crseRegEntry4.setKey(Constants.COURSE_REGISTRY_DURATION_KEY);
			crseRegEntry4.setValue(video.getDuration());
			
			CourseRegistryEntry crseRegEntry5 = new CourseRegistryEntry();
			crseRegEntry5.setCourseId(courseId);
			crseRegEntry5.setKey(Constants.COURSE_REGISTRY_IFRAME);
			crseRegEntry5.setValue(embedCode.substring(0, embedCode.indexOf("src=")+4));
			
			CourseRegistryEntry crseRegEntry6 = new CourseRegistryEntry();
			crseRegEntry6.setCourseId(courseId);
			crseRegEntry6.setKey(Constants.COURSE_REGISTRY_IFRAME_SRC);
			crseRegEntry6.setValue(embedCode.substring(embedCode.indexOf("src=")+4));
			try {
				crseRegEntry0.validate();
				crseRegEntry.validate();
				crseRegEntry1.validate();
				crseRegEntry2.validate();
				crseRegEntry3.validate();
				crseRegEntry4.validate();
				crseRegEntry5.validate();
				crseRegEntry6.validate();
				crseRegistryPersist.deleteByKeyAndCourseId(Constants.COURSE_REGISTRY_KEY, courseId);
				crseRegistryPersist.deleteByKeyAndCourseId(Constants.COURSE_REGISTRY_IMAGE_KEY, courseId);
				crseRegistryPersist.deleteByKeyAndCourseId(Constants.COURSE_REGISTRY_DATE_KEY, courseId);
				crseRegistryPersist.deleteByKeyAndCourseId(Constants.COURSE_REGISTRY_TITLE_KEY, courseId);
				crseRegistryPersist.deleteByKeyAndCourseId(Constants.COURSE_REGISTRY_TYPE_KEY, courseId);
				crseRegistryPersist.deleteByKeyAndCourseId(Constants.COURSE_REGISTRY_DURATION_KEY, courseId);
				crseRegistryPersist.deleteByKeyAndCourseId(Constants.COURSE_REGISTRY_IFRAME, courseId);
				crseRegistryPersist.deleteByKeyAndCourseId(Constants.COURSE_REGISTRY_IFRAME_SRC, courseId);
				crseRegistryPersist.persist(crseRegEntry0);
				crseRegistryPersist.persist(crseRegEntry);
				crseRegistryPersist.persist(crseRegEntry1);
				crseRegistryPersist.persist(crseRegEntry2);
				crseRegistryPersist.persist(crseRegEntry3);
				crseRegistryPersist.persist(crseRegEntry4);
				crseRegistryPersist.persist(crseRegEntry5);
				crseRegistryPersist.persist(crseRegEntry6);
			} catch (ValidationException ve) {
				throw new RuntimeException("Error validating sys registry entry.",ve);
			}
		} catch (PersistenceException pe) {
			throw new RuntimeException("Error saving current NBC course Cue Card Id.",pe);
		}
		
	}
	

}
