/**
 * Ryan Hardy
 * Guilford Group
 * 
 * SecurityTokenManager.java
 * Created: May 14, 2012
 */
package blackboard.gs.nbc.util;

import blackboard.gs.nbc.context.NBCAppContext;

/**
 * Used to create implementations that are responsible for saving and loading
 * security tokens.
 * 
 * @author Ryan Hardy
 */
public interface SecurityTokenManager {
	/**
	 * Loads the token from the underlying persistence store.
	 * 
	 * @return
	 */
	public String loadSecurityToken();	
	/**
	 * Saves the token to the underlying persistence store.
	 * 
	 * @param value
	 * @param appContext 
	 */
    public void saveSecurityToken(String value, NBCAppContext appContext);
}
