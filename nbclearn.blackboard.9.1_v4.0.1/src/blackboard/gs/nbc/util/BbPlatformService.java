/**
 * Ryan Hardy
 * Guilford Group
 * 
 * BbPlatformService.java
 * Created: Jun 7, 2013
 */
package blackboard.gs.nbc.util;

/**
 * Service used to provide information about underlying Building Block
 * deployment.
 * @author Ryan Hardy (rhardy@guilfordgroup.com)
 */
public interface BbPlatformService {
	/**
	 * Obtains the base URI for this Building Block.
	 * 
	 * @return
	 */
	public String getUriStem();
	/**
	 * Gets the server host name this instance is currently deployed on.
	 * 
	 * @return
	 */
	public String getHostName();
	/**
	 * Returns <code>true</code> if the 'Forced SSL' option has been 
	 * configured for this instance.
	 * 
	 * @return
	 */
	public boolean isForcedSystemSSL();
	/**
	 * Returns the unique identifier associated with the
	 * underlying  <code>VirtualInstallation</code>.
	 * 
	 * @return
	 */
	public String getUUID();
}
