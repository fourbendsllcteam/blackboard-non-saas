package blackboard.gs.nbc.util.persist;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import blackboard.persist.DataType;
import blackboard.persist.PersistenceException;
import blackboard.persist.PkId;
import blackboard.platform.persistence.PersistenceServiceFactory;

public class PersistenceBase {

	protected Calendar getCalendar(ResultSet results, String name) throws SQLException {
		Timestamp stamp = results.getTimestamp(name);
		Calendar result = null;
		if (stamp != null) {
			result = new GregorianCalendar();
			result.setTime(stamp);
		}
		return result;
	}

	protected void setCalendar(PreparedStatement call, int index, Calendar date) throws SQLException {
		if (date == null) {
			call.setNull(index, Types.TIMESTAMP);
		} else {
			Timestamp timestamp = new Timestamp(new Date().getTime());
			call.setTimestamp(index, timestamp, date);
		}
	}

	protected void setCurrentCalendar(PreparedStatement call, int index) throws SQLException {
		Timestamp timestamp = new Timestamp(new Date().getTime());
		call.setTimestamp(index, timestamp);
	}

	protected Id getId(ResultSet results, Class<?> type, String name) throws SQLException {
		int key = results.getInt(name);
		Id id = (key == 0) ? null : new Id(type, key);
		return id;
	}

	protected static blackboard.persist.Id generateId(DataType dataType, int pk1) throws PersistenceException {
		return PersistenceServiceFactory.getInstance().getDbPersistenceManager().generateId(dataType, pk1);
	}

	protected int getPk1(blackboard.persist.Id id) {
		int pk1 = -1;
		if (id != null) {
			PkId pkId = (PkId) id;
			pk1 = pkId.getPk1();
		}
		return pk1;
	}
}