/**
 * Ryan Hardy
 * Guilford Group
 * 
 * PersistenceOperationException.java
 * Created: Sep 17, 2013
 */
package blackboard.gs.nbc.util.persist;

import java.sql.SQLException;


/**
 * Exception thrown if any errors occur in call to 
 * {@link PersistUtil#doWork(blackboard.gs.nbc.util.persist.PersistUtil.Work)}.
 * 
 * @author Ryan Hardy (rhardy@guilfordgroup.com)
 */
public class PersistenceOperationException extends SQLException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3764946246548189296L;
	/**
	 * 
	 */
	public PersistenceOperationException() {
	}

	/**
	 * @param reason
	 */
	public PersistenceOperationException(String reason) {
		super(reason);
	}

	/**
	 * @param cause
	 */
	public PersistenceOperationException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param reason
	 * @param SQLState
	 */
	public PersistenceOperationException(String reason, String SQLState) {
		super(reason, SQLState);
	}

	/**
	 * @param reason
	 * @param cause
	 */
	public PersistenceOperationException(String reason, Throwable cause) {
		super(reason, cause);
	}

	/**
	 * @param reason
	 * @param SQLState
	 * @param vendorCode
	 */
	public PersistenceOperationException(String reason, String SQLState, int vendorCode) {
		super(reason, SQLState, vendorCode);
	}

	/**
	 * @param reason
	 * @param sqlState
	 * @param cause
	 */
	public PersistenceOperationException(String reason, String sqlState, Throwable cause) {
		super(reason, sqlState, cause);
	}

	/**
	 * @param reason
	 * @param sqlState
	 * @param vendorCode
	 * @param cause
	 */
	public PersistenceOperationException(String reason, String sqlState, int vendorCode, Throwable cause) {
		super(reason, sqlState, vendorCode, cause);
	}

}
