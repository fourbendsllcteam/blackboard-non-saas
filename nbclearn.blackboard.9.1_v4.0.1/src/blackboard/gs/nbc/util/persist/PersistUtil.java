package blackboard.gs.nbc.util.persist;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import blackboard.gs.nbc.context.NBCAppContext;
import blackboard.gs.nbc.context.NBCAppContext.PersistenceSession;
import blackboard.persist.PersistenceException;

public class PersistUtil {
	/**
	 * Used to define a discrete unit of work to be executed in call to 
	 * {@link PersistUtil#doWork(Work)}. Instance of this class will be 
	 * provided a {@link Connection} instance and should <b>NOT</b> manage
	 * any part of Connection lifecycle (i.e. call commit, rollback, etc).
	 * 
	 * @author Ryan Hardy (rhardy@guilfordgroup.com)
	 *
	 * @param <T>	the return type of {@link Work#execute(Connection)} method
	 */
	public interface Work<T> {
		/**
		 * Performs a discrete unit of work, using the {@link Connection} with
		 * the {@link PersistenceSession} in the given {@link NBCAppContext}.
		 * 
		 * @param appContext
		 * 
		 * @return a result object (if this is a query), may be <code>null</code>
		 * 
		 * @throws Exception
		 */
		public T execute(NBCAppContext appContext) throws Exception;
		/**
		 * If this returns true, will release {@link Connection} once
		 * work unit has been completed (whether successfully or due to error).
		 * 
		 * @return
		 */
		public boolean releaseConnection();
	}
	
	public interface Update<T> {
		/**
		 * Gets the SQL for this query.
		 * 
		 * @return
		 */
		public String getSQL();
		/**
		 * Sets any needed params on the {@link CallableStatement} needed
		 * for executing the update.
		 * 
		 * @param statement
		 * 
		 * @throws Exception
		 */
		public void set(CallableStatement statement) throws Exception;
		/**
		 * 
		 * @param rowCount 
		 * @param statement
		 * @throws Exception
		 */
		public void onSuccess(int rowCount, CallableStatement statement)  throws Exception;
	}
	
	/**
	 * Used to define a query to be executed in call to 
	 * {@link PersistUtil#getUnique(Query)} or 
	 * {@link PersistUtil#getUnique(Query, Connection)}.
	 * 
	 * @author Ryan Hardy (rhardy@guilfordgroup.com)
	 *
	 * @param <T>	the return type of {@link Query#extract(ResultSet)} method
	 */
	public interface Query<T> {
		/**
		 * Gets the SQL for this query.
		 * 
		 * @return
		 */
		public String getSQL();
		/**
		 * Sets any needed params on the {@link PreparedStatement} needed
		 * for executing the query.
		 * 
		 * @param statement
		 * 
		 * @throws Exception
		 */
		public void set(PreparedStatement statement) throws Exception;
		/**
		 * Extracts fields from the current row on the given {@link ResultSet}.
		 * Implementation should <b>NOT</b> call {@link ResultSet#next()}, and
		 * can assume that resultSet is in a valid state to extract fields.
		 * 
		 * @param resultSet
		 * 
		 * @return the entity correlating to the current row in the resultSet
		 * 
		 * @throws Exception
		 */
		public T extract(ResultSet resultSet) throws Exception;
	}
	

	private static final Logger logger = Logger.getLogger(PersistUtil.class);

	//TODO: this needs to be extracted to an interface/impl. pair, with the
	// getter added to NBCAppContext...will allow for better local
	// unit testing (will not rely on actualy Bb DB interaction)
	
	/**
	 * Executes the given {@link Work} instance. This method will handle all
	 * aspects of Connection lifecycle management. The {@link Work} instance
	 * should <b>NOT</b> manually handle any connection-related lifecycle
	 * management.
	 * 
	 * @param work
	 * @throws PersistenceOperationException
	 * 
	 * @see {@link Work}
	 */
	public static final <T> T doWork(Work<T> work, NBCAppContext appContext) throws PersistenceOperationException {
		T retValue = null;
		Connection connection = null; 
		try {
			/*logger.debug("Getting Connection from PersistenceSession.");*/
			connection = appContext.getPersistenceSession().getConnection();
			retValue = work.execute(appContext);
			if (connection.getAutoCommit()) {
				/*logger.warn("ATTENTION: Connection is in auto-commit mode (not calling commit).");*/
			} else {
				connection.commit();				
			}
		} catch (Exception e) {
			try {
				if (connection != null) {
					connection.rollback();
				}
			} catch (SQLException sqex) {
				logger.error("Rollback failed during creating a course module", sqex);
			}
			throw new PersistenceOperationException("Error occurred during database operation. Please contact your system administrator.", e);
		} finally {
			if (work.releaseConnection()) {
				/*logger.debug("Calling release on Connection.");*/
				appContext.getPersistenceSession().close();				
			} else {
				/*logger.debug("NOT calling release on Connection.");	*/			
			}
		}
		return retValue;
	}
	
	/**
	 * 
	 * @param update
	 * @param appContext
	 * @throws PersistenceOperationException
	 */
	public static final <T> void doUpdate(final Update<T> update, NBCAppContext appContext) throws PersistenceOperationException {
		Work<T> work = new Work<T>() {
			/**
			 * {@inheritDoc}
			 */
			@Override
			public T execute(NBCAppContext appContext) throws Exception {
				T item = null;
				
				CallableStatement statement = null;				
				try {
					Connection connection = appContext.getPersistenceSession().getConnection();
					final String querySql = update.getSQL();
					statement = connection.prepareCall(querySql);
					
					update.set(statement);
					
					int rowCount = statement.executeUpdate();
					update.onSuccess(rowCount, statement);
				} catch (SQLException exception) {
					throw new PersistenceException("Item update failed.", exception);
				} finally {
					closeStatement(statement);
				}
				return item;
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public boolean releaseConnection() {
				return false; // do NOT release
			}
			
		};
		doWork(work, appContext);
	}

	/**
	 * 
	 * @param query
	 * @param connection
	 * @return
	 * @throws PersistenceOperationException
	 */
	public static final <T> List<T> get(final Query<T> query, NBCAppContext appContext) {
		Work<List<T>> work = new Work<List<T>>() {
			/**
			 * {@inheritDoc}
			 */
			@Override
			public List<T> execute(NBCAppContext appContext) throws Exception {
				final List<T> items = new ArrayList<T>();
				
				PreparedStatement statement = null;
				ResultSet results = null;
				
				try {
					Connection connection = appContext.getPersistenceSession().getConnection();
					final String querySql = query.getSQL();
					statement = connection.prepareStatement(querySql);
					
					query.set(statement);
					
					results = statement.executeQuery();
					while (results.next()) {
						T item = query.extract(results);
						items.add(item);
						/*if (logger.isDebugEnabled()) {
							logger.debug("Item successfully loaded: " + item);
						}*/
					}
					if (items.isEmpty()) {
						/*logger.warn("No items found in call to get().");*/
					}
				} catch (SQLException exception) {
					//throw new PersistenceException("Item load failed.", exception);
					/*logger.error(exception);*/
				} finally {
					closeResultSet(results);
					closeStatement(statement);
				}
				return items;
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public boolean releaseConnection() {
				return false; // do NOT release
			}
			
		};
		List<T> items=null;
		try{
		items = doWork(work, appContext);
		}catch(Exception e){
			logger.error(e);
		}
		return items;		
	}
	
	/**
	 * 
	 * @param query
	 * @param connection
	 * @return
	 * @throws PersistenceOperationException
	 */
	public static final <T> T getUnique(final Query<T> query, NBCAppContext appContext) throws PersistenceOperationException {
		Work<T> work = new Work<T>() {
			/**
			 * {@inheritDoc}
			 */
			@Override
			public T execute(NBCAppContext appContext) throws Exception {
				T item = null;
				
				PreparedStatement statement = null;
				ResultSet results = null;
				
				try {
					Connection connection = appContext.getPersistenceSession().getConnection();
					final String querySql = query.getSQL();
					statement = connection.prepareStatement(querySql);
					
					query.set(statement);
					
					results = statement.executeQuery();
					if (results.next()) {
						item = query.extract(results);
						/*if (logger.isDebugEnabled()) {
							logger.debug("Item successfully loaded: " + item);
						}
					} else {
						logger.warn("No item found in call to getUnique().");
					*/}
				} catch (SQLException exception) {
					throw new PersistenceException("Item load failed.", exception);
				} finally {
					closeResultSet(results);
					closeStatement(statement);
				}
				return item;
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			public boolean releaseConnection() {
				return false; // do NOT release
			}
			
		};
		T item = doWork(work, appContext);
		return item;		
	}
	

	/**
	 * Does null-safe/exception safe closing of {@link ResultSet}.
	 * 
	 * @param rst
	 */
	private static final void closeResultSet(ResultSet rst) {
		if (rst != null) {
			try {
				rst.close();
			} catch (SQLException sqle) {				
				logger.error("Error closing result set", sqle);
			}
		}
	}


	/**
	 * Does null-safe/exception safe closing of {@link Statement}.
	 * 
	 * @param stmt
	 */
	private static final void closeStatement(Statement stmt) {
		if (stmt != null) {
			try {
				stmt.close();
			} catch (SQLException sqle) {
				logger.error("Error closing the statement ", sqle);
			}
		}
	}

}
