package blackboard.gs.nbc.util.persist;

import java.io.PrintWriter;
import java.io.StringWriter;

public class Id {
	 private static final long serialVersionUID = 1L;
	  
	  private String type;
	  private int key;
	  
	  public Id(String type, int key) {
	    this.type = type;
	    this.key = key;
	  }
	  
	  public Id(Class type, int key) {
	    this.type = type.getName();
	    this.key = key;
	  }
	  
	  public String getType() {
	    return type;
	  }
	  
	  public int getKey() {
	    return key;
	  }
	  
	  public int hashCode() {
	    int code = type.hashCode() * key;
	    return code;
	  }
	  
	  public boolean equals(Object object) {
	    boolean truth = false;
	    if (object instanceof Id) {
	      Id id = (Id) object;
	      truth = (key == id.key) && type.equals(id.type);
	    }
	    return truth;
	  }
	  
	  public String toString(){
	  	StringWriter stringWriter = new StringWriter();
			PrintWriter  printWriter = new PrintWriter(stringWriter);
			
			printWriter.print("Type=");
			printWriter.println(type);
			printWriter.print("Key=");
			printWriter.println(key);
			
	  	return stringWriter.toString();
	  }


}
