package blackboard.gs.nbc.util;

import javax.servlet.http.HttpServletRequest;
import blackboard.data.content.Content;
import blackboard.gs.nbc.Constants;
import blackboard.gs.nbc.action.RequestControllerActionBean;
import blackboard.gs.nbc.parameters.ActionParam;
import blackboard.persist.Id;
import blackboard.persist.PersistenceException;
import blackboard.persist.content.ContentDbLoader;
import blackboard.platform.context.ContextManagerFactory;
import blackboard.platform.persistence.PersistenceServiceFactory;
import blackboard.platform.plugin.PlugInUtil;
import blackboard.util.UrlUtil;

public class ActionUrl {
	private static final String contentUrl = "/webapps/blackboard/content/listContentEditable.jsp";
	private static final String playlistEmbedUrl = "/WEB-INF/jsp/playlist/playlist_embed.jsp";
	private static final String playlistRemoveUrl = "/WEB-INF/jsp/playlist/playlist_remove.jsp";
	private static final String playlistRemoveSuccessUrl = PlugInUtil.getUriStem(Constants.VENDOR_ID,Constants.PLUGIN_ID) + "req_con.action";
	private static final String errorPageUrl = "/WEB-INF/jsp/error.jsp";
	private static final String emptyPlaylistUrl = "/WEB-INF/jsp/playlist/playlist_empty.jsp";
	private static final String launchUrl = "/webapps/blackboard/execute/launcher?type=Course&id=";
	private static final String homeUrl = "/webapps/portal/frameset.jsp";
	private static final String courseToolHomeUrl = "/webapps/blackboard/course/course_button.jsp?family=course_tools_area";
	private static final String redirectToEmbedPlaylistUrl = "req_con.action?embedPlaylistUrl";
	private static final String redirectToRemovePlaylistUrl = "req_con.action?removePlaylistUrl";
	private static final String redirectToEmptyPlaylistUrl = "req_con.action?emptyPlaylistUrl";
	private static final String closeWindowUrl = "/WEB-INF/jsp/mashup/close_window.jsp";
	private static final String insertAndCloseWindowUrl = "/WEB-INF/jsp/mashup/insert_and_close_window.jsp";

	public static String getContentUrl(String courseId, String contentId)
    {
		return contentUrl + "?course_id=" +courseId+ "&content_id="+contentId;
    	
    }
    
	public static String getContentUrlWithFrameset(String courseId, String contentId, HttpServletRequest request)
    {
    	String targetUrl = contentUrl + "?course_id=" +courseId+ "&content_id="+contentId;
    	return UrlUtil.calculateFullCourseFrameUrl(request.getLocalName(), blackboard.util.UrlUtil.isForcedSystemSSL(), targetUrl, courseId);
    }
	
	public static String getPlaylistRemoveSuccessUrl(String returnLoc, String courseId)
	{
		String targetUrl = playlistRemoveSuccessUrl + "?mode=browse&type=playlist&location=blackboard&returnLoc=" + returnLoc + 
	    						"&course_id=" + courseId;
	    return UrlUtil.calculateFullUrl(ContextManagerFactory.getInstance().getContext().getHostName(), blackboard.util.UrlUtil.isForcedSystemSSL(), targetUrl);
	}
	 
    public static String getPlaylistEmbedUrl()
    {
    	return playlistEmbedUrl;
    }
    
    
    public static String getPlaylistRemoveUrl()
    {
    	return playlistRemoveUrl;
    }
    
    
    public static String getErrorPageUrl()
    {
    	return errorPageUrl;
    }
    
    public static String getEmptyPlaylistUrl()
    {
    	return emptyPlaylistUrl;
    }
    
    public static String getCloseWindowUrl()
    {
    	return closeWindowUrl;
    }
    
    public static String getInsertAndCloseWindowUrl()
    {
    	return insertAndCloseWindowUrl;
    }
    
    public static String getLaunchUrl()
    {
    	return launchUrl;
    }
    
    public static String getHomeUrl() throws PersistenceException
    {
    	String targetUrl = homeUrl;
       	return UrlUtil.calculateFullUrl(ContextManagerFactory.getInstance().getContext().getHostName(), blackboard.util.UrlUtil.isForcedSystemSSL(), targetUrl);
    }
    
    public static String getCourseToolHomeUrl(String courseId) throws PersistenceException
    {
    	String targetUrl = courseToolHomeUrl + "&course_id=" + courseId;
    	return UrlUtil.calculateFullUrl(ContextManagerFactory.getInstance().getContext().getHostName(), blackboard.util.UrlUtil.isForcedSystemSSL(), targetUrl);
    }
    
    public static String getCourseUrlWithFrameset(String courseId, HttpServletRequest request)
    {
    	String courseLaunchUrl = getLaunchUrl() + courseId + "&url=" ;
    	return UrlUtil.calculateFullCourseFrameUrl(request.getLocalName(), blackboard.util.UrlUtil.isForcedSystemSSL(), courseLaunchUrl, courseId);	
    }
    
    public static String getCourseToolUrlWithFrameset(String courseId, HttpServletRequest request)
    throws PersistenceException
    {
    	String courseLaunchUrl = getCourseToolHomeUrl(courseId);
    	return UrlUtil.calculateFullCourseFrameUrl(request.getLocalName(), blackboard.util.UrlUtil.isForcedSystemSSL(), courseLaunchUrl, courseId);	
    }
    
    public static String getCourseUrl(String courseId)
    {
    	return getLaunchUrl() + courseId + "&url=" ;  	
    }
    
    public static String getContentUrl(String courseId, String contentId, String action) throws PersistenceException
	{
		
		String returnUrl = null;
		if (action.equals(ActionParam.CREATE.toString()))
		{
			returnUrl = getContentUrl(courseId, contentId);
		}
		else
		{
			Id content_id = PersistenceServiceFactory.getInstance().getDbPersistenceManager().generateId(Content.DATA_TYPE, contentId);
			ContentDbLoader loader = (ContentDbLoader) PersistenceServiceFactory.getInstance().getDbPersistenceManager().getLoader(ContentDbLoader.TYPE);
	        Content content = loader.loadById(content_id);
	        returnUrl = getContentUrl(courseId, content.getParentId().toExternalString());
		}
		return returnUrl;
	}
    
    public static String getTargetContentUrlWithFrameset(String courseId, String contentId, HttpServletRequest request, String action) throws PersistenceException
	{
		
		String returnUrl = null;
		if (action.equals(ActionParam.CREATE.toString()))
		{
			returnUrl = getContentUrlWithFrameset(courseId, contentId, request);
		}
		else
		{
			Id content_id = PersistenceServiceFactory.getInstance().getDbPersistenceManager().generateId(Content.DATA_TYPE, contentId);
			ContentDbLoader loader = (ContentDbLoader) PersistenceServiceFactory.getInstance().getDbPersistenceManager().getLoader(ContentDbLoader.TYPE);
	        Content content = loader.loadById(content_id);
	        returnUrl = getContentUrlWithFrameset(courseId, content.getParentId().toExternalString(), request);
		}
		return returnUrl;
	}
    
    public static String getPlaylistEmbedUrlWithFrameset(HttpServletRequest request, RequestControllerActionBean actionBean)
	{
		
    	String targetUrl = PlugInUtil.getUriStem(Constants.VENDOR_ID,Constants.PLUGIN_ID) + redirectToEmbedPlaylistUrl + getParameters(actionBean);
    	return UrlUtil.calculateFullTabFrameUrl(request.getLocalName(), blackboard.util.UrlUtil.isForcedSystemSSL(), targetUrl, "");
	}
    
    
    public static String getPlaylistRemoveUrlWithFrameset(HttpServletRequest request)
	{
		
		String targetUrl = PlugInUtil.getUriStem(Constants.VENDOR_ID,Constants.PLUGIN_ID) + redirectToRemovePlaylistUrl;
		return UrlUtil.calculateFullTabFrameUrl(request.getLocalName(), blackboard.util.UrlUtil.isForcedSystemSSL(), targetUrl, "");
	}
    
    public static String getEmptyPlaylistUrlWithFrameset(HttpServletRequest request)
	{
		
		String targetUrl = PlugInUtil.getUriStem(Constants.VENDOR_ID,Constants.PLUGIN_ID) + redirectToEmptyPlaylistUrl;
		return UrlUtil.calculateFullTabFrameUrl(request.getLocalName(), blackboard.util.UrlUtil.isForcedSystemSSL(), targetUrl, "");
	}
    

    private static String getParameters(RequestControllerActionBean actionBean)
    {
    	return "&type="+actionBean.getType()+"&action="+actionBean.getAction()+ "&mode="+actionBean.getMode()+"&course_id="+actionBean.getCourse_id()+"&content_id="+actionBean.getContent_id();
    }

}
