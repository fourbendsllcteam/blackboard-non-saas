/**
 * Ryan Hardy
 * Guilford Group
 * 
 * TemplateRenderUtils.java
 * Created: Jun 13, 2012
 */
package blackboard.gs.nbc.util;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Matcher;

import org.apache.log4j.Logger;

import blackboard.gs.nbc.Constants;
import blackboard.gs.nbc.util.impl.DefaultCueCardConfigImpl;

/**
 * Provides static utility methods used to render template content.
 * 
 * @author Ryan Hardy (rhardy@guilfordgroup.com)
 */
public class TemplateRenderUtils {
	private static final Logger logger = Logger.getLogger(TemplateRenderUtils.class);
	
	/**
	 * Wraps the given string with the standard Bb delimiter string sequence.
	 * 
	 * @param constant
	 * @return
	 */
	public static String getDelimitedConst(String constant) {
		if (constant == null) {
			throw new IllegalArgumentException("Can not create delimited constant, constant provided is null.");
		}
		StringBuilder bldr = new StringBuilder();
		bldr.append(Constants.NBC_DELIMITER);
		bldr.append(constant);
		bldr.append(Constants.NBC_DELIMITER);
		return bldr.toString();
	}	

	/**
	 * Performs template token replacement on the given <code>content</code>
	 * using the provided <code>replText</code> map.
	 * 
	 * @param content
	 * @param replText
	 * @return
	 */
	public static String renderTemplateComponent(String content, Map<String, String> replText) {
		Readable readable = new StringReader(content);
		String rendered = doTemplateRendering(readable, replText, "");
		
		return rendered;
	}
	
	/**
	 * Renders the template identified by the <code>contentSource</code> using the replacement
	 * text in the <code>replText</code> map. The <code>contentSource</code> string should return
	 * the template {@link InputStream} in call to {@link Class#getResourceAsStream(String) getResourceAsStream()}.
	 * The replacement text map should contain keys that are the <b>non-delimited</b> template patterns (i.e.
	 * the delimiters will be added within the method call) to search for within the template, mapped to 
	 * the text to replace the pattern with.
	 * 
	 * @param contentSource
	 * @param replText
	 * @param err the text to use for logging and {@link RuntimeException} to throw if any errors occur in rendering template
	 * @return
	 */
	public static String renderTemplate(String contentSource, Map<String, String> replText, String err) {
		InputStream is = null;
		try {
			is = DefaultCueCardConfigImpl.class.getResourceAsStream(contentSource);
		} catch (Throwable th) {
			logger.error("Could not find template file: " + contentSource, th);
			throw new RuntimeException("Could not find template file: " + contentSource, th);
		}		
		String rendered = doTemplateRendering(new InputStreamReader(is), replText, err);
		
		return rendered;
	}

	/**
	 * Helper method used to do template rendering.
	 * @param is
	 * @param replText
	 * @param err
	 * @return
	 */
	private static String doTemplateRendering(Readable is, Map<String, String> replText, String err) {
		StringBuilder bldr = new StringBuilder();		
		Scanner scan = null;
		String pattern= "", replacement = "";
		try {
			scan = new Scanner(is);
			while (scan.hasNextLine()) {
				String line = scan.nextLine();
				if (logger.isTraceEnabled()) {
					logger.trace("line:\t" + line);
				}
				for (Map.Entry<String, String> e : replText.entrySet()) {
					pattern = getDelimitedConst(e.getKey());
					replacement = e.getValue();
					if (replacement == null) {
						//TODO: what is the best thing to do here? for now, simple replace with empty string
						replacement = "";
					}
					// escape special regex chars
					replacement = Matcher.quoteReplacement(replacement);
					
					line = line.replaceAll(pattern, replacement);
				}
				bldr.append(line).append("\n");
			}
		} catch (Exception e) {
			logger.error(err + "\nPattern: '" + pattern + "', Replacement: '" + replacement + "'", e);
			throw new RuntimeException(err, e);
		} finally {
			try {
				// swallow NPE or close errors
				scan.close(); // this call also closes InputStream
			} catch (Exception e) {
				logger.error("Could not close Scanner for template.", e);
			}
		}		
		return bldr.toString();
	}	
}
