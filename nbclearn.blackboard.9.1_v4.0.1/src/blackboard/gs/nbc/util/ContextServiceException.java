/**
 * Ryan Hardy
 * Guilford Group
 * 
 * ContextServiceException.java
 * Created: Jun 14, 2012
 */
package blackboard.gs.nbc.util;

/**
 * Exception thrown if any error occur in obtaining context info in a call
 * to a {@link ContextService} method.
 * 
 * @author Ryan Hardy (rhardy@guilfordgroup.com)
 */
public class ContextServiceException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -330716176970711795L;

	/**
	 * 
	 */
	public ContextServiceException() { }

	/**
	 * @param message
	 */
	public ContextServiceException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public ContextServiceException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public ContextServiceException(String message, Throwable cause) {
		super(message, cause);
	}
}