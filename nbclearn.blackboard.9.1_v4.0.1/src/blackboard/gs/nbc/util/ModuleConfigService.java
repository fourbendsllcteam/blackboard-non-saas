/**
 * Ryan Hardy
 * Guilford Group
 * 
 * ModuleConfigService.java
 * Created: Nov 19, 2012
 */
package blackboard.gs.nbc.util;

/**
 * @author Ryan Hardy (rhardy@guilfordgroup.com)
 *
 */
public interface ModuleConfigService {
	/**
	 * Obtains the URL for use in the African American History module. Will
	 * return <code>null</code> if none has been configured.
	 * 
	 * @return
	 */
	public String getAfricanAmericanHistoryUrl();
	/**
	 * Obtains the URL for use in the Business and Finance module. Will
	 * return <code>null</code> if none has been configured.
	 * 
	 * @return
	 */
	public String getBusinessAndFinanceUrl();
	/**
	 * Obtains the URL for use in the Health and Wellness module. Will
	 * return <code>null</code> if none has been configured.
	 * 
	 * @return
	 */
	public String getHealthAndWellnessUrl();
	/**
	 * Obtains the URL for use in the In the News module. Will
	 * return <code>null</code> if none has been configured.
	 * 
	 * @return
	 */
	public String getInTheNewsUrl();
	/**
	 * Obtains the URL for use in the Language Arts module. Will
	 * return <code>null</code> if none has been configured.
	 * 
	 * @return
	 */
	public String getLanguageArtsUrl();
	/**
	 * Obtains the URL for use in the Science module. Will
	 * return <code>null</code> if none has been configured.
	 * 
	 * @return
	 */
	public String getScienceUrl();
	/**
	 * Obtains the URL for use in the Social Studies module. Will
	 * return <code>null</code> if none has been configured.
	 * 
	 * @return
	 */
	public String getSocialStudiesUrl();
	/**
	 * Obtains the URL for use in the US History module. Will
	 * return <code>null</code> if none has been configured.
	 * 
	 * @return
	 */
	public String getUsHistoryUrl();
	/**
	 * Obtains the URL for use in the Women's History module. Will
	 * return <code>null</code> if none has been configured.
	 * 
	 * @return
	 */
	public String getWomensHistoryUrl();
}
