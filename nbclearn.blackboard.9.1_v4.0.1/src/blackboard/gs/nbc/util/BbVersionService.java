/**
 * Ryan Hardy
 * Guilford Group
 * 
 * BbVersionService.java
 * Created: Jun 13, 2012
 */
package blackboard.gs.nbc.util;

/**
 * Simple service used to obtain information about the version and other
 * information from the underlying Blackboard installation.
 * 
 * @author Ryan Hardy (rhardy@guilfordgroup.com)
 */
public interface BbVersionService {
	/**
	 * Returns the full version number string (i.e. '9.1.70081.25').
	 * @return
	 */
	public String getFullVersion();
	/**
	 * Returns the major version portion of the version number (i.e. in
	 * '9.1.70081.25' it would return 9).
	 * @return
	 */
	public int getMajorVersion();
	/**
	 * Returns the minor version portion of the version number (i.e. in
	 * '9.1.70081.25' it would return 1).
	 * @return
	 */
	public int getMinorVersion();
	/**
	 * Returns the service pack portion of the version number (i.e. in
	 * '9.1.70081.25' it would return '70081.25').
	 * @return
	 */
	public String getFullServicePack();
	/**
	 * Returns the simple service pack portion of the version number (i.e. in
	 * '9.1.70081.25' it would return 7).
	 * @return
	 */
	public String getServicePack();
	/**
	 * Convenience method used to determine if the underlying Blackboard
	 * installation has the given Service Pack installed.
	 * 
	 * @param servicePack
	 * @return
	 */
	//public boolean isSP(int servicePack);
	/**
	 * Convenience method used to determine if the underlying Blackboard
	 * installation has a Service Pack installed greater than number given.
	 * 
	 * @param servicePack
	 * @return
	 */
	//public boolean isGreaterThanSP(int servicePack); 
	/**
	 * Convenience method used to determine if the underlying Blackboard
	 * installation has a Service Pack installed less than number given.
	 * 
	 * @param servicePack
	 * @return
	 */
	//public boolean isLessThanSP(int servicePack);
	/**
	 * Obtains the name of the current Blackboard Theme that is active.
	 * 
	 * @return
	 */
	public String getCurrentThemeName();
}