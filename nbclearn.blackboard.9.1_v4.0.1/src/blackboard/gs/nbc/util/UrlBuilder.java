/**
 * Ryan Hardy
 * Guilford Group
 * 
 * UrlBuilder.java
 * Created: Jun 14, 2012
 */
package blackboard.gs.nbc.util;

import net.sourceforge.stripes.util.StringUtil;

/**
 * Simple helper class used to build URLs, properly performing URL encoding on 
 * query string params.
 * 
 * @author Ryan Hardy (rhardy@guilfordgroup.com)
 */
public class UrlBuilder {
	//TODO: this is a watered-down version of Stripes' UrlBuilder...but the plan
	//is to remove Stripes dependency...so this will do
	
	private StringBuilder url;
	// flag indicating if one param has been added yet
	private boolean hasParam = false;
	
	/**
	 * 
	 * @param baseUrl
	 */
	public UrlBuilder(String baseUrl) {
		this.url = new StringBuilder(baseUrl);
	}
	
	/**
	 * Adds the param to the query string, properly encoding the value first.
	 * 
	 * @param param
	 * @param value
	 * @return
	 */
	public UrlBuilder addParam(String param, String value) {
		return addParam(param, value, true);
	}
	
	/**
	 * Adds the param to the query string, properly encoding the value first if
	 * <code>encodeValue</code> is <code>true</code>.
	 * 
	 * @param param
	 * @param value
	 * @param encodeValue
	 * @return
	 */
	public UrlBuilder addParam(String param, String value, boolean encodeValue) {
		if (!this.hasParam) {
			this.url.append("?");
			this.hasParam = true;
		} else {
			this.url.append("&");			
		}
		this.url.append(param).append("=");
		value = (value == null) ? "" : value;
		String encoded = (encodeValue) ? StringUtil.urlEncode(value) : value;
		this.url.append(encoded);
		
		return this;
	}
	
	/**
	 * Obtains the current URL.
	 * @return
	 */
	public String getUrl() {
		return this.url.toString();
	}
	
    @Override
	public String toString() {
		return this.getUrl();
	}
    
    public static void main(String[] args) {
    	String good = "http%3A%2F%2Fec2-23-20-1-95.compute-1.amazonaws.com%2Fwebapps%2Fbbgs-nbc-content-integration-BBLEARN%2Freq_con.action%3Fcourse_id%3D_21_1%26action%3Dcreate%26content_id%3D_22_1%26mode%3Dembed%26bbsession_id%3DoJCqWLR8XsGetV1MABQph-XBUPz42VLNdG3RzZ4NcNIP1I3n3SqcTQ";
//    	String goodRet = "http://ec2-174-129-15-105.compute-1.amazonaws.com/webapps/bbgs-nbc-content-integration-BBLEARN/req_con.action?course_id=_21_1&action=create&content_id=_22_1&mode=embed&bbsession_id=TMVNViV34f3hXVK0RHe21RkxrV_A5IIXRv97M4aHyl2G4OfAMiw4uA";
    	good = displayUrl(good);
    	String bad = "http%3A%2F%2Fec2-23-20-1-95.compute-1.amazonaws.com%2Fwebapps%2Fbbgs-nbc-content-integration-BBLEARN%2Freq_con.action%3Fcourse_id%3D_21_1%26action%3Dcreate%26content_id%3D_22_1%26mode%3Dembed%26bbsession_id%3DoJCqWLR8XsGetV1MABQph-XBUPz42VLNdG3RzZ4NcNIP1I3n3SqcTQ%0D%0A";
    	//String badRet = "http://ec2-23-20-1-95.compute-1.amazonaws.com/webapps/bbgs-nbc-content-integration-BBLEARN/req_con.action?course_id=_21_1&action=create&content_id=_22_1&mode=embed&bbsession_id=oJCqWLR8XsGetV1MABQph-XBUPz42VLNdG3RzZ4NcNIP1I3n3SqcTQ";
    	bad = displayUrl(bad);
    }

	private static String displayUrl(String str) {
		str = StringUtil.urlDecode(str);
    	String[] c = str.split("\\?");
    	str = c[0];
    	for (String nvp : c[1].split("&")) {
    		String[] comps = nvp.split("=");
    	}
		return str;
	}
}