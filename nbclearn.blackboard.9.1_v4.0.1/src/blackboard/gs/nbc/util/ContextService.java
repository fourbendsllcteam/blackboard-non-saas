/**
 * Ryan Hardy
 * Guilford Group
 * 
 * ContextService.java
 * Created: Jun 14, 2012
 */
package blackboard.gs.nbc.util;

/**
 * Provides access to Blackboard context info (i.e. user and course information).
 * 
 * @author Ryan Hardy (rhardy@guilfordgroup.com)
 */
public interface ContextService {
	/**
	 * Returns the currently authenticated user's ID.
	 * 
	 * @return
	 * @throws ContextServiceException if any errors occur in obtaining context info
	 */
	public String getUserId() throws ContextServiceException;
	/**
	 * Returns the currently authenticated user's username.
	 * 
	 * @return
	 * @throws ContextServiceException if any errors occur in obtaining context info
	 */
	public String getUsername() throws ContextServiceException;
	/**
	 * Returns the currently authenticated user's email.
	 * 
	 * @return
	 * @throws ContextServiceException if any errors occur in obtaining context info
	 */
	public String getUserEmail() throws ContextServiceException;
	/**
	 * Returns the currently authenticated user's role.
	 * 
	 * @return
	 * @throws ContextServiceException if any errors occur in obtaining context info
	 */
	public String getUserRole() throws ContextServiceException;
	/**
	 * 
	 * @return
	 * @throws ContextServiceException if any errors occur in obtaining context info
	 */
	public String getUserFirstName() throws ContextServiceException;
	/**
	 * 
	 * @return
	 * @throws ContextServiceException if any errors occur in obtaining context info
	 */
	public String getUserLastName() throws ContextServiceException;
	/**
	 * Obtains the current course ID. Returns <code>null</code> if no Course is
	 * present in context.
	 * 
	 * @return
	 * @throws ContextServiceException if any errors occur in obtaining context info
	 */
	public String getCourseId() throws ContextServiceException;
	/**
	 * Obtains the current course name, or <code>null</code> if no Course is 
	 * present in context.
	 * 
	 * @return
	 * @throws ContextServiceException if any errors occur in obtaining context info
	 */
	public String getCourseName() throws ContextServiceException;
	
}
