/**
 * Ryan Hardy
 * Guilford Group
 * 
 * DefaultCueCardConfigImpl.java
 * Created: May 14, 2012
 */
package blackboard.gs.nbc.util.impl;

import static blackboard.gs.nbc.util.TemplateRenderUtils.renderTemplate;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;

import blackboard.data.registry.SystemRegistryEntry;
import blackboard.gs.nbc.Constants;
import blackboard.gs.nbc.config.EnvironmentConfiguration;
import blackboard.gs.nbc.context.NBCAppContext;
import blackboard.gs.nbc.playlist.data.video.Video;
import blackboard.gs.nbc.playlist.data.video.VideoType;
import blackboard.gs.nbc.util.ContextService;
import blackboard.gs.nbc.util.ContextServiceException;
import blackboard.gs.nbc.util.CueCardConfiguration;
import blackboard.gs.nbc.util.SecurityTokenManager;
import blackboard.gs.nbc.util.TemplateRenderUtils;
import blackboard.gs.nbc.util.UrlBuilder;
import blackboard.persist.KeyNotFoundException;
import blackboard.persist.PersistenceException;
import blackboard.persist.registry.SystemRegistryEntryDbLoader;
import blackboard.platform.log.LogServiceFactory;
import blackboard.platform.security.SecurityUtil;
import blackboard.util.IOUtil;

/**
 * Implementation of {@link CueCardConfiguration} that, by default, relies upon
 * original hard-coded implementations of {@link EnvironmentConfiguration} and
 * {@link SecurityTokenManager}.
 * 
 * @author Ryan Hardy
 */
public class DefaultCueCardConfigImpl implements CueCardConfiguration {
	private static final Logger logger = Logger.getLogger(DefaultCueCardConfigImpl.class);

	/**
	 * The default path for obtaining the nbcFilesLoader.js script content.
	 */
	// TODO: externalize this (?)
	private static final String NBC_FILES_LOADER_SCRIPT_PATH = "/nbcFilesLoader.js";

	private final NBCAppContext appContext;
	private final EnvironmentConfiguration envConfig;
	private final SecurityTokenManager securityUtil;
	private final ContextService contextService;

	/**
	 * The absolute URL to the NBC Peacock image.
	 */
	private final String PEACOCK_IMAGE;
	private final String PLAY_IMAGE;

	/**
	 * 
	 * @param appContext
	 */
	public DefaultCueCardConfigImpl(NBCAppContext appContext) {
		this.appContext = appContext;
		this.envConfig = appContext.getEnvironmentConfiguration();
		this.securityUtil = appContext.getSecurityTokenManager();
		this.contextService = appContext.getContextService();

		PEACOCK_IMAGE = envConfig.getFilePath("images/nbc_logo_mashup.gif");
		PLAY_IMAGE = envConfig.getFilePath("images/play-icon.png");
	}

	private String getNBCFilesLoaderScriptContent() {
		String rendered = "";
		InputStream is = null;
		InputStreamReader reader = null;
		StringWriter writer = null;
		try {
			String scriptPath = appContext.getAttribute("nbcFilesLoaderScriptPath");
			/*logger.debug("Script path: " + scriptPath);*/
			scriptPath = (scriptPath == null) ? NBC_FILES_LOADER_SCRIPT_PATH : scriptPath;
			/*logger.debug("Script path (after check): " + scriptPath);*/

			is = DefaultCueCardConfigImpl.class.getResourceAsStream(NBC_FILES_LOADER_SCRIPT_PATH);
			reader = new InputStreamReader((InputStream) is);
			writer = new StringWriter();
			IOUtil.writeReaderToWriter(reader, writer);
			writer.flush();
			String scriptContent = writer.toString();

			// need to add newlines to account for opening doc comments
			scriptContent = "\n\n" + scriptContent;

			Map<String, String> replText = new HashMap<String, String>();
			replText.put(Constants.NBC_VI, envConfig.getVi());

			rendered = TemplateRenderUtils.renderTemplateComponent(scriptContent, replText);
		} catch (Exception e) {
			logger.error("Could not read in script content.", e);
		} finally {
			// IOUtil.silentClose(is);
			// IOUtil.silentClose(reader);
			// IOUtil.silentClose(writer);

			silentClose(is);
			silentClose(reader);
			silentClose(writer);
		}
		return rendered;
	}

	private void silentClose(StringWriter writer) {
		try {
			if (null != writer) {
				writer.close();
			}
		} catch (Exception e) {
			LogServiceFactory.getInstance().logDebug("Failed to close resource", e);
		}

	}

	private void silentClose(InputStreamReader reader) {
		try {
			if (null != reader) {
				reader.close();
			}
		} catch (Exception e) {
			LogServiceFactory.getInstance().logDebug("Failed to close resource", e);
		}

	}

	private void silentClose(InputStream reader) {
		try {
			if (null != reader) {
				reader.close();
			}
		} catch (Exception e) {
			LogServiceFactory.getInstance().logDebug("Failed to close resource", e);
		}

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String renderDescription(Video video, String embedCode) {
		Map<String, String> replText = new HashMap<String, String>();
		replText.put(Constants.NBC_CUECARD_DESCRIPTION, video.getDescription());
		replText.put(Constants.NBC_CUECARD_TYPE_IMAGE, video.getContentTypeImage());
		replText.put(Constants.NBC_CUECARD_AIRDATE, video.getAirDateInString());
		replText.put(Constants.NBC_CUECARD_CONTENT, video.getCueCardCourseContent());
		replText.put(Constants.NBC_CUECARD_IFRAMESOURCE, embedCode);
		replText.put(Constants.NBC_CUECARD_ID, video.getVideoId());
		addAltAttributeValues(video, replText);

		String rendered = renderTemplate("/" + Constants.TEMPLATE_CONTENT_DESCRIPTION, replText,
				"There was an issue while rendering content description.");

		return rendered;
	}

	/**
	 * 
	 * @param cueCardId
	 * @return
	 */
	@Override
	public String renderStaticCueCard(String cueCardId) {
		Map<String, String> replText = new HashMap<String, String>();
		replText.put(Constants.NBC_CUECARD_ID, cueCardId);
		replText.put(Constants.NBC_SECURITY_TOKEN, securityUtil.loadSecurityToken());
		replText.put(Constants.NBC_URL, envConfig.getNbcUrl());
		replText.put(Constants.NBC_VI, envConfig.getVi());
		addUserContextValues(replText);

		String rendered = renderTemplate("/" + Constants.TEMPLATE_CUECARD_STATIC, replText,
				"There was an issue while rendering a static Cue Card.");

		return rendered;
	}

	/**
	 * 
	 * @return
	 */
	@Override
	public String renderFloatingCueCardIncludes() {
		Map<String, String> replText = new HashMap<String, String>();
		replText.put(Constants.NBC_SECURITY_TOKEN, securityUtil.loadSecurityToken());
		replText.put(Constants.NBC_URL, envConfig.getNbcUrl());
		replText.put(Constants.NBC_VI, envConfig.getVi());
		replText.put(Constants.NBC_FILES_LOADER_SCRIPT, this.getNBCFilesLoaderScriptContent());

		addUserContextValues(replText);

		String rendered = renderTemplate("/" + Constants.TEMPLATE_CUECARD_FLOAT_INCLUDES, replText,
				"There was an issue while rendering the Cue Card includes template.");

		return rendered;
	}

	/**
	 * 
	 * @return
	 * @deprecated Call {@link #renderFloatingCueCardIncludes()} instead.
	 */
	@Deprecated
	public String initPlaylistCueCards() {
		return renderFloatingCueCardIncludes();
	}

	/**
	 * 
	 * @param cardId
	 * @param thumbnailUrl
	 * @return
	 */
	@Override
	public String renderPlaylistCueCard(Video video) {
		String cardId = video.getVideoId();
		String thumbnailUrl = video.getThumbnailUrl();

		Map<String, String> replText = new HashMap<String, String>();
		replText.put(Constants.NBC_THUMBNAIL, thumbnailUrl);
		replText.put(Constants.NBC_CUECARD_URL, getCueCardUrl(cardId));
		replText.put(Constants.NBC_CUECARD_ID, cardId);
		replText.put(Constants.NBC_CUECARD_TYPE_IMAGE, video.getContentTypeImage());
		addAltAttributeValues(video, replText);
		addUserContextValues(replText);

		String rendered = renderTemplate("/" + Constants.TEMPLATE_CUECARD_FLOAT_BODY, replText,
				"There was an issue while rendering a static floating Cue Card body.");

		return rendered;
	}

	/**
	 * 
	 * @param cardId
	 * @param thumbnailUrl
	 * @return
	 */
	@Override
	public String renderCourseContentCueCard(Video video) {
		String cardId = video.getVideoId();
		String thumbnailUrl = video.getThumbnailUrl();

		Map<String, String> replText = new HashMap<String, String>();
		replText.put(Constants.NBC_SECURITY_TOKEN, securityUtil.loadSecurityToken());
		replText.put(Constants.NBC_THUMBNAIL, thumbnailUrl);
		replText.put(Constants.NBC_URL, envConfig.getNbcUrl());
		replText.put(Constants.NBC_CUECARD_URL, getCueCardUrl(cardId));
		replText.put(Constants.NBC_VI, envConfig.getVi());
		replText.put(Constants.NBC_CUECARD_ID, cardId);

		addAltAttributeValues(video, replText);
		addUserContextValues(replText);

		String rendered = renderTemplate("/" + Constants.TEMPLATE_CUECARD_COURSE_CONTENT, replText,
				"There was an issue while rendering a Course Content Cue Card.");

		return rendered;
	}

	/**
	 * 
	 * @param video
	 * @return
	 */
	@Override
	public String renderMashupCueCardThumbnail(Video video) {
		/*logger.debug("Inside renderMashupCueCardThumbnail---->");*/
		final String cardId = video.getVideoId();
		try {
			Map<String, String> replText = new HashMap<String, String>();
			replText.put(Constants.NBC_URL, envConfig.getNbcUrl());
			replText.put(Constants.NBC_THUMBNAIL, video.getThumbnailUrl());
			/*replText.put(Constants.NBC_PEACOCK_IMAGE, PEACOCK_IMAGE);
			replText.put(Constants.NBC_CUECARD_TYPE_IMAGE, video.getContentTypeImage());*/
			
			replText.put(Constants.NBC_PEACOCK_IMAGE,"https://static.nbclearn.com/files/highered/site/download/blackboard/install/nbc_logo_mashup.gif");
			
			if (video.getType().equalsIgnoreCase("I")) {
				replText.put(Constants.NBC_CUECARD_TYPE_IMAGE,
						"https://static.nbclearn.com/files/highered/site/download/blackboard/install/photo.gif");
			} else if (video.getType().equalsIgnoreCase("D") || video.getType().equalsIgnoreCase("T")) {
				replText.put(Constants.NBC_CUECARD_TYPE_IMAGE,
						"https://static.nbclearn.com/files/highered/site/download/blackboard/install/text.gif");
			} else {
				replText.put(Constants.NBC_CUECARD_TYPE_IMAGE,
						"https://static.nbclearn.com/files/highered/site/download/blackboard/install/video.gif");
			}
			
			
			replText.put(Constants.NBC_CUECARD_TITLE, video.getName());
			replText.put(Constants.NBC_CUECARD_AIRDATE, video.getAirDateInString());
			replText.put(Constants.NBC_CUECARD_DURATION, video.getDuration());
			replText.put(Constants.NBC_VI, envConfig.getVi());
			replText.put(Constants.NBC_CUECARD_IFRAMESOURCE, video.getIframeSource());
			replText.put(Constants.NBC_PLAY_ICON, "https://static.nbclearn.com/files/highered/site/download/blackboard/install/play-icon.png");

			addAltAttributeValues(video, replText);
			/*logger.debug("After addAltAttributeValues");*/

			replText.put(Constants.NBC_CUECARD_ID, cardId);
			/*logger.debug("After NBC_CUECARD_ID");*/
			addUserContextValues(replText);

			for (Map.Entry<String, String> entry : replText.entrySet()) {
				/*logger.debug("KEY--->" + entry.getKey() + "<------>VALUE--->" + entry.getValue());*/
			}
			/*logger.debug("After map");*/
			String rendered = renderTemplate("/" + Constants.TEMPLATE_MASHUP_CUECARD_THUMBNAIL, replText,
					"There was an issue while rendering a Mashup Cue Card.");

			/*logger.debug("rendered---->" + rendered);*/
			return rendered;
		} catch (Exception e) {
			logger.error("Exception---->" + e.getMessage());
			e.printStackTrace();
			logger.error("Exception---->" + e.getCause());
		}
		return "";
	}

	/**
	 * Renders the mashup script template.
	 * 
	 * @param video
	 * @return
	 */
	@Override
	public String renderMashupScript() {
		Map<String, String> replText = new HashMap<String, String>();
		replText.put(Constants.NBC_URL, envConfig.getNbcUrl());
		replText.put(Constants.NBC_VI, envConfig.getVi());
		replText.put(Constants.NBC_FILES_LOADER_SCRIPT, this.getNBCFilesLoaderScriptContent());
		addUserContextValues(replText);

		String rendered = renderTemplate("/" + Constants.TEMPLATE_MASHUP_CUECARD_SCRIPT, replText,
				"There was an issue while rendering a Mashup Cue Card script.");

		return rendered;
	}

	/**
	 * 
	 * @param video
	 * @return
	 */
	@Override
	public String renderContentModule(Video video) {
		final String cardId = video.getVideoId();
		final String REGISTRY_KEY = "gs-nbc-token";
		String securityToken = "";
		String iframeContent = "";

		Map<String, String> replText = new HashMap<String, String>();
		replText.put(Constants.NBC_URL, envConfig.getNbcUrl());
		replText.put(Constants.NBC_SECURITY_TOKEN, securityUtil.loadSecurityToken());
		replText.put(Constants.NBC_THUMBNAIL, video.getThumbnailUrl());
		replText.put(Constants.NBC_CUECARD_URL, getCueCardUrl(cardId));
		replText.put(Constants.NBC_PEACOCK_IMAGE, PEACOCK_IMAGE);
		replText.put(Constants.NBC_CUECARD_TYPE_IMAGE, video.getContentTypeImage());
		replText.put(Constants.NBC_CUECARD_TITLE, video.getName());
		replText.put(Constants.NBC_CUECARD_AIRDATE, video.getAirDateInString());
		replText.put(Constants.NBC_CUECARD_DURATION, video.getDuration());
		replText.put(Constants.NBC_VI, envConfig.getVi());
		replText.put(Constants.NBC_INCLUDES, renderMashupScript());
		replText.put(Constants.NBC_CUECARD_ID, cardId);


		if (video.getIframeContent() != null) {
			if (video.getIframeContent().contains("width: 100%")) {
				iframeContent = video.getIframeContent().replace("width: 100%", "width: 475px; margin-left: -22px");
			}

		} else {
			SystemRegistryEntryDbLoader sysRegistryLoader = null;
			SystemRegistryEntry sysRegEntry = null;
			try {
				sysRegistryLoader = SystemRegistryEntryDbLoader.Default.getInstance();
				try {
					sysRegEntry = sysRegistryLoader.loadByKey(REGISTRY_KEY);
				} catch (KeyNotFoundException knfe) {
					knfe.printStackTrace();
				}
			} catch (PersistenceException pe) {
				logger.error("Error loading sys reg loader.");
				throw new RuntimeException("Error loading sys reg loader.", pe);
			}
			if (null != sysRegEntry) {
				securityToken = sysRegEntry.getValue();
			}

			String pathVariable = "";
			String cuecardUrl = getCueCardUrl(cardId);
			String emailId = cuecardUrl.substring(cuecardUrl.indexOf("email=") + 6);
			emailId = emailId.substring(0, emailId.indexOf("&"));
			String role = cuecardUrl.substring(cuecardUrl.indexOf("role=") + 5);
			role = role.substring(0, role.indexOf("&"));
			pathVariable += securityToken + "&E=" + emailId + "&r=" + role;

			// Get Security Token
			try {
				securityToken = sendPost(appContext.getEnvironmentConfiguration().getSecurityTokenUrl() + pathVariable,
						new ArrayList<NameValuePair>());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			iframeContent = "<iframe style=\"width:475px; height: 336px; margin-left: -22px;\" scrolling=\"no\" src=\""
					+ appContext.getEnvironmentConfiguration().getEmbedPlayerUrl() + securityToken + "/"
					+ video.getVideoId() + "\" allowfullscreen></iframe>";
		}
		replText.put("iframeUrl", iframeContent);

		addAltAttributeValues(video, replText);

		addUserContextValues(replText);

		String rendered = renderTemplate("/" + Constants.TEMPLATE_CONTENT_MODULE, replText,
				"There was an issue while rendering a Mashup Cue Card.");

		return rendered;
	}

	private String sendPost(String url, List<NameValuePair> nvps) throws Exception {

		try {

			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			// add reuqest header
			con.setRequestMethod("POST");

			StringBuffer urlParameters = new StringBuffer();

			if (!nvps.isEmpty()) {
				for (NameValuePair nameValuePair : nvps) {
					urlParameters.append(nameValuePair.getName() + "=" + nameValuePair.getValue() + "&");
				}
			}
			if (urlParameters != null && urlParameters.length() > 0) {
				urlParameters = urlParameters.deleteCharAt(urlParameters.toString().length() - 1);
			}

			// Send post request
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(urlParameters.toString());
			wr.flush();
			wr.close();

			int responseCode = con.getResponseCode();

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result

			return response.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String renderResolverScript() {
		Map<String, String> replText = new HashMap<String, String>();
		replText.put(Constants.NBC_URL, this.appContext.getEnvironmentConfiguration().getNbcUrl());
		replText.put(Constants.NBC_VI, this.appContext.getEnvironmentConfiguration().getVi());
		replText.put(Constants.NBC_FILES_LOADER_SCRIPT, this.getNBCFilesLoaderScriptContent());

		addUserContextValues(replText);

		String rendered = TemplateRenderUtils.renderTemplate("/" + Constants.TEMPLATE_RESOLVER_SCRIPT, replText,
				"Could not render the NBCResolver script.");

		return rendered;
	}

	/*
	 * =========================================================================
	 * ========= Private helper methods
	 * =========================================================================
	 * =========
	 */

	/**
	 * Adds values for all the image ALT attribute values.
	 * 
	 * @param video
	 * @param replText
	 */
	private void addAltAttributeValues(Video video, Map<String, String> replText) {
		final String thumbnailAlt = StringEscapeUtils.escapeHtml((video == null) ? "" : video.getName());
		final String vTypeAlt = (video == null || video.getVideoType() == null) ? VideoType.UNKNOWN.getName()
				: video.getVideoType().getName();

		/*logger.debug("Adding ALT attribute values for Video: " + video);
		logger.debug("thumbnail ALT: " + thumbnailAlt);
		logger.debug("video type ALT: " + vTypeAlt);*/
		if (thumbnailAlt.isEmpty()) {
			/*logger.debug("=========================================================");*/
			/*if (logger.isTraceEnabled()) {
				logger.debug("ATTENTION: Video ALT value is empty", new Exception()); // create
																						// exception
																						// to
																						// get
																						// stacktrace
																						// printed
																						// to
																						// log
			} else if (logger.isDebugEnabled()) {
				logger.debug("ATTENTION: Video ALT value is empty");
			}*/
		}

		replText.put(Constants.NBC_THUMBNAIL_ALT, thumbnailAlt);
		replText.put(Constants.NBC_PEACOCK_IMAGE_ALT, "NBC Learn");
		replText.put(Constants.NBC_CUECARD_TYPE_IMAGE_ALT, vTypeAlt);
	}

	/**
	 * Adds user context info to the replacement map (including user email, user
	 * role, and course name). <br />
	 * <br />
	 * If any error occur in getting context info, puts empty string in map.
	 * 
	 * @param replText
	 */
	private void addUserContextValues(Map<String, String> replText) {
		try {
			replText.put(Constants.CTX_VAR_USER_ID, getValue(contextService.getUserId(), ""));
			replText.put(Constants.CTX_VAR_USER_EMAIL, getValue(contextService.getUserEmail(), ""));
			replText.put(Constants.CTX_VAR_USER_ROLE, getValue(contextService.getUserRole(), ""));
			replText.put(Constants.CTX_VAR_COURSE_ID, getValue(contextService.getCourseId(), ""));
			replText.put(Constants.CTX_VAR_COURSE_NAME, getValue(contextService.getCourseName(), ""));
		} catch (ContextServiceException e) {
			logger.error("Could not add context info to template replacement text map.", e);
			replText.put(Constants.CTX_VAR_USER_EMAIL, "");
			replText.put(Constants.CTX_VAR_USER_ROLE, "");
			replText.put(Constants.CTX_VAR_COURSE_NAME, "");
		}
	}

	/**
	 * Simple utility used to to return value if it is non-null, and default
	 * value otherwise.
	 * 
	 * @param value
	 *            the primary value
	 * @param defValue
	 *            the default value
	 * @return
	 */
	private String getValue(String value, String defValue) {
		if (value != null) {
			return value;
		}
		return defValue;
	}

	/**
	 * Assembles the url for opening a CueCard, using the given
	 * <code>cueCardId</code>.
	 * 
	 * @param cueCardId
	 * @return
	 */
	private String getCueCardUrl(String cueCardId) {
		UrlBuilder urlBldr = new UrlBuilder(envConfig.getCuecardUrl());
		urlBldr.addParam("cuecard", cueCardId);
		try {
			urlBldr.addParam("email", contextService.getUserEmail());
		} catch (ContextServiceException e) {
			logger.error("Could not add user email to cue card url.", e);
			urlBldr.addParam("email", "");
		}
		try {
			urlBldr.addParam("role", contextService.getUserRole());
		} catch (ContextServiceException e) {
			logger.error("Could not add user role to cue card url.", e);
			urlBldr.addParam("role", "");
		}
		try {
			urlBldr.addParam("courseName", contextService.getCourseName());
		} catch (ContextServiceException e) {
			logger.error("Could not add course name to cue card url.", e);
			urlBldr.addParam("courseName", "");
		}

		String cueCardUrl = urlBldr.getUrl();
		/*if (logger.isDebugEnabled()) {
			logger.debug("CueCard URL: '" + cueCardUrl + "'");
		}*/

		return cueCardUrl;
	}

}