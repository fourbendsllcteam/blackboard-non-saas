/**
 * Ryan Hardy
 * Guilford Group
 * 
 * SessionCachingBbPlatformSvcImpl.java
 * Created: Jun 7, 2013
 */
package blackboard.gs.nbc.util.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import blackboard.gs.nbc.context.NBCAppContext;
import blackboard.gs.nbc.util.BbPlatformService;

/**
 * {@link BbPlatformService} implementation used to wrap another instance,
 * and delegate to it ONLY on first call, utilizing an {@link HttpSession}
 * instance to cache results.
 * 
 * @author Ryan Hardy (rhardy@guilfordgroup.com)
 */
public class SessionCachingBbPlatformSvcImpl implements BbPlatformService {
	private static final Logger logger = Logger.getLogger(SessionCachingBbPlatformSvcImpl.class);
	
	private final BbPlatformService platformService;
	private final NBCAppContext appContext;

	/**
	 * Creates an instance that wraps a {@link DefaultBbPlatformServiceImpl}.
	 * 
	 * @param appContext 
	 */
	public SessionCachingBbPlatformSvcImpl(NBCAppContext appContext) {
		this(new DefaultBbPlatformServiceImpl(appContext), appContext);
	}

	/**
	 * Creates a new instance wrapping the given instance.
	 * 
	 * @param platformService
	 * @param appContext 
	 */
	public SessionCachingBbPlatformSvcImpl(BbPlatformService platformService, NBCAppContext appContext) {
		/*if (logger.isTraceEnabled()) {
			logger.trace("Creating new instance of SessionCachingBbPlatformSvcImpl.");
			logger.trace("Wrapped BbPlatformService: " + platformService);
		}*/
		this.platformService = platformService;
		this.appContext = appContext;
		HttpServletRequest req = (HttpServletRequest) this.appContext.getRequestObject();
		if (req == null) {
			throw new IllegalStateException("Can not create SessionCachingBbPlatformSvcImpl instance; no HttpServletRequest is available.");
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getUriStem() {
		return getValue(new ValueFetcher<String>() {
			@Override
			public String getValueName() {
				return "uriStem";
			}

			@Override
			public String getValue() {
				return SessionCachingBbPlatformSvcImpl.this.platformService.getUriStem();
			}
		});
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getHostName() {
		return getValue(new ValueFetcher<String>() {
			@Override
			public String getValueName() {
				return "hostName";
			}

			@Override
			public String getValue() {
				return SessionCachingBbPlatformSvcImpl.this.platformService.getHostName();
			}
		});
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isForcedSystemSSL() {
		return getValue(new ValueFetcher<Boolean>() {
			@Override
			public String getValueName() {
				return "isForcedSystemSSL";
			}

			@Override
			public Boolean getValue() {
				return SessionCachingBbPlatformSvcImpl.this.platformService.isForcedSystemSSL();
			}
		});
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getUUID() {
		return getValue(new ValueFetcher<String>() {
			@Override
			public String getValueName() {
				return "uuid";
			}

			@Override
			public String getValue() {
				return SessionCachingBbPlatformSvcImpl.this.platformService.getUUID();
			}
		});
	}
	
	/**
	 * Gets value by checking session first, and then delegating to {@link ValueFetcher}
	 * method if no value yet exists.
	 * 
	 * @param fetcher
	 * @return
	 */
	private <T> T getValue(ValueFetcher<T> fetcher) {
		final String att = "blackboard.gs.nbc.util.impl.SessionCachingBbPlatformSvcImpl-" + fetcher.getValueName();
		/*logger.debug("Checking session for attribute: '" + att + "'");*/
		HttpServletRequest request = (HttpServletRequest) this.appContext.getRequestObject();
		HttpSession session = request.getSession();
		@SuppressWarnings("unchecked")
		T value = (T) session.getAttribute(att);
		if (value == null) {
			/*logger.debug("Session value is null, pulling from nested BbPlatformService.");*/
			value = fetcher.getValue();
			/*logger.debug("Obtained value, " + att + "=" + value);*/
			session.setAttribute(att, value);
		} else {
			/*logger.debug("Value found in session, returning cached value.");*/
		}
		return value;
	}
	
	private interface ValueFetcher<T> {
		String getValueName();
		T getValue();
	}

}
