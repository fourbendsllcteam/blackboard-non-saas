/**
 * Ryan Hardy
 * Guilford Group
 * 
 * DefaultSecurityTokenMgrImpl.java
 * Created: May 14, 2012
 */
package blackboard.gs.nbc.util.impl;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import blackboard.data.ValidationException;
import blackboard.data.registry.SystemRegistryEntry;
import blackboard.data.user.User;
import blackboard.gs.nbc.Constants;
import blackboard.gs.nbc.context.NBCAppContext;
import blackboard.gs.nbc.context.NBCAppContextFactory;
import blackboard.gs.nbc.playlist.service.PlaylistManagerService;
import blackboard.gs.nbc.security.DesEncrypter;
import blackboard.gs.nbc.util.SecurityTokenManager;
import blackboard.persist.KeyNotFoundException;
import blackboard.persist.PersistenceException;
import blackboard.persist.registry.SystemRegistryEntryDbLoader;
import blackboard.persist.registry.SystemRegistryEntryDbPersister;
import blackboard.platform.context.Context;
import blackboard.platform.context.ContextManager;
import blackboard.platform.context.ContextManagerFactory;

/**
 * Original implementation of {@link SecurityTokenManager}, dependent upon
 * underlying Blackboard platform.
 * 
 * @author Ryan Hardy
 */
public class DefaultSecurityTokenMgrImpl implements SecurityTokenManager {
	private static final Logger logger = Logger.getLogger(DefaultSecurityTokenMgrImpl.class);

	private static final String REGISTRY_KEY = "gs-nbc-token";
	private static final String ACTIVATION_TOKEN_SALT = ".secret.nbcu.aod.security.token.salt";
	private static final DesEncrypter encr = new DesEncrypter(ACTIVATION_TOKEN_SALT);

	private final NBCAppContext appContext;

	/**
	 * @param appContext
	 */
	public DefaultSecurityTokenMgrImpl(NBCAppContext appContext) {
		this.appContext = appContext;
	}

	@Override
	public String loadSecurityToken() {

		String securityToken = "";

		SystemRegistryEntryDbLoader sysRegistryLoader = null;
		SystemRegistryEntry sysRegEntry = null;
		try {
			sysRegistryLoader = SystemRegistryEntryDbLoader.Default.getInstance();
			try {
				sysRegEntry = sysRegistryLoader.loadByKey(REGISTRY_KEY);
			} catch (KeyNotFoundException knfe) {
				// could not find token in sys registry
			}
		} catch (PersistenceException pe) {
			logger.error("Error loading sys reg loader.");
			throw new RuntimeException("Error loading sys reg loader.", pe);
		}
		if (!isGuestAccess()) {
			if (null != sysRegEntry) {
				securityToken = sysRegEntry.getValue();
				securityToken = encr.encrypt(securityToken);
				securityToken = securityToken.trim();
			}
		}
		return securityToken;
	}

	@Override
	public void saveSecurityToken(String value, NBCAppContext nbcAppContext) {
		SystemRegistryEntryDbPersister sysRegistryPersist = null;
		try {
			String securityDescription = loadSecurityTokenDecsription(nbcAppContext);
			sysRegistryPersist = SystemRegistryEntryDbPersister.Default.getInstance();
			SystemRegistryEntry sysRegEntry = new SystemRegistryEntry();
			sysRegEntry.setKey(REGISTRY_KEY);
			sysRegEntry.setValue(value);
			sysRegEntry.setCanBeModified(true);
			sysRegEntry.setDescription(securityDescription);

			try {
				sysRegEntry.validate();
				sysRegistryPersist.deleteByKey(REGISTRY_KEY);
				sysRegistryPersist.persist(sysRegEntry);
			} catch (ValidationException ve) {
				logger.error("Error validating sys registry entry.");
				throw new RuntimeException("Error validating sys registry entry.", ve);
			}
		} catch (PersistenceException pe) {
			logger.error("Error loading sys reg loader.");
			throw new RuntimeException("Error loading sys reg loader.", pe);
		}
	}

	private String loadSecurityTokenDecsription(NBCAppContext nbcAppContext) {

		String securityToken = "";

		SystemRegistryEntryDbLoader sysRegistryLoader = null;
		SystemRegistryEntry sysRegEntry = null;
		try {
			sysRegistryLoader = SystemRegistryEntryDbLoader.Default.getInstance();
			try {
				sysRegEntry = sysRegistryLoader.loadByKey(REGISTRY_KEY);
			} catch (KeyNotFoundException knfe) {
				// could not find token in sys registry
			}
		} catch (PersistenceException pe) {
			logger.error("Error loading sys reg loader.");
			throw new RuntimeException("Error loading sys reg loader.", pe);
		}
		if (!isGuestAccess()) {
			if (null != sysRegEntry) {
				/*logger.info("sysRegEntry.getDescription()--->" + sysRegEntry.getDescription());*/
				securityToken = sysRegEntry.getDescription();
			}
		}
		/*logger.info("securityToken--------->"+securityToken);*/
		return securityToken;
	}

	private boolean isGuestAccess() {
		boolean isGuest = false;
		ContextManager cman = ContextManagerFactory.getInstance();
		Context bbContext = cman.getContext();
		User user = bbContext.getUser();
		if (user.getUserName().equals("guest")) {
			isGuest = true;
		}
		return isGuest;
	}
}
