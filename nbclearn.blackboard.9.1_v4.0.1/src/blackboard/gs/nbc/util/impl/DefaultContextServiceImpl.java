/**
 * Ryan Hardy
 * Guilford Group
 * 
 * DefaultContextServiceImpl.java
 * Created: Jun 14, 2012
 */
package blackboard.gs.nbc.util.impl;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import blackboard.data.course.Course;
import blackboard.data.course.CourseMembership;
import blackboard.data.role.PortalRole;
import blackboard.data.user.User;
import blackboard.gs.nbc.Constants;
import blackboard.gs.nbc.context.NBCAppContext;
import blackboard.gs.nbc.util.ContextService;
import blackboard.gs.nbc.util.ContextServiceException;
import blackboard.persist.BbPersistenceManager;
import blackboard.persist.Id;
import blackboard.persist.PersistenceException;
import blackboard.persist.course.CourseDbLoader;
import blackboard.persist.course.CourseMembershipDbLoader;
import blackboard.persist.user.UserDbLoader;
import blackboard.platform.BbServiceManager;
import blackboard.platform.context.Context;
import blackboard.platform.context.ContextManager;
import blackboard.platform.context.ContextManagerFactory;

/**
 * @author Ryan Hardy (rhardy@guilfordgroup.com)
 *
 */
public class DefaultContextServiceImpl implements ContextService {
	private static final Logger logger = Logger.getLogger(DefaultContextServiceImpl.class);
	
	/**
	 * Session attribute used to cache the User.
	 * Lookup methods will look for cached User before using Blackboard 
	 * utilities for obtaining User.
	 */
	public static final String ATTRIBUTE_USER = "blackboard.gs.nbc.util.impl.CachedUser";
	/**
	 * Session attribute used to cache the Course (or spoof the Course, if needed).
	 * Lookup methods will look for cached Course before using Blackboard 
	 * utilities for obtaining Course.
	 */
	public static final String ATTRIBUTE_COURSE = "blackboard.gs.nbc.util.impl.CachedCourse";
	/**
	 * Session attribute used to cache the CourseMembership.
	 * Lookup methods will look for cached Membership before using Blackboard 
	 * utilities for obtaining CourseMembership.
	 */
	public static final String ATTRIBUTE_COURSE_MEMBERSHIP = "blackboard.gs.nbc.util.impl.CachedCourseMembership";
	
	private final NBCAppContext appContext;
	
	/**
	 * @param appContext
	 */
	public DefaultContextServiceImpl(NBCAppContext appContext) {
		this.appContext = appContext;
	}

	/**
	 * Obtains the request object from the {@link NBCAppContext}.
	 * @return
	 */
	private HttpServletRequest getRequest() {
		HttpServletRequest request = (HttpServletRequest) this.appContext.getRequestObject();
		return request;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getUserId() throws ContextServiceException {
		User user = loadUser();
		/*if (logger.isDebugEnabled()) {
			logger.debug("Getting User: " + toString(user));
		}*/
		String id = (user == null) ? null : user.getId().toExternalString();
		/*if (logger.isDebugEnabled()) {
			logger.debug("Getting user id: " + id);
		}*/
		return id;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getUsername() throws ContextServiceException {
		User user = loadUser();
		/*if (logger.isDebugEnabled()) {
			logger.debug("Getting User: " + toString(user));
		}*/
		String username = (user == null) ? null : user.getUserName();
		/*if (logger.isDebugEnabled()) {
			logger.debug("Getting username: " + username);
		}*/
		return username;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getUserEmail() throws ContextServiceException {
		User user = loadUser();
		/*if (logger.isDebugEnabled()) {
			logger.debug("Getting User: " + toString(user));
		}*/
		String email = (user == null) ? null : user.getEmailAddress();
		/*if (logger.isDebugEnabled()) {
			logger.debug("Getting user email: " + email);
		}*/
		
		return email;
	}

	@Override
	public String getUserFirstName() throws ContextServiceException {
		User user = loadUser();
		/*if (logger.isDebugEnabled()) {
			logger.debug("Getting User: " + toString(user));
		}*/
		String firstname = (user == null) ? null : user.getGivenName();
		/*if (logger.isDebugEnabled()) {
			logger.debug("Getting user's first name: " + firstname);
		}*/
		
		return firstname;
	}

	@Override
	public String getUserLastName() throws ContextServiceException {
		User user = loadUser();
		/*if (logger.isDebugEnabled()) {
			logger.debug("Getting User: " + toString(user));
		}*/
		String lastname = (user == null) ? null : user.getFamilyName();
		/*if (logger.isDebugEnabled()) {
			logger.debug("Getting user's last name: " + lastname);
		}*/
		
		return lastname;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getUserRole() throws ContextServiceException {
		User user = loadUser();
		/*if (logger.isDebugEnabled()) {
			logger.debug("Getting User: " + toString(user));
		}*/
		String role = null;
		if (user != null) {
			PortalRole pRole = user.getPortalRole();
			role = pRole.getRoleName();
		}
		/*if (logger.isDebugEnabled()) {
			logger.debug("Getting user role: " + role);
		}*/
		
		return role;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getCourseId() throws ContextServiceException {
		Course course = loadCourse();
		if (logger.isDebugEnabled()) {
			logger.debug("Getting Course: " + toString(course));
		}
		String id = (course == null) ? null : course.getId().toExternalString();
		if (logger.isDebugEnabled()) {
			logger.debug("Getting course id: " + id);
		}
		
		return id;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getCourseName() throws ContextServiceException {
		Course course = loadCourse();
		/*if (logger.isDebugEnabled()) {
			logger.debug("Getting Course: " + toString(course));
		}*/
		String name = (course == null) ? null : course.getTitle();
		/*if (logger.isDebugEnabled()) {
			logger.debug("Getting course name: " + name);
		}*/
		
		return name;
	}
	
	/**
	 * Obtains the {@link User} associated with the provided request.
	 * @param request
	 * @return
	 * @throws Exception
	 */
	protected User loadUser() throws ContextServiceException {
		User user = null;
		
		HttpServletRequest request = this.getRequest();
		if(false)
		{
			// check first for cached user
			if (request != null && request.getAttribute(ATTRIBUTE_USER) != null) {
				user = (User) request.getAttribute(ATTRIBUTE_USER);
				/*logger.info("ATTENTION: returning cached User from request: " + toString(user));*/
				return user;
			}
		}
		
		Context ctx = getContext();
		
		if (ctx == null) {
			/*logger.warn("Could not load User; Context is null (returning null).");*/
			return null;
		}
		
		user = ctx.getUser();
		if (user == null) {
			/*logger.debug("No User found in Context, returning null.");*/
			return null;
		}
		// need to load User object using Loader, to do a "heavy" load, so
		// that properties are not lazily loaded
		try {
			BbPersistenceManager bbPersMgr = getDbPersistenceManager();
			String userIdStr = user.getId().getExternalString();
			Id userId = bbPersMgr.generateId( Course.DATA_TYPE, userIdStr );
			UserDbLoader loader = (UserDbLoader) bbPersMgr.getLoader( UserDbLoader.TYPE );
			user = loader.loadById(userId, null, true);
		} catch (PersistenceException e) {
			throw new ContextServiceException(e);
		}
		
		// cache user in request //TODO: should this be session cached?
		request.setAttribute(ATTRIBUTE_USER, user);
		
		return user;
	}
	
	/**
	 * Obtains the {@link Course} associated with the provided request.
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	protected Course loadCourse() throws ContextServiceException {
		Course course = null;

		HttpServletRequest request = this.getRequest();
		// check first for cached course
		if (request != null && request.getAttribute(ATTRIBUTE_COURSE) != null) {
			course = (Course) request.getAttribute(ATTRIBUTE_COURSE);
			/*logger.info("ATTENTION: returning cached Course from request: " + toString(course));*/
			return course;
		}
		
		Context ctx = getContext();
		if (ctx == null) {
			/*logger.warn("Could not load Course; Context is null (returning null).");*/
			return null;
		}
		course = ctx.getCourse();
		if (course == null) {
			/*logger.debug("No Course found in Context, attempting to load from session attribute.");*/
			String courseIdStr = (String) request.getSession().getAttribute(Constants.PARAM_COURSE_ID);
			if (courseIdStr == null) {
				/*logger.debug("No Course id found in session, returning null.");*/
				return null;
			}
			// need to load Course object using Loader, to do a "heavy" load, so
			// that properties are not lazily loaded
			try {
				BbPersistenceManager bbPersMgr = getDbPersistenceManager();
				Id courseId = bbPersMgr.generateId( Course.DATA_TYPE, courseIdStr );
				CourseDbLoader loader = (CourseDbLoader) bbPersMgr.getLoader( CourseDbLoader.TYPE );
				course = loader.loadById(courseId);
			} catch (PersistenceException e) {
				throw new ContextServiceException(e);
			}
		}
		
		// cache course in request
		request.setAttribute(ATTRIBUTE_COURSE, course);
		
		return course;
	}
		
	/**
	 * Obtains the {@link CourseMembership} associated with the provided request.
	 * 
	 * @param request
	 * @return
	 * @throws ContextServiceException
	 */
	protected CourseMembership loadCourseMembership() throws ContextServiceException {
		CourseMembership courseMembership = null;
		
		HttpServletRequest request = this.getRequest();
		// check first for cached courseMembership
		if (request != null && request.getAttribute(ATTRIBUTE_COURSE_MEMBERSHIP) != null) {
			courseMembership = (CourseMembership) request.getAttribute(ATTRIBUTE_COURSE_MEMBERSHIP);
			/*logger.info("ATTENTION: returning cached CourseMembership from request: " + toString(courseMembership));*/
			return courseMembership;
		}
				
		Context ctx = getContext();
		if (ctx == null) {
			/*logger.warn("Could not load CourseMembership; Context is null (returning null).");*/
			return null;
		}
		courseMembership = ctx.getCourseMembership();
		if (courseMembership == null) {
			/*logger.debug("No CourseMembership found in Context, attempting to load from Course session attribute.");*/
			Course course = loadCourse();
			if (course == null) {
				/*logger.debug("CourseMembership could not be loaded from Course session attribute, returning null.");*/
				return null;
			}
			User user = loadUser();
			
			try {
				BbPersistenceManager bbPersMgr = getDbPersistenceManager();
				CourseMembershipDbLoader loader = (CourseMembershipDbLoader) bbPersMgr.getLoader( CourseMembershipDbLoader.TYPE );
				courseMembership = loader.loadByCourseAndUserId( course.getId(), user.getId() );
			} catch (PersistenceException e) {
				throw new ContextServiceException(e);
			}
		}
		// cache courseMembership in request
		request.setAttribute(ATTRIBUTE_COURSE_MEMBERSHIP, courseMembership);
		
		return courseMembership;
	}
	
	/**
	 * Obtains the {@link Context} for the current request. If request is 
	 * <code>null</code>, returns <code>null</code>.
	 * 
	 * @param request
	 * @return
	 * @throws ContextServiceException 
	 */
	protected Context getContext() throws ContextServiceException {
		HttpServletRequest request = this.getRequest();
		if (request == null) {
			/*logger.warn("Could not load Context; request is null (returning null).");*/
			return null;
		}
		try {
			return ContextManagerFactory.getInstance().getContext();
			/*ContextManager contextManager = (ContextManager) BbServiceManager.lookupService(ContextManager.class);
			contextManager.setContext(request);
			return contextManager.getContext();*/
		} catch (Exception e) {
			logger.error("Could not obtain request Context.", e);
			// exceptional error....should not be recoverable
			throw new ContextServiceException("Could not obtain request Context.", e);
		}
	}
	
	/**
	 * Loads {@link BbPersistenceManager} from {@link Context}. If the request is
	 * <code>null</code>, returns <code>null</code>.
	 * 
	 * @param request
	 * @return
	 * @throws ContextServiceException
	 */
	private BbPersistenceManager getDbPersistenceManager() throws ContextServiceException {
		Context ctx = getContext();
		if (ctx == null) {
			/*logger.warn("Could not load BbPersistenceManager; Context is null (returning null).");*/
			return null;
		}
		try {
			return BbPersistenceManager.getInstance( ctx.getVirtualInstallation() );
		} catch (Exception e) {
			throw new ContextServiceException(e);
		} 
	}
	
	/**
	 * Performs null-safe string conversion.
	 * @param user
	 * @return
	 */
	protected String toString(User user) {
		if (user == null) {
			return "null";
		}
		return user.getUserName();
	}
	
	/**
	 * Performs null-safe string conversion.
	 * @param course
	 * @return
	 */
	protected String toString(Course course) {
		if (course == null) {
			return "null";
		}
		return course.getTitle();
	}
	
	/**
	 * Performs null-safe string conversion.
	 * @param courseMembership
	 * @return
	 */
	protected String toString(CourseMembership courseMembership) {
		if (courseMembership == null) {
			return "null";
		}
		try {
			return "Course " + courseMembership.getCourseId() + " - " + toString(courseMembership.getUser());
		} catch (Exception e) {
			logger.error("Could not generate toString for CourseMembership.", e);
			return "null";
		}
	}

}