/**
 * Ryan Hardy
 * Guilford Group
 * 
 * DefaultModuleConfigServiceImpl.java
 * Created: Nov 19, 2012
 */
package blackboard.gs.nbc.util.impl;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Map.Entry;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import blackboard.gs.nbc.context.NBCAppContext;
import blackboard.gs.nbc.util.ModuleConfigService;
import blackboard.platform.log.LogServiceFactory;
import blackboard.util.IOUtil;

/**
 * {@link ModuleConfigService} implementation that is backed by a
 * {@link Properties} file, used to retrieve config data for NBC Modules.
 * 
 * @author Ryan Hardy (rhardy@guilfordgroup.com)
 */
public class DefaultModuleConfigServiceImpl implements ModuleConfigService {
	private static final Logger logger = Logger.getLogger(DefaultModuleConfigServiceImpl.class);

	/**
	 * The Servlet Context initialization parameter indicating the
	 * context-relative path to the properties file used to initialize this
	 * instance (must be in classpath).
	 */
	public static final String CTX_PARAM_PROPS_FILE = "blackboard.gs.nbc.util.impl.ModuleConfigService.Properties";

	/**
	 * If no context param value is provided, this is the context-relative path
	 * to the module config properties to use.
	 */
	private static final String DEFAULT_PROPS_FILE = "/WEB-INF/classes/module-config.properties";

	/**
	 * Property key used to retrieve URL for African American History.
	 */
	private static final String PROPERTY_AF_AMER_URL = "africanAmericanHistoryView.url";
	/**
	 * Property key used to retrieve URL for Business and Finance.
	 */
	private static final String PROPERTY_BIZ_FINANCE_URL = "businessAndFinanceView.url";
	/**
	 * Property key used to retrieve URL for Health and Wellness.
	 */
	private static final String PROPERTY_HEALTH_WELL_URL = "healthAndWellnessView.url";
	/**
	 * Property key used to retrieve URL for In the News.
	 */
	private static final String PROPERTY_IN_NEWS_URL = "inTheNewsView.url";
	/**
	 * Property key used to retrieve URL for Language Arts.
	 */
	private static final String PROPERTY_LANG_ARTS_URL = "languageArtsView.url";
	/**
	 * Property key used to retrieve URL for Science.
	 */
	private static final String PROPERTY_SCIENCE_URL = "scienceView.url";
	/**
	 * Property key used to retrieve URL for Social Studies.
	 */
	private static final String PROPERTY_SOC_STUDY_URL = "socialStudiesView.url";
	/**
	 * Property key used to retrieve URL for US History.
	 */
	private static final String PROPERTY_US_HIST_URL = "usHistoryView.url";
	/**
	 * Property key used to retrieve URL for Women's History.
	 */
	private static final String PROPERTY_WOMENS_HIST_URL = "womensHistoryView.url";

	/**
	 * The web-context relative path to the properties file for this instance.
	 */
	private final String propertiesPath;
	/**
	 * The {@link Properties} instance that contains this instance's Module
	 * config data.
	 */
	private final Properties properties = new Properties();
	/**
	 * The {@link File#lastModified() lastModified} value for the
	 * {@link Properties} file specified by {@link #propertiesPath}.
	 */
	private long lastModified;

	private final NBCAppContext appContext;

	/**
	 * @param appContext
	 * 
	 */
	public DefaultModuleConfigServiceImpl(NBCAppContext appContext) {
		/*logger.trace("Creating new instance of DefaultModuleConfigServiceImpl.");*/
		this.appContext = appContext;

		String tempPath = appContext.getAttribute(CTX_PARAM_PROPS_FILE);
		if (tempPath == null) {
			tempPath = DEFAULT_PROPS_FILE;
		}
		this.propertiesPath = tempPath;
		/*logger.trace("DefaultModuleConfigServiceImpl properties file path: '" + this.propertiesPath + "'");*/
	}

	/*
	 * =========================================================================
	 * === ModuleConfigService implementation methods
	 * =========================================================================
	 * ===
	 */

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getAfricanAmericanHistoryUrl() {
		return getProperty(PROPERTY_AF_AMER_URL);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getBusinessAndFinanceUrl() {
		return getProperty(PROPERTY_BIZ_FINANCE_URL);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getHealthAndWellnessUrl() {
		return getProperty(PROPERTY_HEALTH_WELL_URL);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getInTheNewsUrl() {
		return getProperty(PROPERTY_IN_NEWS_URL);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getLanguageArtsUrl() {
		return getProperty(PROPERTY_LANG_ARTS_URL);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getScienceUrl() {
		return getProperty(PROPERTY_SCIENCE_URL);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getSocialStudiesUrl() {
		return getProperty(PROPERTY_SOC_STUDY_URL);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getUsHistoryUrl() {
		return getProperty(PROPERTY_US_HIST_URL);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getWomensHistoryUrl() {
		return getProperty(PROPERTY_WOMENS_HIST_URL);
	}

	/*
	 * =========================================================================
	 * === Private helper methods
	 * =========================================================================
	 * ===
	 */

	/**
	 * Clears {@link #properties} object and reloaded from
	 * {@link #propertiesPath}. Returns true if properties are successfully
	 * reloaded, and false otherwise.
	 */
	private boolean reloadProperties() {
		synchronized (this.properties) {
			/*logger.info("Reloading properties.");*/
			InputStream is = null;
			try {
				HttpServletRequest request = (HttpServletRequest) this.appContext.getRequestObject();
				if (request == null) {
					/*logger.warn(
							"Can not reload properties for DefaultModuleConfigServiceImpl: request object is null.");*/
					return false;
				}

				ServletContext ctx = request.getSession().getServletContext();
				this.properties.clear();
				is = ctx.getResourceAsStream(this.propertiesPath);
				this.properties.load(is);
				/*logger.info("Properties for DefaultModuleConfigServiceImpl have been loaded successfully.");
				if (logger.isTraceEnabled()) {
					logger.trace("==================================================================");
					logger.trace("DefaultModuleConfigServiceImpl.properties:");
					for (Entry<Object, Object> e : this.properties.entrySet()) {
						String key = "" + e.getKey();
						String val = "" + e.getValue();
						logger.trace("\t" + key + ": " + val);
					}
				}*/
				long lastMod = getLastMod();
				this.lastModified = lastMod;
				return true;
			} catch (IOException e) {
				logger.error("Could not load properties for DefaultModuleConfigServiceImpl.", e);
			} catch (Exception e) {
				logger.error("An exceptional error occurred in loading properties for DefaultModuleConfigServiceImpl.",
						e);
			} finally {
				//IOUtil.silentClose(is);
				silentClose(is);
			}
			// reload failed if returning here
			return false;
		}
	}

	private void silentClose(InputStream is) {

		try {
			if (null != is) {
				is.close();
			}
		} catch (Exception e) {
			LogServiceFactory.getInstance().logDebug("Failed to close resource", e);
		}

	}

	/**
	 * Gets property value for given property key, checking to see if
	 * {@link #properties} should be reloaded due to modification. Will return
	 * an empty string if no value is found.
	 * 
	 * @param property
	 * @return
	 */
	private String getProperty(String property) {
		synchronized (this.properties) {
			// check for modified Properties file
			long lastMod = getLastMod();
			if (lastMod == 0 || lastMod != this.lastModified) {
				/*logger.info("Reloading properties due to file modification (this.lastModified=" + this.lastModified
						+ ", lastMod=" + lastMod + ")");*/
				boolean reloaded = this.reloadProperties();
				if (reloaded) {
					// only update if properties were successfully reloaded
					this.lastModified = lastMod;
				}
			}
			String val = this.properties.getProperty(property, "");
			/*if (logger.isDebugEnabled()) {
				logger.debug("Value for key '" + property + "': " + val);
			}*/
			return val;
		}
	}

	private long getLastMod() {
		HttpServletRequest request = (HttpServletRequest) this.appContext.getRequestObject();
		if (request == null) {
			/*logger.warn("Could not get last mod value for module config properties file, returning 0.");*/
			return 0;
		}
		ServletContext ctx = request.getSession().getServletContext();
		String absPath = ctx.getRealPath(this.propertiesPath);
		/*logger.debug("DefaultModuleConfigServiceImpl properties absolute path: '" + absPath + "'");*/
		if (absPath == null) {
			/*logger.warn("Could not determine properties file last modified, returning 0");*/
			return 0;
		}
		File propertiesFile = new File(absPath);
		/*logger.debug("DefaultModuleConfigServiceImpl properties File: '" + propertiesFile + "'");*/
		long lastMod = propertiesFile.lastModified();
		/*logger.info("DefaultModuleConfigServiceImpl properties File last modified: '" + lastMod + "'");*/
		/*if (logger.isDebugEnabled()) {
			logger.info(
					"DefaultModuleConfigServiceImpl properties File last modified Date: '" + new Date(lastMod) + "'");
		}*/
		return lastMod;
	}

}
