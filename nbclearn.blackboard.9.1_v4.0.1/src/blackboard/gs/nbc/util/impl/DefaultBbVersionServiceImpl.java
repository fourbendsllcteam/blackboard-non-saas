/**
 * Ryan Hardy
 * Guilford Group
 * 
 * DefaultBbVersionServiceImpl.java
 * Created: Jun 13, 2012
 */
package blackboard.gs.nbc.util.impl;

import java.util.List;

import javax.servlet.ServletRequest;

import org.apache.log4j.Logger;

import blackboard.gs.nbc.context.NBCAppContext;
import blackboard.gs.nbc.util.BbVersionService;
import blackboard.persist.PersistenceException;
import blackboard.platform.LicenseUtil;
import blackboard.platform.branding.BrandingUtil;
import blackboard.platform.branding.common.Theme;
import blackboard.platform.branding.service.ThemeManagerFactory;

/**
 * Implementation of {@link BbVersionService} that queries the Blackboard
 * {@link LicenseUtil} for build version info.
 * 
 * @author Ryan Hardy (rhardy@guilfordgroup.com)
 */
public class DefaultBbVersionServiceImpl implements BbVersionService {
	private static final Logger logger = Logger.getLogger(DefaultBbVersionServiceImpl.class);
	
	private final NBCAppContext appContext;
	
	/**
	 * @param appContext
	 */
	public DefaultBbVersionServiceImpl(NBCAppContext appContext) {
		this.appContext = appContext;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getFullVersion() {
		String version;
		try {
			version = LicenseUtil.getBuildNumber();
		} catch (PersistenceException e) {
			logger.error("Could not obtain version from Bb.", e);
			throw new RuntimeException("Could not obtain version from Bb.", e);
		}
		return version;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getMajorVersion() {
		String fullVersion = getFullVersion();
		int ind = fullVersion.indexOf(".");
		String mVersStr = fullVersion.substring(0, ind);
		int mVers = Integer.parseInt(mVersStr);
		return mVers;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getMinorVersion() {
		String fullVersion = getFullVersion();
		int ind1 = fullVersion.indexOf(".") + 1;
		int ind2 = fullVersion.indexOf(".", ind1);
		String mVersStr = fullVersion.substring(ind1, ind2);
		int mVers = Integer.parseInt(mVersStr);
		return mVers;
	}

	@Override
	public String getFullServicePack() {
		String fullVersion = getFullVersion();
		String majMin = getMajorVersion() + "." + getMinorVersion() + ".";
		String sp = fullVersion.replace(majMin, "");
		return sp;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getServicePack() { 
		return getFullServicePack();
		/*// assume that if fullSp starts with 1, it is SP 10 and up (not 1 and up)
		String spNum;
		if ('1' == fullSp.charAt(0)) {
			spNum = fullSp.substring(0, 2);
		} else if ('2' == fullSp.charAt(0)) {
			spNum = fullSp.substring(0, 2);
		} else {
			spNum = fullSp.substring(0, 1);			
		}
		int sp = Integer.parseInt(spNum);
		return sp;*/
	}

	/*@Override
	public boolean isSP(int servicePack) {
		int sp = getServicePack();
		boolean is = sp == servicePack;
		return is;
	}

	@Override
	public boolean isGreaterThanSP(int servicePack) {
		int sp = getServicePack();
		boolean gt = sp > servicePack;
		return gt;
	}

	@Override
	public boolean isLessThanSP(int servicePack) {
		int sp = getServicePack();
		boolean lt = sp < servicePack;
		return lt;
	}*/

	/**
	 * 
	 * {@inheritDoc}
	 */
	@Override
	public String getCurrentThemeName() {
		/*logger.debug("Attempting to get current Theme name.");*/
		String name = null;
		try {
			ServletRequest req = (ServletRequest) this.appContext.getRequestObject();
			name = BrandingUtil.getCurrentBrandThemeName(req);
			/*logger.info("Getting current Theme name: '" + name + "'");*/
			
			/*if (logger.isTraceEnabled()) {
				List<Theme> themes = ThemeManagerFactory.getInstance().getAllThemes();
				logger.trace("------------------------------------------------------------------");
				logger.trace("Available Themes: ");
				for (Theme th : themes) {
					logger.trace("\t" + th.getName());
				}
				logger.trace("------------------------------------------------------------------");
			}*/
			
		} catch (Exception e) {
			logger.error("Can not get Theme name.", e);
		}
		
		return name;
	}
}