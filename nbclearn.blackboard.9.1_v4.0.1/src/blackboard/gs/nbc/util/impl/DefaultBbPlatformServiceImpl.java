/**
 * Ryan Hardy
 * Guilford Group
 * 
 * DefaultBbPLatformServiceImpl.java
 * Created: Jun 7, 2013
 */
package blackboard.gs.nbc.util.impl;

import org.apache.log4j.Logger;

import blackboard.gs.nbc.Constants;
import blackboard.gs.nbc.context.NBCAppContext;
import blackboard.gs.nbc.util.BbPlatformService;
import blackboard.platform.context.ContextManagerFactory;
import blackboard.platform.plugin.PlugInUtil;
import blackboard.platform.vxi.data.VirtualInstallation;
import blackboard.platform.vxi.service.VirtualInstallationManagerFactory;
import blackboard.util.UrlUtil;

/**
 * @author Ryan Hardy (rhardy@guilfordgroup.com)
 */
public class DefaultBbPlatformServiceImpl implements BbPlatformService {
	private static final Logger logger = Logger.getLogger(DefaultBbPlatformServiceImpl.class);

	private final NBCAppContext appContext;
	
	/**
	 * @param appContext 
	 * 
	 */
	public DefaultBbPlatformServiceImpl(NBCAppContext appContext) {
		/*logger.trace("Creating new instance of DefaultBbPlatformServiceImpl.");*/
		this.appContext = appContext;
	}
	
	/**
	 * @return the appContext
	 */
	protected final NBCAppContext getAppContext() {
		return this.appContext;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getUriStem() {
		return PlugInUtil.getUriStem(Constants.VENDOR_ID, Constants.PLUGIN_ID);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getHostName() {
		return ContextManagerFactory.getInstance().getContext().getHostName();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isForcedSystemSSL() {
		return UrlUtil.isForcedSystemSSL();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getUUID() {
		VirtualInstallation viInst = null;
		try {
			viInst = ContextManagerFactory.getInstance().getContext().getVirtualInstallation();
		} catch (Throwable e) {
			logger.error("Could not obtain VirtualInstallation.", e);
			viInst = VirtualInstallationManagerFactory.getInstance().getDefaultVirtualInstallation();
		}

		if (viInst != null) {
			return viInst.getBbUid();
		} else {
			return null;
		}
	}
}
