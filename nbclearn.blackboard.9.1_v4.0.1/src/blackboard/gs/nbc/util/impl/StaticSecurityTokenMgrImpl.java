/**
 * Ryan Hardy
 * Guilford Group
 * 
 * StaticSecurityTokenMgrImpl.java
 * Created: May 14, 2012
 */
package blackboard.gs.nbc.util.impl;

import blackboard.gs.nbc.context.NBCAppContext;
import blackboard.gs.nbc.util.SecurityTokenManager;

/**
 * Simple {@link SecurityTokenManager} implementation that stores token in-memory, performing
 * no encryption. Suitable only for testing purposes.
 * 
 * @author Ryan Hardy
 */
public class StaticSecurityTokenMgrImpl implements SecurityTokenManager {
	private String token;
	
	@Override
	public String loadSecurityToken() {
		return this.token;
	}

	@Override
	public void saveSecurityToken(String value, NBCAppContext nbcAppContext) {
		this.token = value;
	}
}