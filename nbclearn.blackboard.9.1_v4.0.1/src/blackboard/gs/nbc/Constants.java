// Testing Bitbucket
// Testing Bitbucket VA
package blackboard.gs.nbc;

import blackboard.gs.nbc.context.NBCResolver;


public class Constants {
	public static final String VENDOR_ID = "bbgs";
	public static final String PLUGIN_ID = "nbc-content-integration";
	public static final String NBC_ARCHIVE_LABEL = "Browse NBC Learn";
	public static final String NBC_PLAYLIST_LABEL = "NBC Learn Playlist";
	
	/*
	============================================================================ 
							Request Parameters
	============================================================================ 
	*/

	public static final String PARAM_COURSE_ID = "course_id";
	public static final String PARAM_ACTION = "action";
	public static final String PARAM_CONTENT_ID = "content_id";
	public static final String PARAM_MODE = "mode";
	public static final String PARAM_RETURN_URL = "return_url";
	public static final String PARAM_BBSESSION_ID = "bbsession_id";
	public static final String PARAM_TOKEN = "token";
	public static final String PARAM_CUE_CARD_URL = "cue_card_url";
	public static final String PARAM_LAUNCH_POST = "post_to";
	public static final String PARAM_LOCATION = "location";
	public static final String PARAM_RETURN_LOC = "returnLoc";
	
	public static final String PARAM_USER_ID = "userId";
	public static final String PARAM_USER_EMAIL = "userEmail";
	public static final String PARAM_USER_NAME = "user_id";
	public static final String PARAM_USER_FIRST_NAME = "firstName";
	public static final String PARAM_USER_LAST_NAME = "lastName";
	public static final String PARAM_USER_ROLE = "userRole";
	public static final String PARAM_COURSE_NAME = "courseName";
	public static final String PARAM_USER_TOOL_TYPE = "userToolType";
	
	/*
	============================================================================ 
							Request Attributes
	============================================================================ 
	*/
	
	/**
	 * The path to the NBC logo image.
	 */
	public static final String ATTRIBUTE_LOGO_PATH = "nbcLogo";
	
	/*
	============================================================================ 
							Course Registry Keys
	============================================================================ 
	*/
	
	public static final String COURSE_REGISTRY_KEY = "gs-nbc-video-id";
	public static final String COURSE_REGISTRY_IMAGE_KEY = "gs-nbc-video-image";
	public static final String COURSE_REGISTRY_DATE_KEY = "gs-nbc-video-date";
	public static final String COURSE_REGISTRY_DURATION_KEY = "gs-nbc-video-duration";
	public static final String COURSE_REGISTRY_TITLE_KEY = "gs-nbc-video-title";
	public static final String COURSE_REGISTRY_TYPE_KEY = "gs-nbc-video-type";
	public static final String SYSTEM_REGISTRY_KEY = "gs-nbc-token";
	public static final String COURSE_REGISTRY_IFRAME = "gs-nbc-video-iframe";
	public static final String COURSE_REGISTRY_IFRAME_SRC = "gs-nbc-video-src";	
	
	/*
	============================================================================ 
							JSP Forwards
	============================================================================ 
	*/
	public static final String FORWARD_CONFIG_SUCCESS = "/WEB-INF/jsp/admin/config_success.jsp";
	public static final String FORWARD_CONFIG_FAIL = "/WEB-INF/jsp/admin/config_fail.jsp";
	public static final String FORWARD_CONFIG_PAGE = "/WEB-INF/jsp/admin/config.jsp";
	public static final String FORWARD_LAUNCH_NBC = "/WEB-INF/jsp/launch/launch_nbc_site.jsp";
	
	/*
	============================================================================ 
							Context Variables
	============================================================================ 
	*/
	
	public static final String NBC_CUECARD_ID = "nbc.cuecard.id";
	public static final String NBC_SECURITY_TOKEN = "nbc.security.token";
	public static final String NBC_DELIMITER = "@X@";
	public static final String NBC_CUECARD_TITLE = "nbc.cuecard.title";
	public static final String NBC_CUECARD_DESCRIPTION = "nbc.cuecard.description";
	public static final String NBC_CUECARD_AIRDATE = "nbc.cuecard.airdate";
	public static final String NBC_CUECARD_DURATION = "nbc.cuecard.duration";	
	public static final String NBC_CUECARD_URL = "nbc.cuecardurl";
	public static final String SNBC_CUECARD_URL ="snbc.cuecardurl";
	public static final String NBC_VI = "nbc.vi";
	
	public static final String NBC_FILES_LOADER_SCRIPT = "nbc.fileLoaderJs";
	
	public static final String NBC_THUMBNAIL = "nbc.thumbnail";
	public static final String NBC_PEACOCK_IMAGE = "nbc.peacock.image"; 
	public static final String NBC_CUECARD_TYPE_IMAGE="nbc.cuecard.type.image";
	public static final String NBC_CUECARD_RETURN_IMAGE="nbc.cuecard.return.image";
	
	public static final String NBC_THUMBNAIL_ALT = "nbc.thumbnail.alt";
	public static final String NBC_PEACOCK_IMAGE_ALT = "nbc.peacock.image.alt"; 
	public static final String NBC_CUECARD_TYPE_IMAGE_ALT = "nbc.cuecard.type.image.alt";
	public static final String NBC_CUECARD_RETURN_IMAGE_ALT = "nbc.cuecard.return.image.alt";
	
	public static final String NBC_CUECARD_CONTENT = "nbc.cuecard.content";
	public static final String NBC_CUECARD_IFRAMESOURCE = "nbc.cuecard.iframesrc";
	public static final String NBC_PLAY_ICON = "nbc.play.image"; 
	
	/**
	 * Context variable used to inject script used to include NBC client-side
	 * files.
	 */
	public static final String NBC_INCLUDES = 			"nbc.includes";
	/**
	 * Context variable used to inject a boolean flag indicating if underlying
	 * Blackboard is 9.1 SP8 or greater.
	 */
	public static final String CTX_VAR_IS_GTE_SP8 = 	"nbc.is.gte.sp8";
	/**
	 * Context variable used to inject user's id.
	 */
	public static final String CTX_VAR_USER_ID = 		"nbc.user.id";
	/**
	 * Context variable used to inject user's email.
	 */
	public static final String CTX_VAR_USER_EMAIL = 	"nbc.user.email";
	/**
	 * Context variable used to inject user's role.
	 */
	public static final String CTX_VAR_USER_ROLE = 		"nbc.user.role";
	/**
	 * Context variable used to inject course batch id.
	 */
	public static final String CTX_VAR_COURSE_ID = 		"nbc.course.id";
	/**
	 * Context variable used to inject course name.
	 */
	public static final String CTX_VAR_COURSE_NAME = 	"nbc.course.name";
	
	
	/*
	============================================================================ 
							Template File Names
	============================================================================ 
	*/
	/**
	 * @deprecated use {@link #TEMPLATE_CUECARD_STATIC} instead.
	 */
	@Deprecated
	public static final String CUECARD_STATIC = "static-cuecard.template";
	/**
	 * @deprecated use {@link #TEMPLATE_CUECARD_FLOAT_INCLUDES} instead.
	 */
	@Deprecated
	public static final String CUECARD_FLOAT_INCLUDES = "floating-cuecard-includes.template";
	/**
	 * @deprecated use {@link #TEMPLATE_CUECARD_FLOAT_BODY} instead.
	 */
	@Deprecated
	public static final String CUECARD_FLOAT_BODY = "floating-cuecard-body.template";
	/**
	 * @deprecated use {@link #TEMPLATE_CUECARD_COURSE_CONTENT} instead.
	 */
	@Deprecated
	public static final String CUECARD_COURSE_CONTENT = "course-content-cuecard.template";
	/**
	 * @deprecated use {@link #TEMPLATE_MASHUP_CUECARD_SCRIPT} instead.
	 */
	@Deprecated
	public static final String MASHUP_CUECARD_SCRIPT = "mashup-cuecard-script.template";
	/**
	 * @deprecated use {@link #TEMPLATE_MASHUP_CUECARD_THUMBNAIL} instead.
	 */
	@Deprecated
	public static final String MASHUP_CUECARD_THUMBNAIL = "mashup-cuecard-thumbnail.template";
	/**
	 * @deprecated use {@link #TEMPLATE_CONTENT_MODULE} instead.
	 */
	@Deprecated
	public static final String CONTENT_MODULE = "module-cuecard.template";
	
	public static final String ENVIRONMENT = "environment.template";
	public static final String SCHEMA_VERSION_FILE = "schema_version.properties";

	/**
	 * The name of the file (on the classpath) used to render Video content description.
	 */
	public static final String TEMPLATE_CONTENT_DESCRIPTION = "content-description.template";
	/**
	 * The name of the file (on the classpath) used to render static cue cards.
	 */
	public static final String TEMPLATE_CUECARD_STATIC = "static-cuecard.template";
	/**
	 * The name of the file (on the classpath) used to render the includes (scripts and stylesheets) for floating cue cards.
	 */
	public static final String TEMPLATE_CUECARD_FLOAT_INCLUDES = "floating-cuecard-includes.template";
	/**
	 * The name of the file (on the classpath) used to render floating cue cards.
	 */
	public static final String TEMPLATE_CUECARD_FLOAT_BODY = "floating-cuecard-body.template";
	/**
	 * The name of the file (on the classpath) used to render course content cue cards.
	 */
	public static final String TEMPLATE_CUECARD_COURSE_CONTENT = "course-content-cuecard.template";
	/**
	 * The name of the file (on the classpath) used to render mashup scripts.
	 */
	public static final String TEMPLATE_MASHUP_CUECARD_SCRIPT = "mashup-cuecard-script.template";
	/**
	 * The name of the file (on the classpath) used to render mashup content.
	 */
	public static final String TEMPLATE_MASHUP_CUECARD_THUMBNAIL = "mashup-cuecard-thumbnail.template";
	/**
	 * The name of the file (on the classpath) used to render module content.
	 */
	public static final String TEMPLATE_CONTENT_MODULE = "module-cuecard.template";
	/**
	 * The name of the file (on the classpath) used to render the {@link NBCResolver} script.
	 */
	public static final String TEMPLATE_RESOLVER_SCRIPT = "resolver-script.template";
	

	/*
	============================================================================ 
								Miscellaneous
	============================================================================ 
	*/

	public static final String NBC_URL = "nbc.url";
	public static final String SNBC_URL ="snbc.url";
	public static final String SCHEMA_VER_REGISTRY_KEY = "nbc.schema.ver";
	public static final String OVERWRITE_VERSION_DELIM = ",";
	public static final String SCHEMA_VERSION_FILE_DELIM = " ";
	public static final String MYSQL_POSTFIX = "_mysql.sql";
	public static final String ORACLE_POSTFIX = "_oracle.sql";
	public static final String SQLSERVER_POSTFIX = "_sqlserver.sql";
	public static final String POSTGRESSERVER_POSTFIX = "_postgres.sql";
	public static final String MYSQL_DELIM = "/";
	public static final String ORACLE_DELIM = "/";
	public static final String SQLSERVER_DELIM = "GO";
	public static final String NAV_ITEM_PREFIX = "-nbc_archives";
	public static final String NAV_FIRST_POSTFIX = "-nav-1";
	public static final String NAV_SECOND_POSTFIX = "-nav-2";
	public static final String SECRET_KEY = "nbc.secret.key";

}