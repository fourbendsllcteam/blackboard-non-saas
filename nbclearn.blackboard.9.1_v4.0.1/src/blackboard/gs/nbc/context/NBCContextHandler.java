package blackboard.gs.nbc.context;

import blackboard.gs.nbc.web.RequestCache;
import blackboard.persist.*;
import blackboard.platform.context.*;
import blackboard.platform.security.*;
import blackboard.util.resolver.*;
import java.util.*;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

/**
 * {@link ContextHandler} implementation used in conjunction with {@link NBCResolver}
 * to search for '@X@nbc.cuecard@X@' context variables and replace with
 * a script used to inject framework scripts and stylesheets, and fix style issues. 
 * 
 * @author Ryan Hardy (rhardy@guilfordgroup.com)
 * 
 * @see NBCResolver
 */
public class NBCContextHandler implements ContextHandler {
	private static final Logger logger = Logger.getLogger(NBCContextHandler.class);
	
	/**
	 * 
	 * {@inheritDoc}
	 */
	public List<ContextEntry> resolveKeys(HttpServletRequest request, BbPersistenceManager bpm) {
		/*logger.trace("Calling resolveKeys().");*/
		RequestCache.setRequest(request);
		NBCResolver resolver = new NBCResolver(request);
		Resolver.attachResolverToContext(resolver);
		/*logger.trace("NBCResolver has been attached to context.");*/

		return new LinkedList<ContextEntry>();
	}

	/**
	 * 
	 * {@inheritDoc}
	 */
	public Entitlements getEffectiveEntitlements(Context ctx) {
		return new EntitlementList();
	}

	/**
	 * 
	 * {@inheritDoc}
	 */
	public List<SecurityContext> getSecurityContexts(Context ctx) {
		return new ArrayList<SecurityContext>();
	}

	/**
	 * 
	 * {@inheritDoc}
	 */
	public Entitlements getRestrictedEntitlements(Context ctx) {
		return null;
	}
}