/**
 * Ryan Hardy
 * Guilford Group
 * 
 * RequestBackedNBCAppCtxImpl.java
 * Created: Jun 7, 2013
 */
package blackboard.gs.nbc.context.impl;

import java.io.Serializable;
import java.net.URL;

import blackboard.gs.nbc.config.impl.DefaultEnvConfigImpl;
import blackboard.gs.nbc.context.NBCAppContext;
import blackboard.gs.nbc.playlist.persist.playlistitem.impl.PlaylistItemDbLoaderImpl;
import blackboard.gs.nbc.playlist.persist.playlistitem.impl.PlaylistItemDbPersisterImpl;
import blackboard.gs.nbc.playlist.persist.video.impl.VideoDbLoaderImpl;
import blackboard.gs.nbc.playlist.persist.video.impl.VideoDbPersisterImpl;
import blackboard.gs.nbc.playlist.service.impl.PlaylistManagerServiceImpl;
import blackboard.gs.nbc.security.impl.UserSessionServiceImpl;
import blackboard.gs.nbc.util.ContextService;
import blackboard.gs.nbc.util.CueCardConfiguration;
import blackboard.gs.nbc.util.impl.DefaultBbPlatformServiceImpl;
import blackboard.gs.nbc.util.impl.DefaultBbVersionServiceImpl;
import blackboard.gs.nbc.util.impl.DefaultModuleConfigServiceImpl;
import blackboard.gs.nbc.util.impl.DefaultSecurityTokenMgrImpl;

/**
 * {@link NBCAppContext} class that is not backed by a request or session 
 * (i.e. is a 'throw-away' instance).
 * <br /><br />
 * Since not tied to servlet in any way, can not perform URL resolution
 * in call to {@link #getResource(String)} - will throw an exception.
 * 
 * @author Ryan Hardy (rhardy@guilfordgroup.com)
 *
 */
public class TransientNBCAppCtxImpl extends BaseNBCAppCtxImpl implements NBCAppContext, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4950273046665602582L;
	
	/**
	 * 
	 */
	public TransientNBCAppCtxImpl() {
		super();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onUpdatedRequestContext() {
		// this MUST be set prior to EnvironmentConfiguration
		this.setBbPlatformService(new DefaultBbPlatformServiceImpl(this));
		
		// check for custom env. props file
		String envFile = this.getAttribute("envFile");
		if (envFile == null) {
			this.setEnvironmentConfiguration(new DefaultEnvConfigImpl(this));			
		} else {
			this.setEnvironmentConfiguration(new DefaultEnvConfigImpl(this, envFile));
		}
		
		this.setBbVersionService(new DefaultBbVersionServiceImpl(this));
		this.setUserSessionService(new UserSessionServiceImpl(this));
		
		// NOT creating ContextService since it will throw exceptions when no 
		// request is available (CueCardConfig relies on ContextService, so
		// not creating that either)
				
		this.setSecurityTokenManager(new DefaultSecurityTokenMgrImpl(this));
		this.setModuleConfigService(new DefaultModuleConfigServiceImpl(this));
		
		this.setPlaylistManagerService(new PlaylistManagerServiceImpl(this));
		this.setPlaylistItemDbLoader(new PlaylistItemDbLoaderImpl(this));
		this.setPlaylistItemDbPersister(new PlaylistItemDbPersisterImpl(this));
		this.setVideoDbLoader(new VideoDbLoaderImpl(this));
		this.setVideoDbPersister(new VideoDbPersisterImpl(this));
	}

	/**
	 * {@inheritDoc}
	 * <b>IMPLEMENTATION NOTE:</b>
	 * Not supported; throws {@link UnsupportedOperationException}.
	 */
	@Override
	public URL getResource(String relativePath) {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 * <b>IMPLEMENTATION NOTE:</b>
	 * Not supported; throws {@link UnsupportedOperationException}.
	 */
	@Override
	public ContextService getContextService() {
		// ContextService impl. will throw exceptions when no 
		// request is available...so make this unsupported
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 * <b>IMPLEMENTATION NOTE:</b>
	 * Not supported; throws {@link UnsupportedOperationException}.
	 */
	@Override
	public CueCardConfiguration getCueCardConfiguration() {
		// CueCardConfiguration impl. will throw exceptions when no 
		// request is available...so make this unsupported
		// (relies on ContextService)
		throw new UnsupportedOperationException();
	}
	
	
	
	
}