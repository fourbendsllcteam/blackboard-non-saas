/**
 * Ryan Hardy
 * Guilford Group
 * 
 * RequestBackedNBCAppCtxImpl.java
 * Created: Jun 7, 2013
 */
package blackboard.gs.nbc.context.impl;

import java.io.Serializable;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import blackboard.gs.nbc.config.impl.DefaultEnvConfigImpl;
import blackboard.gs.nbc.context.NBCAppContext;
import blackboard.gs.nbc.playlist.persist.playlistitem.impl.PlaylistItemDbLoaderImpl;
import blackboard.gs.nbc.playlist.persist.playlistitem.impl.PlaylistItemDbPersisterImpl;
import blackboard.gs.nbc.playlist.persist.video.impl.VideoDbLoaderImpl;
import blackboard.gs.nbc.playlist.persist.video.impl.VideoDbPersisterImpl;
import blackboard.gs.nbc.playlist.service.impl.PlaylistManagerServiceImpl;
import blackboard.gs.nbc.security.impl.UserSessionServiceImpl;
import blackboard.gs.nbc.util.impl.DefaultBbVersionServiceImpl;
import blackboard.gs.nbc.util.impl.DefaultContextServiceImpl;
import blackboard.gs.nbc.util.impl.DefaultCueCardConfigImpl;
import blackboard.gs.nbc.util.impl.DefaultModuleConfigServiceImpl;
import blackboard.gs.nbc.util.impl.DefaultSecurityTokenMgrImpl;
import blackboard.gs.nbc.util.impl.SessionCachingBbPlatformSvcImpl;

/**
 * {@link NBCAppContext} class that is backed by an {@link HttpSession}.
 * 
 * @author Ryan Hardy (rhardy@guilfordgroup.com)
 *
 */
public class SessionBackedNBCAppCtxImpl extends BaseNBCAppCtxImpl implements NBCAppContext, Serializable {
	private static final Logger logger = Logger.getLogger(SessionBackedNBCAppCtxImpl.class);
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4950273046665602582L;
	

	/**
	 * 
	 */
	public SessionBackedNBCAppCtxImpl() {
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onUpdatedRequestContext() {
		//TODO: can RequestCache.setRequest() call be eliminated (i.e. ensure that all services utilize
		// NBCAppContext to access request)?
		
		// arbitrary choice of service impl. to check to see if this has already
		// been initialized
		boolean isInitialized = this.getBbPlatformService() != null; 
		/*logger.debug("SessionBackedNBCAppCtxImpl.isInitialized: " + isInitialized);*/
		
		if (isInitialized) {
			/*logger.debug("ATTENTION: create new ContextService impl instance.");*/
			// ContextService REQUIRES current request on creation...so
			// re-create on every update to request context
			this.setContextService(new DefaultContextServiceImpl(this));
			return;
		}
		/*logger.debug("Intiializing SessionBackedNBCAppCtxImpl instance.");*/
		
		// is NOT initialized, so need to create all services
		
		this.setBbPlatformService(new SessionCachingBbPlatformSvcImpl(this));
		
		// check for custom env. props file
		String envFile = this.getAttribute("envFile");
		if (envFile == null) {
			this.setEnvironmentConfiguration(new DefaultEnvConfigImpl(this));			
		} else {
			this.setEnvironmentConfiguration(new DefaultEnvConfigImpl(this, envFile));
		}

		this.setBbVersionService(new DefaultBbVersionServiceImpl(this));
		this.setContextService(new DefaultContextServiceImpl(this));
		this.setUserSessionService(new UserSessionServiceImpl(this));
		this.setSecurityTokenManager(new DefaultSecurityTokenMgrImpl(this));
		this.setModuleConfigService(new DefaultModuleConfigServiceImpl(this));
		this.setCueCardConfig(new DefaultCueCardConfigImpl(this));
		
		this.setPlaylistManagerService(new PlaylistManagerServiceImpl(this));
		this.setPlaylistItemDbLoader(new PlaylistItemDbLoaderImpl(this));
		this.setPlaylistItemDbPersister(new PlaylistItemDbPersisterImpl(this));
		this.setVideoDbLoader(new VideoDbLoaderImpl(this));
		this.setVideoDbPersister(new VideoDbPersisterImpl(this));
	}
		
}