/**
 * Ryan Hardy
 * Guilford Group
 * 
 * BaseNBCAppCtxImpl.java
 * Created: Jun 10, 2013
 */
package blackboard.gs.nbc.context.impl;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import blackboard.db.BbDatabase;
import blackboard.db.ConnectionManager;
import blackboard.db.ConnectionNotAvailableException;
import blackboard.gs.nbc.config.EnvironmentConfiguration;
import blackboard.gs.nbc.context.NBCAppContext;
import blackboard.gs.nbc.playlist.persist.playlistitem.PlaylistItemDbLoader;
import blackboard.gs.nbc.playlist.persist.playlistitem.PlaylistItemDbPersister;
import blackboard.gs.nbc.playlist.persist.video.VideoDbLoader;
import blackboard.gs.nbc.playlist.persist.video.VideoDbPersister;
import blackboard.gs.nbc.playlist.service.PlaylistManagerService;
import blackboard.gs.nbc.security.UserSessionService;
import blackboard.gs.nbc.util.BbPlatformService;
import blackboard.gs.nbc.util.BbVersionService;
import blackboard.gs.nbc.util.ContextService;
import blackboard.gs.nbc.util.CueCardConfiguration;
import blackboard.gs.nbc.util.ModuleConfigService;
import blackboard.gs.nbc.util.SecurityTokenManager;
import blackboard.gs.nbc.util.persist.PersistenceOperationException;
import blackboard.gs.nbc.web.RequestCache;
import blackboard.platform.db.JdbcServiceFactory;

/**
 * Base implementation of {@link NBCAppContext}. Provides default attribute
 * implementation and protected setters for all member fields.
 * 
 * @author Ryan Hardy (rhardy@guilfordgroup.com)
 */
abstract class BaseNBCAppCtxImpl implements NBCAppContext, Serializable {
	
	/**
	 * Default implementation of {@link PersistenceSession} that obtains
	 * Connection from Blackboard's {@link ConnectionManager}.
	 * 
	 * @author Ryan Hardy (rhardy@guilfordgroup.com)
	 *
	 */
	private class PersistenceSessionImpl implements PersistenceSession {
		private final Logger logger = Logger.getLogger("blackboard.gs.nbc.context.impl.BaseNBCAppCtxImpl.PersistenceSessionImpl");
		private Connection connection;
		
		/**
		 * {@inheritDoc}
		 * @throws PersistenceOperationException 
		 */
		@Override
		public Connection getConnection() throws PersistenceOperationException {
			/*logger.debug("Getting Connection from ConnectionManager.");*/
			if (this.connection == null) {
				try {
					BbDatabase bbDb = JdbcServiceFactory.getInstance().getDefaultDatabase();
					ConnectionManager bbDbMgr = bbDb.getConnectionManager();
					this.connection = bbDbMgr.getConnection();
					
					//TODO: this is a fail-safe...should not be needed
					if (this.connection.isClosed()) {
						/*logger.warn("Connection was not previously closed appropriately...trying to re-obtain.");*/
						this.close();
						this.connection = bbDbMgr.getConnection();
					}
					
					this.connection.setAutoCommit(false);
					/*logger.debug("Connection has been successfully obtained from ConnectionManager.");*/
				} catch (ConnectionNotAvailableException e) {
					logger.error("Could not get Connection from ConnectionManager.", e);
					throw new PersistenceOperationException(e);
				} catch (SQLException e) {
					logger.error("Could not get Connection from ConnectionManager.", e);
					throw new PersistenceOperationException(e);
				} catch (RuntimeException e) {
					logger.error("Could not get Connection from ConnectionManager.", e);
					// ensure against runtime exceptions
					throw new PersistenceOperationException(e);
				}
			} else if (logger.isDebugEnabled()) {
				/*logger.debug("Getting already created Connection from PersistenceSession.");*/
			}
			return this.connection;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void close() {
			if (this.connection != null) {
				try {
					/*logger.debug("Releasing Connection from ConnectionManager.");*/
					BbDatabase bbDb = JdbcServiceFactory.getInstance().getDefaultDatabase();
					ConnectionManager bbDbMgr = bbDb.getConnectionManager();
					bbDbMgr.releaseConnection(this.connection);
					/*logger.debug("Connection has been successfully released from ConnectionManager.");*/
				} catch (RuntimeException e) {
					logger.error("An error occurred releasing Connection object.", e);
				} finally {
					this.connection = null;					
				}
			} else if (logger.isDebugEnabled()) {
				/*logger.warn("WARNING: Calling close() on a PersistenceSession with a null Connection.");*/
			}
		}		
	}
	
	// =========================================================================
	
	private static final Logger logger = Logger.getLogger(BaseNBCAppCtxImpl.class);
	/**
	 * 
	 */
	private static final long serialVersionUID = -5529532327674417186L;
	
	private EnvironmentConfiguration environmentConfiguration;
	private BbPlatformService bbPlatformService;
	private BbVersionService bbVersionService;
	private ContextService contextService;
	private UserSessionService userSessionService;
	
	private CueCardConfiguration cueCardConfig;
	private ModuleConfigService moduleConfigService;
	private SecurityTokenManager securityTokenManager;

	private PlaylistManagerService playlistManagerService;
	private PlaylistItemDbLoader playlistItemDbLoader;
	private PlaylistItemDbPersister playlistItemDbPersister;
	private VideoDbLoader videoDbLoader;
	private VideoDbPersister videoDbPersister;
	
	private final PersistenceSession persistenceSession = new PersistenceSessionImpl();
		
	/**
	 */
	protected BaseNBCAppCtxImpl() {
		
	}

	/**
	 * Is called when a new request object has been set on this instance. Will
	 * be called even when request object is <code>null</code>. Implementations
	 * should call {@link #getRequestObject()} to get updated context object (and
	 * ensure against null-pointer issues when request IS null).
	 */
	protected abstract void onUpdatedRequestContext();
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public final void updateRequestContext(Object requestObject) {
		/*if (logger.isDebugEnabled()) {
			logger.debug("Calling updateRequestContext() on " + this + " with: " + requestObject);
		}*/
		if (requestObject instanceof HttpServletRequest) {
			/*logger.debug("ATTENTION: AppContext getting updated with new request object.");*/
		}
		
		// ensure that PersistenceSession is closed (if previously open)
		this.getPersistenceSession().close();
		
		/*logger.debug("Calling onUpdatedRequestContext()...");*/
		onUpdatedRequestContext();
		/*logger.debug("onUpdatedRequestContext() has returned successfully.");*/
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object getRequestObject() {
		// due to 'request object has been recycled' error, no longer
		// storing reference to request...fetch from cache on each call...
		Object request = RequestCache.getRequest();
		return request;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PersistenceSession getPersistenceSession() {
		return this.persistenceSession;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public URL getResource(String relativePath) {
		try {
			/*logger.debug("Getting URL resource for path: " + relativePath);*/
			String fullPath = this.getEnvironmentConfiguration().getFilePath(relativePath);
			URL url = new URL(fullPath);
			return url;
		} catch (MalformedURLException e) {
			logger.error("Can not get URL for resource '" + relativePath + "'", e);
			return null;
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setAttribute(String attName, Object value) {
		try {
			ServletRequest request = (ServletRequest) this.getRequestObject();
			request.setAttribute(attName, value);
		} catch (Exception e) {
			logger.error("Could not set value for context attribute '"+attName+"': " + e.getMessage());
		}
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public <T> T getAttribute(String attName) {
		try {
			ServletRequest request = (ServletRequest) this.getRequestObject();
			@SuppressWarnings("unchecked")
			T val = (T) request.getAttribute(attName);
			return val;
		} catch (Exception e) {
			logger.warn("Could not get value for context attribute '"+attName+"': " + e.getMessage());
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EnvironmentConfiguration getEnvironmentConfiguration() {
		return this.environmentConfiguration;
	}

	/**
	 * @param environmentConfiguration the environmentConfiguration to set
	 */
	protected final void setEnvironmentConfiguration(EnvironmentConfiguration environmentConfiguration) {
		this.environmentConfiguration = environmentConfiguration;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BbPlatformService getBbPlatformService() {
		return this.bbPlatformService;
	}

	/**
	 * @param bbPlatformService the bbPlatformService to set
	 */
	protected final void setBbPlatformService(BbPlatformService bbPlatformService) {
		this.bbPlatformService = bbPlatformService;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BbVersionService getBbVersionService() {
		return this.bbVersionService;
	}

	/**
	 * @param bbVersionService the bbVersionService to set
	 */
	protected final void setBbVersionService(BbVersionService bbVersionService) {
		this.bbVersionService = bbVersionService;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ContextService getContextService() {
		return this.contextService;
	}

	/**
	 * @param contextService the contextService to set
	 */
	protected final void setContextService(ContextService contextService) {
		this.contextService = contextService;
	}

	/**
	 * @return the userSessionService
	 */
	@Override
	public UserSessionService getUserSessionService() {
		return userSessionService;
	}

	/**
	 * @param userSessionService the userSessionService to set
	 */
	protected final void setUserSessionService(UserSessionService userSessionService) {
		this.userSessionService = userSessionService;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CueCardConfiguration getCueCardConfiguration() {
		return this.cueCardConfig;
	}

	/**
	 * @param cueCardConfig the cueCardConfig to set
	 */
	protected final void setCueCardConfig(CueCardConfiguration cueCardConfig) {
		this.cueCardConfig = cueCardConfig;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ModuleConfigService getModuleConfigService() {
		return this.moduleConfigService;
	}

	/**
	 * @param moduleConfigService the moduleConfigService to set
	 */
	protected final void setModuleConfigService(ModuleConfigService moduleConfigService) {
		this.moduleConfigService = moduleConfigService;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SecurityTokenManager getSecurityTokenManager() {
		return this.securityTokenManager;
	}

	/**
	 * @param securityTokenManager the securityTokenManager to set
	 */
	protected final void setSecurityTokenManager(SecurityTokenManager securityTokenManager) {
		this.securityTokenManager = securityTokenManager;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PlaylistManagerService getPlaylistManagerService() {
		return this.playlistManagerService;
	}

	/**
	 * @param playlistManagerService the playlistManagerService to set
	 */
	protected final void setPlaylistManagerService(PlaylistManagerService playlistManagerService) {
		this.playlistManagerService = playlistManagerService;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PlaylistItemDbLoader getPlaylistItemDbLoader() {
		return this.playlistItemDbLoader;
	}

	/**
	 * @param playlistItemDbLoader the playlistItemDbLoader to set
	 */
	protected final void setPlaylistItemDbLoader(PlaylistItemDbLoader playlistItemDbLoader) {
		this.playlistItemDbLoader = playlistItemDbLoader;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PlaylistItemDbPersister getPlaylistItemDbPersister() {
		return this.playlistItemDbPersister;
	}

	/**
	 * @param playlistItemDbPersister the playlistItemDbPersister to set
	 */
	protected final void setPlaylistItemDbPersister(PlaylistItemDbPersister playlistItemDbPersister) {
		this.playlistItemDbPersister = playlistItemDbPersister;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public VideoDbLoader getVideoDbLoader() {
		return this.videoDbLoader;
	}

	/**
	 * @param videoDbLoader the videoDbLoader to set
	 */
	protected final void setVideoDbLoader(VideoDbLoader videoDbLoader) {
		this.videoDbLoader = videoDbLoader;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public VideoDbPersister getVideoDbPersister() {
		return this.videoDbPersister;
	}

	/**
	 * @param videoDbPersister the videoDbPersister to set
	 */
	protected final void setVideoDbPersister(VideoDbPersister videoDbPersister) {
		this.videoDbPersister = videoDbPersister;
	}

}
