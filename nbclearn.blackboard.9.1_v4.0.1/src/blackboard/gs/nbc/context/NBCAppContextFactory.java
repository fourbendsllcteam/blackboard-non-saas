/**
 * Ryan Hardy
 * Guilford Group
 * 
 * NBCAppContextFactory.java
 * Created: Jun 7, 2013
 */
package blackboard.gs.nbc.context;

import java.lang.reflect.Constructor;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import blackboard.gs.nbc.context.impl.SessionBackedNBCAppCtxImpl;
import blackboard.gs.nbc.context.impl.TransientNBCAppCtxImpl;
import blackboard.gs.nbc.web.RequestCache;

/**
 * Factory class used to create {@link NBCAppContext} instances.
 * 
 * @author Ryan Hardy (rhardy@guilfordgroup.com)
 */
public class NBCAppContextFactory {
	private static final Logger logger = Logger.getLogger(NBCAppContextFactory.class);
	/**
	 * Session attribute used to store {@link NBCAppContext} instance.
	 */
	protected static final String SESSION_ATT_NBC_CTX = "blackboard.gs.nbc.context.NBCAppContext";
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	public static NBCAppContext createAppContext(HttpServletRequest request) {
		return createAppContext(request, null);
	}

	/**
	 * 
	 * @param request
	 * @return
	 */
	public static NBCAppContext createAppContext(HttpServletRequest request, Map<String, Object> attributes) {
		NBCAppContext appContext = null;
		if (request != null) {
			
			// manually place request in cache (accounts for cases where
			// request is coming in without hitting this app's listener that
			// puts requests in RequestCache)
			RequestCache.setRequest(request);
			
			boolean isTest = Boolean.parseBoolean(request.getParameter("isTest"));
			if (isTest) {
				try {
					// use reflection as class is only available in local test environment
					@SuppressWarnings("unchecked")
					Class<NBCAppContext> clazz = (Class<NBCAppContext>) Class.forName("blackboard.gs.nbc.context.impl.TestNBCAppCtxImpl");
					Constructor<NBCAppContext> cons = clazz.getConstructor();
					appContext = cons.newInstance();
					/*logger.debug("Returning new test NBCAppContent: " + appContext);*/
				} catch (Exception e) {
					logger.error("Can not create test NBCAppContext instance.", e);
					throw new RuntimeException(e);
				}
			} else {
				
				if(false)
				{
				try {
					HttpSession session = request.getSession();
					Object toChk = session.getAttribute(SESSION_ATT_NBC_CTX);
					/*logger.debug("AppContext in session: " + toChk);*/
					
					if (toChk != null) {
						appContext = (NBCAppContext) toChk;
						/*logger.debug("Returning cached NBCAppContent: " + appContext);*/
					}
				} catch (ClassCastException ce) {
					// this seems to just happen when plugin has been reloaded and there are
					// sessions referencing context objects from previous
					// plugin deployment....
					logger.error("Can not create SessionBackedNBCAppCtxImpl NBCAppContext instance due to ClassCastException: " + ce.getMessage());
				}
				}
				
				try {
					if (appContext == null) {
						appContext = new SessionBackedNBCAppCtxImpl();
						HttpSession session = request.getSession();
						session.setAttribute(SESSION_ATT_NBC_CTX, appContext);
						/*logger.debug("Created and returning new NBCAppContent: " + appContext);*/
					}
				} catch (Exception e) {
					logger.error("Can not create SessionBackedNBCAppCtxImpl NBCAppContext instance.", e);
				}
								
			}
		} 
		// if there is no request, or an error occurred creating
		// non-transient instance...
		if (appContext == null) {
			appContext = new TransientNBCAppCtxImpl();
			/*logger.debug("Created and returning new NBCAppContent: " + appContext);*/
		}
		
		if (attributes != null) {
			for (Map.Entry<String, Object> e : attributes.entrySet()) {
				String attName = e.getKey();
				Object attVal = e.getValue();
				appContext.setAttribute(attName, attVal);
			}
		}
		// ensure that instance has current request as context
		appContext.updateRequestContext(request);
		
		return appContext;		
	}
}
