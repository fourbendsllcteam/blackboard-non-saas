package blackboard.gs.nbc.context;

import java.util.Arrays;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import blackboard.util.resolver.ResolverComponent;

/**
 * {@link ResolverComponent} implementation used in conjunction with
 * {@link NBCContextHandler} to search for '@X@nbc.cuecard@X@' context variables
 * and replace with a script used to inject framework scripts and stylesheets,
 * and fix style issues.
 * 
 * @author Ryan Hardy (rhardy@guilfordgroup.com)
 * 
 * @see NBCContextHandler
 */
public class NBCResolver implements ResolverComponent {
	private static final Logger logger = Logger.getLogger(NBCResolver.class);

	private final ServletContext context;
	private final NBCAppContext appContext;
	
	/**
	 * Creates an instance using the specified request to obtain an {@link NBCAppContext}
	 * and {@link ServletContext}.
	 * 
	 * @param request
	 * @param envConfig
	 * @param bbVersionSvc
	 * @param contextService
	 * @param cueCardConfig
	 */
	public NBCResolver(HttpServletRequest request) {
		this.appContext = NBCAppContextFactory.createAppContext(request);
		this.context = request.getSession().getServletContext();
		this.context.setAttribute("cuecard_script", null);
	}

	/**
	 * 
	 * {@inheritDoc}
	 */
	@Override
	public String[] getKeys() {
		return (new String[] { "nbc" });
	}

	/**
	 * 
	 * {@inheritDoc}
	 */
	@Override
	public String resolve(String method, String attributes[]) {
		/*logger.info("Calling resolve");
		logger.info("\twith method: " + method);
		logger.info("\twith attributes: " + Arrays.toString(attributes));*/

		// method should return null if this Resolver does not handle
		// any variable resolution
		String script = null;
		if ("cuecard".equalsIgnoreCase(method)) {
			// check to see if one-time initialization has occurred
			if (context.getAttribute("cuecard_script") == null) {
				/*logger.info("Embedding cue card script.");*/
				script = this.appContext.getCueCardConfiguration().renderResolverScript();
				context.setAttribute("cuecard_script", "initialized");
			} else {
				/*logger.info("NOT embedding cue card script (has already been initialized).");*/
				script = "";
			}
		} 
		
		return script;
	}
}