/**
 * Ryan Hardy
 * Guilford Group
 * 
 * NBCAppContext.java
 * Created: Jun 7, 2013
 */
package blackboard.gs.nbc.context;

import java.net.URL;
import java.sql.Connection;

import blackboard.gs.nbc.config.EnvironmentConfiguration;
import blackboard.gs.nbc.playlist.persist.playlistitem.PlaylistItemDbLoader;
import blackboard.gs.nbc.playlist.persist.playlistitem.PlaylistItemDbPersister;
import blackboard.gs.nbc.playlist.persist.video.VideoDbLoader;
import blackboard.gs.nbc.playlist.persist.video.VideoDbPersister;
import blackboard.gs.nbc.playlist.service.PlaylistManagerService;
import blackboard.gs.nbc.security.UserSessionService;
import blackboard.gs.nbc.util.BbPlatformService;
import blackboard.gs.nbc.util.BbVersionService;
import blackboard.gs.nbc.util.ContextService;
import blackboard.gs.nbc.util.CueCardConfiguration;
import blackboard.gs.nbc.util.ModuleConfigService;
import blackboard.gs.nbc.util.SecurityTokenManager;
import blackboard.gs.nbc.util.persist.PersistenceOperationException;

/**
 * Serves as a single-point-of-contact for all application services.
 * 
 * @author Ryan Hardy (rhardy@guilfordgroup.com)
 */
public interface NBCAppContext {
	/**
	 * Creates a scoped session for a single database {@link Connection}. 
	 * 
	 * @author Ryan Hardy (rhardy@guilfordgroup.com)
	 */
	public interface PersistenceSession {
		/**
		 * Creates and/or obtains the {@link Connection} object for this
		 * instance.
		 *  
		 * @return
		 * @throws PersistenceOperationException
		 */
		public Connection getConnection() throws PersistenceOperationException;
		/**
		 * Is called to close and release the {@link Connection} object 
		 * created/obtained in first call to {@link #getConnection()}. Will 
		 * NOT propagate any exceptions. Will do nothing if no Connection 
		 * was created/obtained.
		 * 
		 * <br /><br />
		 * 
		 * This method <b>MUST</b> be called at the end of a unit of work
		 * to release the Connection back to the underlying connection pool.
		 */
		public void close();
	}
	
	/**
	 * Gets the {@link PersistenceSession} used for all persistence interactions.
	 * @return
	 */
	public PersistenceSession getPersistenceSession();
	
	/**
	 * Is called to set the current request context on this instance (is cases
	 * where {@link NBCAppContext} is a long-running instance, spanning multiple
	 * requests). Generally, in a server environment, the request
	 * object will be an HTTPServletRequest.
	 * 
	 * @param requestObject
	 */
	public void updateRequestContext(Object requestObject);
	/**
	 * If this instance is backed by an HTTP request, returns the request
	 * object (will be an HTTPServletRequest). May return <code>null</code>.
	 * 
	 * @return
	 */
	public Object getRequestObject();
	/**
	 * Obtains a {@link URL} to the resource with the web-context relative
	 * path provided. Returns <code>null</code> if none found.
	 * 
	 * @param relativePath
	 * @return
	 */
	public URL getResource(String relativePath);
	/**
	 * Allows the storage of a context-scoped attribute within this instance.
	 * 
	 * @param attName
	 * @return
	 */
	public void setAttribute(String attName, Object value);
	/**
	 * Allows the retrieval of a context-scoped attribute from this instance.
	 * 
	 * @param attName
	 * @return
	 */
	public <T> T getAttribute(String attName);
	
	//TODO: finish documentation
	
	public EnvironmentConfiguration getEnvironmentConfiguration();
	
	public BbPlatformService getBbPlatformService();
	public BbVersionService getBbVersionService();
	public ContextService getContextService();
	public UserSessionService getUserSessionService();
	
	public CueCardConfiguration getCueCardConfiguration();
	public ModuleConfigService getModuleConfigService();
	
	public SecurityTokenManager getSecurityTokenManager();
	
	public PlaylistManagerService getPlaylistManagerService();
	public PlaylistItemDbLoader getPlaylistItemDbLoader();
	public PlaylistItemDbPersister getPlaylistItemDbPersister();
	public VideoDbLoader getVideoDbLoader();
	public VideoDbPersister getVideoDbPersister();
}
