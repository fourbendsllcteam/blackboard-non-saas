/**
 * Ryan Hardy
 * Guilford Group
 * 
 * RequestCachingListener.java
 * Created: Jun 14, 2012
 */
package blackboard.gs.nbc.web;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

/**
 * Is used to cache each {@link HttpServletRequest} that is created in the
 * {@link RequestCache}.
 * 
 * @author Ryan Hardy (rhardy@guilfordgroup.com)
 */
public class RequestCachingListener implements ServletRequestListener {
	private static final Logger logger = Logger.getLogger(RequestCachingListener.class);
	
	@Override
	public void requestInitialized(ServletRequestEvent sre) {
		HttpServletRequest request = (HttpServletRequest) sre.getServletRequest();

		/*if (logger.isTraceEnabled()) {
			String uri = request.getRequestURI();
			String qry = request.getQueryString();
			if (qry == null) {
				logger.trace("Request: " + uri);				
			} else {
				logger.trace("Request: " + uri + "?" + qry);				
			}
		}*/
		RequestCache.setRequest(request);
	}
	
	@Override
	public void requestDestroyed(ServletRequestEvent sre) {
		RequestCache.clearRequest();
	}
}