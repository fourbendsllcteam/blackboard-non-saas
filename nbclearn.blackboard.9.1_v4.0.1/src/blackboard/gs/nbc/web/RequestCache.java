/**
 * Ryan Hardy
 * Guilford Group
 * 
 * RequestCache.java
 * Created: Jun 14, 2012
 */
package blackboard.gs.nbc.web;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * Caches the current {@link HttpServletRequest} in a {@link ThreadLocal}
 * variable, and provides static access to the request.
 * 
 * @author Ryan Hardy (rhardy@guilfordgroup.com)
 */
public class RequestCache {
	private static final Logger logger = Logger.getLogger(RequestCache.class);
	
	private static ThreadLocal<HttpServletRequest> request = new ThreadLocal<HttpServletRequest>();
	
	/**
	 * Caches the {@link HttpServletRequest} associated with this thread.
	 * 
	 * @param request
	 */
	public static void setRequest(HttpServletRequest request) {
		/*if (logger.isTraceEnabled()) {
			logger.trace("Setting request in RequestCache: " + request);
		}*/
		RequestCache.request.set(request);
	}
	
	/**
	 * Obtains the {@link HttpServletRequest} associated with this thread.
	 * 
	 * @return the {@link HttpServletRequest} for this thread
	 */
	public static HttpServletRequest getRequest() {
		HttpServletRequest req = RequestCache.request.get();
		/*if (logger.isTraceEnabled()) {
			logger.trace("Getting request from RequestCache: " + req);
		}*/
		if (req == null) {
			/*if (logger.isEnabledFor(Level.WARN)) {
				logger.warn("ATTENTION: cached request is null.");
			}
			else if (logger.isDebugEnabled()) {
                // create an exception in log statement so stack trace will be printed
                logger.warn("ATTENTION: cached request is null.", new Exception());
            }*/
		}
		return req;
	}
	
	/**
	 * Clears the cached {@link HttpServletRequest} for this thread.
	 */
	public static void clearRequest() {
		HttpServletRequest request = getRequest();
		/*if (logger.isTraceEnabled()) {
			logger.trace("Removing request from RequestCache: " + request);
		}*/
		RequestCache.request.remove();
	}
}
