/**
 * Ryan Hardy
 * Guilford Group
 * 
 * DefaultEnvConfigImpl.java
 * Created: May 14, 2012
 */
package blackboard.gs.nbc.config.impl;

import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

import blackboard.gs.nbc.Constants;
import blackboard.gs.nbc.config.EnvironmentConfiguration;
import blackboard.gs.nbc.context.NBCAppContext;
import blackboard.gs.nbc.util.BbPlatformService;

/**
 * Original implementations of {@link EnvironmentConfiguration} that relies upon
 * hard-coded {@link Properties} file to load data.
 * 
 * @author Ryan Hardy
 */
public class DefaultEnvConfigImpl implements EnvironmentConfiguration {
	private static final Logger logger = Logger.getLogger(DefaultEnvConfigImpl.class);

	private final BbPlatformService platformService;

	/**
	 * The classpath-relative path to the properties file to load environment
	 * information from.
	 */
	private final String propertiesFile;
	/**
	 * Flag indicating if properties have been loaded. Properties will be
	 * lazy-loaded on first call to an impl. method.
	 */
	private boolean isLoaded = false;

	// environment properties

	private String nbcUrl;
	private String outboundUrl;
	private String outboundPlaylistUrl;
	private String cuecardUrl;
	private String vi;
	private String snasUrl;
	private String securityTokenUrl;
	private String embedPlayerUrl;
	private String moduleRssUrl;
	private String modulePlayerUrl;
	private String cuecardXmlUrl;

	/**
	 * @param appContext
	 */
	public DefaultEnvConfigImpl(NBCAppContext appContext) {
		this(appContext, "/" + Constants.ENVIRONMENT);
	}

	/**
	 * @param appContext
	 * @param envFile
	 *            the name of the file containing environment properties (is
	 *            generally only provided when used for testing)
	 */
	public DefaultEnvConfigImpl(NBCAppContext appContext, String envFile) {
		this.platformService = appContext.getBbPlatformService();
		this.propertiesFile = envFile;
	}

	/**
	 * Checks to see if properties have been loaded yet, loading them if not.
	 */
	private void checkLoaded() {
		if (this.isLoaded) {
			return;
		}
		this.loadProperties();
		this.isLoaded = true;
	}

	/**
	 * 
	 */
	@Override
	public String getFilePath(String file) {
		checkLoaded();
		StringBuilder bldr = new StringBuilder();
		bldr.append(getHostUrl());
		bldr.append(this.platformService.getUriStem());
		bldr.append(file);
		return bldr.toString();
	}

	/**
	 * Obtains the host URL for this deployment, checking for forced SSL.
	 * 
	 * @return
	 */
	@Override
	public String getHostUrl() {
		checkLoaded();
		String url;
		if (this.platformService.isForcedSystemSSL()) {
			url = "https://" + this.platformService.getHostName();
		} else {
			url = "http://" + this.platformService.getHostName();
		}
		/*if (logger.isTraceEnabled()) {
			logger.trace("Host URL: '" + url + "'");
		}*/
		return url;
	}

	/**
	 * 
	 */
	@Override
	public String getNbcUrl() {
		checkLoaded();
		return nbcUrl;
	}

	/**
	 * 
	 */
	@Override
	public String getOutboundUrl() {
		checkLoaded();
		return outboundUrl;
	}

	/**
	 * 
	 */
	@Override
	public String getCuecardUrl() {
		checkLoaded();
		return cuecardUrl;
	}

	/**
	 * 
	 */
	@Override
	public String getVi() {
		checkLoaded();
		String uuid = this.platformService.getUUID();
		if (uuid != null) {
			return uuid;
		} else {
			return this.vi;
		}
	}

	private void setNbcUrl(String nbcUrl) {
		this.nbcUrl = nbcUrl;
	}

	private void setOutboundUrl(String outboundUrl) {
		this.outboundUrl = outboundUrl;
	}

	private void setCuecardUrl(String cuecardUrl) {
		this.cuecardUrl = cuecardUrl;
	}

	private void setVi(String vi) {
		this.vi = vi;
	}

	private void loadProperties() {
		InputStream in = null;
		try {
			Properties properties = new Properties();
			in = DefaultEnvConfigImpl.class.getResourceAsStream(this.propertiesFile);
			properties.load(in);
			if (this.platformService.isForcedSystemSSL()) {
				setNbcUrl(properties.getProperty("snbc_url"));
				setOutboundUrl(properties.getProperty("soutbound_url"));
				setCuecardUrl(properties.getProperty("scuecard_url"));
				setOutboundPlaylistUrl(properties.getProperty("soutbound_playlist_url"));
				setSnasUrl(properties.getProperty("sec_snas_url"));
				setSecurityTokenUrl(properties.getProperty("security_token_url"));
				setEmbedPlayerUrl(properties.getProperty("sec_embed_player_url"));
				setModuleRssUrl(properties.getProperty("sec_module_rss_url"));
				setModulePlayerUrl(properties.getProperty("sec_module_player"));
				setCuecardXmlUrl(properties.getProperty("sec_cuecard_xml_url"));
			} else {
				setNbcUrl(properties.getProperty("nbc_url"));
				setOutboundUrl(properties.getProperty("outbound_url"));
				setCuecardUrl(properties.getProperty("cuecard_url"));
				setOutboundPlaylistUrl(properties.getProperty("outbound_playlist_url"));
				setSnasUrl(properties.getProperty("snas_url"));
				setSecurityTokenUrl(properties.getProperty("sec_token_url"));
				setEmbedPlayerUrl(properties.getProperty("embed_player_url"));
				setModuleRssUrl(properties.getProperty("module_rss_url"));
				setModulePlayerUrl(properties.getProperty("module_player"));
				setCuecardXmlUrl(properties.getProperty("cuecard_xml_url"));
			}

			setVi(properties.getProperty("vi"));
		} catch (Exception ex) {
			throw new RuntimeException("Could not find environment template file.", ex);
		} finally {
			if (null != in) {
				try {
					in.close();
				} catch (Exception ioe) {
					logger.error("Could not close input reader for template: " + Constants.ENVIRONMENT, ioe);
				}
			}
		}
	}

	private void setOutboundPlaylistUrl(String outboundPlaylistUrl) {
		this.outboundPlaylistUrl = outboundPlaylistUrl;
	}

	@Override
	public String getOutboundPlayListUrl() {
		checkLoaded();
		return outboundPlaylistUrl;
	}

	@Override
	public String getSnasUrl() {
		checkLoaded();
		return snasUrl;
	}

	private void setSnasUrl(String snasUrl) {
		this.snasUrl = snasUrl;
	}

	public String getSecurityTokenUrl() {
		return securityTokenUrl;
	}

	public void setSecurityTokenUrl(String securityTokenUrl) {
		this.securityTokenUrl = securityTokenUrl;
	}

	public void setEmbedPlayerUrl(String embedPlayerUrl) {
		this.embedPlayerUrl = embedPlayerUrl;
	}

	public String getEmbedPlayerUrl() {
		return embedPlayerUrl;
	}

	public String getModuleRssUrl() {
		return moduleRssUrl;
	}

	public void setModuleRssUrl(String moduleRssUrl) {
		this.moduleRssUrl = moduleRssUrl;
	}

	public String getModulePlayerUrl() {
		return modulePlayerUrl;
	}

	public void setModulePlayerUrl(String modulePlayerUrl) {
		this.modulePlayerUrl = modulePlayerUrl;
	}

	public String getCuecardXmlUrl() {
		return cuecardXmlUrl;
	}

	public void setCuecardXmlUrl(String cuecardXmlUrl) {
		this.cuecardXmlUrl = cuecardXmlUrl;
	}

}