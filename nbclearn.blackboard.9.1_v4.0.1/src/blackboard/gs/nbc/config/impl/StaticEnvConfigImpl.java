/**
 * Ryan Hardy
 * Guilford Group
 * 
 * StaticEnvConfigImpl.java
 * Created: May 14, 2012
 */
package blackboard.gs.nbc.config.impl;

import java.util.Properties;

import blackboard.gs.nbc.config.EnvironmentConfiguration;

/**
 * {@link EnvironmentConfiguration} implementation that is initialized via 
 * {@link Properties} instance provided to constructor.
 * 
 * @author Ryan Hardy
 */
public class StaticEnvConfigImpl implements EnvironmentConfiguration {

	private String nbcUrl;
	private String outboundUrl;
	private String outboundPlaylistUrl;
	private String cuecardUrl;
	private String vi;
	private String hostUrl;
	private String snasUrl;
	private String securityTokenUrl;
	private String embedPlayerUrl;
	private String moduleRssUrl;
	private String modulePlayerUrl;
	public String getCuecardXmlUrl() {
		return cuecardXmlUrl;
	}

	public void setCuecardXmlUrl(String cuecardXmlUrl) {
		this.cuecardXmlUrl = cuecardXmlUrl;
	}

	private String cuecardXmlUrl;
	
	
	
	/**
	 * Creates an instance that will NOT use SSL.
	 * @param props
	 */
	public StaticEnvConfigImpl(Properties props) {
		this(props, false);
	}
	
	/**
	 * 
	 * @param props
	 * @param useSSL
	 */
	public StaticEnvConfigImpl(Properties props, boolean useSSL) {
		this.vi = props.getProperty("vi");
		this.hostUrl = props.getProperty("host_url");
		
		if (useSSL) {
			this.nbcUrl = props.getProperty("snbc_url");
			this.outboundUrl = props.getProperty("soutbound_url");
			this.cuecardUrl = props.getProperty("scuecard_url");
			this.outboundPlaylistUrl = props.getProperty("soutbound_playlist_url");
			this.snasUrl = props.getProperty("sec_snas_url");
			this.securityTokenUrl = props.getProperty("security_token_url");
			this.embedPlayerUrl = props.getProperty("sec_embed_player_url");
			this.moduleRssUrl = props.getProperty("sec_module_rss_url");
			this.modulePlayerUrl = props.getProperty("sec_module_player");
			this.cuecardXmlUrl = props.getProperty("sec_cuecard_xml_url");
		} else {
			this.nbcUrl = props.getProperty("nbc_url");
			this.outboundUrl = props.getProperty("outbound_url");
			this.cuecardUrl = props.getProperty("cuecard_url");		
			this.outboundPlaylistUrl = props.getProperty("outbound_playlist_url");
			this.snasUrl = props.getProperty("snas_url");
			this.securityTokenUrl = props.getProperty("sec_token_url");
			this.embedPlayerUrl = props.getProperty("embed_player_url");
			this.moduleRssUrl = props.getProperty("module_rss_url");
			this.modulePlayerUrl = props.getProperty("module_player");
			this.cuecardXmlUrl = props.getProperty("cuecard_xml_url");
		}
	}
	
	public String getModuleRssUrl() {
		return moduleRssUrl;
	}

	public void setModuleRssUrl(String moduleRssUrl) {
		this.moduleRssUrl = moduleRssUrl;
	}

	@Override
	public String getFilePath(String file) {
		return getHostUrl() + "/" + file;
	}

	@Override
	public String getNbcUrl() {
		return this.nbcUrl;
	}

	@Override
	public String getOutboundUrl() {
		return this.outboundUrl;
	}

	@Override
	public String getCuecardUrl() {
		return this.cuecardUrl;
	}

	@Override
	public String getVi() {
		return this.vi;
	}

	@Override
	public String getHostUrl() {
		return this.hostUrl;
	}

	@Override
	public String getOutboundPlayListUrl() {
		return outboundPlaylistUrl;
	}

	@Override
	public String getSnasUrl() {
		// TODO Auto-generated method stub
		return snasUrl;
	}

	@Override
	public String getSecurityTokenUrl() {
		// TODO Auto-generated method stub
		return securityTokenUrl;
	}

	public String getEmbedPlayerUrl() {
		return embedPlayerUrl;
	}

	public void setEmbedPlayerUrl(String embedPlayerUrl) {
		this.embedPlayerUrl = embedPlayerUrl;
	}

	public void setSecurityTokenUrl(String securityTokenUrl) {
		this.securityTokenUrl = securityTokenUrl;
	}

	public String getModulePlayerUrl() {
		return modulePlayerUrl;
	}

	public void setModulePlayerUrl(String modulePlayerUrl) {
		this.modulePlayerUrl = modulePlayerUrl;
	}
	
}