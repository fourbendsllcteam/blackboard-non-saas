/**
 * Ryan Hardy
 * Guilford Group
 * 
 * EnvironmentConfiguration.java
 * Created: May 14, 2012
 */
package blackboard.gs.nbc.config;

/**
 * Used to create implementations that can load environment info, primarily deployment URLs.
 * 
 * @author Ryan Hardy
 */
public interface EnvironmentConfiguration {
	/**
	 * 
	 * @return
	 */
	public String getNbcUrl();
	/**
	 * 
	 * @return
	 */
	public String getOutboundUrl();
	
	public String getOutboundPlayListUrl();
	/**
	 * 
	 * @return
	 */
	public String getCuecardUrl();
	/**
	 * 
	 * @return
	 */
	public String getVi();
	/**
	 * Obtains the host URL for this deployment, checking for forced SSL.
	 * @return
	 */
	public String getHostUrl();
	/**
	 * Obtains a fully qualified URL string to the plugin file provided, based
	 * on the current plugin context.
	 * @param file
	 * @return
	 */
	public String getFilePath(String file);
	
	
	public String getSnasUrl();
	
	public String getSecurityTokenUrl();
	
	public String getEmbedPlayerUrl();
	
	public String getModuleRssUrl();
	public String getModulePlayerUrl();
	
	public String getCuecardXmlUrl();
}

