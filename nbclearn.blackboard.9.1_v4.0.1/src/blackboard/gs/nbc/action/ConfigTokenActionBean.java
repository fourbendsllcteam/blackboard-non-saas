package blackboard.gs.nbc.action;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import blackboard.data.registry.SystemRegistryEntry;
import blackboard.gs.nbc.Constants;
import blackboard.gs.nbc.context.NBCAppContext;
import blackboard.gs.nbc.context.NBCAppContextFactory;
import blackboard.gs.nbc.playlist.service.PlaylistManagerService;
import blackboard.gs.nbc.util.ActionUrl;
import blackboard.gs.nbc.util.SecurityTokenManager;
import blackboard.persist.KeyNotFoundException;
import blackboard.persist.PersistenceException;
import blackboard.persist.registry.SystemRegistryEntryDbLoader;
import blackboard.platform.security.NonceUtil;
import blackboard.platform.security.SecurityUtil;
import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.HandlesEvent;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.validation.Validate;

/**
 * Action for security token configuration page.
 * 
 * @author kborgeson
 */
@UrlBinding("/execute/configSecurityToken.action")
public class ConfigTokenActionBean implements ActionBean{
	private static Logger logger = Logger.getLogger(ConfigTokenActionBean.class);

	private ActionBeanContext context;

	@Validate(required = false, on = { "top_Submit", "bottom_Submit" }, minlength = 0, maxlength = 32)
	private String securityToken = "";

	@Validate(required = false, on = { "top_Submit", "bottom_Submit" })
	private String courseId = "";

	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public ActionBeanContext getContext() {
		return context;
	}

	public void setContext(ActionBeanContext context) {
		this.context = context;
	}

	public String getSecurityToken() {
		return securityToken;
	}

	public void setSecurityToken(String securityToken) {
		this.securityToken = securityToken;
	}

	@DefaultHandler
	@HandlesEvent("load")
	public Resolution loadToken() {
		SystemRegistryEntryDbLoader sysRegistryLoader = null;
		SystemRegistryEntry sysRegEntry = null;

		if (!isUserHasUpdatePermission()) {
			return new ForwardResolution(ActionUrl.getErrorPageUrl());
		}

		try {
			sysRegistryLoader = SystemRegistryEntryDbLoader.Default.getInstance();
			try {
				sysRegEntry = sysRegistryLoader.loadByKey(Constants.SYSTEM_REGISTRY_KEY);
			} catch (KeyNotFoundException knfe) {
				logger.error("Existing value for the NBC Security License was not found.");
			}
		} catch (PersistenceException pe) {
			throw new RuntimeException("Error loading sys reg loader.", pe);
		}

		if (null != sysRegEntry) {
			securityToken = sysRegEntry.getValue();
		}

		return new ForwardResolution(Constants.FORWARD_CONFIG_PAGE);
	}

	@HandlesEvent("top_Submit")
	public Resolution topSubmit() {
		return bottomSubmit();
	}

	@HandlesEvent("bottom_Submit")
	public Resolution bottomSubmit() {

		if (!isUserHasUpdatePermission()) {
			return new ForwardResolution(ActionUrl.getErrorPageUrl());
		}
	//	Thread t1 = new Thread(this, "t1");
	//	t1.start(); 
		if (NonceUtil.validate(context.getRequest(), "nonce_id")) {
			if (saveToken()) {
				return new ForwardResolution(Constants.FORWARD_CONFIG_SUCCESS);
			} else {
				return new ForwardResolution(Constants.FORWARD_CONFIG_FAIL);
			}
		} else {
			return new ForwardResolution(ActionUrl.getErrorPageUrl());
		}

	}

	private boolean saveToken() {
		try {
			HttpServletRequest request = this.getContext().getRequest();
			NBCAppContext appContext = NBCAppContextFactory.createAppContext(request);
			SecurityTokenManager tokenManager = appContext.getSecurityTokenManager();
			tokenManager.saveSecurityToken(this.getSecurityToken(),appContext);

			PlaylistManagerService playlistManager = appContext.getPlaylistManagerService();
			
			if(this.courseId!=null){
				String[] courseIdArray = this.courseId.trim().split(",");
				for(String crsId : courseIdArray){
					playlistManager.updateOldPlayerContent(securityToken,
							appContext.getEnvironmentConfiguration().getCuecardXmlUrl(),
							appContext.getEnvironmentConfiguration().getNbcUrl(),
							"https://static.nbclearn.com/files/highered/site/download/blackboard/install/nbc_logo_mashup.gif",
							"https://static.nbclearn.com/files/highered/site/download/blackboard/install/play-icon.png",
							appContext.getEnvironmentConfiguration().getVi(),crsId.trim());
				}
			}
			
			// Playlist Migration if any
			playlistManager.exportUsersPlaylist();
			

			return true;
		} catch (Exception e) {
			logger.error("Could not save security token: " + e.getMessage());
			return false;
		}
	}

	private boolean isUserHasUpdatePermission() {
		if (SecurityUtil.userHasEntitlement("system.plugin.MODIFY")) {
			/*if (logger.isDebugEnabled()) {
				logger.debug("User has permission to update configuration token value");
			}*/
			return true;
		}
		return false;
	}

	/*@Override
	public void run() {
		logger.debug("BEGINING OF EXPORT PLAYLIST ACTION------>");
		try {
			HttpServletRequest request = getContext().getRequest();
			NBCAppContext appContext = NBCAppContextFactory.createAppContext(request);
			PlaylistManagerService playlistManager = appContext.getPlaylistManagerService();
			// Convert html player content in db again to new template
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error in export------>" + e);
		}

	}*/
}
