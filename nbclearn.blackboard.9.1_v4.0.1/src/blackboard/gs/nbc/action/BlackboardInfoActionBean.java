/**
 * Ryan Hardy
 * Guilford Group
 * 
 * BlackboardInfoActionBean.java
 * Created: Jun 20, 2012
 */
package blackboard.gs.nbc.action;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;
import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.StreamingResolution;
import net.sourceforge.stripes.action.UrlBinding;


import org.apache.log4j.Logger;

import blackboard.gs.nbc.context.NBCAppContext;
import blackboard.gs.nbc.context.NBCAppContextFactory;
import blackboard.gs.nbc.util.BbVersionService;
import blackboard.gs.nbc.playlist.persist.playlistitem.PlaylistItemDbLoader;

/**
 * {@link ActionBean} used to provide information about the underlying
 * Blackboard installation (i.e. version info, currently active theme, etc.) 
 * via Ajax call.
 * 
 * @author Ryan Hardy (rhardy@guilfordgroup.com)
 */
@UrlBinding("/bb_info.action")
public class BlackboardInfoActionBean implements ActionBean {
	private static final Logger logger = Logger.getLogger(BlackboardInfoActionBean.class);
	
	private ActionBeanContext context;
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setContext(ActionBeanContext context) {
		this.context = context;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ActionBeanContext getContext() {
		return this.context;
	}
	
	/**
	 * Is main (only) handler method for this {@link ActionBean}.
	 * 
	 * @return
	 */
	@DefaultHandler
	public Resolution process() { 
		/*logger.debug("Attempting to obtain security token and context info.");*/
		try {
			HttpServletRequest request = this.getContext().getRequest();
			NBCAppContext appContext = NBCAppContextFactory.createAppContext(request);
			BbVersionService versionSvc = appContext.getBbVersionService();
			PlaylistItemDbLoader playlistloader=appContext.getPlaylistItemDbLoader();
			
			JSONObject json = new JSONObject();
			
			json.put("fullVersion", versionSvc.getFullVersion());
			json.put("majorVersion", versionSvc.getMajorVersion());
			json.put("minorVersion", versionSvc.getMinorVersion());
			json.put("servicePack", versionSvc.getServicePack());
			json.put("currentTheme", versionSvc.getCurrentThemeName());
			
			json.put("nbcVersion", playlistloader.getNBCBBVersion());
			
			return new StreamingResolution("application/json", json.toString());
		} catch (Exception e) {
			logger.error("Could not obtain security token and context info.", e);
			return new StreamingResolution("application/json", "{\"error\": \"Could not obtain Blackboard info.\"}");
			
		}
		
	}
}