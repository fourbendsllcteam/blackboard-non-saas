package blackboard.gs.nbc.action;

import blackboard.gs.nbc.util.ActionUrl;
import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;

@UrlBinding("/error.action")
public class ErrorActionBean implements ActionBean {
	private ActionBeanContext context;

	public ActionBeanContext getContext() {
		return context;
	}

	public void setContext(ActionBeanContext context) {
		this.context = context;

	}

	@DefaultHandler
	public Resolution displayErrorPage() {
		return new ForwardResolution(ActionUrl.getErrorPageUrl());
	}

}
