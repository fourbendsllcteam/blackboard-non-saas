package blackboard.gs.nbc.action;

import net.sourceforge.stripes.action.Resolution;

public interface Handler {
	
	public Resolution handle(RequestControllerActionBean actionBean) throws Exception;

}
