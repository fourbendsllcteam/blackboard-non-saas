	package blackboard.gs.nbc.action;


import javax.servlet.http.HttpServletRequest;

import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import blackboard.gs.nbc.context.NBCAppContext;
import blackboard.gs.nbc.context.NBCAppContextFactory;
import blackboard.gs.nbc.parameters.ActionParam;
import blackboard.gs.nbc.parameters.ModeParam;
import blackboard.gs.nbc.util.ActionUrl;



public class ReturnBackHandler implements Handler {
	
	public Resolution handle(RequestControllerActionBean actionBean) throws Exception {
		
		Resolution resolution= null;
		HttpServletRequest request = actionBean.getContext().getRequest();
		NBCAppContext appContext = NBCAppContextFactory.createAppContext(request);
		
		appContext.getUserSessionService().loadUser();
		try
		{

		if (actionBean.getMode().equals(ModeParam.EMBED.toString()))
		{
			if (actionBean.getAction().equals(ActionParam.CREATE.toString()) || (actionBean.getAction().equals(ActionParam.MODIFY.toString())))
			{
				resolution = new RedirectResolution(ActionUrl.getTargetContentUrlWithFrameset(actionBean.getCourse_id(), actionBean.getContent_id(), actionBean.getContext().getRequest(), actionBean.getAction()), false);
			}
			else if (actionBean.getAction().equals(ActionParam.MODULE.toString()))
			{
				resolution = new RedirectResolution(ActionUrl.getCourseUrlWithFrameset(actionBean.getCourse_id(), actionBean.getContext().getRequest()), false);
			}
			else if (actionBean.getAction().equals(ActionParam.MASHUP.toString()))
			{
				return new ForwardResolution(ActionUrl.getCloseWindowUrl());
			}
		} 
		else if (actionBean.getMode().equals(ModeParam.BROWSE.toString()))
		{
			if ((null!=actionBean.getAction())&&(actionBean.getAction().equals(ActionParam.MODULE.toString())) || (null!=actionBean.getAction())&&(actionBean.getAction().equals(ActionParam.COURSETOOL.toString())))
			{
				
				resolution = new RedirectResolution(ActionUrl.getCourseUrlWithFrameset(actionBean.getCourse_id(), actionBean.getContext().getRequest()), false);
			}
			else if ((null!=actionBean.getAction())&&(actionBean.getAction().equals(ActionParam.TOOL.toString())))
			{
 				//Ramesh BB - 22 Fix : Start
				
				if (null!=actionBean.getContent_id())
				{
					
					resolution = new RedirectResolution(ActionUrl.getContentUrlWithFrameset(actionBean.getCourse_id(), actionBean.getContent_id(), actionBean.getContext().getRequest()), false);
				}
				else
				{
					
					resolution = new RedirectResolution(ActionUrl.getCourseToolUrlWithFrameset(actionBean.getCourse_id(), actionBean.getContext().getRequest()), false);
				}
				// Ramesh BB - 22 Fix : End
			}
			/*else if((null!=actionBean.getAction())&&(actionBean.getAction().equals(ActionParam.COURSETOOL.toString()))){
				
				System.out.println("------------------- before generating return Url --- for the coursetool option");
				resolution = new RedirectResolution(ActionUrl.getCourseUrlWithFrameset(actionBean.getCourse_id(), actionBean.getContext().getRequest()), false);
				
			}*/
			
			else
			{
				resolution = new RedirectResolution(ActionUrl.getHomeUrl(), false);
			}
		}
		else
		{
			resolution = null;
		}
			
		} catch (Exception ex) {
			
			throw new Exception("",
					ex);
	}
		return resolution;
	}
	
	
}
