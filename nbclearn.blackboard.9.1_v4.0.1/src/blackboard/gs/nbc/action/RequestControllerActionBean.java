package blackboard.gs.nbc.action;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;

import org.apache.log4j.Logger;

import blackboard.data.course.Course;
import blackboard.data.user.User;
import blackboard.gs.nbc.context.NBCAppContext;
import blackboard.gs.nbc.context.NBCAppContextFactory;
import blackboard.gs.nbc.parameters.ActionParam;
import blackboard.gs.nbc.parameters.ModeParam;
import blackboard.gs.nbc.parameters.TypeParam;
import blackboard.gs.nbc.playlist.data.playlistitem.PlaylistItem;
import blackboard.gs.nbc.playlist.service.PlaylistManagerService;
import blackboard.gs.nbc.util.ActionUrl;
import blackboard.gs.nbc.util.ContextService;
import blackboard.gs.nbc.util.impl.DefaultContextServiceImpl;

@UrlBinding("/req_con.action")
public class RequestControllerActionBean implements ActionBean {
	private static final Logger logger = Logger.getLogger(RequestControllerActionBean.class);
	
	private ActionBeanContext context;
	private String bbsession_id;
	private String course_id;
	private String action;
	private String content_id;
	private String mode;
	private String type;
	private String video_id;
	private String name;
	private String description;
	private String air_date;
	private String duration;
	private String thumbnail_url;
	private String content_type;
	private int item_key = -1;
	private List<Integer> item_keys = new ArrayList<Integer>();
	private List<PlaylistItem> playlist = new ArrayList<PlaylistItem>();
	private BusinessHandler handler;
	private String location;
	private String returnLoc;
	private String iframeContent;
	private String cuecardID;
	
	/**
	 * Pretty-prints all of the parameter values to the provided {@link Logger}.
	 * 
	 * @param request
	 * @param logger
	 */
	public static void displayRequestParams(HttpServletRequest request, Logger logger) {
		/*logger.debug("=========================================================");
		logger.debug("Request params:");*/
		@SuppressWarnings("unchecked") 
		Enumeration<String> e = request.getParameterNames();
		while (e.hasMoreElements()) {
			String param = e.nextElement();
			String[] vals = request.getParameterValues(param);
			/*logger.debug("\t" + param + " = " + Arrays.toString(vals));*/
		}
	}
	
	/**
	 * Pretty-prints all of the request attribute values to the provided {@link Logger}.
	 * 
	 * @param request
	 * @param logger
	 */
	public static void displayRequestAttributes(HttpServletRequest request, Logger logger) {
		/*logger.debug("=========================================================");
		logger.debug("Request attributes:");*/
		@SuppressWarnings("unchecked") 
		Enumeration<String> e = request.getAttributeNames();
		while (e.hasMoreElements()) {
			String att = e.nextElement();
			Object val = request.getAttribute(att);
			/*logger.debug("\t" + att + " = " + val);*/
		}
	}

	public ActionBeanContext getContext() {
		return context;
	}

	public void setContext(ActionBeanContext context) {
		this.context = context;
	}

	public void setBbsession_id(String bbsession_id) {
		this.bbsession_id = bbsession_id;
	}

	public String getBbsession_id() {
		return bbsession_id;
	}

	public void setCourse_id(String course_id) {
		this.course_id = course_id;
	}

	public String getCourse_id() {
		return course_id;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getAction() {
		return action;
	}

	public void setContent_id(String content_id) {
		this.content_id = content_id;
	}

	public String getContent_id() {
		return content_id;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getMode() {
		return mode;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public void setVideo_id(String video_id) {
		this.video_id = video_id;
	}

	public String getVideo_id() {
		return video_id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setAir_date(String air_date) {
		this.air_date = air_date;
	}

	public String getAir_date() {
		return air_date;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getDuration() {
		return duration;
	}

	public void setThumbnail_url(String thumbnail_url) {
		this.thumbnail_url = thumbnail_url;
	}

	public String getThumbnail_url() {
		return thumbnail_url;
	}

	public void setContent_type(String content_type) {
		this.content_type = content_type;
	}

	public String getContent_type() {
		return content_type;
	}

	public void setItem_key(int item_key) {
		this.item_key = item_key;
	}

	public int getItem_key() {
		return item_key;
	}

	public void setItem_keys(List<Integer> item_keys) {
		this.item_keys = item_keys;
	}

	public List<Integer> getItem_keys() {
		return item_keys;
	}

	public void setPlaylist(List<PlaylistItem> playlist) {
		this.playlist = playlist;
	}

	public List<PlaylistItem> getPlaylist() {
		return playlist;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}

	public String getLocation() {
		return location;
	}
	public void setReturnLoc(String returnLoc) {
		this.returnLoc = returnLoc;
	}

	public String getReturnLoc() {
		return returnLoc;
	}

	public String getIframeContent() {
		return iframeContent;
	}

	public void setIframeContent(String iframeContent) {
		this.iframeContent = iframeContent;
	}

	public String getCuecardID() {
		return cuecardID;
	}

	public void setCuecardID(String cuecardID) {
		this.cuecardID = cuecardID;
	}

	@DefaultHandler
	public Resolution callHandler() throws Exception {
		TypeParam type = TypeParam.getTypeParam(getType());
		ModeParam mode = ModeParam.getModeParam(getMode());
		ActionParam action = ActionParam.getActionParam(getAction());
		
		/*logger.debug("Type: " + type);
		logger.debug("Mode: " + mode);
		logger.debug("Action: " + action);*/
		
		Resolution resolution = getResolution(type, mode, action);
		/*logger.debug("Resolution: " + resolution);*/
		/*if (resolution == null) {
			logger.warn("ATTENTION: Resolution is null.");
		}*/
		
		return resolution;
	}

	/**
	 * Helper method used to obtain and call handler, and returns the {@link Resolution}
	 * for this call.
	 * 
	 * @param type
	 * @param mode
	 * @param action
	 * @return
	 * @throws Exception
	 */
	private Resolution getResolution(TypeParam type, ModeParam mode, ActionParam action) throws Exception {
		Resolution resolution = null;
		switch (type) {
		case PLAYLIST:
			// adds a PlaylistCourse to session for spoofing in calls to 
			// ContextService
			setPlaylistCourseInSession();
			List<PlaylistItem> itemList = getPlaylistItems();
			boolean isEmpty = itemList.isEmpty();
			switch (mode) {
			case EMBED:
				/*logger.info("Calling displayPlaylistForEmbed, isEmpty=" + isEmpty);*/
				resolution = (isEmpty) ? displayEmptyPlaylistForEmbed() : displayPlaylistForEmbed();
				break;
			case BROWSE:
				/*logger.info("Calling displayPlaylistForBrowse, isEmpty=" + isEmpty);*/
				resolution = (isEmpty) ? displayEmptyPlaylistForBrowse() : displayPlaylistForRemove();
				break;
			}	
			break;
		case SAVE:
			/*logger.info("Calling saveInPlaylist");*/
			resolution = saveInPlaylist();
			break;
		case EMBED: 
			switch (action) {
			case CREATE:
				/*logger.info("Calling createCustomContent");*/
				resolution = createCustomContent();
				break;
			case MODIFY:
				/*logger.info("Calling modifyCustomContent");*/
				resolution = modifyCustomContent();
				break;
			case MODULE:
				/*logger.info("Calling embedInCourseModule");*/
				resolution = embedInCourseModule();
				break;
			case MASHUP:
				/*logger.info("Calling saveMashup");*/
				resolution = saveMashup();
				break;
			}
			break;
		case RETURN:
			/*logger.info("Calling returnBack");*/
			resolution = returnBack();
			break;
		}
		return resolution;
	}

	public Resolution saveInPlaylist() throws Exception {
		handler = new BusinessHandler(new SaveInPlaylistHandler());
		return handler.handle(this);
	}

	public Resolution removeFromPlaylist() throws Exception {
		handler = new BusinessHandler(new RemoveFromPlaylistHandler());
		return handler.handle(this);
	}

	public Resolution embedInCourseModule() throws Exception {
		handler = new BusinessHandler(new CreateCourseModuleHandler());
		return handler.handle(this);
	}

	public Resolution createCustomContent() throws Exception {
		handler = new BusinessHandler(new CreateCustomContentHandler());
		return handler.handle(this);
	}

	public Resolution modifyCustomContent() throws Exception {
		handler = new BusinessHandler(new ModifyCustomContentHandler());
		return handler.handle(this);
	}

	public Resolution displayEmptyPlaylistForEmbed() throws Exception {
		handler = new BusinessHandler(new DisplayEmptyPlaylistForEmbedHandler());
		return handler.handle(this);
	}

	public Resolution displayEmptyPlaylistForBrowse() throws Exception {
		handler = new BusinessHandler(new DisplayEmptyPlaylistForBrowseHandler());
		return handler.handle(this);
	}

	public Resolution displayPlaylistForEmbed() throws Exception {
		handler = new BusinessHandler(new DisplayPlaylistForEmbedHandler());
		return handler.handle(this);
	}

	public Resolution displayPlaylistForRemove() throws Exception {
		handler = new BusinessHandler(new DisplayPlaylistForRemoveHandler());
		return handler.handle(this);
	}

	public Resolution saveMashup() throws Exception {
		handler = new BusinessHandler(new SaveMashupHandler());
		return handler.handle(this);
	}
	public Resolution returnBack() throws Exception {
		handler = new BusinessHandler(new ReturnBackHandler());
		return handler.handle(this);
	}

	public Resolution embedPlaylistUrl() throws Exception {
   		this.playlist = getPlaylistItems();
   		return new ForwardResolution(ActionUrl.getPlaylistEmbedUrl());
   	}
   	
   	public Resolution removePlaylistUrl() throws Exception {
   		this.playlist = getPlaylistItems();
   		return new ForwardResolution(ActionUrl.getPlaylistRemoveUrl());
   	}
   	
   	public Resolution emptyPlaylistUrl() throws Exception {
   		return new ForwardResolution(ActionUrl.getEmptyPlaylistUrl());
   	}
   	
   	public Resolution errorPageUrl() throws Exception {
   		return new ForwardResolution(ActionUrl.getErrorPageUrl());
   	}

	private List<PlaylistItem> getPlaylistItems() throws Exception {
		HttpServletRequest request = getContext().getRequest();
		NBCAppContext appContext = NBCAppContextFactory.createAppContext(request);
		
		User user = appContext.getUserSessionService().loadUser();
		List<PlaylistItem> list = new ArrayList<PlaylistItem>();
		PlaylistManagerService playlistManager = appContext .getPlaylistManagerService();
		list = playlistManager.retrieveVideos(user.getId());
		return list;
	}
	
	/**
	 * Places a {@link PlayListCourse} instance in request attribute so that
	 * calls to {@link ContextService} will return a course name of "Playlist"
	 * in all calls for current request.
	 */
	private void setPlaylistCourseInSession() {
		getContext().getRequest().setAttribute(DefaultContextServiceImpl.ATTRIBUTE_COURSE, new PlayListCourse());
	}
	
	/**
	 * {@link Course} subclass used for spoofing calls to {@link ContextService}
	 * when Playlist access occurs.
	 * 
	 * @author Ryan Hardy (rhardy@guilfordgroup.com)
	 */
	private class PlayListCourse extends Course {
		/**
		 * 
		 */
		private static final long serialVersionUID = -1555333844921455473L;

		@Override
		public String getTitle() {
			return "Playlist";
		}
	}
}