package blackboard.gs.nbc.action;

import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;

import org.apache.log4j.Logger;

import blackboard.base.FormattedText;
import blackboard.data.content.Content;
import blackboard.data.course.Course;
import blackboard.data.user.User;
import blackboard.gs.nbc.content.service.ContentManagerService;
import blackboard.gs.nbc.content.service.impl.ContentManagerServiceImpl;
import blackboard.gs.nbc.context.NBCAppContext;
import blackboard.gs.nbc.context.NBCAppContextFactory;
import blackboard.gs.nbc.playlist.data.playlistitem.PlaylistItem;
import blackboard.gs.nbc.playlist.data.video.Video;
import blackboard.gs.nbc.playlist.service.PlaylistManagerService;
import blackboard.gs.nbc.security.AuthPermission;
import blackboard.gs.nbc.security.Authorization;
import blackboard.gs.nbc.util.ActionUrl;
import blackboard.gs.nbc.util.persist.Id;
import blackboard.persist.content.ContentDbLoader;
import blackboard.platform.persistence.PersistenceServiceFactory;


public class ModifyCustomContentHandler implements Handler {
	private static final Logger log = Logger
			.getLogger(ModifyCustomContentHandler.class.getName());

	public Resolution handle(RequestControllerActionBean actionBean)
			throws Exception {
		Connection connection = null;
		blackboard.persist.Id courseId = null;
		blackboard.persist.Id contentId = null;
		blackboard.persist.Id parentId = null;
		
		HttpServletRequest request = actionBean.getContext().getRequest();
		NBCAppContext appContext = NBCAppContextFactory.createAppContext(request);
		
		User user = appContext.getUserSessionService().loadUser();
		if (!Authorization.getInstance().isCourseRoleGranted(AuthPermission.EMBED)) return null;
		
		
		try {
			/*if (log.isDebugEnabled()) {
				log.debug("Modifying custom content: " + "video_id: " + actionBean.getVideo_id() + ", name: " + actionBean.getName() + ", description: "
						+ actionBean.getDescription() + ", air_date: " + actionBean.getAir_date() + ", duration: " + actionBean.getDuration() 
						+ ", thumbnail: " + actionBean.getThumbnail_url() + ", content_type: " + actionBean.getContent_type());
			}*/
			connection = appContext.getPersistenceSession().getConnection();

			Video video = new Video(appContext);
			if (actionBean.getItem_key() != -1) {
				PlaylistManagerService playlistManager = appContext.getPlaylistManagerService();
				Id id = new Id(PlaylistItem.class, actionBean.getItem_key());
				PlaylistItem item = playlistManager.getPlaylistItem(id);
				video = item.getVideo();
			} else {
				video.setVideoId(actionBean.getVideo_id());
                video.setDuration(actionBean.getDuration());				
				video.setAirDateInString(actionBean.getAir_date());
				video.setName(actionBean.getName());
				video.setDescription(actionBean.getDescription());
				video.setThumbnailDecodedUrl(actionBean.getThumbnail_url());
				video.setShortType(actionBean.getContent_type());
			}
			courseId = PersistenceServiceFactory
					.getInstance().getDbPersistenceManager().generateId(
							Course.DATA_TYPE, actionBean.getCourse_id());
			contentId = PersistenceServiceFactory
					.getInstance().getDbPersistenceManager().generateId(
							Content.DATA_TYPE, actionBean.getContent_id());
			ContentDbLoader loader = (ContentDbLoader) PersistenceServiceFactory
					.getInstance().getDbPersistenceManager().getLoader(
							ContentDbLoader.TYPE);
			Content content = loader.loadById(contentId);
			parentId = content.getParentId();
			content.setCourseId(courseId);
			String title = video.getDuration() + " " + video.getName();
			content.setTitle(title);
			
			String embedCode = actionBean.getIframeContent().replace("RCRDPLACEHOLDER",actionBean.getVideo_id()).replace("xxxx", "iframe");
			embedCode = embedCode.substring(embedCode.indexOf("http"), embedCode.indexOf("RCRD")+4);
			/*log.debug(" embedCode ===>> " + embedCode);*/
			video.setIframeSource(embedCode);
			String description = video.getCueCardMashupContent();
			//String description = appContext.getCueCardConfiguration().renderDescription(video,embedCode);
			
			/*log.debug(" description ===>> " + description);*/
			
			description = description.replaceAll("\\&apos\\;", "'");
			FormattedText.Type textType = FormattedText.Type.HTML;
			FormattedText text = new FormattedText(description, textType);
			content.setBody(text);
			ContentManagerService contentManager = new ContentManagerServiceImpl();
			contentManager.modifyItem(content, connection);
		
			PlaylistManagerService playlistManager = appContext.getPlaylistManagerService();
			playlistManager.saveVideo(user, video);
			//connection.commit();
			/*if (log.isDebugEnabled()) {
				log.debug("Content item has been successfully modified");
			}*/
		} catch (Exception ex) {
			try {
				if (null!=connection)
				{
					connection.rollback();
				}
			} catch (SQLException sqex) {
				log.error("Rollback failed for modifying custom content item");
			}
			throw new Exception("Error during modifying custom content item. Please contact your system administrator.",
					ex);

		} finally {
			appContext.getPersistenceSession().close();
		}		
			
			return new RedirectResolution(ActionUrl.getTargetContentUrlWithFrameset(courseId.getExternalString(), contentId.getExternalString(), actionBean.getContext().getRequest(), actionBean.getAction()), false);
	    }

}
