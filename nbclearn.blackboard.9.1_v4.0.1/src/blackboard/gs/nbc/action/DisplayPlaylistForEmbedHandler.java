package blackboard.gs.nbc.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import blackboard.data.user.User;
import blackboard.gs.nbc.context.NBCAppContext;
import blackboard.gs.nbc.context.NBCAppContextFactory;
import blackboard.gs.nbc.playlist.data.playlistitem.PlaylistItem;
import blackboard.gs.nbc.playlist.service.PlaylistManagerService;
import blackboard.gs.nbc.util.ActionUrl;

public class DisplayPlaylistForEmbedHandler implements Handler {

	public Resolution handle(RequestControllerActionBean actionBean) throws Exception {
		HttpServletRequest request = actionBean.getContext().getRequest();
		NBCAppContext appContext = NBCAppContextFactory.createAppContext(request);
		
		User user = appContext.getUserSessionService().loadUser();
		// if
		// (!Authorization.getInstance().isCourseRoleGranted(AuthPermission.EMBED))
		// return null;
		List<PlaylistItem> playlist = getPlaylistItems(user, appContext);
		actionBean.setPlaylist(playlist);
		if (null != actionBean.getLocation()) {
			return new ForwardResolution(ActionUrl.getPlaylistEmbedUrl());
		} else {
			return new RedirectResolution(ActionUrl.getPlaylistEmbedUrlWithFrameset(request, actionBean), false);
		}

	}

	private List<PlaylistItem> getPlaylistItems(User user, NBCAppContext appContext) throws Exception {
		List<PlaylistItem> list = new ArrayList<PlaylistItem>();
		PlaylistManagerService playlistManager = appContext.getPlaylistManagerService();
		list = playlistManager.retrieveVideos(user.getId());
		return list;
	}
}
