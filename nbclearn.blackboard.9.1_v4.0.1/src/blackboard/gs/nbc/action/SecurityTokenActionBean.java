package blackboard.gs.nbc.action;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;
import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.StreamingResolution;
import net.sourceforge.stripes.action.UrlBinding;

import org.apache.log4j.Logger;

import blackboard.gs.nbc.Constants;
import blackboard.gs.nbc.context.NBCAppContext;
import blackboard.gs.nbc.context.NBCAppContextFactory;
import blackboard.gs.nbc.util.ContextService;
import blackboard.gs.nbc.util.ContextServiceException;

@UrlBinding("/sec_tok.action")
public class SecurityTokenActionBean implements ActionBean {
	private static final Logger logger = Logger.getLogger(SecurityTokenActionBean.class);
	
	private ActionBeanContext context;
	
	/**
	 * 
	 */
	public SecurityTokenActionBean() {
		/*logger.debug("Creating new instance of SecurityTokenActionBean.");*/
	}

	public ActionBeanContext getContext() {
		return context;
	}

	public void setContext(ActionBeanContext context) {
		this.context = context;
	}
	
	@DefaultHandler
	public Resolution get()
	{
		/*logger.info("Attempting to obtain security token and context info.");*/
		try {
			HttpServletRequest request = this.getContext().getRequest();
			NBCAppContext appContext = NBCAppContextFactory.createAppContext(request);
			
			String token = appContext.getSecurityTokenManager().loadSecurityToken();
			/*logger.info("Security Token: " + token);*/
			
			JSONObject json = new JSONObject();
			json.put("token", token);
			
			// need to add user context data
			ContextService ctxSvc = appContext.getContextService();
			try {
				String email = ctxSvc.getUserEmail();
				/*logger.info("Security Token Email: " + email);*/
				json.put(Constants.PARAM_USER_EMAIL, email);
			} catch (ContextServiceException e) {
				logger.error("Could not add user email to JSON response.", e);
				json.put("userEmail", "");			
			}
			try {
				String role = ctxSvc.getUserRole();
				/*logger.info("Security Token User role: " + role);*/
				json.put(Constants.PARAM_USER_ROLE, role);
			} catch (ContextServiceException e) {
				logger.error("Could not add user role to JSON response.", e);
				json.put("userRole", "");			
			}
			try {
				String courseName = ctxSvc.getCourseName();
				/*logger.info("Security Token Course name: " + courseName);*/
				json.put(Constants.PARAM_COURSE_NAME, courseName);
			} catch (ContextServiceException e) {
				logger.error("Could not add course name to JSON response.", e);
				json.put("courseName", "");
			}
			
			return new StreamingResolution("application/json", json.toString());
		} catch (Exception e) {
			logger.error("Could not obtain security token and context info.", e);
			return new StreamingResolution("application/json", "{\"error\": \"Could not obtain security token and context info.\"}");
		}
	}
}