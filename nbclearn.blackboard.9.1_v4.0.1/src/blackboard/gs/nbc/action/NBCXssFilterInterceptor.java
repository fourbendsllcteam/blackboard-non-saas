package blackboard.gs.nbc.action;

import org.apache.log4j.Logger;

import blackboard.platform.security.XssFilterInterceptor;

/**
 * 
 * @author Ryan Hardy (rhardy@guilfordgroup.com)
 * 
 * @deprecated Now that onclick values are no longer be embedded in
 * generated HTML, this can probably be removed in future release.
 */
@Deprecated
public class NBCXssFilterInterceptor implements XssFilterInterceptor {    
	private static final Logger logger = Logger.getLogger(NBCXssFilterInterceptor.class);
		
	/**
	 * 
	 * {@inheritDoc}
	 */
	@Override
	public boolean isUnsafeHtmlAllowed(String html) {
		/*logger.info("Checking HTML for XSS issues.");
		if (logger.isDebugEnabled()) {
			logger.debug("HTML:\n" + html);
		}*/
		return false;
	}

	/**
	 * 
	 * {@inheritDoc}
	 */
	@Override
	public String getArchiveUrlPattern() {	 
		return "";
	}
}