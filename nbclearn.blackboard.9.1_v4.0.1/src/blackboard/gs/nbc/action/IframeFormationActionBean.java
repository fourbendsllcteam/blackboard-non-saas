package blackboard.gs.nbc.action;

import java.io.StringReader;
import java.net.URLDecoder;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import blackboard.gs.nbc.Constants;
import blackboard.gs.nbc.context.NBCAppContext;
import blackboard.gs.nbc.context.NBCAppContextFactory;
import blackboard.gs.nbc.playlist.service.PlaylistManagerService;
import blackboard.gs.nbc.util.ActionUrl;
import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.StreamingResolution;
import net.sourceforge.stripes.action.UrlBinding;
import blackboard.data.registry.SystemRegistryEntry;
import blackboard.persist.registry.SystemRegistryEntryDbLoader;
import blackboard.persist.KeyNotFoundException;
import blackboard.persist.PersistenceException;

@UrlBinding("/execute/iframeFormation.action")
public class IframeFormationActionBean implements ActionBean {
	private static Logger logger = Logger.getLogger(IframeFormationActionBean.class);

	private ActionBeanContext context;
	private String securityToken = "";

	public ActionBeanContext getContext() {
		return context;
	}

	public void setContext(ActionBeanContext context) {
		this.context = context;
	}

	public String getSecurityToken() {
		return securityToken;
	}

	public void setSecurityToken(String securityToken) {
		this.securityToken = securityToken;
	}

	@DefaultHandler
	public StreamingResolution getIframeUrl() {

		HttpServletRequest request = getContext().getRequest();
		NBCAppContext appContext = NBCAppContextFactory.createAppContext(request);
		PlaylistManagerService playlistManager = appContext.getPlaylistManagerService();

		try {
			securityToken = playlistManager.getSecurityToken();
			if (securityToken == null) {
				SystemRegistryEntryDbLoader sysRegistryLoader = null;
				SystemRegistryEntry securityTokenRegistry = null;
				try {
					sysRegistryLoader = SystemRegistryEntryDbLoader.Default.getInstance();
					try {
						securityTokenRegistry = sysRegistryLoader.loadByKey(Constants.SYSTEM_REGISTRY_KEY);
					} catch (KeyNotFoundException knfe) {
						logger.error("Existing value for the NBC Schema version was not found.");
					}
				} catch (PersistenceException pe) {
					throw new RuntimeException("Error loading sys reg loader.", pe);
				}
				if (securityTokenRegistry.getValue() != null) {
					playlistManager.updateIframeSecurityToken(securityTokenRegistry.getValue(), appContext.getEnvironmentConfiguration().getSecurityTokenUrl());
					securityToken = playlistManager.getSecurityToken();
				}
			}
		} catch (Exception e) {
			logger.error("Error while getting iframe" + e.getCause());
		}
		/*logger.debug("securityToken-->" + securityToken);*/
		String iframeContent = appContext.getEnvironmentConfiguration().getEmbedPlayerUrl() + securityToken;
		/*logger.debug("iframeContent-->" + iframeContent);*/

		return new StreamingResolution("text", new StringReader(iframeContent));
	}

}
