package blackboard.gs.nbc.action;

import javax.servlet.http.HttpServletRequest;

import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import blackboard.gs.nbc.context.NBCAppContext;
import blackboard.gs.nbc.context.NBCAppContextFactory;
import blackboard.gs.nbc.util.ActionUrl;

public class DisplayEmptyPlaylistForBrowseHandler implements Handler {

	public Resolution handle(RequestControllerActionBean actionBean) throws Exception {
		HttpServletRequest request = actionBean.getContext().getRequest();
		NBCAppContext appContext = NBCAppContextFactory.createAppContext(request);
		
		appContext.getUserSessionService().loadUser();
		if (null!=actionBean.getLocation())
		{
			return new ForwardResolution(ActionUrl.getEmptyPlaylistUrl());
		}
		else
		{
			
			return new RedirectResolution(ActionUrl.getEmptyPlaylistUrlWithFrameset(actionBean.getContext().getRequest()), false);
		}

	}

}
