package blackboard.gs.nbc.action;

import java.io.StringReader;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.StreamingResolution;

import org.apache.log4j.Logger;

import blackboard.data.user.User;
import blackboard.gs.nbc.context.NBCAppContext;
import blackboard.gs.nbc.context.NBCAppContextFactory;
import blackboard.gs.nbc.playlist.data.video.Video;
import blackboard.gs.nbc.playlist.service.PlaylistManagerService;

public class SaveInPlaylistHandler implements Handler {
	private static final Logger log = Logger
			.getLogger(SaveInPlaylistHandler.class.getName());
	private static final String SUCCESS = "success";
	private static final String FAILURE = "failure";
	private static final String STATUS = "status";
	private static final String MESSAGE = "message";
	private static final String SAVERESPONSE = "saveresponse";
	private static final String TEXT = "text";
	private static final String ERROR_MESSAGE = "An application error occured when saving the video in your playlist.";

	public Resolution handle(RequestControllerActionBean actionBean)
			throws Exception {

		Connection connection = null;
		
		HttpServletRequest request = actionBean.getContext().getRequest();
		NBCAppContext appContext = NBCAppContextFactory.createAppContext(request);
		
		try {
			User usr = appContext.getUserSessionService().loadUserBySessionId(actionBean.getBbsession_id());
			
			/*if (log.isDebugEnabled()) {
				log.debug("Saving video: " + "video_id: " + actionBean.getVideo_id() + ", name: " + actionBean.getName() + ", description: "
						+ actionBean.getDescription() + ", air_date: " + actionBean.getAir_date() + ", duration: " + actionBean.getDuration() 
						+ ", thumbnail: " + actionBean.getThumbnail_url() + ", content_type: " + actionBean.getContent_type() + ", bbsession_id: " + actionBean
						.getBbsession_id());
			}*/
			
			connection = appContext.getPersistenceSession().getConnection();

			Video video = new Video(appContext);
			video.setVideoId(actionBean.getVideo_id());
			video.setDuration(actionBean.getDuration());			
			video.setAirDateInString(actionBean.getAir_date());
			video.setName(actionBean.getName());
			video.setDescription(actionBean.getDescription());
			video.setThumbnailDecodedUrl(actionBean.getThumbnail_url());
			video.setShortType(actionBean.getContent_type());
			PlaylistManagerService playlistManager = appContext.getPlaylistManagerService();
			playlistManager.saveVideo(usr, video);
			//connection.commit();
			/*if (log.isDebugEnabled()) {
				log.debug("Video successfully saved in the playlist: " + getJSONResponse(SUCCESS, ""));
			}*/
			return new StreamingResolution(TEXT, new StringReader(
					getJSONResponse(SUCCESS, "")));
		} catch (Exception ex) {
			try {
				if (null != connection) {
					connection.rollback();
				}
			} catch (SQLException sqex) {
				
				
				log.error("Rollback failed for saving item in playlist", sqex);
			}
			log.error("Error during saving video in playlist: " + getJSONResponse(FAILURE, ERROR_MESSAGE + ": " + ex.getMessage()), ex);
			return new StreamingResolution(TEXT, new StringReader(
					getJSONResponse(FAILURE, ERROR_MESSAGE + ": " + ex.getMessage())));
		} finally {
			appContext.getPersistenceSession().close();
		}

	}

	private String getJSONResponse(String status, String message) {
		JSONArray responseList = new JSONArray();
		JSONObject response = new JSONObject();
		response.put(STATUS, status);
		response.put(MESSAGE, message);
		responseList.add(response);
		JSONObject saveResponseObj = new JSONObject();
		saveResponseObj.put(SAVERESPONSE, responseList);
		return saveResponseObj.toString();
	}

}
