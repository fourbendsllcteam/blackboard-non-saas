package blackboard.gs.nbc.action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;

import org.apache.log4j.Logger;

import blackboard.data.user.User;
import blackboard.gs.nbc.context.NBCAppContext;
import blackboard.gs.nbc.context.NBCAppContextFactory;
import blackboard.gs.nbc.playlist.data.playlistitem.PlaylistItem;
import blackboard.gs.nbc.playlist.service.PlaylistManagerService;
import blackboard.gs.nbc.util.ActionUrl;
import blackboard.gs.nbc.util.persist.Id;


public class RemoveFromPlaylistHandler implements Handler {
	private static final Logger log = Logger.getLogger(RemoveFromPlaylistHandler.class.getName());
	
	public Resolution handle(RequestControllerActionBean actionBean) throws Exception {
		HttpServletRequest request = actionBean.getContext().getRequest();
		NBCAppContext appContext = NBCAppContextFactory.createAppContext(request);
		
		@SuppressWarnings("unused")
		User user = appContext.getUserSessionService().loadUser();
		try {
			List<Integer> itemKeys = new ArrayList<Integer>();
			itemKeys = actionBean.getItem_keys();
			List<PlaylistItem> list = new ArrayList<PlaylistItem>();
			PlaylistManagerService playlistManager = appContext.getPlaylistManagerService();
			for (int key : itemKeys) {
				Id id = new Id(PlaylistItem.class, key);
				playlistManager.getPlaylistItem(id);
				list.add(playlistManager.getPlaylistItem(id));
			}
			if (!list.isEmpty()) {
				playlistManager.removeVideos(list);
				/*if (log.isDebugEnabled()) {
					log.debug("Playlist items have been successfully removed");
				}*/
			}
			String returnLoc = "";
			if (null!=actionBean.getReturnLoc())
			{
				returnLoc=actionBean.getReturnLoc();
			}
			String courseId = "";
			if (null!=actionBean.getCourse_id())
			{
				courseId=actionBean.getCourse_id();
			}
			return new RedirectResolution(ActionUrl.getPlaylistRemoveSuccessUrl(returnLoc,courseId), false);

		} catch (Exception ex) {
			log.error("Removing playlist items failed", ex);
            
			throw new Exception(" Error during removing playlist items. Please contact your system administrator.", ex);
		}

	}
	
	
}
