package blackboard.gs.nbc.action;

import javax.servlet.http.HttpServletRequest;

import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.util.StringUtil;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import blackboard.gs.nbc.Constants;
import blackboard.gs.nbc.context.NBCAppContext;
import blackboard.gs.nbc.context.NBCAppContextFactory;
import blackboard.gs.nbc.parameters.ModeParam;
import blackboard.gs.nbc.playlist.service.PlaylistManagerService;
import blackboard.gs.nbc.util.ActionUrl;
import blackboard.gs.nbc.util.ContextService;
import blackboard.gs.nbc.util.UrlBuilder;
import blackboard.platform.plugin.PlugInUtil;
import blackboard.platform.session.BbSession;
import blackboard.platform.session.BbSessionManagerServiceFactory;

/**
 * Action for redirecting from Blackboard to NBC.
 * 
 * @author kborgeson
 */
@UrlBinding("/execute/out_req_con.action")
public class OutboundRequestAction implements ActionBean {
	static Logger logger = Logger.getLogger(OutboundRequestAction.class);

	private ActionBeanContext context;

	/**
	 * 
	 * {@inheritDoc}
	 */
	@Override
	public ActionBeanContext getContext() {
		return context;
	}

	/**
	 * 
	 * {@inheritDoc}
	 */
	@Override
	public void setContext(ActionBeanContext context) {
		this.context = context;
	}

	@DefaultHandler
	public Resolution launchNBCSite() {
		HttpServletRequest request = this.getContext().getRequest();
		NBCAppContext appContext = NBCAppContextFactory.createAppContext(request);

		RequestControllerActionBean.displayRequestParams(request, logger);

		String courseId = getParam(Constants.PARAM_COURSE_ID);
		String action = getParam(Constants.PARAM_ACTION);
		String contentId = getParam(Constants.PARAM_CONTENT_ID);
		ModeParam mode = ModeParam.getModeParam(getParam(Constants.PARAM_MODE));
		String userToolType = getParam(Constants.PARAM_USER_TOOL_TYPE);

		// Ramesh BB - 22 Fix : Start

		if (contentId.equals("")) {
			String referrer = request.getHeader("referer");
			contentId = getContentIdFromString(referrer);
		}
		if (courseId.equals("")) {
			courseId = getUltraCourseId();
		}
		if (contentId.equals("")) {
			String referrer = request.getHeader("referer");
			contentId = getUltraContentId(referrer);
		}

		// Ramesh BB - 22 Fix : End

		// mode check begin
		if (mode == null) {
			logger.error("Redirecting to error page, invalid mode parameter: '" + getParam(Constants.PARAM_MODE) + "'");
			context.getRequest().setAttribute("error",
					new Exception("Error during accessing NBC site. Please contact your system administrator."));
			return new ForwardResolution(ActionUrl.getErrorPageUrl());
		}

		String returnUrl = buildReturnUrl(courseId, action, contentId, mode.toString());
		/*logger.info("Return URL: '" + returnUrl + "'");*/
		request.setAttribute(Constants.PARAM_RETURN_URL, returnUrl);

		/*logger.info("Mode: " + mode);*/
		request.setAttribute(Constants.PARAM_MODE, mode);

		String secToken = appContext.getSecurityTokenManager().loadSecurityToken();
		/*logger.info("Security token: '" + secToken + "'");*/
		request.setAttribute(Constants.PARAM_TOKEN, secToken);

		String outboundUrl;
		if (!userToolType.equals("")) {
			outboundUrl = appContext.getEnvironmentConfiguration().getOutboundPlayListUrl();
		} else {
			outboundUrl = appContext.getEnvironmentConfiguration().getOutboundUrl();
		}

		/*logger.info("Outbound URL: '" + outboundUrl + "'");*/
		request.setAttribute(Constants.PARAM_LAUNCH_POST, outboundUrl);

		String imageLogo = PlugInUtil.getUriStem(Constants.VENDOR_ID, Constants.PLUGIN_ID) + "images/nbc_logo.gif";
		/*logger.info("Image Logo: '" + imageLogo + "'");*/
		request.setAttribute(Constants.ATTRIBUTE_LOGO_PATH, imageLogo);

		try {
			// add user context info
			// (https://jira.gg.virtual.vps-host.net/browse/NBC-12)
			ContextService ctxSvc = appContext.getContextService();

			String email = ctxSvc.getUserEmail();
			/*logger.info("Email: " + email);*/
			if (email == null || email.trim().length()==0) {
				/*logger.debug("ctxSvc.getUserId()"+ctxSvc.getUsername());*/
				PlaylistManagerService playlistManager = appContext.getPlaylistManagerService();
				email = playlistManager.generateEmailId(ctxSvc.getUsername());
			}
			request.setAttribute(Constants.PARAM_USER_EMAIL, email);
			
			String role = ctxSvc.getUserRole();
			/*logger.info("Role: " + role);*/
			request.setAttribute(Constants.PARAM_USER_ROLE, role);

			String firstname = ctxSvc.getUserFirstName();
			/*logger.info("First Name: " + firstname);*/
			request.setAttribute(Constants.PARAM_USER_FIRST_NAME, firstname);

			String lastname = ctxSvc.getUserLastName();
			/*logger.info("Last Name: " + lastname);*/
			request.setAttribute(Constants.PARAM_USER_LAST_NAME, lastname);
			
			String coursename = ctxSvc.getCourseName();
			request.setAttribute(Constants.PARAM_COURSE_NAME, coursename);

		} catch (Exception e) {
			logger.error("Can not obtain context info.", e);
			// TODO: throw ex? or NOT send User info?
			throw new RuntimeException("Can not obtain Bb User.", e);
		}

		RequestControllerActionBean.displayRequestAttributes(request, logger);

		return new ForwardResolution(Constants.FORWARD_LAUNCH_NBC);

	}

	private String getUltraContentId(String referrer) {
		String contentId = "";
		if (StringUtils.isNotBlank(referrer) && referrer.indexOf("blti_placement_id") != -1) {
			contentId = referrer.substring(referrer.indexOf("=") + 1, referrer.indexOf("&course_id="));
		}
		return contentId;
	}

	private String getUltraCourseId() {
		String courseName = context.getRequest().getParameter("resource_link_title");
		String courseNameId = context.getRequest().getParameter("resource_link_id");
		String courseId = "";
		if (StringUtils.isNotBlank(courseNameId))
			courseId = courseNameId.replace(courseName, "");
		return courseId;
	}

	/**
	 * Returns the value for the given request parameter, or the empty string if
	 * it is not present.
	 * 
	 * @param param
	 * @return
	 */
	private String getParam(String param) {
		String val = context.getRequest().getParameter(param);
		return val = (val == null) ? "" : val;
	}

	private String buildReturnUrl(String courseId, String action, String contentId, String mode) {
		HttpServletRequest req = context.getRequest();
		NBCAppContext appContext = NBCAppContextFactory.createAppContext(req);

		BbSession bbSession = BbSessionManagerServiceFactory.getInstance().getSession(req);
		String sessId = appContext.getUserSessionService().encryptSession(bbSession.getBbSessionIdMd5());
		// this has a dangling line-break...was causing ALL sorts of problems..
		// when encoded, added a %0D%0A ('\r\n') to the end...
		sessId = sessId.trim();

		String baseUrl = req.getScheme() + "://" + req.getServerName()
				+ PlugInUtil.getUri(Constants.VENDOR_ID, Constants.PLUGIN_ID, "req_con.action");
		UrlBuilder urlBldr = new UrlBuilder(baseUrl);

		// do NOT do encoding, as entire URL must be encoded
		urlBldr.addParam(Constants.PARAM_COURSE_ID, courseId, false);
		urlBldr.addParam(Constants.PARAM_ACTION, action, false);
		urlBldr.addParam(Constants.PARAM_CONTENT_ID, contentId, false);
		urlBldr.addParam(Constants.PARAM_MODE, mode, false);
		urlBldr.addParam(Constants.PARAM_BBSESSION_ID, sessId, false);

		String url = urlBldr.getUrl();
		/*logger.debug("URL (before encoding): '" + url + "'");*/
		url = StringUtil.urlEncode(url);

		return url;
	}

	// Ramesh BB - 22 Fix : Start

	private String getContentIdFromString(String param) {
		StringBuffer referrer = new StringBuffer(param);
		String content_string = referrer.substring(
				(referrer.indexOf("content_id") == -1 ? referrer.length() : referrer.indexOf("content_id")),
				referrer.length());
		String content_param_string = content_string.substring(0,
				(content_string.indexOf("&") == -1 ? content_string.length() : content_string.indexOf("&")));
		String content_value_string = content_param_string.replace("content_id=", "");
		return content_value_string;
	}

	// Ramesh BB - 22 Fix : End
}
