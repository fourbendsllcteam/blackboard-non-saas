package blackboard.gs.nbc.action;


import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;

import org.apache.log4j.Logger;

import blackboard.data.user.User;
import blackboard.gs.nbc.context.NBCAppContext;
import blackboard.gs.nbc.context.NBCAppContextFactory;
import blackboard.gs.nbc.playlist.data.playlistitem.PlaylistItem;
import blackboard.gs.nbc.playlist.data.video.Video;
import blackboard.gs.nbc.playlist.service.PlaylistManagerService;
import blackboard.gs.nbc.security.AuthPermission;
import blackboard.gs.nbc.security.Authorization;
import blackboard.gs.nbc.util.ActionUrl;
import blackboard.gs.nbc.util.CourseModuleUtil;
import blackboard.gs.nbc.util.persist.Id;

public class CreateCourseModuleHandler implements Handler {
	private static final Logger log = Logger
			.getLogger(CreateCourseModuleHandler.class.getName());

	public Resolution handle(RequestControllerActionBean actionBean)
			throws Exception {
		HttpServletRequest request = actionBean.getContext().getRequest();
		NBCAppContext appContext = NBCAppContextFactory.createAppContext(request);
		
		User user = appContext.getUserSessionService().loadUser();
	    if (!Authorization.getInstance().isCourseRoleGranted(AuthPermission.EMBED)) return null;
		Connection connection = null;
		
		try {
			/*if (log.isDebugEnabled()) {
				log.debug("Creating custom module: " + "video_id: " + actionBean.getVideo_id() + ", name: " + actionBean.getName() + ", description: "
						+ actionBean.getDescription() + ", air_date: " + actionBean.getAir_date() + ", duration: " + actionBean.getDuration() 
						+ ", thumbnail: " + actionBean.getThumbnail_url() + ", content_type: " + actionBean.getContent_type());
			}	*/		
			connection = appContext.getPersistenceSession().getConnection();
			
			Video video = new Video(appContext);
			if (actionBean.getItem_key() != -1) {
				PlaylistManagerService playlistManager = appContext.getPlaylistManagerService();
				Id id = new Id(PlaylistItem.class, actionBean.getItem_key());
				PlaylistItem item = playlistManager.getPlaylistItem(id);
				video = item.getVideo();
			} else {
				video.setVideoId(actionBean.getVideo_id());
				video.setDuration(actionBean.getDuration());
				video.setAirDateInString(actionBean.getAir_date());
				video.setName(actionBean.getName());
				video.setDescription(actionBean.getDescription());
				video.setThumbnailDecodedUrl(actionBean.getThumbnail_url());
				video.setShortType(actionBean.getContent_type());
				
			}
			String embedCode = actionBean.getIframeContent().replace("RCRDPLACEHOLDER",actionBean.getCuecardID()).replace("xxxx", "iframe");
			/*log.debug("embedCode ===>> " + embedCode);*/
			
			//CourseModuleUtil.setCurrentCueCardId(actionBean.getCourse_id(),video.getVideoId());
			CourseModuleUtil.setCurrentCurCardId(actionBean.getCourse_id(),video,embedCode);
			PlaylistManagerService playlistManager = appContext.getPlaylistManagerService();
			playlistManager.saveVideo(user, video);
			
			//connection.commit();
			/*if (log.isDebugEnabled()) {
				log.debug("Course module has been successfully created");
			}*/
		} catch (Exception ex) {
			try {
				if (null != connection) {
					connection.rollback();
				}
			} catch (SQLException sqex) {
				log.fatal("Rollback failed during creating a course module",
						sqex);
			}
			throw new Exception("Error during creating the course module. Please contact your system administrator.",
					ex);

		} finally {
			appContext.getPersistenceSession().close();
		}
					
		return new RedirectResolution(ActionUrl.getCourseUrlWithFrameset(actionBean.getCourse_id(), actionBean.getContext().getRequest()), false);		

	}

}
