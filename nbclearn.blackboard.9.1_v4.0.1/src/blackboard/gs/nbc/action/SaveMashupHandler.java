package blackboard.gs.nbc.action;

import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

import net.sourceforge.stripes.action.Resolution;

import org.apache.log4j.Logger;

import blackboard.data.user.User;
import blackboard.gs.nbc.context.NBCAppContext;
import blackboard.gs.nbc.context.NBCAppContextFactory;
import blackboard.gs.nbc.playlist.data.playlistitem.PlaylistItem;
import blackboard.gs.nbc.playlist.data.video.Video;
import blackboard.gs.nbc.playlist.service.PlaylistManagerService;
import blackboard.gs.nbc.util.ActionUrl;
import blackboard.gs.nbc.util.persist.Id;


public class SaveMashupHandler implements Handler {
	private static final Logger log = Logger.getLogger(SaveMashupHandler.class);

	public Resolution handle(RequestControllerActionBean actionBean) throws Exception {
		Connection connection = null;
		HttpServletRequest request = actionBean.getContext().getRequest();
		NBCAppContext appContext = NBCAppContextFactory.createAppContext(request);
		
		User user = appContext.getUserSessionService().loadUser();
		
		
		try {
			/*if (log.isDebugEnabled()) {
				log.debug("Creating mashup: " + "video_id: " + actionBean.getVideo_id() + ", name: " + actionBean.getName() + ", description: "
						+ actionBean.getDescription() + ", air_date: " + actionBean.getAir_date() + ", duration: " + actionBean.getDuration() 
						+ ", thumbnail: " + actionBean.getThumbnail_url() + ", content_type: " + actionBean.getContent_type());
			}*/
			connection = appContext.getPersistenceSession().getConnection();

			Video video = new Video(appContext);
			if (actionBean.getItem_key() != -1) {
				PlaylistManagerService playlistManager = appContext.getPlaylistManagerService();
				Id id = new Id(PlaylistItem.class, actionBean.getItem_key());
				PlaylistItem item = playlistManager.getPlaylistItem(id);
				video = item.getVideo();
			} else {
				video.setVideoId(actionBean.getVideo_id());
				video.setDuration(actionBean.getDuration());
				video.setAirDateInString(actionBean.getAir_date());
				video.setName(actionBean.getName());
				video.setDescription(actionBean.getDescription());
				video.setThumbnailDecodedUrl(actionBean.getThumbnail_url());
				video.setShortType(actionBean.getContent_type());
			}
			
			
			String embedCode = actionBean.getIframeContent().replace("RCRDPLACEHOLDER",actionBean.getVideo_id()).replace("xxxx", "iframe");
			/*log.debug(" embedCode ===>> " + embedCode);*/
			embedCode = embedCode.substring(embedCode.indexOf("http"), embedCode.indexOf("RCRD")+4);
			video.setIframeSource(embedCode);
			String embedHTML = video.getCueCardMashupContent();
		//	String embedHTML = appContext.getCueCardConfiguration().renderDescription(video,embedCode);
            
			actionBean.getContext().getRequest().setAttribute("embedHTML", embedHTML); 
			String embedUrl = ActionUrl.getInsertAndCloseWindowUrl();
			
		    PlaylistManagerService playlistManager = appContext.getPlaylistManagerService();
		    
			playlistManager.saveVideo(user, video);
			//connection.commit();
            RequestDispatcher rd = actionBean.getContext().getServletContext().getRequestDispatcher(embedUrl);
		    rd.forward(actionBean.getContext().getRequest(), actionBean.getContext().getResponse());		  

		} catch (Exception ex) {
			try {
				if (null!=connection)
				{
					connection.rollback();
				}
			} catch (SQLException sqex) {
				log.error("Rollback failed for creating the mashup item");
			}
			throw new Exception("Error during creating the mashup item. Please contact your system administrator.",ex);

		}
		finally {
			appContext.getPersistenceSession().close();
		}
		return null;
	}
	
	
}
