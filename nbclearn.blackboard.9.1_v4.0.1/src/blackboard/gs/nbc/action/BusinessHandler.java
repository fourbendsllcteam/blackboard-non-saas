package blackboard.gs.nbc.action;

import javax.servlet.http.HttpServletRequest;

import net.sourceforge.stripes.action.Resolution;

import org.apache.log4j.Logger;

import blackboard.gs.nbc.context.NBCAppContext;
import blackboard.gs.nbc.context.NBCAppContextFactory;

public class BusinessHandler implements Handler {
	private static final Logger logger = Logger.getLogger(BusinessHandler.class);
	
	private final Handler handler;

	/**
	 * 
	 * @param handler
	 */
	public BusinessHandler(Handler handler) {
		this.handler = handler;
	}

	public Resolution handle(RequestControllerActionBean actionBean) throws Exception {
		final String handlerName = handler.getClass().getSimpleName();
       /* logger.info("Calling handler: " + handlerName);*/
		
        HttpServletRequest request = actionBean.getContext().getRequest();
		@SuppressWarnings("unused")
        NBCAppContext context = NBCAppContextFactory.createAppContext(request);
		
		try {
			Resolution resolution = handler.handle(actionBean);
			/*if (logger.isDebugEnabled()) {
			    if (resolution != null) {
	                logger.info("Resolution (for "+handlerName+"): " + resolution);			        
			    } else {
	                logger.warn("ATTENTION: Resolution (for "+handlerName+") is null.");			        
			    }
			}*/
			
			return resolution;
		} catch (Exception e) {
			logger.error("An error occurred in calling Handler ("+handlerName+").", e);
			//TODO: SHOULD we propagate all exceptions?
			throw e;
		}
	}

}
