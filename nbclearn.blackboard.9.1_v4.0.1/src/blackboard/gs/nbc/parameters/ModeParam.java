package blackboard.gs.nbc.parameters;

public enum ModeParam {
	EMBED("embed"),
	BROWSE("browse"),
	REMOVE("remove");

    private String name;
	
	private ModeParam(String name) {
		this.name = name;
	}
	
	public String toString() {
		return name;
	}
	/**
	 * Obtains the ModeParam that corresponds to the <code>value</code>
	 * string provided. Returns <code>null</code> if none found.
	 * 
	 * @param value
	 * @return
	 */
	public static ModeParam getModeParam(String value) {
		if (value == null) {
			return null;
		}
		for (ModeParam mp : values()) {
			if (value.equalsIgnoreCase(mp.name()) || value.equalsIgnoreCase(mp.name)) {
				return mp;
			}
		}
		return null;
	}
}