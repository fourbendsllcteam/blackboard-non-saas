package blackboard.gs.nbc.parameters;

public enum TypeParam {
	EMBED("embed"),
	SAVE("save"),
	PLAYLIST("playlist"),
	RETURN("return");
	
    private String name;
	
	private TypeParam(String name) {
		this.name = name;
	}
	
	public String toString() {
		return name;
	}
	
	/**
	 * Obtains the TypeParam that corresponds to the <code>value</code>
	 * string provided. Returns <code>null</code> if none found.
	 * 
	 * @param value
	 * @return
	 */
	public static TypeParam getTypeParam(String value) {
		if (value == null) {
			return null;
		}
		for (TypeParam tp : values()) {
			if (value.equalsIgnoreCase(tp.name()) || value.equalsIgnoreCase(tp.name)) {
				return tp;
			}
		}
		return null;
	}
}