package blackboard.gs.nbc.parameters;

public enum ActionParam {
	CREATE("create"),
	MODIFY("modify"),
	MODULE("module"),
	TOOL("tool"),
    MASHUP("mashup"),
    COURSETOOL("coursetool");
	 

	private String name;

	private ActionParam(String name) {
		this.name = name;
	}

	public String toString() {
		return name;
	}
	
	/**
	 * Obtains the ActionParam that corresponds to the <code>value</code>
	 * string provided. Returns <code>null</code> if none found.
	 * 
	 * @param value
	 * @return
	 */
	public static ActionParam getActionParam(String value) {
		if (value == null) {
			return null;
		}
		for (ActionParam ap : values()) {
			if (value.equalsIgnoreCase(ap.name()) || value.equalsIgnoreCase(ap.name)) {
				return ap;
			}
		}
		return null;
	}
}