<%@ page import="blackboard.gs.nbc.Constants" %><%@ page import="blackboard.gs.nbc.context.NBCAppContextFactory" %><%@ page import="blackboard.gs.nbc.context.NBCAppContext" %><%@ page import="blackboard.gs.nbc.playlist.data.video.Video" %><%@ page import="blackboard.gs.nbc.security.AuthPermission" %><%@ page import="blackboard.gs.nbc.security.Authorization" %><%@ page import="blackboard.gs.nbc.util.CourseModuleUtil" %><%@ page import="blackboard.gs.nbc.web.RequestCache" %><%@ page import="blackboard.platform.plugin.PlugInUtil" %>
<%@ taglib prefix="bbData" uri="/bbData" %><%@ taglib prefix="bbNG" uri="/bbNG" %>
<bbData:context id="ctx">
	<% if (null != ctx.getCourse()) { %>	
	<% 	    // this is a required call...the request was null when 	    // attempting to render cue card if not cached from within module view	    RequestCache.setRequest(request);        NBCAppContext appContext = NBCAppContextFactory.createAppContext(request);        	    final String browseButtonUrl =  PlugInUtil.getUri(Constants.VENDOR_ID,Constants.PLUGIN_ID,"execute/out_req_con.action?action=module&mode=browse&content_id=&course_id="+ctx.getCourse().getId().toExternalString()); 	    final String swapButtonUrl =  PlugInUtil.getUri(Constants.VENDOR_ID,Constants.PLUGIN_ID,"execute/out_req_con.action?action=module&mode=embed&content_id=&course_id="+ctx.getCourse().getId().toExternalString()); 	    	    	    Video video = CourseModuleUtil.loadCurrentCueCard(ctx.getCourseId());		    if(video == null) {            video = new Video(appContext);	    }	
	    if(null == video.getAirDateInString() || "".equals(video.getAirDateInString())) {	        video.setAirDateInString("2009-06-25 15:10:03.0");    		    }	
		if(null == video.getShortType() || "".equals(video.getShortType())) {	        video.setShortType("Video");				}		
		if(null == video.getDuration() || "".equals(video.getDuration())) {	        video.setDuration("00:03:35");				}		
		if(null == video.getName() || "".equals(video.getName())) {	        video.setName("Brian Williams on NBC Learn on Demand.");				}		
		if(null == video.getThumbnailUrl() || "".equals(video.getThumbnailUrl())) {	        video.setThumbnailUrl("http://icue.nbcunifiles.com/icue/files/icue/about_the_collections/promos/1129485_130x100.jpg");				}		
		if(null == video.getVideoId() || "".equals(video.getVideoId())) {	        video.setVideoId("44473");				}
	%>	
	<div><%= appContext.getCueCardConfiguration().renderContentModule(video) %>	</div>	<div align="center" style="width: 140px;float: left;margin-left: -10px;">	
	    <bbNG:button url='<%= browseButtonUrl %>' label="Browse NBC Learn" />	    </div><div style="width: 90px;float: left;">	
	    <%     	        try {	            if (Authorization.getInstance().isCourseRoleGranted(AuthPermission.EMBED)) {	    %>	                <bbNG:button url='<%= swapButtonUrl %>' label="Swap Video" />	    <%      } 	        } catch (Exception e) {	            // Error is thrown when student accesses module, catch it here	        }	     %>	
	</div>	
	
	<% } else { %>	
	<div style="float: center;text-align: center;">	    <img src="/webapps/bbgs-nbc-content-integration-bb_bb60/images/nbc_logo.gif" alt="NBC Learn" />	</div>	
	<div>This module is only for use within a Blackboard course.</div>	
	<% } %>	
</bbData:context>