<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@ page errorPage="/error.action"%>
<%@ taglib uri="/bbNG" prefix="bbNG"%>
<bbNG:genericPage ctxId="ctx" bodyClass="normalBackground">
	<%@include file="playlist_common.jspf"%>
	<%
	    String cancelUrl = PlugInUtil.getUriStem(Constants.VENDOR_ID,Constants.PLUGIN_ID) + "execute/out_req_con.action?mode=browse";   
    	String mode = request.getParameter(Constants.PARAM_MODE);
    	String returnLoc = request.getParameter(Constants.PARAM_RETURN_LOC);
    	String courseId = request.getParameter(Constants.PARAM_COURSE_ID);
       
    	if (null==mode) mode ="";
    	if (null==returnLoc) returnLoc ="";
    	if (null==courseId) courseId ="";
    	
    	String submitUrl = PlugInUtil.getUriStem(Constants.VENDOR_ID,Constants.PLUGIN_ID) + 
    				"req_con.action?removeFromPlaylist&location=blackboard&returnLoc=" + returnLoc + 
    				"&course_id=" + courseId;
    	String sortUrl = PlugInUtil.getUriStem(Constants.VENDOR_ID,Constants.PLUGIN_ID) + 
    				"req_con.action?location=blackboard&type=playlist&mode=browse&returnLoc=" + returnLoc + 
    				"&course_id=" + courseId;
	%>
	<c:set var="submitUrl" value="<%=submitUrl%>" />
	<c:set var="sortUrl" value="<%=sortUrl%>" />
	<c:set var="mode" value="<%=mode%>" />
	<c:set var="returnLoc" value="<%=returnLoc%>" />
	 
	<c:choose>
	  	<c:when test="${mode == 'browse' and returnLoc=='blackboard'}">
		  	<%
		  		cancelUrl = "javascript:window.parent.location='"+ActionUrl.getHomeUrl()+"';"; 		
		  	%>		  		  		 		
	    </c:when>
	    <c:when test="${mode == 'browse' and returnLoc=='coursetools'}">
		  	<%
		  		cancelUrl = ActionUrl.getCourseToolHomeUrl(courseId);
		  	%>		  		  		 		
	    </c:when>
	</c:choose>
	<c:set var="cancelUrl" value="<%=cancelUrl%>"/> 
	<stripes:form name="playlistRemoveForm" action="${submitUrl}">	   	   
	   	<input type="hidden" name="type" value="playlist" />
		<input type="hidden" name="mode" value="${mode}" />

		<bbNG:inventoryList collection="${actionBean.playlist}"
			objectVar="item"
			className="blackboard.gs.nbc.playlist.data.playlistitem.PlaylistItem"
			url="${sortUrl}">
			
			<bbNG:listActionBar>
			    <bbNG:listActionItem url="${cancelUrl}" title="Cancel" minSelected="0"/>
				<bbNG:listActionItem url="#" title="Remove" onClick="return validateRemove();"/>
			</bbNG:listActionBar>

			<bbNG:listCheckboxElement name="item_keys" value="${item.id.key}"
				showCheckbox="true" />
			<%@include file="playlist_listitems.jspf"%>
		</bbNG:inventoryList>
	</stripes:form>
</bbNG:genericPage>
