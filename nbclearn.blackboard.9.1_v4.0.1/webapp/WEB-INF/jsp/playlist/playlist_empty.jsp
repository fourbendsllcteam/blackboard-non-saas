<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@ page errorPage="/error.action"%>
<%@ taglib uri="/bbNG" prefix="bbNG"%>
<bbNG:genericPage ctxId="ctx">
<%@include file="playlist_common.jspf"%>
	<%
		String okUrl = PlugInUtil.getUriStem(Constants.VENDOR_ID,Constants.PLUGIN_ID) + "execute/out_req_con.action?mode=browse";
    	String returnLoc = request.getParameter(Constants.PARAM_RETURN_LOC);
    	String mode = request.getParameter(Constants.PARAM_MODE);
    	String courseId = request.getParameter(Constants.PARAM_COURSE_ID);
    	if (null==mode) mode ="";
    	if (null==returnLoc) returnLoc ="";
    	if (null==courseId) courseId ="";
	  
	%>
	<c:set var="returnLoc" value="<%=returnLoc%>" />
	<c:set var="mode" value="<%=mode%>" />
		
	<c:choose>
	   <c:when test="${mode == 'browse' and returnLoc=='blackboard'}">
		<%
		okUrl = "javascript:window.parent.location='"+ActionUrl.getHomeUrl()+"';";
		%>
		</c:when>
		<c:when test="${mode == 'browse' and returnLoc=='coursetools'}">
		<%
		okUrl = ActionUrl.getCourseToolHomeUrl(courseId);
		%>
		</c:when>
	</c:choose>
	<c:set var="okUrl" value="<%=okUrl%>" />

	${strEmptyPlaylistMessage}
	<bbNG:okButton url="${okUrl}"/>
</bbNG:genericPage>