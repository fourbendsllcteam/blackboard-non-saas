<%@ taglib prefix="bbNG" uri="/bbNG"%>

<bbNG:genericPage title="NBC Learn Configuration" bodyClass="normalBackground">

<bbNG:receipt type="SUCCESS" 
			  title="Configuration Success" 
			  recallUrl="/webapps/blackboard/admin/manage_plugins.jsp">
			  
You have successfully converted the courses and configured the security token.
			  
</bbNG:receipt>
			  
</bbNG:genericPage>