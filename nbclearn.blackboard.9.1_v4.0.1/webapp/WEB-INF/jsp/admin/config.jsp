<%@ taglib prefix="bbNG" uri="/bbNG"%>
<%@ taglib prefix="stripes" uri="http://stripes.sourceforge.net/stripes.tld"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>  
<%-- <fmt:message var="strSubmit" key="common.button.submit" bundle="${bundles.common}"/> 
<fmt:message var="strCancel" key="common.button.cancel" bundle="${bundles.common}"/> --%>

<bbNG:genericPage title="NBC Learn Configuration" bodyClass="normalBackground"> 

<bbNG:breadcrumbBar navItem="admin_plugin_manage" environment="SYS_ADMIN">
	<bbNG:breadcrumb>NBC Learn Configuration</bbNG:breadcrumb>
</bbNG:breadcrumbBar>

<bbNG:pageHeader>
	<bbNG:pageTitleBar title="NBC Learn Configuration"></bbNG:pageTitleBar>
	<bbNG:cssBlock>
	   <style>
        /*
        ========================================================================
            https://jira.gg.virtual.vps-host.net/browse/NBC-24 - 
            Security token renders to the left for IE: apply style to fix
            margin-left: 40px  
            
            Also, IE seems to not handle the following Bb theme.css style well:
            
            div[id*='stepcontent']
            
            so set explicitly
        ========================================================================
        */
            div#stepcontent1 {
				padding-top: 0px;
				padding-right: 0px;
				padding-bottom: 0px;
				padding-left: 48px;
				border-top: 0;
            }
            div#stepcontent2 {
				padding-top: 0px;
				padding-right: 0px;
				padding-bottom: 0px;
				padding-left: 48px;
				border-top: 0;
            }
            input.securityToken {
                margin-left: 40px;
            }
	   </style>
	</bbNG:cssBlock>
</bbNG:pageHeader>

<a href="http://www.nbclearn.com/blackboard" target="_SELF">For more information, click here.</a>

<stripes:form beanclass="blackboard.gs.nbc.action.ConfigTokenActionBean">
<stripes:errors/>

<%-- Hide the standard submit button and add our own.  Otherwise the
BBNG tags will localize the button name, which will confuse stripes --%>
<p class="taskbuttondiv">
<input  class="button-2" type="button" name="top_Cancel" value="Cancel" onclick="document.location='/webapps/blackboard/admin/manage_plugins.jsp';">
<input  class="submit button-1" name="top_Submit" type="submit" value="Submit" onClick="">
</p>

<bbNG:dataCollection showSubmitButtons="false">
<bbNG:step title="Configure NBC Security Token"  
		instructions="To use the NBC Learn building block you will need a security code provided by NBC.
		              Enter your code below and click Submit to configure this building block and enable usage.">
	<bbNG:dataElement label="Security Token">
		<stripes:text name="securityToken" style="margin-left: 40px;"/>
	</bbNG:dataElement>
	<%=blackboard.platform.security.NonceUtil.getNonceHtmlString(request,"nonce_id")%>
</bbNG:step>

<bbNG:step title="Course Data Conversion"  
		instructions="To convert the Html embedded player to on demand loading thumbnail view, you will have to provide the course id. Multiple courses can be entered in a comma separated form.">
	<bbNG:dataElement label="Course Id">
		
		<stripes:textarea name="courseId" cols="50" rows="4" style="margin-left: 40px; border-radius: 6px;"/>
	</bbNG:dataElement>
	<p><strong>Note :</strong> Course conversion is only required if you had previously installed NBC Learn building block version 7.0.0</p>
	</br>
	<%=blackboard.platform.security.NonceUtil.getNonceHtmlString(request,"nonce_id")%>
</bbNG:step>

<bbNG:step title="Submit" >
    <p class="taskbuttondiv">
    <input  class="button-2" type="button" name="bottom_Cancel" value="Cancel" onclick="document.location='/webapps/blackboard/admin/manage_plugins.jsp';">
    <input  class="submit button-1" name="bottom_Submit" type="submit" value="Submit" onClick="">
    </p>
</bbNG:step>
<bbNG:stepSubmit title="Submit" cancelUrl="/webapps/blackboard/admin/manage_plugins.jsp" />
</bbNG:dataCollection>

</stripes:form>

</bbNG:genericPage>
