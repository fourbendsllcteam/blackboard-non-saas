<%@ taglib prefix="bbNG" uri="/bbNG"%>

<bbNG:genericPage title="NBC Learn Configuration" bodyClass="normalBackground">

<bbNG:receipt type="FAIL" 
			  title="Security Token Not Saved" 
			  recallUrl="/webapps/blackboard/admin/manage_plugins.jsp">
			  
There were problems while saving your security token. Please check the logs for more information.
			  
</bbNG:receipt>
			  
</bbNG:genericPage>