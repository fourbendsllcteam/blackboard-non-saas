<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@ taglib uri="/bbNG" prefix="bbNG"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:choose>
	<c:when test="${not empty embedHTML }">
		<c:set var="htmlToInsert" value="${embedHTML}" />
	</c:when>
	<c:otherwise>
		<c:set var="htmlToInsert" value="${param['embedHTML']}" />
	</c:otherwise>
</c:choose>
<bbNG:genericPage onLoad="insertAndClose();">
	<bbNG:jsBlock>
		<script type="text/javascript">
			function insertAndClose() {
				var currentEditor = window.opener.currentVTBE;
				if (window.opener.tinyMceWrapper && window.opener.tinyMceWrapper.setMashupData) { //New tinyMce
					window.opener.tinyMceWrapper.setMashupData('${bbNG:EncodeLabel( htmlToInsert )}');
				} else if (currentEditor) {
					var ed = window.opener.editors[currentEditor];
					ed.insertHTML("${bbNG:EncodeLabel( htmlToInsert )}");
				}
				self.close();
			}
		</script>
	</bbNG:jsBlock>

</bbNG:genericPage>