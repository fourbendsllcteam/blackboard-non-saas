/**
 * Ryan Hardy
 * Guilford Group
 * 
 * VideoTest.java
 * Created: Sep 11, 2013
 */
package blackboard.gs.nbc.playlist.data.video;

import static org.junit.Assert.*;

import java.util.Calendar;

import junit.framework.Assert;

import org.junit.BeforeClass;
import org.junit.Test;

import blackboard.gs.nbc.context.NBCAppContext;
import blackboard.gs.nbc.context.impl.TestNBCAppCtxImpl;
import blackboard.gs.nbc.test.TestUtils;

/**
 * @author Ryan Hardy (rhardy@guilfordgroup.com)
 *
 */
public class VideoTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * Test method for {@link blackboard.gs.nbc.playlist.data.video.Video#getCueCardWithJavascriptAndCss()}.
	 */
	@Test
	public void testGetCueCardWithJavascriptAndCss() {
		//TODO
	}

	/**
	 * Test method for {@link blackboard.gs.nbc.playlist.data.video.Video#getCueCardWithoutJavascriptAndCss()}.
	 */
	@Test
	public void testGetCueCardWithoutJavascriptAndCss() {
		//TODO
	}

	/**
	 * Test method for {@link blackboard.gs.nbc.playlist.data.video.Video#getCueCardCourseContent()}.
	 */
	@Test
	public void testGetCueCardCourseContent() {
		//TODO
	}

	/**
	 * Test method for {@link blackboard.gs.nbc.playlist.data.video.Video#getCueCardMashupContent()}.
	 */
	@Test
	public void testGetCueCardMashupContent() {
		// test issue found in NBC-37
		NBCAppContext appCtx = TestUtils.getTestContext("SP13", "Default");
		Video video = new Video(appCtx);
		video.setAirDate(Calendar.getInstance());
		video.setDescription("Organized retail crime is sweeping the country, with thieves stealing some of the most popular products available in retail stores and reselling them on the black market.");
		video.setDuration("00:02:11");
		video.setName("Retail Crime a $12 Billion Problem");
		video.setShortType(VideoType.VIDEO.toString());
		video.setThumbnailDecodedUrl("https://static.nbclearn.com/files/icue/in_the_news/june_2012/week_of_june_10/1405942_130x100.jpg");
		video.setVideoId("test-id");
		
		String mashupContent = video.getCueCardMashupContent();
		// remove @X@nbc.cuecard@X@ before check (that is a legitimate 'dangling' context var)
		mashupContent = mashupContent.replace("@X@nbc.cuecard@X@", "");
		TestUtils.doContextVarCheck(mashupContent);
	}

}
