/**
 * Ryan Hardy
 * Guilford Group
 * 
 * NBCXssFilterInterceptorTest.java
 * Created: Jun 23, 2012
 */
package blackboard.gs.nbc.action;

import junit.framework.Assert;

import org.junit.BeforeClass;
import org.junit.Test;

import blackboard.gs.nbc.context.NBCAppContext;
import blackboard.gs.nbc.playlist.data.video.Video;
import blackboard.gs.nbc.test.TestUtils;
import blackboard.gs.nbc.util.BbVersionServiceTest;
import blackboard.gs.nbc.util.CueCardConfiguration;

/**
 * @author Ryan Hardy (rhardy@guilfordgroup.com)
 *
 */
public class NBCXssFilterInterceptorTest {
	
	@BeforeClass
	public static void setUp() {
	}

	/**
	 * Test method for {@link blackboard.gs.nbc.action.NBCXssFilterInterceptor#isUnsafeHtmlAllowed(java.lang.String)}.
	 */
	@Test
	public void testIsUnsafeHtmlAllowed() {
		NBCXssFilterInterceptor interceptor = new NBCXssFilterInterceptor();
		NBCAppContext appContext = TestUtils.getTestContext(BbVersionServiceTest.SP10_FULL_SERVICE_PACK, BbVersionServiceTest.THEME_NAME);
		Video video = TestUtils.createTestVideoInstance();		
		CueCardConfiguration cueCardConfig = appContext.getCueCardConfiguration();
		String mashup = cueCardConfig.renderMashupCueCardThumbnail(video);
		boolean isAllowed = interceptor.isUnsafeHtmlAllowed(mashup);
		Assert.assertTrue(isAllowed);
	}
}