/**
 * Ryan Hardy
 * Guilford Group
 * 
 * BbVersionServiceTest.java
 * Created: Jun 13, 2012
 */
package blackboard.gs.nbc.util;

import junit.framework.Assert;

import org.junit.Test;

import blackboard.gs.nbc.context.NBCAppContext;
import blackboard.gs.nbc.test.TestUtils;
import blackboard.gs.nbc.util.impl.TestBbVersionServiceImpl;

/**
 * @author Ryan Hardy (rhardy@guilfordgroup.com)
 *
 */
public class BbVersionServiceTest {
	public static final String THEME_NAME = 					"Bb Learn 2012";
	
	public static final String SP7_FULL_VERSION = 				"9.1.70081.25";
	public static final String SP7_MAJOR_VERSION = 				"9";
	public static final String SP7_MINOR_VERSION = 				"1";
	public static final String SP7_FULL_SERVICE_PACK = 			"70081.25";
	public static final String SP7_SERVICE_PACK = 				"7";
	
	private static final BbVersionService sp7versionSvc = new TestBbVersionServiceImpl(createAppContext(SP7_FULL_VERSION, THEME_NAME), SP7_FULL_VERSION, THEME_NAME);
	
	public static final String SP8_FULL_VERSION = 				"9.1.82223.0";
	public static final String SP8_MAJOR_VERSION = 				"9";
	public static final String SP8_MINOR_VERSION = 				"1";
	public static final String SP8_FULL_SERVICE_PACK = 			"82223.0";
	public static final String SP8_SERVICE_PACK = 				"8";
	
	private static final BbVersionService sp8versionSvc = new TestBbVersionServiceImpl(createAppContext(SP8_FULL_VERSION, THEME_NAME), SP8_FULL_VERSION, THEME_NAME);

	public static final String SP9_FULL_VERSION = 				"9.1.90132.0";
	public static final String SP9_MAJOR_VERSION = 				"9";
	public static final String SP9_MINOR_VERSION = 				"1";
	public static final String SP9_FULL_SERVICE_PACK = 			"90132.0";
	public static final String SP9_SERVICE_PACK = 				"9";
	
	private static final BbVersionService sp9versionSvc = new TestBbVersionServiceImpl(createAppContext(SP9_FULL_VERSION, THEME_NAME), SP9_FULL_VERSION, THEME_NAME);

	public static final String SP10_FULL_VERSION = 				"9.1.100367.0";
	public static final String SP10_MAJOR_VERSION = 			"9";
	public static final String SP10_MINOR_VERSION = 			"1";
	public static final String SP10_FULL_SERVICE_PACK = 		"100367.0";
	public static final String SP10_SERVICE_PACK = 				"10";
	
	private static final BbVersionService sp10versionSvc = new TestBbVersionServiceImpl(createAppContext(SP10_FULL_VERSION, THEME_NAME), SP10_FULL_VERSION, THEME_NAME);

	/**
	 * @param spFullVersion
	 * @param themeName
	 * @return
	 */
	private static NBCAppContext createAppContext(String spFullVersion, String themeName) {
		NBCAppContext appContext = TestUtils.getTestContext(spFullVersion, themeName);
		return appContext;
	}

	/**
	 * Test method for {@link blackboard.gs.nbc.util.BbVersionService#getFullVersion()}.
	 */
	@Test
	public void testGetFullVersion() {
		Assert.assertEquals(SP7_FULL_VERSION, sp7versionSvc.getFullVersion());
		Assert.assertEquals(SP8_FULL_VERSION, sp8versionSvc.getFullVersion());
		Assert.assertEquals(SP9_FULL_VERSION, sp9versionSvc.getFullVersion());
		Assert.assertEquals(SP10_FULL_VERSION, sp10versionSvc.getFullVersion());
	}

	/**
	 * Test method for {@link blackboard.gs.nbc.util.BbVersionService#getMajorVersion()}.
	 */
	@Test
	public void testGetMajorVersion() {
		Assert.assertEquals(SP7_MAJOR_VERSION, ""+sp7versionSvc.getMajorVersion());
		Assert.assertEquals(SP8_MAJOR_VERSION, ""+sp8versionSvc.getMajorVersion());
		Assert.assertEquals(SP9_MAJOR_VERSION, ""+sp9versionSvc.getMajorVersion());
		Assert.assertEquals(SP10_MAJOR_VERSION, ""+sp10versionSvc.getMajorVersion());
	}

	/**
	 * Test method for {@link blackboard.gs.nbc.util.BbVersionService#getMinorVersion()}.
	 */
	@Test
	public void testGetMinorVersion() {
		Assert.assertEquals(SP7_MINOR_VERSION, ""+sp7versionSvc.getMinorVersion());
		Assert.assertEquals(SP8_MINOR_VERSION, ""+sp8versionSvc.getMinorVersion());
		Assert.assertEquals(SP9_MINOR_VERSION, ""+sp9versionSvc.getMinorVersion());
		Assert.assertEquals(SP10_MINOR_VERSION, ""+sp10versionSvc.getMinorVersion());
	}

	/**
	 * Test method for {@link blackboard.gs.nbc.util.BbVersionService#getFullServicePack()}.
	 */
	@Test
	public void testGetFullServicePack() {
		Assert.assertEquals(SP7_FULL_SERVICE_PACK, sp7versionSvc.getFullServicePack());
		Assert.assertEquals(SP8_FULL_SERVICE_PACK, sp8versionSvc.getFullServicePack());
		Assert.assertEquals(SP9_FULL_SERVICE_PACK, sp9versionSvc.getFullServicePack());
		Assert.assertEquals(SP10_FULL_SERVICE_PACK, sp10versionSvc.getFullServicePack());
	}

	/**
	 * Test method for {@link blackboard.gs.nbc.util.BbVersionService#getServicePack()}.
	 */
	@Test
	public void testGetServicePack() {
		Assert.assertEquals(SP7_SERVICE_PACK, ""+sp7versionSvc.getServicePack());
		Assert.assertEquals(SP8_SERVICE_PACK, ""+sp8versionSvc.getServicePack());
		Assert.assertEquals(SP9_SERVICE_PACK, ""+sp9versionSvc.getServicePack());
		Assert.assertEquals(SP10_SERVICE_PACK, ""+sp10versionSvc.getServicePack());
	}

	@Test
	public void testIsSP() {
		Assert.assertTrue(sp7versionSvc.isSP(7));
		Assert.assertTrue(sp8versionSvc.isSP(8));
		Assert.assertTrue(sp9versionSvc.isSP(9));
		Assert.assertTrue(sp10versionSvc.isSP(10));
	}
	
	@Test
	public void testIsGreaterThanSP() {
		Assert.assertTrue(sp7versionSvc.isGreaterThanSP(6));
		Assert.assertFalse(sp7versionSvc.isGreaterThanSP(7));
		Assert.assertFalse(sp7versionSvc.isGreaterThanSP(8));
		
		Assert.assertTrue(sp8versionSvc.isGreaterThanSP(7));
		Assert.assertFalse(sp8versionSvc.isGreaterThanSP(8));
		Assert.assertFalse(sp8versionSvc.isGreaterThanSP(9));
		
		Assert.assertTrue(sp9versionSvc.isGreaterThanSP(8));
		Assert.assertFalse(sp9versionSvc.isGreaterThanSP(9));
		Assert.assertFalse(sp9versionSvc.isGreaterThanSP(10));
		
		Assert.assertTrue(sp10versionSvc.isGreaterThanSP(9));
		Assert.assertFalse(sp10versionSvc.isGreaterThanSP(10));
		Assert.assertFalse(sp10versionSvc.isGreaterThanSP(11));
		
	}
	
	@Test
	public void testIsLessThanSP() {
		Assert.assertTrue(sp7versionSvc.isLessThanSP(8));
		Assert.assertFalse(sp7versionSvc.isLessThanSP(7));
		Assert.assertFalse(sp7versionSvc.isLessThanSP(6));
		
		Assert.assertTrue(sp8versionSvc.isLessThanSP(9));
		Assert.assertFalse(sp8versionSvc.isLessThanSP(8));
		Assert.assertFalse(sp8versionSvc.isLessThanSP(7));
		
		Assert.assertTrue(sp9versionSvc.isLessThanSP(10));
		Assert.assertFalse(sp9versionSvc.isLessThanSP(9));
		Assert.assertFalse(sp9versionSvc.isLessThanSP(8));
		
		Assert.assertTrue(sp10versionSvc.isLessThanSP(11));
		Assert.assertFalse(sp10versionSvc.isLessThanSP(10));
		Assert.assertFalse(sp10versionSvc.isLessThanSP(9));
		
	}
}