/**
 * Ryan Hardy
 * Guilford Group
 * 
 * TestContextServiceImpl.java
 * Created: Jun 14, 2012
 */
package blackboard.gs.nbc.util.impl;

import blackboard.data.course.Course;
import blackboard.data.course.CourseMembership;
import blackboard.data.role.PortalRole;
import blackboard.data.user.User;
import blackboard.gs.nbc.context.NBCAppContext;
import blackboard.gs.nbc.util.ContextServiceException;

/**
 * @author Ryan Hardy (rhardy@guilfordgroup.com)
 *
 */
public class TestContextServiceImpl extends DefaultContextServiceImpl {

	/**
	 * @param appContext
	 */
	public TestContextServiceImpl(NBCAppContext appContext) {
		super(appContext);
	}

	@Override
	protected User loadUser() throws ContextServiceException {
		User user = new User();
		user.setBatchUid("testUserBatchUid");
		@SuppressWarnings("serial")
		PortalRole role = new PortalRole() {
			@Override
			public String getRoleName() {
				return "INSTRUCTOR";
			}			
		};
		user.setPortalRole(role);
		user.setEmailAddress("test@test.com");
		return user;
	}

	@Override
	protected Course loadCourse() throws ContextServiceException {
		Course course = new Course();
		course.setBatchUid("testCourseBatchUid");
		course.setTitle("Test Course");
		return course;
	}

	@Override
	protected CourseMembership loadCourseMembership() throws ContextServiceException {
		@SuppressWarnings("serial")
		CourseMembership cMembership = new CourseMembership() {
			@Override
			public Role getRole() {
				return CourseMembership.Role.INSTRUCTOR;
			}			
		};
				
		return cMembership;
	}
	
	
}