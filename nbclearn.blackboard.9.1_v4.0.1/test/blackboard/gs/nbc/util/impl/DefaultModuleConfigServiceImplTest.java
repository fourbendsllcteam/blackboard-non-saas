/**
 * Ryan Hardy
 * Guilford Group
 * 
 * DefaultModuleConfigServiceImplTest.java
 * Created: Nov 19, 2012
 */
package blackboard.gs.nbc.util.impl;

import javax.servlet.ServletContextEvent;

import junit.framework.Assert;
import net.sourceforge.stripes.mock.MockServletContext;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import blackboard.gs.nbc.context.NBCAppContext;
import blackboard.gs.nbc.test.TestUtils;
import blackboard.gs.nbc.util.ModuleConfigService;

/**
 * @author Ryan Hardy (rhardy@guilfordgroup.com)
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/applicationContext.xml")
public class DefaultModuleConfigServiceImplTest {
	private static final Logger logger = Logger.getLogger(DefaultModuleConfigServiceImplTest.class);
	static {
		Logger.getLogger(DefaultModuleConfigServiceImpl.class).setLevel(Level.TRACE);
	}
	
	// the static values that SHOULD be returned in calls to ModuleConfigService impl.
	private static final String africanAmericanHistoryView = "https://static.nbclearn.com/files/nbclearn/site/video/widget/multiWidget.swf?source=bb&type=multi&WID=African%20American%20Studies";
	private static final String businessAndFinanceView = "https://static.nbclearn.com/files/nbclearn/site/video/widget/multiWidget.swf?source=bb&type=multi&WID=Business%20%26%20Financial%20Literacy";
	private static final String healthAndWellnessView = "https://static.nbclearn.com/files/nbclearn/site/video/widget/multiWidget.swf?source=bb&type=multi&WID=Health%20%26%20Wellness";
	private static final String inTheNewsView = "https://static.nbclearn.com/files/nbclearn/site/video/widget/multiWidget.swf?source=bb&type=multi&WID=Current%20Events";
	private static final String languageArtsView = "https://static.nbclearn.com/files/nbclearn/site/video/widget/multiWidget.swf?source=bb&type=multi&WID=Language%20Arts";
	private static final String scienceView = "https://static.nbclearn.com/files/nbclearn/site/video/widget/multiWidget.swf?source=bb&type=multi&WID=Science";
	private static final String socialStudiesView = "https://static.nbclearn.com/files/nbclearn/site/video/widget/multiWidget.swf?source=bb&type=multi&WID=Social%20Studies";
	private static final String usHistoryView = "https://static.nbclearn.com/files/nbclearn/site/video/widget/multiWidget.swf?source=bb&type=multi&WID=U.S.%20History";
	private static final String womensHistoryView = "https://static.nbclearn.com/files/nbclearn/site/video/widget/multiWidget.swf?source=bb&type=multi&WID=Women%27s%20Studies";
	
	private static ModuleConfigService moduleConfigScv;
	
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		logger.info("Creating new Mock ServletContext instance.");
		MockServletContext ctx = new MockServletContext("nbc-learn-test");
		
		NBCAppContext appContext = TestUtils.getTestContext("SP13", "Default Theme");
		appContext.setAttribute(DefaultModuleConfigServiceImpl.CTX_PARAM_PROPS_FILE, "/test-resources/module-config.TEST.properties");
		DefaultModuleConfigServiceImpl svcImpl = new DefaultModuleConfigServiceImpl(appContext);
	
		DefaultModuleConfigServiceImplTest.moduleConfigScv = svcImpl;
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * Test method for {@link blackboard.gs.nbc.util.impl.DefaultModuleConfigServiceImpl#getAfricanAmericanHistoryUrl()}.
	 */
	@Test
	public void testGetAfricanAmericanHistoryUrl() {
		String url = DefaultModuleConfigServiceImplTest.moduleConfigScv.getAfricanAmericanHistoryUrl();
		Assert.assertNotNull("The African American History Url is null.", url);
		Assert.assertEquals("The African American History Url is incorrect.", africanAmericanHistoryView, url);
	}

	/**
	 * Test method for {@link blackboard.gs.nbc.util.impl.DefaultModuleConfigServiceImpl#getBusinessAndFinanceUrl()}.
	 */
	@Test
	public void testGetBusinessAndFinanceUrl() {
		String url = DefaultModuleConfigServiceImplTest.moduleConfigScv.getBusinessAndFinanceUrl();
		Assert.assertNotNull("The Business And Finance Url is null.", url);
		Assert.assertEquals("The BusinessAndFinance Url is incorrect.", businessAndFinanceView, url);
	}

	/**
	 * Test method for {@link blackboard.gs.nbc.util.impl.DefaultModuleConfigServiceImpl#getHealthAndWellnessUrl()}.
	 */
	@Test
	public void testGetHealthAndWellnessUrl() {
		String url = DefaultModuleConfigServiceImplTest.moduleConfigScv.getHealthAndWellnessUrl();
		Assert.assertNotNull("The Health And Wellness Url is null.", url);
		Assert.assertEquals("The Health And Wellness Url is incorrect.", healthAndWellnessView, url);
	}

	/**
	 * Test method for {@link blackboard.gs.nbc.util.impl.DefaultModuleConfigServiceImpl#getInTheNewsUrl()}.
	 */
	@Test
	public void testGetInTheNewsUrl() {
		String url = DefaultModuleConfigServiceImplTest.moduleConfigScv.getInTheNewsUrl();
		Assert.assertNotNull("The In The News Url is null.", url);
		Assert.assertEquals("The In The News Url is incorrect.", inTheNewsView, url);
	}

	/**
	 * Test method for {@link blackboard.gs.nbc.util.impl.DefaultModuleConfigServiceImpl#getLanguageArtsUrl()}.
	 */
	@Test
	public void testGetLanguageArtsUrl() {
		String url = DefaultModuleConfigServiceImplTest.moduleConfigScv.getLanguageArtsUrl();
		Assert.assertNotNull("The Language Arts Url is null.", url);
		Assert.assertEquals("The Language Arts Url is incorrect.", languageArtsView, url);
	}

	/**
	 * Test method for {@link blackboard.gs.nbc.util.impl.DefaultModuleConfigServiceImpl#getScienceUrl()}.
	 */
	@Test
	public void testGetScienceUrl() {
		String url = DefaultModuleConfigServiceImplTest.moduleConfigScv.getScienceUrl();
		Assert.assertNotNull("The Science URL is null.", url);
		Assert.assertEquals("The Science Url is incorrect.", scienceView, url);
	}

	/**
	 * Test method for {@link blackboard.gs.nbc.util.impl.DefaultModuleConfigServiceImpl#getSocialStudiesUrl()}.
	 */
	@Test
	public void testGetSocialStudiesUrl() {
		String url = DefaultModuleConfigServiceImplTest.moduleConfigScv.getSocialStudiesUrl();
		Assert.assertNotNull("The Social Studies Url is null.", url);
		Assert.assertEquals("The Social Studies Url is incorrect.", socialStudiesView, url);
	}

	/**
	 * Test method for {@link blackboard.gs.nbc.util.impl.DefaultModuleConfigServiceImpl#getUsHistoryUrl()}.
	 */
	@Test
	public void testGetUsHistoryUrl() {
		String url = DefaultModuleConfigServiceImplTest.moduleConfigScv.getUsHistoryUrl();
		Assert.assertNotNull("The US History Url is null.", url);
		Assert.assertEquals("The US History Url is incorrect.", usHistoryView, url);
	}

	/**
	 * Test method for {@link blackboard.gs.nbc.util.impl.DefaultModuleConfigServiceImpl#getWomensHistoryUrl()}.
	 */
	@Test
	public void testGetWomensHistoryUrl() {
		String url = DefaultModuleConfigServiceImplTest.moduleConfigScv.getWomensHistoryUrl();
		Assert.assertNotNull("The Women's History Url is null.", url);
		Assert.assertEquals("The Women's History Url is incorrect.", womensHistoryView, url);
	}

}
