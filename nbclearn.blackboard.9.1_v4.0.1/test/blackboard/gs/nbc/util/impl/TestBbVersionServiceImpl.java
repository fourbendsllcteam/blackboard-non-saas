/**
 * Ryan Hardy
 * Guilford Group
 * 
 * TestBbVersionServiceImpl.java
 * Created: Jun 13, 2012
 */
package blackboard.gs.nbc.util.impl;

import blackboard.gs.nbc.context.NBCAppContext;

/**
 * @author Ryan Hardy (rhardy@guilfordgroup.com)
 *
 */
public class TestBbVersionServiceImpl extends DefaultBbVersionServiceImpl {
	private String staticVersion;
	private String staticTheme;

	/**
	 * staticVersion MUST be of form 'x.x.x', where each x is one or 
	 * more digits. There may be MORE '.x' segments, but not less. 
	 * 
	 * @param staticVersion
	 */
	public TestBbVersionServiceImpl(NBCAppContext appContext, String staticVersion, String staticTheme) {
		super(appContext);
		this.staticVersion = staticVersion;
		this.staticTheme = staticTheme;
		
	}

	@Override
	public String getFullVersion() {
		return this.staticVersion;
	}

	@Override
	public String getCurrentThemeName() {
		return this.staticTheme;
	}	
}
