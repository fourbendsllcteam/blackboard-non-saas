package blackboard.gs.nbc.util.impl;

import junit.framework.Assert;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

import blackboard.gs.nbc.context.NBCAppContext;
import blackboard.gs.nbc.playlist.data.video.Video;
import blackboard.gs.nbc.test.TestUtils;
import blackboard.gs.nbc.util.BbVersionServiceTest;
import blackboard.gs.nbc.util.CueCardConfiguration;

public class CueCardConfigurationTest {
	private static final Logger logger = Logger.getLogger(CueCardConfigurationTest.class);
	private static CueCardConfiguration cueCardUtil;
	
	@BeforeClass
	public static void setUp() {
		NBCAppContext appContext = TestUtils.getTestContext(BbVersionServiceTest.SP10_FULL_SERVICE_PACK, BbVersionServiceTest.THEME_NAME);
		cueCardUtil = appContext.getCueCardConfiguration();
	}
	
	@Test
	public void testRenderContentDescription() {
		final Video video = TestUtils.createTestVideoInstance();
		String str = cueCardUtil.renderDescription(video,null);
		Assert.assertNotNull(str);
		displayContent("renderDescription", str);
		// this ensures that test does not fail due to 'dangling' @X@nbc.cuecard@X@ variable that
		// is used by NBCResolver
		str = str.replace("@X@nbc.cuecard@X@", "");
		TestUtils.doContextVarCheck(str);
		//TODO restore this once stub DB components are in place
		//Assert.assertFalse("Content includes empty ALT attributes.", str.contains("alt=\"\""));
	}
	
	@Test
	public void testRenderStaticCueCard() {
		final String cueCardId = "TEST_ID";
		String str = cueCardUtil.renderStaticCueCard(cueCardId);
		Assert.assertNotNull(str);
		displayContent("renderStaticCueCard", str);
		Assert.assertTrue(str.contains(cueCardId));
		Assert.assertFalse(str.contains("@X@"));
	}

	@Test
	public void testRenderFloatingCueCardIncludes() {
		String str = cueCardUtil.renderFloatingCueCardIncludes();
		Assert.assertNotNull(str);
		displayContent("renderFloatingCueCardIncludes", str);
		TestUtils.doContextVarCheck(str);
		Assert.assertTrue("", str.contains("var NBCLoader = {"));
	}

	@Test
	public void testRenderPlaylistCueCard() {
		Video video = TestUtils.createTestVideoInstance();
		String str = cueCardUtil.renderPlaylistCueCard(video);
		Assert.assertNotNull(str);
		displayContent("renderCourseContentCueCard", str);
		Assert.assertTrue(str.contains(video.getVideoId()));
		Assert.assertTrue(str.contains(video.getThumbnailUrl()));
		Assert.assertFalse(str.contains("@X@"));
	}

	@Test
	public void testRenderCourseContentCueCard() {
		Video video = TestUtils.createTestVideoInstance();
		String str = cueCardUtil.renderCourseContentCueCard(video);
		Assert.assertNotNull(str);
		displayContent("renderCourseContentCueCard", str);
		Assert.assertTrue(str.contains(video.getVideoId()));
		Assert.assertTrue(str.contains(video.getThumbnailUrl()));
		// this ensures that test does not fail due to 'dangling' @X@nbc.cuecard@X@ variable that
		// is used by NBCResolver
		Assert.assertFalse(str.replace("@X@nbc.cuecard@X@", "").contains("@X@"));
	}

	@Test
	public void testRenderMashupCueCardThumbnail() {
		Video video = TestUtils.createTestVideoInstance();
		String str = cueCardUtil.renderMashupCueCardThumbnail(video);
		Assert.assertNotNull(str);
		displayContent("renderMashupCueCardThumbnail", str);
		// this ensures that test does not fail due to 'dangling' @X@nbc.cuecard@X@ variable that
		// is used by NBCResolver
		str = str.replace("@X@nbc.cuecard@X@", "");
		TestUtils.doContextVarCheck(str);
		Assert.assertFalse("Content includes empty ALT attributes.", str.contains("alt=\"\""));
	}

	@Test
	public void testRenderContentModule() {
		Video video = TestUtils.createTestVideoInstance();
		String str = cueCardUtil.renderContentModule(video);
		Assert.assertNotNull(str);
		displayContent("renderContentModule", str);
		// this ensures that test does not fail due to 'dangling' @X@nbc.cuecard@X@ variable that
		// is used by NBCResolver
		str = str.replace("@X@nbc.cuecard@X@", "");
		TestUtils.doContextVarCheck(str);
		Assert.assertFalse("Content includes empty ALT attributes.", str.contains("alt=\"\""));
	}
	
	@Test
	public void testRenderMashupScript() {
		String str = cueCardUtil.renderMashupScript();
		Assert.assertNotNull(str);
		displayContent("renderMashupScript", str);
		TestUtils.doContextVarCheck(str);
		Assert.assertTrue("", str.contains("var NBCLoader = {"));
	}
	
	private void displayContent(String method, String str) {
		logger.info("===========================================================================");
		logger.info("Rendered content for " + method + ":");
		logger.info(
				"\n-------------------------------------------------------------------------\n" + 
				str + 
				"\n-------------------------------------------------------------------------\n");
	}

}