/**
 * Ryan Hardy
 * Guilford Group
 * 
 * AllTests.java
 * Created: Jun 13, 2012
 */
package blackboard.gs.nbc;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import blackboard.gs.nbc.action.NBCXssFilterInterceptorTest;
import blackboard.gs.nbc.context.NBCAppContextFactoryTest;
import blackboard.gs.nbc.context.NBCResolverTest;
import blackboard.gs.nbc.playlist.data.video.VideoTest;
import blackboard.gs.nbc.util.BbVersionServiceTest;
import blackboard.gs.nbc.util.impl.CueCardConfigurationTest;
import blackboard.gs.nbc.util.impl.DefaultModuleConfigServiceImplTest;

/**
 * Test suite containing all NBC unit tests (will not be necessary after Maven integration).
 * 
 * @author Ryan Hardy (rhardy@guilfordgroup.com)
 */
@RunWith(Suite.class)
@SuiteClasses({ 	NBCResolverTest.class, 
					NBCXssFilterInterceptorTest.class, 
					BbVersionServiceTest.class, 
					CueCardConfigurationTest.class, 
					DefaultModuleConfigServiceImplTest.class,
					NBCAppContextFactoryTest.class,
					VideoTest.class
})
public class AllTests {

}