/**
 * Ryan Hardy
 * Guilford Group
 * 
 * NBCAppContextFactoryTest.java
 * Created: Jun 10, 2013
 */
package blackboard.gs.nbc.context;

import static org.junit.Assert.fail;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import junit.framework.Assert;

import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;

import blackboard.gs.nbc.context.impl.SessionBackedNBCAppCtxImpl;
import blackboard.gs.nbc.context.impl.TransientNBCAppCtxImpl;

/**
 * @author Ryan Hardy (rhardy@guilfordgroup.com)
 *
 */
public class NBCAppContextFactoryTest {
	private static final Logger logger = Logger.getLogger(NBCAppContextFactoryTest.class);
	
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * Test method for {@link blackboard.gs.nbc.context.NBCAppContextFactory#createAppContext(javax.servlet.http.HttpServletRequest)}.
	 */
	@Test
	public void testCreateAppContext() {
		MockHttpServletRequest request = new MockHttpServletRequest();
		HttpSession session = request.getSession();
		// placing these into session ensures that actual lookup in Bb core does not happen
		session.setAttribute("blackboard.gs.nbc.util.impl.SessionCachingBbPlatformSvcImpl-uriStem", "BBLEARN");
		session.setAttribute("blackboard.gs.nbc.util.impl.SessionCachingBbPlatformSvcImpl-hostName", "localhost");
		session.setAttribute("blackboard.gs.nbc.util.impl.SessionCachingBbPlatformSvcImpl-isForcedSystemSSL", true);
		session.setAttribute("blackboard.gs.nbc.util.impl.SessionCachingBbPlatformSvcImpl-uuid", "testuuid");
		
		NBCAppContext appContext;
		try {
			Map<String, Object> atts = new HashMap<String, Object>();
			atts.put("envFile", "/environment.DEV.template");
			appContext = NBCAppContextFactory.createAppContext(request, atts);
		} catch (Exception e) {
			logger.error("Could not create NBCApppContext.", e);
			fail(e.getMessage());
			return;
		}
		Assert.assertNotNull(appContext);
		Assert.assertTrue(appContext instanceof SessionBackedNBCAppCtxImpl);
	}

	
	@Test
	public void testCreateTransientAppContext() {
		NBCAppContext appContext;
		try {
			Map<String, Object> atts = new HashMap<String, Object>();
			atts.put("envFile", "/environment.DEV.template");
			appContext = NBCAppContextFactory.createAppContext(null, atts);
		} catch (Exception e) {
			logger.error("Could not create NBCApppContext.", e);
			fail(e.getMessage());
			return;
		}
		Assert.assertNotNull(appContext);
		Assert.assertTrue(appContext instanceof TransientNBCAppCtxImpl);
		Assert.assertNotNull("BbVersionService is null.", appContext.getBbVersionService());		
	}
	
	@Test
	public void testClassCastError() {
		MockHttpServletRequest request = new MockHttpServletRequest();
		HttpSession session = request.getSession();
		// placing these into session ensures that actual lookup in Bb core does not happen
		session.setAttribute("blackboard.gs.nbc.util.impl.SessionCachingBbPlatformSvcImpl-uriStem", "BBLEARN");
		session.setAttribute("blackboard.gs.nbc.util.impl.SessionCachingBbPlatformSvcImpl-hostName", "localhost");
		session.setAttribute("blackboard.gs.nbc.util.impl.SessionCachingBbPlatformSvcImpl-isForcedSystemSSL", true);
		session.setAttribute("blackboard.gs.nbc.util.impl.SessionCachingBbPlatformSvcImpl-uuid", "testuuid");
		
		final NBCAppContext appContext;
		try {
			Map<String, Object> atts = new HashMap<String, Object>();
			atts.put("envFile", "/environment.DEV.template");
			appContext = NBCAppContextFactory.createAppContext(request, atts);
		} catch (Exception e) {
			logger.error("Could not create NBCApppContext.", e);
			fail(e.getMessage());
			return;
		}
		Assert.assertNotNull(appContext);
		Assert.assertTrue(appContext instanceof SessionBackedNBCAppCtxImpl);
		
		Object toChk = session.getAttribute(NBCAppContextFactory.SESSION_ATT_NBC_CTX);
		Assert.assertNotNull(toChk);
		Assert.assertTrue(toChk instanceof SessionBackedNBCAppCtxImpl);
		
		final NBCAppContext appContext2;
		try {
			appContext2 = NBCAppContextFactory.createAppContext(request);
		} catch (Exception e) {
			logger.error("Could not create NBCApppContext.", e);
			fail(e.getMessage());
			return;
		}
		Assert.assertNotNull(appContext2);
		Assert.assertTrue(appContext2 instanceof SessionBackedNBCAppCtxImpl);
		Assert.assertEquals(appContext, appContext2);
		
	}
}
