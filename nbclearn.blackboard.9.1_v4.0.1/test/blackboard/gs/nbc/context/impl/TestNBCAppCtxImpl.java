/**
 * Ryan Hardy
 * Guilford Group
 * 
 * RequestBackedNBCAppCtxImpl.java
 * Created: Jun 7, 2013
 */
package blackboard.gs.nbc.context.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import blackboard.gs.nbc.config.impl.StaticEnvConfigImpl;
import blackboard.gs.nbc.context.NBCAppContext;
import blackboard.gs.nbc.playlist.persist.playlistitem.impl.PlaylistItemDbLoaderImpl;
import blackboard.gs.nbc.playlist.persist.playlistitem.impl.PlaylistItemDbPersisterImpl;
import blackboard.gs.nbc.playlist.persist.video.impl.VideoDbLoaderImpl;
import blackboard.gs.nbc.playlist.persist.video.impl.VideoDbPersisterImpl;
import blackboard.gs.nbc.playlist.service.impl.PlaylistManagerServiceImpl;
import blackboard.gs.nbc.test.TestUtils;
import blackboard.gs.nbc.util.impl.DefaultBbPlatformServiceImpl;
import blackboard.gs.nbc.util.impl.DefaultCueCardConfigImpl;
import blackboard.gs.nbc.util.impl.DefaultModuleConfigServiceImpl;
import blackboard.gs.nbc.util.impl.StaticSecurityTokenMgrImpl;
import blackboard.gs.nbc.util.impl.TestBbVersionServiceImpl;
import blackboard.gs.nbc.util.impl.TestContextServiceImpl;

/**
 * {@link NBCAppContext} class that is not backed by a session (i.e. is a
 * 'throw-away' instance).
 * 
 * @author Ryan Hardy (rhardy@guilfordgroup.com)
 *
 */
public class TestNBCAppCtxImpl extends BaseNBCAppCtxImpl implements NBCAppContext, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4950273046665602582L;
	
	/**
	 * 
	 */
	public TestNBCAppCtxImpl() {
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onUpdatedRequestContext() {
		HttpServletRequest request = (HttpServletRequest) this.getRequestObject();
		
		this.setBbPlatformService(new DefaultBbPlatformServiceImpl(this));
		Properties envProps;
		InputStream is = TestNBCAppCtxImpl.class.getResourceAsStream("/environment.DEV.template");
		envProps = new Properties();
		try {
			envProps.load(is);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		// inject dynamically so that test file resolution works
		String hostUrl = TestUtils.getTestHostUrl();
		envProps.setProperty("host_url", hostUrl);
		this.setEnvironmentConfiguration(new StaticEnvConfigImpl(envProps));
		
		
		final String version = request.getParameter("version");
		final String theme = request.getParameter("theme");
		this.setBbVersionService(new TestBbVersionServiceImpl(this, version, theme));
		this.setContextService(new TestContextServiceImpl(this));
		this.setSecurityTokenManager(new StaticSecurityTokenMgrImpl());
		this.setModuleConfigService(new DefaultModuleConfigServiceImpl(this));
		this.setCueCardConfig(new DefaultCueCardConfigImpl(this));
		
		this.setPlaylistManagerService(new PlaylistManagerServiceImpl(this));
		this.setPlaylistItemDbLoader(new PlaylistItemDbLoaderImpl(this));
		this.setPlaylistItemDbPersister(new PlaylistItemDbPersisterImpl(this));
		this.setVideoDbLoader(new VideoDbLoaderImpl(this));
		this.setVideoDbPersister(new VideoDbPersisterImpl(this));
	}
	
	
}
