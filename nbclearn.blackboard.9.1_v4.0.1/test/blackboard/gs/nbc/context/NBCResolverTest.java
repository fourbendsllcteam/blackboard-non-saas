/**
 * Ryan Hardy
 * Guilford Group
 * 
 * NBCResolverTest.java
 * Created: Jun 13, 2012
 */
package blackboard.gs.nbc.context;

import junit.framework.Assert;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;

import blackboard.gs.nbc.test.TestUtils;

/**
 * @author Ryan Hardy (rhardy@guilfordgroup.com)
 *
 */
public class NBCResolverTest {
	private static final String THEME_NAME = "Bb Learn 2012";
	private static final String BB_VER_SP7 = "9.1.70081.25";
	private static final String BB_VER_SP8 = "9.1.82223.0";
	
	private static NBCResolver resolver;
	
	@BeforeClass
	public static void setUp() {
		String sp = BB_VER_SP7;
		String theme = THEME_NAME;
		MockHttpServletRequest request = createTestRequest(sp, theme);
		
		resolver = new NBCResolver(request);
	}

	private static MockHttpServletRequest createTestRequest(String sp, String theme) {
		MockHttpServletRequest request = new MockHttpServletRequest();
		request.addParameter("isTest", "true");
		request.addParameter("version", sp);
		request.addParameter("theme", theme);
//		request.setAttribute("nbcFilesLoaderScriptPath", "/webapp/js/nbcFilesLoader.js");
		return request;
	}
	
	/**
	 * Test method for {@link blackboard.gs.nbc.context.NBCResolver#getKeys()}.
	 */
	@Test
	public void testGetKeys() {
		String[] keys = resolver.getKeys();
		Assert.assertNotNull("The resolver keys are null.", keys);
		Assert.assertTrue("The resolver keys have a length of zero.", keys.length > 0);
		Assert.assertEquals("The resolver key is not correct.", "nbc", keys[0]);
	}

	/**
	 * Test method for {@link blackboard.gs.nbc.context.NBCResolver#resolve(java.lang.String, java.lang.String[])}.
	 */
	@Test
	public void testResolve() {
		String script = resolver.resolve("cuecard", new String[0]);
		Assert.assertNotNull("The resolver script is null.", script);
		Assert.assertFalse("The resolver script is an empty string.", script.isEmpty());
		
		TestUtils.doContextVarCheck(script);
		
		//TODO: more stringent content checking
		String sp = BB_VER_SP8;
		String theme = THEME_NAME;
		MockHttpServletRequest request = createTestRequest(sp, theme);
		
		resolver = new NBCResolver(request);
		script = resolver.resolve("cuecard", new String[0]);
		
		TestUtils.doContextVarCheck(script);
	}


}
