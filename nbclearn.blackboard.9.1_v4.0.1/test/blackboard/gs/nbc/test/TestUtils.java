/**
 * Ryan Hardy
 * Guilford Group
 * 
 * TestUtils.java
 * Created: Jun 23, 2012
 */
package blackboard.gs.nbc.test;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletContext;

import junit.framework.Assert;

import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockServletContext;

import blackboard.gs.nbc.context.NBCAppContext;
import blackboard.gs.nbc.context.NBCAppContextFactory;
import blackboard.gs.nbc.playlist.data.video.Video;
import blackboard.gs.nbc.playlist.data.video.VideoType;
import blackboard.gs.nbc.util.BbVersionServiceTest;
import blackboard.gs.nbc.util.persist.Id;

/**
 * Collection of static utility methods used for unit testing.
 * 
 * @author Ryan Hardy (rhardy@guilfordgroup.com)
 */
public class TestUtils {	
	/**
	 * Creates a test {@link NBCAppContext} instance using the given values.
	 * @param basePath
	 * @param sp
	 * @param theme
	 * @return
	 */
	public static NBCAppContext getTestContext(String sp, String theme) {
		ServletContext ctx = getTestServletContext();
		MockHttpServletRequest request = new MockHttpServletRequest(ctx);
		//TODO: use new context attributes for these
		request.addParameter("isTest", "true");
		request.addParameter("version", sp);
		request.addParameter("theme", theme);
		request.addParameter("testToken", "<TEST TOKEN>");//TODO
		
		NBCAppContext appContext = NBCAppContextFactory.createAppContext(request);
		appContext.setAttribute("nbcFilesLoaderScriptPath", "/webapp/js/nbcFilesLoader.js");

		return appContext;
	}
	
	/**
	 * Performs a check of the rendered script for any lingering unresolved
	 * context variables (i.e. surrounded in '@X@' demarcations).
	 * <br /><br />
	 * This method should be kept in sync with nbcFilesLoader.js to ensure
	 * that false negatives are not thrown here (i.e. there will be legitimate
	 * occurrences of '@X@' within script that need to be accounted for
	 * when checking).
	 * 
	 * @param script
	 */
	public static void doContextVarCheck(String script) {
		final String MARKER = "path.indexOf(\"";
		final int MARKER_LEN = MARKER.length();
		int scrIndex = 0;
		do {
			// need to account for the fact tha script now contains code checking for
			// presence of '@X@'
			int indexOfCheckCode = script.indexOf(MARKER + "@X@", scrIndex);
			int indexOfVar = script.indexOf("@X@", scrIndex);
			if (indexOfVar == -1) {
				break;
			}
			int diff = indexOfVar - indexOfCheckCode;
			
			Assert.assertTrue("The resolver script still contains context variables ("+getCtxVars(script.substring(scrIndex))+").", (diff == MARKER_LEN));
			scrIndex = indexOfVar + 3;
			
		} while (scrIndex > 0 && scrIndex < script.length());
	}
	
	private static List<String> getCtxVars(String str) {
		List<String> list = new ArrayList<String>();
		int index = 0;
		do {
			int indexOfCheckCode = str.indexOf("@X@", index);
			if (indexOfCheckCode == -1) {
				break;
			}
			int indexOfVar = str.indexOf("@X@", indexOfCheckCode + 1);
			if (indexOfVar == -1) {
				break;
			}
			index = indexOfVar + 3;
			String var = str.substring(indexOfCheckCode, index);
			list.add(var);
		} while (index > 0 && index < str.length());
		return list;
	}

	/**
	 * Creates a {@link MockServletContext}, using the current base execution
	 * path as the servlet context base path.
	 * @return
	 */
	public static ServletContext getTestServletContext() {
		String basePath = getTestHostUrl();
		MockServletContext ctx = new MockServletContext(basePath);
		return ctx;
	}

	/**
	 * 
	 * @return
	 */
	public static String getTestHostUrl() {
		File f = new File(".");
		String basePath = f.getAbsolutePath();
		String thisDir = File.separator + ".";
		int ind = basePath.lastIndexOf(thisDir);
		if (ind > 0) {
			basePath = basePath.substring(0, ind);
		}
		if (basePath.startsWith("C:")) {
			basePath = basePath.replaceFirst("C:", "");
		}
		basePath = basePath.replaceAll("\\\\", "/");
		
		basePath = "file://" + basePath;
		return basePath;
	}

	/**
	 * Creates an instance of {@link Video} with dummy data.
	 * @return
	 */
	public static Video createTestVideoInstance() {
		MockHttpServletRequest request = new MockHttpServletRequest();
		request.addParameter("isTest", "true");
		request.addParameter("version", BbVersionServiceTest.SP10_FULL_SERVICE_PACK);
		request.addParameter("theme", BbVersionServiceTest.THEME_NAME);
		NBCAppContext appContext = NBCAppContextFactory.createAppContext(request);
		final Video video = new Video(appContext) {
			{
				this.setAirDate(Calendar.getInstance());
				this.setContentTypeImage("contentTypeImage");
				this.setCueCardCourseContent("cueCardCourseContent");
				this.setCueCardMashupContent("cueCardMashupContent");
				this.setCueCardWithJavascriptAndCss("cueCardWithJavascriptAndCss");
				this.setCueCardWithoutJavascriptAndCss("cueCardWithoutJavascriptAndCss");
				this.setCueCardWithoutJavascriptAndCss("description");
				this.setDuration("100");
				this.setId(new Id(Video.class, 99));
				this.setName("videoName");
				this.setShortType(VideoType.VIDEO.getShortType());
				this.setThumbnailDecodedUrl("thumbnailDecodedUrl");
				this.setVideoId("videoId");
			}
		};
		return video;
	}
}
