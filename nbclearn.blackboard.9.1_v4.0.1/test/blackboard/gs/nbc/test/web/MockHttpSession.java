/**
 * Ryan Hardy
 * Guilford Group
 * 
 * MockHttpSession.java
 * Created: Jun 13, 2012
 */
package blackboard.gs.nbc.test.web;

import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionContext;

import org.apache.commons.collections.iterators.IteratorEnumeration;

/**
 * QnD mock implementation of {@link HttpSession}. Will be removed once
 * Spring test framework is incorporated.
 * 
 * @author Ryan Hardy (rhardy@guilfordgroup.com)
 */
@SuppressWarnings("deprecation")
public class MockHttpSession implements HttpSession {
	private static int idCounter = 0;
	
	private Map<String, Object> attributes = new HashMap<String, Object>();
	private Date created;
	private String id;
		
	/**
	 * 
	 */
	public MockHttpSession() {
		super();
		this.created = new Date();
		this.id = "" + idCounter++;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object getAttribute(String attribute) {
		return this.attributes.get(attribute);
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Enumeration getAttributeNames() {
		return new IteratorEnumeration(this.attributes.keySet().iterator());
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void removeAttribute(String attribute) {
		this.attributes.remove(attribute);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setAttribute(String attribute, Object value) {
		this.attributes.put(attribute, value);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getCreationTime() {
		return this.created.getTime();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getId() {
		return this.id;
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public ServletContext getServletContext() {
		return MockServletContext.INSTANCE;
	}
	
	/*
	============================================================================ 
							NON-IMPLEMENTED METHODS
	============================================================================ 
	*/
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getLastAccessedTime() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getMaxInactiveInterval() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpSessionContext getSessionContext() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object getValue(String arg0) {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] getValueNames() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void invalidate() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isNew() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void putValue(String arg0, Object arg1) {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void removeValue(String arg0) {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setMaxInactiveInterval(int arg0) {
		throw new UnsupportedOperationException();
	}
}