/**
 * Ryan Hardy
 * Guilford Group
 * 
 * MockHttpServletRequest.java
 * Created: Jun 13, 2012
 */
package blackboard.gs.nbc.test.web;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.Principal;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletInputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections.iterators.IteratorEnumeration;

/**
 * QnD mock implementation of {@link HttpServletRequest}. Will be removed once
 * Spring test framework is incorporated.
 * 
 * @author Ryan Hardy (rhardy@guilfordgroup.com)
 */
public class MockHttpServletRequest implements HttpServletRequest {
	private Map<String, String[]> params = new HashMap<String, String[]>();
	private Map<String, Object> attributes = new HashMap<String, Object>();
	private HttpSession session;
	
	/**
	 * 
	 */
	public MockHttpServletRequest() {
		super();
	}
	
	/**
	 * Sets a param value for testing.
	 * @param param
	 * @param value
	 */
	public void addParam(String param, String value) {
		addParam(param, new String[] { value });
	}
	
	/**
	 * Sets param values for testing.
	 * 
	 * @param param
	 * @param values
	 */
	public void addParam(String param, String[] values) {
		this.params.put(param, values);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getParameter(String param) {
		String[] ary = params.get(param);
		return (ary == null || ary.length == 0) ? null : ary[0];
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Map getParameterMap() {
		return new HashMap<String, String[]>(this.params);
	}
	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Enumeration getParameterNames() {
		return new IteratorEnumeration(this.params.keySet().iterator());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] getParameterValues(String param) {
		return this.params.get(param);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object getAttribute(String attribute) {
		return this.attributes.get(attribute);
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Enumeration getAttributeNames() {
		return new IteratorEnumeration(this.attributes.keySet().iterator());
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void removeAttribute(String attribute) {
		this.attributes.remove(attribute);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setAttribute(String attribute, Object value) {
		this.attributes.put(attribute, value);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpSession getSession() {
		return getSession(true);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HttpSession getSession(boolean create) {
		if (this.session != null) {
			return this.session;
		}
		if (create) {
			this.session = new MockHttpSession();
		}
		return this.session;
	}

	/*
	============================================================================ 
						NON-IMPLEMENTED METHODS
	============================================================================ 
	*/
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getCharacterEncoding() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getContentLength() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getContentType() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ServletInputStream getInputStream() throws IOException {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getLocalAddr() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getLocalName() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getLocalPort() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Locale getLocale() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Enumeration getLocales() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getProtocol() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BufferedReader getReader() throws IOException {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getRealPath(String arg0) {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getRemoteAddr() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getRemoteHost() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getRemotePort() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RequestDispatcher getRequestDispatcher(String arg0) {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getScheme() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getServerName() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getServerPort() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isSecure() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setCharacterEncoding(String arg0) throws UnsupportedEncodingException {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getAuthType() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getContextPath() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Cookie[] getCookies() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getDateHeader(String arg0) {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getHeader(String arg0) {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Enumeration getHeaderNames() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Enumeration getHeaders(String arg0) {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getIntHeader(String arg0) {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getMethod() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getPathInfo() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getPathTranslated() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getQueryString() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getRemoteUser() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getRequestURI() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public StringBuffer getRequestURL() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getRequestedSessionId() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getServletPath() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Principal getUserPrincipal() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isRequestedSessionIdFromCookie() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isRequestedSessionIdFromURL() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isRequestedSessionIdFromUrl() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isRequestedSessionIdValid() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isUserInRole(String arg0) {
		throw new UnsupportedOperationException();
	}
}