/**
 * Ryan Hardy
 * Guilford Group
 * 
 * MockServletContext.java
 * Created: Jun 13, 2012
 */
package blackboard.gs.nbc.test.web;

import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.Servlet;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.apache.commons.collections.iterators.IteratorEnumeration;

/**
 * QnD mock implementation of {@link ServletContext}. Will be removed once
 * Spring test framework is incorporated.
 * 
 * @author Ryan Hardy (rhardy@guilfordgroup.com)
 */
public class MockServletContext implements ServletContext {
	static final ServletContext INSTANCE = new MockServletContext();
	
	private Map<String, Object> attributes = new HashMap<String, Object>();
	
	/**
	 * 
	 */
	public MockServletContext() {
		super();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object getAttribute(String name) {
		return this.attributes.get(name);
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Enumeration getAttributeNames() {
		return new IteratorEnumeration(this.attributes.keySet().iterator());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setAttribute(String name, Object object) {
		this.attributes.put(name, object);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void removeAttribute(String name) {
		this.attributes.remove(name);
	}
	
	/*
	============================================================================ 
						NON-IMPLEMENTED METHODS
	============================================================================ 
	*/

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ServletContext getContext(String uripath) {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getMajorVersion() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getMinorVersion() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getMimeType(String file) {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Set getResourcePaths(String path) {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public URL getResource(String path) throws MalformedURLException {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public InputStream getResourceAsStream(String path) {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RequestDispatcher getRequestDispatcher(String path) {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RequestDispatcher getNamedDispatcher(String name) {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Servlet getServlet(String name) throws ServletException {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Enumeration getServlets() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Enumeration getServletNames() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void log(String msg) {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void log(Exception exception, String msg) {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void log(String message, Throwable throwable) {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getRealPath(String path) {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getServerInfo() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getInitParameter(String name) {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Enumeration getInitParameterNames() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getServletContextName() {
		throw new UnsupportedOperationException();
	}
}