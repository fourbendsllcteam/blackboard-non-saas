<%@ page import="blackboard.gs.nbc.context.NBCAppContextFactory" %>
<%@ page import="blackboard.gs.nbc.context.NBCAppContext" %>

<% 
    NBCAppContext appContext = NBCAppContextFactory.createAppContext(request);
    String script = appContext.getCueCardConfiguration().renderResolverScript();
    String vi = appContext.getEnvironmentConfiguration().getVi();
    String courseId = appContext.getContextService().getCourseId();
%>
<html>
    <head>
        <script>
        (function() {
            var ns = window.parent;
            if (!ns) {
                return;
            }
            if (ns.NBCLoader) {
                return;
            }
            // set the flag for NBCFileLoader to use parent window
            window.NBCLoaderWindow = ns;
            
            window.preProcessNbcLoader = function(nbcLoader) {
                ns.NBCLoader = nbcLoader;
            };
            
        })();
        </script>
        <%= script %>
    </head>
</html>