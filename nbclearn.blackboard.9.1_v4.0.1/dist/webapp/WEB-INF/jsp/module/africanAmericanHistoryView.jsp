<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.net.URLDecoder"%>
<%@ page import="blackboard.gs.nbc.Constants"%>
<%@ page import="blackboard.gs.nbc.context.NBCAppContextFactory"%>
<%@ page import="blackboard.gs.nbc.context.NBCAppContext,javax.xml.parsers.DocumentBuilder,javax.xml.parsers.DocumentBuilderFactory,javax.xml.parsers.ParserConfigurationException,org.w3c.dom.Document,org.w3c.dom.NodeList,org.xml.sax.InputSource,org.xml.sax.SAXException"%>
<%@ page import="blackboard.gs.nbc.util.ModuleConfigService,java.io.BufferedReader,java.io.DataOutputStream,java.io.InputStreamReader,java.net.HttpURLConnection,java.net.URL,java.io.IOException,java.io.StringReader,org.w3c.dom.Node,org.w3c.dom.Element"%>

<% 
    NBCAppContext appContext = NBCAppContextFactory.createAppContext(request);    ModuleConfigService cfgSvc = appContext.getModuleConfigService();    String widgetUrl = cfgSvc.getAfricanAmericanHistoryUrl();
	widgetUrl = widgetUrl.substring(widgetUrl.indexOf("WID=")+4);
%>

<div id="NBCLearn_multiWidget">
<iframe style="width: 100%; height: 336px;" id="bbModuleAfrId" src="<%=appContext.getEnvironmentConfiguration().getModulePlayerUrl()%><%=widgetUrl%>" allowfullscreen="allowfullscreen" webkitallowfullscreen="webkitallowfullscreen" mozallowfullscreen="mozallowfullscreen"></iframe>
 	<!--[if IE]>
	<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="445" height="300" id="multiWidget">
		<param name="movie" value="<%=widgetUrl%>" />
		<param name="quality" value="high" />
		<param name="bgcolor" value="#000000" />
		<param name="wmode" value="opaque" />
		<param name="allowScriptAccess" value="always" />
		<param name="allowfullscreen" value="true" />
    </object>
<![endif]-->

	<!--[if !IE]><!-->

	<%-- <object type="application/x-shockwave-flash" data="<%=widgetUrl%>"
		width="445" height="300" id="multiWidget">
		<param name="movie" value="multiWidget.swf" />
		<param name="quality" value="high" />
		<param name="bgcolor" value="#000000" />
		<param name="wmode" value="direct" />
		<param name="allowScriptAccess" value="always" />
		<param name="allowfullscreen" value="true" />
	</object> --%>

	<!--<![endif]-->

</div>