<%@ page import="blackboard.gs.nbc.Constants, blackboard.platform.plugin.PlugInUtil" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div style="float: center;text-align: center;">
    <form name="form1" method="POST" target="_top"  action='<%=request.getAttribute(Constants.PARAM_LAUNCH_POST)%>' >
    
        <input type="hidden" name="return_url"      value='<%=request.getAttribute(Constants.PARAM_RETURN_URL)%>' >
        <input type="hidden" name="mode"            value='<%=request.getAttribute(Constants.PARAM_MODE)%>' >
        <input type="hidden" name="token"           value='<%=request.getAttribute(Constants.PARAM_TOKEN)%>' >
        <input type="hidden" name="firstName"       value='<%=request.getAttribute(Constants.PARAM_USER_FIRST_NAME)%>' >
        <input type="hidden" name="lastName"        value='<%=request.getAttribute(Constants.PARAM_USER_LAST_NAME)%>' >
        <input type="hidden" name="email"           value='<%=request.getAttribute(Constants.PARAM_USER_EMAIL)%>' >
        <input type="hidden" name="role"            value='<%=request.getAttribute(Constants.PARAM_USER_ROLE)%>' >
        <input type="hidden" name="user_id"            value='<%=request.getAttribute(Constants.PARAM_USER_NAME)%>' >
        <input type="hidden" name="context_title"      value='<%=request.getAttribute(Constants.PARAM_COURSE_NAME) %>'>
        
        <img alt="NBC Logo"                         src="<%=request.getAttribute(Constants.ATTRIBUTE_LOGO_PATH)%>" /> 
        Loading NBC Learn...    
    </form>
</div>
<script type="text/javascript">
    window.moveTo(0, 0);
    window.resizeTo(screen.availWidth, screen.availHeight);
    document.form1.submit();
</script>