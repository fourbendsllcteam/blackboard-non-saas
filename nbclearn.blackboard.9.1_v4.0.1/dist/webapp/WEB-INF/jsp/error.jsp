<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@ taglib uri="/bbNG" prefix="bbNG"%>
<%@ page isErrorPage="true" %>
<bbNG:genericPage authentication="N"> 
<%
  Throwable error = (Throwable) request.getAttribute("error");  
  if( null == error ) error = exception;
  pageContext.setAttribute("error", error);
%>
 
  <bbNG:error exception="${error}"/>  
</bbNG:genericPage>
