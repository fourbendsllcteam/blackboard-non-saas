<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@ page errorPage="/error.action"%>
<%@ taglib uri="/bbNG" prefix="bbNG"%>
<bbNG:genericPage ctxId="ctx" bodyClass="normalBackground">
	<%@include file="playlist_common.jspf"%>
	<%
		
	    String courseId = request.getParameter(Constants.PARAM_COURSE_ID);
    	String action = request.getParameter(Constants.PARAM_ACTION);
    	String contentId = request.getParameter(Constants.PARAM_CONTENT_ID);
    	String mode = request.getParameter(Constants.PARAM_MODE);
    	String cancelUrl = PlugInUtil.getUriStem(Constants.VENDOR_ID,Constants.PLUGIN_ID) + "execute/out_req_con.action?action="+action+"&mode="+mode+"&course_id="+courseId+"&content_id="+contentId;
    	
    	if (null==courseId) courseId ="";
    	if (null==action) action ="";
    	if (null==contentId) contentId ="";
    	if (null==mode) mode ="";
    	
    	String sortUrl = PlugInUtil.getUriStem(Constants.VENDOR_ID,Constants.PLUGIN_ID) + "req_con.action?location=blackboard";
 	    String submitUrl = PlugInUtil.getUriStem(Constants.VENDOR_ID,Constants.PLUGIN_ID) + "req_con.action?location=blackboard";
	  
	%>
		<c:set var="cancelUrl" value="<%=cancelUrl%>" />
		<c:set var="submitUrl" value="<%=submitUrl%>" />
		<c:set var="sortUrl" value="<%=sortUrl%>" />
		<c:set var="courseId" value="<%=courseId%>" />
		<c:set var="contentId" value="<%=contentId%>" />
		<c:set var="mode" value="<%=mode%>" />
		<c:set var="action" value="<%=action%>" />
		

	<stripes:form name="playlistEmbedForm" action="${submitUrl}" onsubmit="return validateSelected(this);" target="_top">
		<input type="hidden" name="type" value="embed" />
		<input type="hidden" name="mode" value="${mode}" />
		<input type="hidden" name="action" value="${action}" />
		<input type="hidden" name="content_id" value="${contentId}" />
		<input type="hidden" name="course_id" value="${courseId}" />

		<bbNG:dataCollection>
			<bbNG:step title="${strEmbedInstructions}" instructions="">
				<bbNG:dataElement label="">

					<bbNG:inventoryList collection="${actionBean.playlist}"
						objectVar="item"
						className="blackboard.gs.nbc.playlist.data.playlistitem.PlaylistItem"
						url="${sortUrl}">
						<bbNG:listElement label="&nbsp;" name="radio">
						<input type="radio" name="item_key" value="0" style="display:none" >
						<input type="radio" name="item_key" value="${item.id.key}">
						</bbNG:listElement>
						<%@include file="playlist_listitems.jspf"%>
					</bbNG:inventoryList>

					<bbNG:stepSubmit cancelUrl="${cancelUrl}" showCancelButton="true"/>

				</bbNG:dataElement>
			</bbNG:step>
		</bbNG:dataCollection>

	</stripes:form>
</bbNG:genericPage>