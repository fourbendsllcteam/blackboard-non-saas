
CREATE TABLE dbo.bbgs_video
(
	pk1                INT  IDENTITY(1,1) NOT NULL, 
    id                 NVARCHAR(128) NOT NULL, 
	name               NVARCHAR(510) NULL, 
	description        NVARCHAR(2000) NULL, 
	duration           VARCHAR(50) NULL, 
	air_date           DATETIME NULL,
    url                VARCHAR(4000) NULL,
    type               CHAR(1) NULL, 
    CONSTRAINT bbgs_video_pk PRIMARY KEY(PK1),
    CONSTRAINT bbgs_video_unique1 UNIQUE(ID)
)

GO

IF EXISTS(SELECT * FROM sys.objects WHERE type = 'P' AND name = 'bbgs_video_cr')
     DROP PROCEDURE dbo.bbgs_video_cr
GO
 
CREATE PROCEDURE dbo.bbgs_video_cr (
    @idIn                                     NVARCHAR(128)                    = NULL ,
	@nameIn                                   NVARCHAR(510)                    = NULL ,
	@descriptionIn                            NVARCHAR(4000)                   = NULL ,
	@durationIn                               VARCHAR(50)                      = NULL ,
	@airDateIn                                DATETIME                         = NULL ,
	@urlIn                                    NVARCHAR(4000)                   = NULL ,
	@typeIn                                     CHAR(1)                        = NULL ,
	@pkOut int = -1 output 
 ) 
 AS
	BEGIN
		INSERT INTO  dbo.bbgs_video (  
				id , 
				name , 
				description , 
				duration , 
				air_date , 
				url,
				type )  
		 VALUES ( 
				@idIn , 
				@nameIn , 
				@descriptionIn , 
				@durationIn , 
				@airDateIn , 
				@urlIn,
				@typeIn );
				 SET @pkOut = @@IDENTITY;
		IF @@error <> 0 SET @pkOut = -1;	
	END;
GO


CREATE TABLE dbo.bbgs_playlist_item
(
    pk1                INT  IDENTITY(1,1) NOT NULL, 
    users_pk1          INT NOT NULL, 
	video_pk1          INT NOT NULL,
	dtmodified         DATETIME NOT NULL DEFAULT (getdate()),
    CONSTRAINT bbgs_playlist_item_pk PRIMARY KEY(pk1), 
    CONSTRAINT bbgs_playlist_item_fk1 FOREIGN KEY(users_pk1) REFERENCES users(pk1) ON DELETE CASCADE,
    CONSTRAINT bbgs_playlist_item_fk2 FOREIGN KEY(video_pk1) REFERENCES bbgs_video(pk1) ON DELETE CASCADE
)
GO

IF EXISTS(SELECT * FROM sys.objects WHERE type = 'P' AND name = 'bbgs_playlist_item_cr')
     DROP PROCEDURE dbo.bbgs_playlist_item_cr
GO

CREATE PROCEDURE dbo.bbgs_playlist_item_cr (
    @usersPk1In		  INT = NULL,
    @videoPk1In	      INT = NULL,
    @dtmodifiedIn     DATETIME = NULL,
	@pkOut int = -1 output 
)
AS
BEGIN

  INSERT INTO dbo.bbgs_playlist_item (users_pk1, video_pk1, dtmodified)
  VALUES (@usersPk1In, @videoPk1In, @dtmodifiedIn);

  SET @pkOut = @@IDENTITY;
  IF @@error <> 0 SET @pkOut = -1;	
  END;
GO
  
