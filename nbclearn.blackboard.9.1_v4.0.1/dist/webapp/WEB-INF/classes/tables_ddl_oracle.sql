DECLARE
 v_CheckObjectExists NUMBER;
 v_sql LONG;
 
BEGIN
	SELECT COUNT(*)
	INTO v_CheckObjectExists
	FROM User_Tables
 	WHERE Table_Name = UPPER('BBGS_VIDEO');
	
 	IF v_CheckObjectExists = 0 THEN
 		v_sql:='CREATE TABLE BBGS_VIDEO
				(
					PK1                NUMBER(22,0) NOT NULL, 
    				ID                 NVARCHAR2(128) NOT NULL, 
					NAME               NVARCHAR2(510) NULL, 
					DESCRIPTION        NVARCHAR2(2000) NULL, 
					DURATION           VARCHAR2(50) NULL, 
					AIR_DATE           DATE NULL,
    				URL                VARCHAR2(4000) NULL,
    				TYPE               CHAR(1) NULL,
    				CONSTRAINT BBGS_VIDEO_PK PRIMARY KEY(PK1), 
    				CONSTRAINT BBGS_VIDEO_UNIQUE1 UNIQUE(ID)
				)';
 		EXECUTE IMMEDIATE v_sql;
 		
 		v_sql:='CREATE SEQUENCE BBGS_VIDEO_SEQ INCREMENT BY 1 START WITH 1 NOCYCLE';
 		EXECUTE IMMEDIATE v_sql;
 		
 	END IF;
END;
/

CREATE OR REPLACE PROCEDURE BBGS_VIDEO_CR (
            idIn		     NVARCHAR2 := NULL,
            nameIn           NVARCHAR2 := NULL,
            descriptionIn    NVARCHAR2 := NULL,
            durationIn       VARCHAR2 := NULL,
            airDateIn        DATE := NULL,
            urlIn            VARCHAR2 := NULL,
            typeIn           CHAR := NULL,
            pkOut               OUT      NUMBER

)
IS
BEGIN

  SELECT BBGS_VIDEO_SEQ.nextval INTO pkOut FROM DUAL;

  INSERT INTO BBGS_VIDEO
    (pk1, id, name, description, duration, air_date, url, type)
    VALUES (pkOut, idIn, nameIn, descriptionIn, durationIn, airDateIn, urlIn, typeIn);

  EXCEPTION
    WHEN OTHERS THEN
      pkOut := -1;
      RAISE;

END;
/

DECLARE
 v_CheckObjectExists NUMBER;
 v_sql LONG;

BEGIN
	SELECT COUNT(*)
	INTO v_CheckObjectExists
	FROM User_Tables
 	WHERE Table_Name = UPPER('BBGS_PLAYLIST_ITEM');
	
 	IF v_CheckObjectExists = 0 THEN
 		v_sql:='CREATE TABLE BBGS_PLAYLIST_ITEM
				(
					PK1                NUMBER(22,0) NOT NULL, 
    				USERS_PK1          NUMBER(22,0) NOT NULL, 
					VIDEO_PK1          NUMBER(22,0) NOT NULL,
					DTMODIFIED         DATE DEFAULT SYSDATE NOT NULL,
    				CONSTRAINT BBGS_PLAYLIST_ITEM_PK PRIMARY KEY(PK1), 
    				CONSTRAINT BBGS_PLAYLIST_ITEM_FK1 FOREIGN KEY(USERS_PK1) REFERENCES USERS(PK1) ON DELETE CASCADE,
    				CONSTRAINT BBGS_PLAYLIST_ITEM_FK2 FOREIGN KEY(VIDEO_PK1) REFERENCES BBGS_VIDEO(PK1) ON DELETE CASCADE
				)';
		EXECUTE IMMEDIATE v_sql;
		
		v_sql:='CREATE SEQUENCE BBGS_PLAYLIST_ITEM_SEQ INCREMENT BY 1 START WITH 1 NOCYCLE';
 		EXECUTE IMMEDIATE v_sql;

 	END IF;
END;
/

CREATE OR REPLACE PROCEDURE BBGS_PLAYLIST_ITEM_CR (
    usersPk1In		  NUMBER := NULL,
    videoPk1In	      NUMBER := NULL,
    dtmodifiedIn      DATE := NULL,
	pkOut			  OUT	NUMBER
)
IS
BEGIN

  SELECT BBGS_PLAYLIST_ITEM_SEQ.nextval INTO pkOut FROM DUAL;

  INSERT INTO BBGS_PLAYLIST_ITEM (pk1, users_pk1, video_pk1, dtmodified)
    VALUES (pkOut, usersPk1In, videoPk1In, dtmodifiedIn);

  EXCEPTION
    WHEN OTHERS THEN
      pkOut := -1;
      RAISE;
END;
/

DECLARE
 v_CheckObjectExists NUMBER;
 v_sql LONG;

BEGIN
	SELECT COUNT(*)
	INTO v_CheckObjectExists
	FROM all_indexes
	WHERE index_name = UPPER('BBGS_PLAYLIST_ITEM_IF1');

 	IF v_CheckObjectExists = 0 THEN
 		v_sql:='CREATE INDEX BBGS_PLAYLIST_ITEM_IF1 ON BBGS_PLAYLIST_ITEM(USERS_PK1)';
 		EXECUTE IMMEDIATE v_sql;
 	END IF;
END;
/

DECLARE
 v_CheckObjectExists NUMBER;
 v_sql LONG;

BEGIN
	SELECT COUNT(*)
	INTO v_CheckObjectExists
	FROM all_indexes
	WHERE index_name = UPPER('BBGS_PLAYLIST_ITEM_IF2');

 	IF v_CheckObjectExists = 0 THEN
 		v_sql:='CREATE INDEX BBGS_PLAYLIST_ITEM_IF2 ON BBGS_PLAYLIST_ITEM(VIDEO_PK1)';
 		EXECUTE IMMEDIATE v_sql;
 	END IF;
END;
/