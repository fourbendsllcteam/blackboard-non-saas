/**
 * Simple object used to do client-side file detection and loading for NBC Learn
 * Building Block.
 * 
 * This script MUST be injected, rather than included via script tag, to allow
 * for resolving of context variables.
 * 
 * @author Ryan Hardy (rhardy@GuilfordGroup.com)
 */
var NBCLoader = {
	/**
	 * Reference to the DOM Window to use throughout calls; may be set if needed
	 * (i.e. being loaded by an iframe into parent).
	 */
	_window: window["NBCLoaderWindow"] || window,
	
	/**
	 * Obtains DOM Document object from NBCLoader._window.
	 */
	_getDocument: function() {
		return NBCLoader._window.document;
	},
		
	/**
	 * If no args are provided, returns reference to the jQuery object (for
	 * making 'static' calls), and executes jQuery operation if args are provided,
	 * taking into account the NBCLoader._window value.
	 * 
	 * @returns
	 */
	$: function() {
		if (arguments.length == 0) {
			return NBCLoader._window.jQuery;
		}
		return NBCLoader._window.jQuery.apply(NBCLoader._window, Array.prototype.slice.call(arguments, 0));
	},
	
	/**
	 * Checks for existence of Console object and prints message to it
	 * if found.
	 * 
	 * @param msg
	 */
	_log: function(msg) {
		// do safe check for presence of console (needed for older IE)
		var _console = window["console"];
		if (_console != undefined) {
			_console.log(msg);
		}
	},
	
	// =========================================================================
	
	/**
	 * Is called to create and insert into the DOM an element of type specified
	 * by name. Will not insert element if one with the same id is found in
	 * document.
	 * 
	 * @param name     type of element to create
	 * @param atts     name/value pairs of attribute values to set on element
	 */
	createElement: function(name, atts) {
		var element = NBCLoader._getDocument().createElement(name);
		NBCLoader.$().each(atts, function(nm, val) {
			element[nm] = val;
		});
		var id = NBCLoader.$(element).attr("id");
		var isPresent = NBCLoader.$("#" + id).length != 0;
		if (!isPresent) {
			NBCLoader._getDocument().body.appendChild(element);			
		}
	},
	
    /**
     * Is called to parse through 'script' and 'link' tags until one
     * if found that has the correct path to the NBC Learn webapp. Returns
     * an empty string if none is found.
     * 
     * @param doc
     * @returns
     */
    findNbcPath: function (doc) {
        /**
         * Is called to pull the context-relative path out of the string provided.
         * Returns an empty string if path is null.
         * 
         * @param path
         * @returns
         */
        function extractPath(path) {
            if (!path) {
                return "";
            }
            var i1 = path.indexOf("/webapps");
            var i2 = path.indexOf("bbgs-nbc-content-integration");
            var i3 = path.indexOf("/", i2);
            var subPath = path.substring(i1, i3 + 1);
            return subPath;
        }
        // ensure that we are starting at the top-most window
        if (!doc) {
           var win = window;
           do {
               win = win.parent;
           } while (win.parent != win);
           doc = window.document;
        } 
        var path = null;
        var scripts = doc.body.getElementsByTagName("script");
        for (var i = 0; i < scripts.length; i++) {
           var script = scripts[i];
           var src = script.src;
           if (src && src.indexOf("bbgs-nbc-content-integration") > 0) {
               path = extractPath(src);
               break;
           }
        }
        var links = doc.body.getElementsByTagName("link");
        for (var i = 0; i < links.length; i++) {
            var link = links[i];
            var href = link.href;
            if (href && href.indexOf("bbgs-nbc-content-integration") > 0) {
               path = extractPath(href);
               break;
            }
        }
        var iframes = doc.body.getElementsByTagName("iframe");
        for (var i = 0; i < iframes.length; i++) {
           var iframe = iframes[i];
           var toChk = findNbcPath(iframe.contentDocument);
           if (toChk) {
               path = toChk;
               break;
           }
        }
        // ensure it is not null
        path = (!path) ? "" :path;
        return path;
    },
    /**
     * Checks for presence of jQuery, and loads it if it is not found.
     * 
     * @param {Function} callback        (optional) a function to call when jQuery has been loaded
     */
    checkForJQuery: function(callback) {
        NBCLoader.doLoad([{
            type : "script",
            path : (function() {
                var path = "/webapps/bbgs-nbc-content-integration-@X@nbc.vi@X@/js/jquery-1.10.2.js";
                // check to see if context variable resolving has not occurred
                if (path.indexOf("@X@") > 0) {
                    // no resolving, find NBC base path...
                    path = NBCLoader.findNbcPath();
                    path += "js/jquery-1.10.2.js";
                } 
                return path;
            })(),
            isLoaded : function(fileObj, node) {
                return NBCLoader.isJQueryLoaded();
            },
            onload: function() {
                // ensure that jQuery does not clobber dollar sign if in use
                NBCLoader.$().noConflict();
                if (callback) {
                    callback();
                }
            }
        }], null, true);
    },
    /**
     * Returns true if the jQuery object is found.
     * 
     * @returns {Boolean}
     */
    isJQueryLoaded: function() {
        // simply check for presence of jQuery object
        try {
            if (NBCLoader._window.jQuery && NBCLoader._window.jQuery.expando) {
                return true;
            }
        } catch (e) {
            // IE may sometimes puke on this call...will just
            // return false
        }
        return false;        
    },
    /**
     * Checks for presence of Prototype, and loads it if it is not found.
     * 
     * @param {Function} callback        (optional) a function to call when Prototype has been loaded
     */
    checkForPrototype: function(callback) {
        NBCLoader.doLoad([{
            type : "script",
            path :  (function() {
                var path = "/webapps/bbgs-nbc-content-integration-@X@nbc.vi@X@/js/prototype.js";
                // check to see if context variable resolving has not occurred
                if (path.indexOf("@X@") > 0) {
                    // no resolving, find NBC base path...
                    path = NBCLoader.findNbcPath();
                    path += "js/prototype.js";
                } 
                return path;
            })(),
            isLoaded : function(fileObj, node) {
                return NBCLoader.isPrototypeLoaded();
            },
            onload: function() {
                if (callback) {
                    callback();
                }
            }        
        }], null, true);
    },
    /**
     * Returns true if the Prototype object is found.
     * 
     * @returns {Boolean}
     */
    isPrototypeLoaded: function() {
        // simply check for presence of Prototype object
        try {
            if (NBCLoader._window.Prototype && NBCLoader._window.Prototype.Version) {
                return true;
            }
        } catch (e) {
            // IE may sometimes puke on this call...will just
            // return false
        }
        return false;        
    },
    /**
     * Examines the file object, and returns true if a DOM element is present in
     * the HEAD tag of the given type (either 'script' or 'link') that shares
     * the given path ('src' property for SCRIPT tags or 'href' property for
     * LINK tags).
     * 
     * @param {Object} file         object with properties 'path' and 'type'
     * @param {Function} fn         (optional) function to call on each node in head to check
     *                                 for presence of file (has signature: isLoaded(fileObj, node) )
     * @returns {Boolean}
     */
    isLoaded: function(file, fn) {
        fn = fn || function(fileObj, node) {
            var path = file.path;
            var type = file.type;
            var nodeName = (node.nodeName != null) ? node.nodeName.toLowerCase() : "";
            
            var toCheck;
            if (nodeName == "script" && type == "script") {
                toCheck = node.src;
            } else  if (nodeName == "link" && type == "link") {
                toCheck = node.href;
            } else {
                return false;
            }
            var loaded = toCheck.indexOf(path) >= 0;
            return loaded;
        };
        
        var head = NBCLoader._getDocument().getElementsByTagName('head')[0];
        for (var i = 0; i < head.childNodes.length; i++) {
            var node = head.childNodes[i];
            var isLoaded = fn(file, node);
            if (isLoaded) {
                return true;
            }            
        }
        return false;
    },
    /**
     * Takes the array of file objects (objects with properties 'type' and 
     * 'path' and an optional 'onload' property that will be a function
     * attached to the onload event - used only if type is script) and creates 
     * and inserts DOM elements for each based on type 
     * (either 'script' or 'link') if they are not detected to be already 
     * loaded (determined in call to NBCLoader.isLoaded(file)).
     * 
     * If files are loaded, the onLoad function will be called (or scheduled
     * to be called via Event.observe()).
     * 
     * @param {Array} files
     * @param {Function} onLoad
     * @param {Boolean} ignoreSupportFiles	(optional) if true, will NOT do check
     * 										for jQuery/Prototype first (used for 
     * 										loading jQuery and Prototype)
     */
    doLoad: function(files, onLoad, ignoreSupportFiles) {
    	ignoreSupportFiles = !!ignoreSupportFiles;
    	
    	if (!ignoreSupportFiles) {
        	// ONLY do loading once support JS has been loaded (jQuery and Prototype files)
            if (!NBCLoader.isJQueryLoaded() || !NBCLoader.isPrototypeLoaded()) {
                // wait until jQuery/Prototype have been loaded
                window.setTimeout(function() { NBCLoader.doLoad(files, onLoad); }, 10);
                return;
            }    		
    	}
        
        files = (!files) ? [] : files;
        var head = NBCLoader._getDocument().getElementsByTagName('head')[0];
                
        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            // file.isLoaded: fn to send to isLoaded to check for presence of file (file object may provide custom)
            if (NBCLoader.isLoaded(file, file.isLoaded)) {
                // ensure that onload callback is triggered if provided
                if (file.onload) {
                    file.onload();
                }
                continue;
            }
            var path = file.path;
            var type = file.type;
            if (type == "script") {
                var script = NBCLoader._getDocument().createElement("script");
                script.type = "text/javascript";
                script.src = path;
                if (file.onload) {
                    // borrowed from jQuery script loading:
                    
                    // Handle Script loading
                    var done = false;
    
                    // Attach handlers for all browsers
                    script.onload = script.onreadystatechange = function() {
                        if ( !done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete") ) {
                            done = true;
                            file.onload();
                            
                            // Handle memory leak in IE
                            script.onload = script.onreadystatechange = null;
                            if ( head && script.parentNode ) {
                                head.removeChild( script );
                            }
                        }
                    };
                }
                head.appendChild(script);
            }
            else if (type == "link") {
                var link = NBCLoader._getDocument().createElement("link");
                link.type = "text/css";
                link.rel = "stylesheet";
                link.href = path;
                head.appendChild(link);
            }
        }
        
        if (onLoad && NBCLoader.isJQueryLoaded()) {
            // will execute when DOM is ready - will execute immediately if DOM is already loaded
            NBCLoader.$(onLoad);
        }    
    },
    /**
     * Adds click handlers to all NBC anchor tags, used to open video on click.
     */
    iframeTemplate:'',
    addClickHandlers: function() {
    	NBCLoader.$(function() {
        	NBCLoader.$(".nbc-click", NBCLoader._getDocument()).each(function () {
        		var curCardId = NBCLoader.$(this).attr("id");
        		var hasErr = false;
        		var thisItem = NBCLoader.$(this);
        		
        		//Add Play icon for already embedded videos
        		if(NBCLoader.$(this).find('img').length==1){
        			NBCLoader.$(this).find('img').before('<img src="https://static.nbclearn.com/files/highered/site/download/blackboard/install/play-icon.png" height="32" width="32" style="margin: 34px 0px 0px 44px; position: absolute;cursor: pointer;">');
        		}
        		
        		NBCLoader.$(this).click(function onclick(evt) {
        			 if (evt.handled !== true) { 
        			        evt.handled = true; 
        			try {
        				//NBCLoader._log("iframeTemplate..."+NBCLoader.iframeTemplate);
    					if(NBCLoader.$(this).closest('.vtbegenerated').find("#iframeSpanId"+curCardId).length == 0) {
    						
    						var embededType="";
    						var exisIframe="";
    						var iframeExist=false;
    						var parentType = NBCLoader.$(this).parent()[0].tagName.toLowerCase();
    						if(NBCLoader.$('[id^=iframeSpanId]')[0]!==undefined){
    							//NBCLoader._log("inside exsuting iframe");
    							exisIframe = NBCLoader.$('[id^=iframeSpanId]').html();
    							NBCLoader.iframeTemplate = exisIframe.substring(0,exisIframe.lastIndexOf('/')+1);
    							localStorage.setItem("locIframeTemp", NBCLoader.iframeTemplate);
    						}else if(NBCLoader.iframeTemplate=='' && localStorage.getItem("locIframeTemp")!==null){
    						//	NBCLoader._log("localStorage.getItem"+localStorage.getItem("locIframeTemp"));
    							NBCLoader.iframeTemplate = localStorage.getItem("locIframeTemp");
    						}
    						else if(NBCLoader.iframeTemplate==''){
    							new NBCLoader._window.Ajax.Request('/webapps/bbgs-nbc-content-integration-@X@nbc.vi@X@/execute/iframeFormation.action?', {
        			                method: 'get',
        			                onSuccess: function(transport) {
        			                    var response = transport.responseText || "no response text";
        			                    	NBCLoader.iframeTemplate = response;
        			                    	localStorage.setItem("locIframeTemp", response);
        			                    	var cn = NBCLoader._getDocument().getElementById("courseMenu_link").text;
        			                    	var encodedCN = encodeURI(cn); 
        			                    	var isource = response+'/'+curCardId+'?cn='+encodedCN;
        			                    	NBCLoader._log("ISource---> "+isource);
        			                    	if(parentType=='div'){
        			                    		thisItem.parents().eq(2).prev().find('#iframeId'+curCardId).attr('src',isource);
            			                    	thisItem.parents().eq(2).prev().before('<h3 id="loadingContent" style="color: #000000;float:left">Loading...</h3>');
            			                    	thisItem.parents().eq(2).prev().css('display','block');
            			                    	thisItem.parents().eq(2).prev().find('#iframeId'+curCardId).load(function(){
            			                    		NBCLoader.$(this).parents().eq(1).prev().remove();
            			                    		NBCLoader.$(this).parents().eq(1).prepend('<img src="https://static.nbclearn.com/files/highered/site/download/blackboard/install/lms-player-close.png" class="view-thumb" title="close" onclick="NBCLoader.viewFlashThumb(event)" style="margin-left: -10px;margin-top: 5px;cursor: pointer;">');
            		    						});
        			                    	}else{
        			                    		thisItem.closest('.vtbegenerated').find('#iframeId'+curCardId).attr('src',isource);
            			                    	thisItem.closest('.vtbegenerated').find('#contentIframeId'+curCardId).before('<h3 id="loadingContent" style="color: #000000;float:left">Loading...</h3>');
            			                    	thisItem.closest('.vtbegenerated').find('#contentIframeId'+curCardId).css('display','block');
            			                    	thisItem.closest('.vtbegenerated').find('#iframeId'+curCardId).load(function(){
            			                    		NBCLoader.$(this).parents().eq(1).prev().remove();
            		    							NBCLoader.$(this).parents().eq(1).prepend('<img src="https://static.nbclearn.com/files/highered/site/download/blackboard/install/lms-player-close.png" class="view-thumb" title="close" onclick="NBCLoader.viewFlashTableThumb(event)" style="margin-left: -10px;margin-top: 5px;cursor: pointer;">');
            		    							NBCLoader.$(event.target).off('load');
            		    						});
        			                    	}
        			                  },
        			                  onFailure: function() { NBCLoader._log('Something went wrong...'); }
        						 });
    						}
    						var immedParent = NBCLoader.$(this).parent()[0];
    						if(immedParent.tagName.toLowerCase() =='div'){
    							embededType='item';
    						//	NBCLoader.$(NBCLoader.$(this)).parents().eq(2).find('div').slice(5,6).remove();
    							
    							/*NBCLoader.$(this).parents().eq(3).find('div').each(function(){
 	 							   NBCLoader.$(this).css('display','none')
    							});*/
    							
    							NBCLoader.$(this).find('img').css('display','none');
    							NBCLoader.$(this).parents().eq(2).children().eq(1).children().eq(0).find('img').css('display','none');
    							NBCLoader.$(this).parents().eq(2).children().eq(1).children().eq(1).children().eq(0).css('display','none');
    							NBCLoader.$(this).parents().eq(2).children().eq(1).children().eq(1).children().eq(2).css('display','none');
    							NBCLoader.$(this).parents().eq(2).children().eq(1).children().eq(1).children().eq(4).css('display','none');
    							NBCLoader.$(this).parents().eq(1).css('border-style','none');
    							
    							if(NBCLoader.$(this).parents().eq(2).prev().attr('id')!=='contentIframeId'+curCardId){
    							
    							NBCLoader.$(this).parents().eq(2).before('<div id="contentIframeId'+curCardId+'" style="word-wrap: break-word;height: 336px;width: 100%;display:none">'+   
        								'<div style="float: left;width: 450px;height: 100%;background: #d3dae0;border-radius: 4px;margin-top: 10px;">'+
        								'<iframe id="iframeId'+curCardId+'" style="width: 100%; height: 315px;" scrolling="no" allowfullscreen=""></iframe>'+
        							'</div>'+ 
        							'</div>');
    							}
    							
    						}else{
    							embededType='default';
    							NBCLoader.$(this).parents().eq(8).find('table').css('display','none');
    							
    							if(NBCLoader.$(this).parents().eq(8).next().attr('id')!=='contentIframeId'+curCardId){
    							NBCLoader.$(this).closest('.vtbegenerated').append('<div id="contentIframeId'+curCardId+'" style="word-wrap: break-word;height: 336px;width: 100%;display:none">'+   
        								'<div style="float: left;width: 450px;height: 100%;background: #d3dae0;border-radius: 4px;margin-top: 10px;">'+
        								'<iframe id="iframeId'+curCardId+'" style="width: 100%; height: 315px;" scrolling="no" allowfullscreen=""></iframe>'+
        							'</div>'+ 
        							'</div>');
    							}
    						}
    						if(NBCLoader.iframeTemplate!='' && embededType=='default'){
    							//NBCLoader._log("Flash Inside Default-->"+curCardId);
    							var cn = NBCLoader._getDocument().getElementById("courseMenu_link").text;
		                    	var encodedCN = encodeURI(cn); 
    							var isource = NBCLoader.iframeTemplate+'/'+curCardId+'?cn='+encodedCN;
    							NBCLoader._log("ISource---> "+isource);
    							if(NBCLoader.$(this).closest('.vtbegenerated').find('#iframeId'+curCardId).attr('src')!==undefined){
        							//NBCLoader._log("Iframe default src available");
            						NBCLoader.$(this).closest('.vtbegenerated').find('#contentIframeId'+curCardId).css('display','block');
            						NBCLoader.$(this).closest('.vtbegenerated').find('#contentIframeId'+curCardId).prepend('<img src="https://static.nbclearn.com/files/highered/site/download/blackboard/install/lms-player-close.png" class="view-thumb" title="close" onclick="NBCLoader.viewFlashTableThumb(event)" style="margin-left: -10px;margin-top: 5px;cursor: pointer;">');
        						}else{
    							NBCLoader.$(this).closest('.vtbegenerated').find('#iframeId'+curCardId).attr('src',isource);
	    						NBCLoader.$(this).closest('.vtbegenerated').find('#contentIframeId'+curCardId).css('display','block');
	    						NBCLoader.$(this).closest('.vtbegenerated').find('#contentIframeId'+curCardId).before('<h3 id="loadingContent" style="color: #000000;float:left">Loading...</h3>');
	    						NBCLoader.$(this).closest('.vtbegenerated').find('#iframeId'+curCardId).load(function(){
	    							NBCLoader.$(this).parents().eq(1).prev().remove();
	    							NBCLoader.$(this).parents().eq(1).prepend('<img src="https://static.nbclearn.com/files/highered/site/download/blackboard/install/lms-player-close.png" class="view-thumb" title="close" onclick="NBCLoader.viewFlashTableThumb(event)" style="margin-left: -10px;margin-top: 5px;cursor: pointer;">');
	    							NBCLoader.$(event.target).off('load');
	    						});
        						}
	    						
    						}else if(NBCLoader.iframeTemplate!='' && embededType=='item'){
    							//NBCLoader._log("Flash Inside Item-->"+curCardId);
    							var cn = NBCLoader._getDocument().getElementById("courseMenu_link").text;
		                    	var encodedCN = encodeURI(cn); 
    							var isource = NBCLoader.iframeTemplate+'/'+curCardId+'?cn='+encodedCN;
    							NBCLoader._log("ISource---> "+isource);
    							if(NBCLoader.$(this).parents().eq(2).prev().find('#iframeId'+curCardId).attr('src')!==undefined){
    							//	NBCLoader._log("Flash iframe source available");
        							NBCLoader.$(this).parents().eq(2).prev().css('display','block');
        							NBCLoader.$(this).parents().eq(2).prev().prepend('<img src="https://static.nbclearn.com/files/highered/site/download/blackboard/install/lms-player-close.png" class="view-thumb" title="close" onclick="NBCLoader.viewFlashThumb(event)" style="margin-left: -10px;margin-top: 5px;cursor: pointer;">');
    							}else{
    								NBCLoader.$(this).parents().eq(2).prev().find('#iframeId'+curCardId).attr('src',isource);
        							NBCLoader.$(this).parents().eq(2).prev().css('display','block');
        							NBCLoader.$(this).parents().eq(2).prev().before('<h3 id="loadingContent" style="color: #000000;float:left">Loading...</h3>');
        							NBCLoader.$(this).parents().eq(2).prev().find('#iframeId'+curCardId).load(function(){
    	    							NBCLoader.$(this).parents().eq(1).prev().remove();
    	    							NBCLoader.$(this).parents().eq(1).prepend('<img src="https://static.nbclearn.com/files/highered/site/download/blackboard/install/lms-player-close.png" class="view-thumb" title="close" onclick="NBCLoader.viewFlashThumb(event)" style="margin-left: -10px;margin-top: 5px;cursor: pointer;">');
    	    							NBCLoader.$(event.target).off('load');
    	    						});
    							}
    						}
    					}else{
    						var cn = NBCLoader._getDocument().getElementById("courseMenu_link").text;
	                    	var encodedCN = encodeURI(cn); 
    						// For Items with same video twice
    						if(NBCLoader.$(this).closest('.vtbegenerated').find("div[id='contentIframeId"+curCardId+"']").length>1){
    							//NBCLoader._log("Html Inside same-->"+curCardId);
    							if(NBCLoader.$(this).parents().eq(2).prev().find('#iframeId'+curCardId).attr('src')!==undefined){
    							//	NBCLoader._log("Iframe src available");
        							NBCLoader.$(this).parents().eq(2).css('display','none');
        							NBCLoader.$(this).parents().eq(2).prev().css('display','block');
        							NBCLoader.$(this).parents().eq(2).prev().prepend('<img src="https://static.nbclearn.com/files/highered/site/download/blackboard/install/lms-player-close.png" class="view-thumb" title="close" onclick="NBCLoader.viewThumb(event)" style="margin-left: -10px;margin-top: 5px;cursor: pointer;">');
    							}else{
    								NBCLoader.$(this).parents().eq(2).prev().find('#iframeId'+curCardId).attr('src',NBCLoader.$(this).closest('.vtbegenerated').find('#iframeSpanId'+curCardId).html()+'?cn='+encodedCN);
        							NBCLoader.$(this).parents().eq(2).css('display','none');
        							NBCLoader.$(this).parents().eq(2).prev().css('display','block');
        							NBCLoader.$(this).parents().eq(2).prev().before('<h3 id="loadingContent" style="color: #000000;float:left">Loading...</h3>');
        							NBCLoader.$(this).parents().eq(2).prev().find('#iframeId'+curCardId).load(function(){
        							//	NBCLoader._log("Html Inside same Load");
        								NBCLoader.$(this).parents().eq(1).prev().remove();
        								NBCLoader.$(this).parents().eq(1).prepend('<img src="https://static.nbclearn.com/files/highered/site/download/blackboard/install/lms-player-close.png" class="view-thumb" title="close" onclick="NBCLoader.viewThumb(event)" style="margin-left: -10px;margin-top: 5px;cursor: pointer;">');
        								NBCLoader.$(event.target).off('load');
            						});
    							}
    							
    						}else{
    							var cn = NBCLoader._getDocument().getElementById("courseMenu_link").text;
    	                    	var encodedCN = encodeURI(cn); 
    						// Regular Items
    						//NBCLoader._log("Html Inside diff-->"+curCardId);
    						if(NBCLoader.$(this).closest('.vtbegenerated').find('#iframeId'+curCardId).attr('src')!==undefined){
    							//NBCLoader._log("Iframe src available");
    							NBCLoader.$(this).closest('.vtbegenerated').find('#contentThumbId'+curCardId).css('display','none');
        						NBCLoader.$(this).closest('.vtbegenerated').find('#contentIframeId'+curCardId).css('display','block');
        						NBCLoader.$(this).closest('.vtbegenerated').find('#contentIframeId'+curCardId).prepend('<img src="https://static.nbclearn.com/files/highered/site/download/blackboard/install/lms-player-close.png" class="view-thumb" title="close" onclick="NBCLoader.viewThumb(event)" style="margin-left: -10px;margin-top: 5px;cursor: pointer;">');
    						}else{
    							NBCLoader.$(this).closest('.vtbegenerated')	.find('#iframeId'+curCardId).attr('src',NBCLoader.$(this).closest('.vtbegenerated').find('#iframeSpanId'+curCardId).html()+'?cn='+encodedCN);
        						NBCLoader.$(this).closest('.vtbegenerated').find('#contentThumbId'+curCardId).css('display','none');
        						NBCLoader.$(this).closest('.vtbegenerated').find('#contentIframeId'+curCardId).css('display','block');
        						NBCLoader.$(this).closest('.vtbegenerated').find('#contentIframeId'+curCardId).before('<h3 id="loadingContent" style="color: #000000;float:left">Loading...</h3>');
        						NBCLoader.$(this).closest('.vtbegenerated').find('#iframeId'+curCardId).load(function(){
        							NBCLoader._log("Html Inside diff Load"+NBCLoader.$(this).attr('src'));
        							if(NBCLoader.$(this).attr('src')!==undefined){
        								NBCLoader._log("Inside add close");
        								NBCLoader.$(this).parents().eq(1).prev().remove();
        								NBCLoader.$(this).closest('.vtbegenerated').find('#contentIframeId'+curCardId).prepend('<img src="https://static.nbclearn.com/files/highered/site/download/blackboard/install/lms-player-close.png" class="view-thumb" title="close" onclick="NBCLoader.viewThumb(event)" style="margin-left: -10px;margin-top: 5px;cursor: pointer;">');
        								NBCLoader.$(event.target).off('load');
        							}
        						});
    						}
    						}
    					}
    					NBCLoader._log("CueCard.openCard() returned successfully.");
            			hasErr = false;
        			} catch (e) {
        				// the Cue Card JS occasionially throws an error when
        				// it appears to be 'unready' for opening...
        				// if an error occurs, attempt one retry...
        				NBCLoader._log("ERROR: " + e.message);
        				var doOnclick = !hasErr;
        				hasErr = true;
        				if (doOnclick) {
        					NBCLoader._log("Trying onclick again...");
            				window.setTimeout(onclick, 500);
        				}
        			}
        			return false;
        			 }
        		});
        	});    
        	
        	NBCLoader.$(".nbc-click").mouseover(function(){
        		NBCLoader.$(".nbc-click").find('img').css('cursor', 'pointer');
        	});
        	
    	});
    },

    viewThumb: function(event) {
    	NBCLoader._log("Inside View Thumb"+event.target);
    	NBCLoader.$(event.target).parent().eq(0).find('iframe').removeAttr('src');
    	NBCLoader.$(event.target).parent().eq(0).css('display','none');
    	NBCLoader.$(event.target).parent().next().css('display','block');
    	NBCLoader.$(event.target).remove();
    },
    
    viewFlashThumb: function(event) {
    	//NBCLoader._log("Inside View Flash Thumb"+event.target);
    	NBCLoader.$(event.target).parent().eq(0).find('iframe').removeAttr('src');
    	NBCLoader.$(event.target).parent().eq(0).css('display','none');
    	NBCLoader.$(event.target).parent().next().children().eq(0).find('img').css('display','block');
    	NBCLoader.$(event.target).parent().next().children().eq(0).css('border-style: solid;');
    	NBCLoader.$(event.target).parent().next().children().eq(1).children().eq(0).find('img').css('display','inline-block');
    	NBCLoader.$(event.target).parent().next().children().eq(1).children().eq(1).children().eq(0).css('display','inline-block');
    	NBCLoader.$(event.target).parent().next().children().eq(1).children().eq(1).children().eq(2).css('display','inline-block');
    	NBCLoader.$(event.target).parent().next().children().eq(1).children().eq(1).children().eq(4).css('display','inline-block');
    	NBCLoader.$(event.target).remove();
    },
    viewFlashTableThumb: function(event) {
    	//NBCLoader._log("Inside View Flash Table Thumb"+event.target);
    	NBCLoader.$(event.target).parent().eq(0).find('iframe').removeAttr('src');
    	NBCLoader.$(event.target).parent().eq(0).css('display','none');
    	NBCLoader.$(event.target).parent().eq(0).prev().css('display','block');
    	NBCLoader.$(event.target).parent().eq(0).prev().find('table').css('display','block');
    	NBCLoader.$(event.target).remove();
    },
    
    
    /**
     * Is called to indicate that all JS loading and Ajax calls have completed,
     * and that listeners should be added to enable Cue Card opening.
     */
    signalLoadComplete: function() {
    	NBCLoader._log("Adding NBC click handler to links...");
    	NBCLoader.addClickHandlers();
    	window.setTimeout(function() {
        	NBCLoader._log("Removing NBC mask...");
        	NBCLoader.$("div.nbc-mask", NBCLoader._getDocument()).remove();    		
    	}, 1000);
    },
    
    /*
    ========================================================================
                   This is the mapping of SP and Theme combos,
                      mapped to CSS files to load for each
                       (could be that NO CSS is needed)
    ========================================================================
    */
    nbcCssFiles: {
        SP10: {
            'Bb Learn 2012':    [],
            'Bb Learn 2008':    ['nbc_content_overwrite.css'],
            'Bb Classic':       [],
            'WebCT Classic 1':  [],
            'WebCT Classic 2':  [],
            'Brushed Metal':    [],
            'Bb Schoolhouse':   []
        },
        SP9: {
            'Bb Learn 2012':    [],
            'Bb Learn 2008':    ['nbc_content_overwrite.css'],
            'Bb Classic':       [],
            'WebCT Classic 1':  [],
            'WebCT Classic 2':  [],
            'Brushed Metal':    [],
            'Bb Schoolhouse':   []
        },
        SP8: {
            'Bb Learn 2012':    [],
            'Bb Learn 2008':    ['nbc_content_overwrite.css'],
            'Bb Classic':       [],
            'WebCT Classic 1':  [],
            'WebCT Classic 2':  [],
            'Brushed Metal':    [],
            'Bb Schoolhouse':   []
        },
        SP7: {
            'Bb Learn':         ['nbc_content_overwrite.css', 'nbc_content_overwrite.bblearn.css'],
            'Bb Classic':       ['nbc_content_overwrite.css', 'nbc_content_overwrite.classic.css'],
            'WebCT Classic 1':  ['nbc_content_overwrite.css', 'nbc_content_overwrite.webct1.css'],
            'WebCT Classic 2':  ['nbc_content_overwrite.css', 'nbc_content_overwrite.webct2.css'],
            'Brushed Metal':    ['nbc_content_overwrite.css', 'nbc_content_overwrite.metal.css'],
            'Bb Schoolhouse':   ['nbc_content_overwrite.css', 'nbc_content_overwrite.schoolhouse.css']
        }
    },
    /**
     * Loads CSS files needed to perform theme fixes due to NBC content.
     * 
     * @param {Object} bbInfo     object containing Bb deployment info (obtained in server call)
     * @param {String} nbcVi    string specifying the Bb Virtual Installation UID 
     */
    doThemeFix: function(bbInfo, nbcVi) {
        // get ref. to head tag; used throughout function (and inner functions)
        var head = NBCLoader._getDocument().getElementsByTagName('head')[0];
        
        /**
         * Obtains the path to the currently loaded Theme folder by parsing
         * the href of the theme.css file.
         * @param {Object} cssHref
         */
        function _getThemePath(cssHref) {
            var protocolInd = cssHref.indexOf("//") + 2;
            var path = cssHref.substr(protocolInd);
            var firstSlashInd = path.indexOf("/");
            path = path.substr(firstSlashInd);
            
            var qryInd = path.indexOf("?");
            if (qryInd > 0) {
                path = path.substr(0, qryInd);    
            }
            path = path.replace("/theme.css", "");
            return path;
        } 
        
        /**
         * Creates a STYLE tag, adds the css content to it and adds to the
         * document's HEAD tag.
         * 
         * @param {String} css        the STYLE tag inner contents
         */
        function _addStyle(css){
            var styleTag = NBCLoader._getDocument().createElement("style");
            styleTag.type = "text/css";
            styleTag.innerHTML = css;
            head.appendChild(styleTag);
        }

        /**
         * Performs specific UI fixes based on current Theme.
         */     
        function _doFix() {
            var cssFiles = NBCLoader._getDocument().styleSheets;
            // the path to the folder holding the current theme
            // (the current theme is the one for which the theme.css is loaded)
            var themePath = "";
            // the border-top-color style rule active for .navPaletteContent a
            var border = ""; 
            
            for (var i = 0; i < cssFiles.length; i++) {
                var css = cssFiles[i];
                var href = css.href;
                if (href && href.indexOf("theme.css") > 0) {
                    themePath = _getThemePath(href);
                    for (var j = 0; j < css.cssRules.length; j++) {
                        var rule = css.cssRules[j];
                        if (rule && rule.selectorText && rule.selectorText.indexOf(".controlpanel a") >= 0) {
                            var style = rule.style;
                            if (style.borderTopColor) {
                                border = style.borderTopColor;
                                break;
                            }
                        }
                    }
                }
            }
            
            var indImg = "url(" + themePath + "/images/indicators.png)";
            var style = ".navPaletteContent a { background: transparent " + indImg +"  no-repeat scroll 4px -593px; border-top-color: " + border + ";  }";
            _addStyle(style);            
        }
        
        var sp = 'SP' + bbInfo.servicePack;
        var theme = bbInfo.currentTheme;
        var cssFiles = [];
        var needsThemeFix = false;
        try {
            cssFiles = NBCLoader.nbcCssFiles[sp][theme];
        } catch (e) {
            //catch and continue...is an unknown SP/Theme combo
        }
        
        if (!cssFiles) {
            // by default, for newer SPs, always do Theme fix for 2008 Theme
            if (theme == "Bb Learn 2008") {
                cssFiles = ["nbc_content_overwrite.css"];
                needsThemeFix = true;
            }
            // if unknown combo in SP7 or below, assign default
            if (bbInfo.servicePack <= 7) {
                cssFiles = ["nbc_content_overwrite.css"];
                needsThemeFix = true;
            }
        }
        
        if (cssFiles) {
            for (var i = 0; i < cssFiles.length; i++) {
                var cssNode = NBCLoader._getDocument().createElement('link');
                cssNode.type = 'text/css';
                cssNode.rel = 'stylesheet';
                cssNode.href = '/webapps/bbgs-nbc-content-integration-' + nbcVi + '/css/' + cssFiles[i];
                head.appendChild(cssNode);
            }
            if (needsThemeFix) {
                // performs JS fixes, without needing addtl. CSS file loaded (for color/image fixes)
                _doFix();
            }
        }
    },
    /**
     * Performs style rule removal based on the type provided (at this point,
     * is either 'mashup' or 'resolver').
     * 
     * @param type
     */
    doStyleRemoval: function(type) {
        var isMashup = (type == "mashup");
        var numRules = (isMashup) ? 3 : 10;
        
        if (NBCLoader._getDocument().styleSheets) {
            for (var i = 0; i < NBCLoader._getDocument().styleSheets.length; i++) {
                var styleSheet = NBCLoader._getDocument().styleSheets[i];
                var ii = 0;
                var count = 0;
                var cssRule = false;
                do {
                    try {
                        if (styleSheet.cssRules) {
                            cssRule = styleSheet.cssRules[ii];
                        }
                        else {
                            cssRule = styleSheet.rules[ii];
                        }
                        if (cssRule && cssRule.selectorText) {
                            var selText = cssRule.selectorText.toLowerCase();
                            
                            if (selText == 'ul.contentlist li div.details') {
                                if (styleSheet.cssRules) {
                                    styleSheet.deleteRule(ii);
                                }
                                count++;
                            }
                            else if (selText == 'div.listcm ul.coursemenu li a') {
                                if (styleSheet.cssRules) {
                                    styleSheet.deleteRule(ii);
                                }
                                count++;
                            } 
                            else if (selText == 'div.navpalette a.combolink, div.navpalette a.combolink_active') {
                                if (styleSheet.cssRules) {
                                    styleSheet.deleteRule(ii);
                                }
                                count++;
                            } 
                            if (!isMashup) {
                                
                                if (selText == '.collapsible') {
                                    if (styleSheet.cssRules) {
                                        styleSheet.deleteRule(ii);
                                    }
                                    count++;
                                } 
                                else if (selText == '#courseMenu, .navPaletteContent a'.toLowerCase()) {
                                    if (styleSheet.cssRules) {
                                        styleSheet.deleteRule(ii);
                                    }
                                    count++;
                                } 
                                else if (selText == '.eudModule .eudModule-inner, .styledModule-dark .module-inner'.toLowerCase()) {
                                    if (styleSheet.cssRules) {
                                        styleSheet.deleteRule(ii);
                                    }
                                    count++;
                                } 
                                else if (selText == '.navPaletteContent a'.toLowerCase()) {
                                    if (styleSheet.cssRules) {
                                        styleSheet.deleteRule(ii);
                                    }
                                    count++;
                                } 
                                else if (selText == '.announcementList .item, .announcementList-read .item, #announcementList .item'.toLowerCase()) {
                                    if (styleSheet.cssRules) {
                                        styleSheet.deleteRule(ii);
                                    }
                                    count++;
                                } 
                                else if (selText == '.announcementList .details, #announcementList .details'.toLowerCase()) {
                                    if (styleSheet.cssRules) {
                                        styleSheet.deleteRule(ii);
                                    }
                                    count++;
                                } 
                                else if (selText == '.contentList .details'.toLowerCase()) {
                                    if (styleSheet.cssRules) {
                                        styleSheet.deleteRule(ii);
                                    }
                                    count++;
                                }
                            }
                            
                            if (count == numRules) {
                                break;
                            }
                            
                        }
                    } 
                    catch (error) { }
                    ii++;
                } while (cssRule);
            }
        }
        
        if (!isMashup) {
            var allDivs = NBCLoader._getDocument().getElementsByTagName('div');
            for (var k = 0; k < allDivs.length; k++) {
                if (allDivs[k].className == 'collapsible') {
                    allDivs[k].removeAttribute('style');
                }
            }            
        }
    }    
};

//do immediate check for jQuery...
NBCLoader.checkForJQuery(function() {
    // do immediate check for Prototype...
    NBCLoader.checkForPrototype(function() {
    });
});
