DO
$do$
DECLARE
   _kind "char";
BEGIN
   SELECT INTO _kind  c.relkind
   FROM   pg_class     c
   JOIN   pg_namespace n ON n.oid = c.relnamespace
   WHERE  c.relname = 'video_pk1_seq';
   IF NOT FOUND THEN       
      CREATE SEQUENCE video_pk1_seq;
   ELSIF _kind = 'S' THEN  
      -- do nothing!
   ELSE                   
      -- do nothing!
   END IF;

   SELECT INTO _kind  c.relkind
   FROM   pg_class     c
   JOIN   pg_namespace n ON n.oid = c.relnamespace
   WHERE  c.relname = 'plalistitem_pk1_seq';

   IF NOT FOUND THEN      
      CREATE SEQUENCE plalistitem_pk1_seq;
   ELSIF _kind = 'S' THEN  
      -- do nothing?
   ELSE                   
      -- do nothing!
   END IF;
END
$do$;

/
COMMIT;
/
CREATE TABLE IF NOT EXISTS bbgs_video
(
	pk1 INTEGER DEFAULT NEXTVAL('video_pk1_seq') NOT NULL, 
    id                 VARCHAR(128) NOT NULL, 
	name               VARCHAR(510) NULL, 
	description        VARCHAR(2000) NULL, 
	duration           VARCHAR(50) NULL, 
	air_date           DATE NULL,
    url                VARCHAR(4000) NULL,
    type               CHAR(1) NULL, 
    CONSTRAINT bbgs_video_pk PRIMARY KEY(PK1),
    CONSTRAINT bbgs_video_unique1 UNIQUE(ID)
);
/
COMMIT; 
/
DROP FUNCTION IF EXISTS BBGS_VIDEO_CR(VARCHAR,VARCHAR,VARCHAR,VARCHAR,DATE,VARCHAR,CHAR);
/
COMMIT;
/
 

	CREATE OR REPLACE FUNCTION BBGS_VIDEO_CR   (
	            idIn		VARCHAR(20),
	            nameIn           VARCHAR(20),
	            descriptionIn    VARCHAR(20),
	            durationIn       VARCHAR(20),
	            airDateIn        DATE,
	            urlIn            VARCHAR(20),
	            typeIn           CHAR(1)) RETURNS INTEGER AS 
	       $$
	
	BEGIN
	  
		INSERT INTO BBGS_VIDEO
	    ( id, name, description, duration, air_date, url, type)
	    VALUES ( idIn, nameIn, descriptionIn, durationIn, airDateIn, urlIn, typeIn);
		 
	   return NEXTVAL('video_pk1_seq');
	
	   END
	$$ LANGUAGE 'plpgsql';
	


/
COMMIT;
/

CREATE TABLE IF NOT EXISTS bbgs_playlist_item
(
	pk1 INTEGER DEFAULT NEXTVAL('plalistitem_pk1_seq') NOT NULL, 
  	users_pk1          INTEGER NOT NULL, 
	video_pk1          INTEGER NOT NULL,
	dtmodified         timestamp NOT NULL DEFAULT (clock_timestamp()),
    CONSTRAINT bbgs_playlist_item_pk PRIMARY KEY(pk1), 
    CONSTRAINT bbgs_playlist_item_fk1 FOREIGN KEY(users_pk1) REFERENCES users(pk1) ON DELETE CASCADE,
    CONSTRAINT bbgs_playlist_item_fk2 FOREIGN KEY(video_pk1) REFERENCES bbgs_video(pk1) ON DELETE CASCADE
);

/
COMMIT;
/


DROP FUNCTION IF EXISTS bbgs_playlist_item_cr(int,int,timestamp);
/
commit;
/

DROP FUNCTION IF EXISTS bbgs_playlist_item_cr(int,int);
/
commit;
/

CREATE OR REPLACE FUNCTION bbgs_playlist_item_cr  (
    usersPk1In		  INT ,
    videoPk1In	      INT ,
    dtmodifiedIn     timestamp 
) RETURNS INTEGER AS $$
BEGIN

 -- INSERT INTO bbgs_playlist_item (users_pk1, video_pk1, dtmodified) VALUES (@usersPk1In, @videoPk1In, dtmodifiedIn);
    INSERT INTO bbgs_playlist_item (users_pk1, video_pk1,dtmodified) VALUES (@usersPk1In, @videoPk1In,dtmodifiedIn);
  return NEXTVAL('plalistitem_pk1_seq');
  END
$$  LANGUAGE 'plpgsql';
/  
commit;
/